"""
To run MINT grid
"""

import os

from mint_general_interpolation_grid_builder.functions.grid_functions.run_grid import (
    run_grid,
)
from main_config import main_config
from mint_general_interpolation_grid_builder.eureka.check_nodes import (
    check_for_bad_nodes,
)

from mint_general_interpolation_grid_builder.core.MESA.grid_runners.TPAGB_MESA_gridbuilder import (
    ThermallyPulsingAsymptoticGiantBranchMESAGridBuilder,
)


def agb_grid(config):
    """
    Example grid. This function controls which phases are executed and with which settings this happens
    """

    bad_nodes = check_for_bad_nodes()
    node_list = ",".join(bad_nodes)

    # The input config should contain some global configuration, and probably default value for all the parameters

    # We then use this configuration and update it to reflect the settings that we want to use in this specific grid.
    grid_config = {
        **config,
        #################################
        # system properties
        "grids_root_directory": "/users/nr00492/post_agb_grid_23051",
        "workload_manager": "slurm",
        # "auto_submit": True,
        # "archive_previous_grid": True, # moves existing grid with same metallicity to archive
        "email_notifications_recipients": ["nr00492@surrey.ac.uk"],
        ##################################
        # MESA setup
        # "mesa_version": "15140",
        "mesa_version": "23051",
        "num_cores_for_MESA": 12,  # number of processes used by MESA
        "MESA_output_directory": "/users/nr00492/parallel_scratch/post_agb_grid_23051",
        "caches_dir": "/users/nr00492/parallel_scratch/caches/post_agb_grid_23051",
        #         "MESA_setup_code":"""
        # module load mesa/r15140
        # module load mesasdk/21.4.1""",
        "MESA_setup_code": """
export MESA_DIR=/users/nr00492/mesa-r23.05.1
module load mesasdk/22.6.1""",
        #################################
        # physics
        "MS_terminate_h1_mass_fraction": "1d-6",
        "HB_limit": 1 - 0.014 - 0.001,
        "CHeB_terminate_he4_mass_fraction": "1d-4",
        "kap_lowT_prefix": "AESOPUS",  #'lowT_fa05_a09p',
        "aesopus_filename": "AESOPUS_AGSS09.h5",
        "mixing_length_alpha": "1.931",
        "metallicity": "0.014",
        "eta_center_limit": 200,
        "TPAGB_use_vassilidadis_wood_wind": ".true.",
        "TPAGB_overshoot_scheme_IPCZ": "exponential",
        "TPAGB_overshoot_f_IPCZ": "0.008",  # Ritter et al 2018
        "TPAGB_overshoot_f0_IPCZ": "0.0008",
        "TPAGB_overshoot_scheme_CE": "exponential",
        "TPAGB_overshoot_f_CE": "0.016",  # Herwig 2000
        "TPAGB_overshoot_f0_CE": "0.0016",
        ###################################
        # varying options
        "use_alpha_mlt": ".true.",
        "avoid_HRI": ".true.",
        "envelope_mass_limit": "0.0001",
        ################################
        # slurm options
        "extra_sbatch_commands": f"#SBATCH --exclusive=user\n#SBATCH --mem=10GB\n#SBATCH --exclude={node_list}",
        #########################
    }

    grid_config["grid_directory"] = os.path.join(
        grid_config["grids_root_directory"], "Z" + str(grid_config["metallicity"])
    )

    grid_builder = ThermallyPulsingAsymptoticGiantBranchMESAGridBuilder(
        settings=grid_config
    )

    grid_builder.write_and_run_MESA_grid()


if __name__ == "__main__":
    agb_grid(config=main_config)
