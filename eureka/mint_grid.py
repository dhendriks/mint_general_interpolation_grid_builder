"""
To run MINT grid
"""

from mint_general_interpolation_grid_builder.functions.grid_functions.run_grid import (
    run_grid,
)
from main_config import main_config
from mint_general_interpolation_grid_builder.eureka.check_nodes import (
    check_for_bad_nodes,
)


def mint_grid(config):
    """
    Example grid. This function controls which phases are executed and with which settings this happens
    """

    bad_nodes = check_for_bad_nodes()
    node_list = ",".join(bad_nodes)

    # The input config should contain some global configuration, and probably default value for all the parameters

    # We then use this configuration and update it to reflect the settings that we want to use in this specific grid.
    grid_config = {
        **config,
        #################################
        # system properties
        "grids_root_directory": "/users/nr00492/MINT_grids/",
        "workload_manager": "slurm",
        # "workload_manager": "multiprocessing",
        "binary_grid": True,
        # "auto_submit": True,
        # "archive_previous_grid": True, # moves existing grid with same metallicity to archive
        # "email_notifications_recipients": ["nr00492@surrey.ac.uk"],
        # "plot_all_models": False,
        ##################################
        # MESA setup
        "mesa_version": "15140",
        "num_cores_for_MESA": 6,  # number of processes used by MESA
        "MESA_output_directory": "/users/nr00492/parallel_scratch/MINT_grids",
        "caches_dir": "/users/nr00492/parallel_scratch/caches",
        "save_photos_at_grid_points": False,
        "MESA_setup_code": """
module load mesa/r15140
module load mesasdk/21.4.1""",
        # "restart_unfinished_runs": True,
        #################################
        # for multiprocessing during table building
        "max_queue_size": 10,
        "max_result_queue_size": 10,
        "num_processes": 20,
        #################################
        # global grid properties
        "metallicity": 0.02,
        ##############################
        # physics
        "EAGB_use_vassilidadis_wood_wind": ".false.",
        "save_MS_model_at_core_formation": ".true.",
        ################################
        # slurm options
        "extra_sbatch_commands": f"#SBATCH --exclusive=user\n#SBATCH --mem=10GB\n#SBATCH --exclude={node_list}",
    }

    # Execute the grid with the appropriate control flags
    run_grid(
        grid_config=grid_config,
        stellar_type_handle_dict={
            # 'MS':True,
            #'MC': True,
            "GB": True,
            #'CHeB':True,
        },
        run_mesa=True,
        # build_interpolation_tables=True,
    )


if __name__ == "__main__":
    mint_grid(config=main_config)
