#!/usr/bin/env python3

import os


def check_for_bad_nodes():

    output = os.popen(
        "sinfo --Node --format='%10N %.6D %10P %10T %20E %.4c %.8z %8O %.6m %10e %.6w %.60f'"
    ).read()
    lines = output.split("\n")

    bad_nodes_list = []

    for line in lines[1:-1]:
        print(line)
        values = line.split()
        name = values[0]
        state = values[3]
        if state in ["drained", "drained*", "down", "down*"]:
            continue
        num_cpus = float(values[5])
        cpu_load = float(values[7])

        if state == "idle":
            if cpu_load > 1.0:
                bad_nodes_list.append(name)
        elif state == "mixed":
            node_info = os.popen(f"scontrol show nodes {name}").read().split()
            cpus_alloc = float(node_info[3].split("=")[-1])
            if cpu_load > cpus_alloc:
                bad_nodes_list.append(name)
        elif state == "allocated":
            if cpu_load > num_cpus:
                bad_nodes_list.append(name)

    return bad_nodes_list
