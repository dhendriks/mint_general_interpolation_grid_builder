"""
Main config class
"""

import os
import voluptuous as vol
from collections.abc import Mapping


class Config(Mapping):
    def __init__(self):
        self.main_config_default = {
            #################################
            # system properties
            "mesa_version": {
                "value": "15140",
                "description": "",
                "validation": str,
            },
            "max_queue_size": {
                "value": 10,
                "description": "",
                "validation": int,
            },
            "num_processes": {
                "value": 10,
                "description": "",
                "validation": int,
            },
            "num_cores_for_MESA": {
                "value": 12,
                "description": "",
                "validation": int,
            },
            "table_name": {
                "value": "grid_Z2.00e-02",
                "description": "",
                "validation": str,
            },
            "grid_directory": {
                "value": "/ms_grid/Z0p02_grid_v3/",
                "description": "",
                "validation": str,
            },
            "eureka_version": {
                "value": "eureka",
                "description": "",
                "validation": str,
            },
            "caches_dir": {
                "value": "/parallel_scratch/caches",
                "description": "",
                "validation": str,
            },
            "workload_manager": {
                "value": "slurm",
                "description": "",
                "validation": str,
            },
            "binary_grid": {
                "value": False,
                "description": "",
                "validation": bool,
            },
            ################################
            # for copying models between evol phases
            "copy_models_from_previous_evol_phase": {
                "value": True,
                "description": "",
                "validation": bool,
            },
            "MS_grid_directory": {
                "value": "/ms_grid/Z0p02_grid/",
                "description": "",
                "validation": str,
            },
            "GB_grid_directory": {
                "value": "/rgb_grid/Z0p02_grid/",
                "description": "",
                "validation": str,
            },
            "CHeB_grid_directory": {
                "value": "/cheb_grid/Z0p02_grid/",
                "description": "",
                "validation": str,
            },
            "EAGB_grid_directory": {
                "value": "/eagb_grid/Z0p02_grid/",
                "description": "",
                "validation": str,
            },
            #################################
            # global grid properties
            "metallicity": {
                "value": 0.02,
                "description": "",
                "validation": float,
            },
            "kap_lowT_prefix": {
                "value": "lowT_fa05_a09p",  #'lowT_fa05_a09p',
                "description": "",
                "validation": str,
            },
            "aesopus_filename": {
                "value": "AESOPUS_AGSS09.h5",  # only used in kap_lowT_prefix = 'AESOPUS'
                "description": "",
                "validation": str,
            },
            "aesopus_filename": {
                "value": "1.931",  # Cinquegrana & Joyce 2022, solar calibration with AESOPUS opacities
                "description": "",
                "validation": str,
            },
            ################################
            # MS specific controls
            "delta_XH_cntr_limit": {
                "value": "0.001d0",
                "description": "",
                "validation": str,
            },
            "MS_mass_change": {
                "value": "-1d-90",
                "description": "",
                "validation": str,
            },
            "MS_overshoot_prescription": {
                "value": "jermyn22",  # set to empty for mass-indepdendent overshooting
                "description": "",
                "validation": str,
            },
            "MS_overshoot_scheme": {
                "value": "step",
                "description": "",
                "validation": str,
            },
            "MS_overshoot_f": {
                "value": "0.01",
                "description": "",
                "validation": str,
            },
            "MS_overshoot_f0": {
                "value": "0.001",  # only used if MS_overshoot_prescription = ''
                "description": "",
                "validation": str,
            },
            "MS_do_conv_premix": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "MS_use_Ledoux_criterion": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "MS_alpha_semiconvection": {
                "value": "0",
                "description": "",
                "validation": str,
            },
            ###############################
            # GB specific controls
            "GB_do_conv_premix": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "GB_do_conv_premix": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "GB_do_conv_premix": {
                "value": "0",
                "description": "",
                "validation": str,
            },
            ###############################
            # CHeB specific controls
            "delta_XHe_cntr_limit": {
                "value": "0.001d0",
                "description": "",
                "validation": str,
            },
            "delta_lg_XHe_cntr_limit": {
                "value": "0.01d0",
                "description": "",
                "validation": str,
            },
            "CHeB_do_predictive_mix": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "CHeB_use_Ledoux_criterion": {
                "value": ".false.",
                "description": "",
                "validation": str,
            },
            ###############################
            # EAGB specific controls
            "EAGB_delta_lgL_He_limit": {
                "value": "0.01",
                "description": "",
                "validation": str,
            },
            "EAGB_use_Ledoux_criterion": {
                "value": ".false.",
                "description": "",
                "validation": str,
            },
            "EAGB_alpha_semiconvection": {
                "value": "1d-2",
                "description": "",
                "validation": str,
            },
            "EAGB_overshoot_prescription": {
                "value": "",  # set to empty for mass-indepdendent overshooting
                "description": "",
                "validation": str,
            },
            "EAGB_overshoot_scheme": {
                "value": "exponential",
                "description": "",
                "validation": str,
            },
            "EAGB_overshoot_f": {
                "value": "0.005",  # only used if MS_overshoot_prescription = ''
                "description": "",
                "validation": str,
            },
            "EAGB_overshoot_f": {
                "value": "0.0005",  # only used if MS_overshoot_prescription = ''
                "description": "",
                "validation": str,
            },
            "EAGB_use_vassilidadis_wood_wind": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            ###############################
            # TPAGB specific controls
            "VCT_target": {
                "value": "2d-5",
                "description": "",
                "validation": str,
            },
            "MXP_target": {
                "value": "1.0",
                "description": "",
                "validation": str,
            },
            "avoid_HRI": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "save_hdf5_files": {
                "value": ".false.",
                "description": "",
                "validation": str,
            },
            "envelope_mass_limit": {
                "value": "0.1",
                "description": "",
                "validation": str,
            },
            "use_alpha_mlt": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "TPAGB_use_Ledoux_criterion": {
                "value": ".false.",
                "description": "",
                "validation": str,
            },
            "TPAGB_delta_lgL_He_limit": {
                "value": "0.01",
                "description": "",
                "validation": str,
            },
            "TPAGB_use_vassilidadis_wood_wind": {
                "value": ".true.",
                "description": "",
                "validation": str,
            },
            "TPAGB_overshoot_scheme_IPCZ": {
                "value": "exponential",
                "description": "",
                "validation": str,
            },
            "TPAGB_overshoot_f_IPCZ": {
                "value": "0.008",  # Ritter et al 2018
                "description": "",
                "validation": str,
            },
            "TPAGB_overshoot_f0_IPCZ": {
                "value": "0.0008",
                "description": "",
                "validation": str,
            },
            "TPAGB_overshoot_scheme_CE": {
                "value": "exponential",
                "description": "",
                "validation": str,
            },
            "TPAGB_overshoot_f_CE": {
                "value": "0.016",  # Herwig 2000
                "description": "",
                "validation": str,
            },
            "TPAGB_overshoot_f0_CE": {
                "value": "0.0016",
                "description": "",
                "validation": str,
            },
            "double_exponential_overshoot_f2": {
                "value": "0.25",
                "description": "",
                "validation": str,
            },
            "double_exponential_overshoot_D2": {
                "value": "1d11",
                "description": "",
                "validation": str,
            },
            # use following for double exponential overshoot of Battino et al 2016
            # 'TPAGB_overshoot_scheme_CE':'other',
            # 'TPAGB_overshoot_f_CE':'0.014',
            # 'TPAGB_overshoot_f0_CE':'0.014',
        }

        # load validation
        self.set_validation_schema()

        # load config
        self.load_default_config()

    ################
    # config functions

    def set_validation_schema(self):
        """
        Function to set the validation schema of the population_options
        """

        # from the main dictionary, create a validation scheme
        validation_dict = {
            key: value["validation"]
            for key, value in self.main_config_default.items()
            if "validation" in value
        }
        self.validation_schema = vol.Schema(validation_dict, extra=vol.ALLOW_EXTRA)

    def _validate_population_options(self, key, value):
        """
        Function to handle validation of the arguments passed to the population options
        """

        # validate
        self.validation_schema({key: value})

    def load_default_config(self):
        """
        Function to load the population_options with the default values
        """

        # Grid options
        self.config = {
            key: value["value"] for key, value in self.main_config_default.items()
        }

    def set(self, **kwargs):
        """
        Function to handle setting values in the config. Applies the validation scheme
        """

        for key, value in kwargs.items():
            # Check if key is known
            if not key in self.config.keys():
                raise ValueError(
                    "key '{}' not a known config entry in config. Please add it it to the default dictionary or select from {}".format(
                        key, list(self.config.keys())
                    )
                )

            # validate values
            self._validate_population_options(key, value)

            # Set value if all passed
            self.config[key] = value

    def __iter__(self):
        return iter(self.config)

    def __len__(self):
        return len(self.config)

    def __getitem__(self, key):
        if not key in self.config.keys():
            raise ValueError(
                "key '{}' not present in config. Please select from {}".format(
                    key, list(self.config.keys())
                )
            )

        return self.config[key]

    def __setitem__(self, key, value):
        if not key in self.config.keys():
            raise ValueError(
                "key '{}' not a known config entry in config. Please add it it to the default dictionary or select from {}".format(
                    key, list(self.config.keys())
                )
            )

        # validate values
        self._validate_population_options(key, value)

        # Set value
        self.config[key] = value

    def get(self, key, default):
        """
        Function to get values from the config
        """

        # Check if key actually is present
        if not key in self.config.keys():
            return default

        return self.config[key]

    def dump(self):
        """
        Function to dump the entire config
        """

        return self.config

    #############################
    # Documentation functions
    def configuration_description_parse_description(self, description_dict):
        """
        Function to parse the description dict
        """

        description_string = description_dict["description"]

        # Capitalise first letter
        if description_dict["description"]:
            description_string = (
                description_string[0].capitalize() + description_string[1:]
            )

            # Add period
            if description_string[-1] != ".":
                description_string = description_string + "."

        # Add unit (in latex)
        if "unit" in description_dict:
            if description_dict["unit"] != dimensionless_unit:
                description_string = description_string + " Unit: [{}].".format(
                    description_dict["unit"].to_string("latex_inline")
                )

        return description_string

    def generate_configuration_descriptions_table(self):
        """
        Function to create the rst table for the configuration descriptions
        """

        #
        indent = "   "

        # Get parameter list and parse descriptions
        parameter_list = sorted(list(self.main_config_default.keys()))
        parameter_list_with_descriptions = [
            [
                parameter,
                self.configuration_description_parse_description(
                    description_dict=self.main_config_default[parameter]
                ),
            ]
            for parameter in parameter_list
        ]

        # Construct parameter list
        rst_event_table = """
.. list-table:: {}
{}:widths: 25, 75
{}:header-rows: 1

""".format(
            " Config description", indent, indent
        )

        #
        rst_event_table += indent + "* - Parameter\n"
        rst_event_table += indent + "  - Description\n"

        for parameter_el in parameter_list_with_descriptions:
            rst_event_table += indent + "* - {}\n".format(parameter_el[0])
            rst_event_table += indent + "  - {}\n".format(parameter_el[1])

        return rst_event_table

    def generate_configuration_descriptions_rst(self):
        """
        Function to generate a static webpage for the configuration parameters.
        """

        configuration_rst = ""

        ###############
        # Create section header
        name = "Configuration description"

        configuration_rst += name + "\n"
        configuration_rst += "=" * len(name) + "\n\n"

        configuration_table_rst = self.generate_configuration_descriptions_table()
        configuration_rst += configuration_table_rst

        #
        return configuration_rst

    def write_configuration_description_rst(self, rst_outputname):
        """
        Function to handle generating the configuration description rst and write it to a file
        """

        #
        if not rst_outputname.endswith(".rst"):
            raise ValueError("rst_outputname has to end with '.rst'")

        #
        os.makedirs(os.path.dirname(os.path.abspath(rst_outputname)), exist_ok=True)

        #
        configuration_descriptions_rst = self.generate_configuration_descriptions_rst()

        #
        with open(rst_outputname, "w") as f:
            f.write(configuration_descriptions_rst)


if __name__ == "__main__":
    Config_instance = Config()

    Config_instance.set(mesa_version="10")

    Config_instance["num_cores_for_MESA"] = "ten"
    Config_instance.set(num_cores_for_MESA="ten")

    # #
    # print(Config_instance.dump())

    # #
    # print(Config_instance.get("mesa_versionv", None))

    # ding = {**Config_instance}
    # print(ding)

    # Config_instance["mesa_versionv"] = 10
    # print(Config_instance["mesa_versionv"])

    print

    # print(Config_instance.generate_configuration_descriptions_rst())
