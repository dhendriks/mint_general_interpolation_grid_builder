# Readme
This repository contains the code and table descriptions for the MINT stellar grids developed at the University of Surrey.

To generate the interpolation tables we take the follow two broad steps:

1. run a grid of stellar models with MESA and store their evolution
2. extract the important quantities from the stellar models and compile an interpolation table from that

Each stellar type thus has two main parts:

1. MESA models grid evolution
2. interpolation table building

## Stellar types
### Main-sequence
The MESA grid and its descriptions are available through: [description]() and [code]().

The interpolation table building and its descriptions are available through: [description](stellar_type_description_pages/MS.rst) and [code](_modules/mint_general_interpolation_grid_builder/interpolation_table_builders/MS_table_builder.html)


# Other software
We make use of the SweepIntersector code from https://github.com/prochitecture/sweep_intersector to detect MESA track (self) intersections. Credits to them for that functionality.


# Resources regarding MESA & Grids
some scripts from mathieu https://github.com/mathren/compare_workdir_MESA
posydon codebase probably is useful https://posydon.org/
writeup by rob farmer https://cococubed.com/mesa_market/many_jobs.html
