#!/usr/bin/python3
""" Functions used in the writing of MESA inlists and source code"""

import os
import re
import numpy as np
import random
import sys
import json


def write_inlists_from_inlists_dictionary(direc_path, inlist_dict):

    for inlist_name in inlist_dict.keys():
        inlist_filepath = os.path.join(direc_path, inlist_name)
        with open(inlist_filepath, "w") as f:
            for inlist_section in inlist_dict[inlist_name].keys():
                f.write(f"&{inlist_section}\n")
                previous_control = ""
                for control_name, control_value in inlist_dict[inlist_name][
                    inlist_section
                ].items():
                    common_values = set(previous_control.split("_")) & set(
                        control_name.split("_")
                    )
                    if len(common_values) > 0:
                        f.write(f"\t{control_name} = {control_value}\n")
                    else:
                        f.write(f"\n\t{control_name} = {control_value}\n")
                    previous_control = control_name
                f.write(f"\n/ !end of {inlist_section} namelist\n\n")


def set_control_value_in_all_inlists(inlists, control_name, value):
    for inlist_name in inlists.keys():
        for inlist_section in inlists[inlist_name].keys():
            if inlists[inlist_name][inlist_section].get(control_name, None) is not None:
                inlists[inlist_name][inlist_section][control_name] = value
    return inlists


def find_and_replace_text(file, find_text, replace_text):
    with open(file, "r+") as f:
        text = f.read()
        text = re.sub(find_text, replace_text, text)
        f.seek(0)
        f.write(text)
        f.truncate()

    return


def read_masses_dict_from_file(file):
    # read masses from masses.dict
    masses_dict = json.load(open(file))
    # make sure masses are floats not strings and masses sorted
    masses = {}
    for k, v in masses_dict.items():
        masses[float(k)] = np.sort(v)

    return masses


def invert_dict(dictionary):
    """
    Inverts dictionary
    e.g. for masses
    INPUT: masses in a dictionary with keys = Mi, values = list of Mt for each Mi
    OUTPUT: masses in a dictionary with keys = Mt, values = list of Mi for each Mt
    or vice versa with values sorted in ascending order
    """
    inverted_dict = {}
    for k, v in dictionary.items():
        for x in v:
            inverted_dict.setdefault(x, []).append(k)
    # ensure keys (Mt) are sorted in ascending order
    sorted_inverted_dict = {
        key: sorted(inverted_dict[key]) for key in sorted(inverted_dict.keys())
    }

    return sorted_inverted_dict


def conv_pen_alpha_step_overshoot(m):
    """alpha for step overshooting taken from Eq 11 of Jermyn et al. 2022
    - relevant for MS models
    INPUT: m = Mstar/Msun
    OUTPUT: alpha for step overshooting
    """
    a = 2.47109
    b = -1.19087
    c = 0.724183
    d = 0.0470249
    e = 0.560757
    if m >= 1.1:
        alpha = round(
            np.tanh(m - 1.1)
            * np.sqrt(m - 1.1)
            * (a + b * m ** 2 + c * m ** 3 + d * m ** 5)
            / (e + m ** 5),
            3,
        )
    else:
        alpha = 0

    if alpha == 0:
        scheme = " "
    else:
        scheme = "step"
    return scheme, alpha


def ZAMS_lum(mass, metallicity):
    solar_metallicity = 0.02
    if metallicity == 0:
        metallicity = 1e-9
    logZ = np.log10(metallicity / solar_metallicity)
    alpha = (
        0.39704170
        - 0.32913574 * logZ
        + 0.34776688 * logZ ** 2
        + 0.37470851 * logZ ** 3
        + 0.09011915 * logZ ** 4
    )
    beta = (
        8.52762600
        - 24.41225973 * logZ
        + 56.43597107 * logZ ** 2
        + 37.06152575 * logZ ** 3
        + 5.45624060 * logZ ** 4
    )
    gamma = (
        0.00025546
        - 0.00123461 * logZ
        - 0.00023246 * logZ ** 2
        + 0.00045519 * logZ ** 3
        + 0.00016176 * logZ ** 4
    )
    delta = (
        5.43288900
        - 8.62157806 * logZ
        + 13.44202049 * logZ ** 2
        + 14.51584135 * logZ ** 3
        + 3.39793084 * logZ ** 4
    )
    epsilon = (
        5.56357900
        - 10.32345224 * logZ
        + 19.44322980 * logZ ** 2
        + 18.97361347 * logZ ** 3
        + 4.16903097 * logZ ** 4
    )
    zeta = (
        0.78866060
        - 2.90870942 * logZ
        + 6.54713531 * logZ ** 2
        + 4.05606657 * logZ ** 3
        + 0.53287322 * logZ ** 4
    )
    eta = (
        0.00586685
        - 0.01704237 * logZ
        + 0.03872348 * logZ ** 2
        + 0.02570041 * logZ ** 3
        + 0.00383376 * logZ ** 4
    )
    L = alpha * pow(mass, 5.5) + beta * pow(mass, 11) / (
        gamma
        + delta * pow(mass, 5)
        + epsilon * pow(mass, 7)
        + zeta * pow(mass, 8)
        + eta * pow(mass, 9.5)
    )
    return np.log10(L)
