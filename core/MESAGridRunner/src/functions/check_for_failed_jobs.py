#!/usr/bin/python3

"""
Checks all models to see what is still running
Converts status file to FAILED if no change in logs after some time
"""

import os
import time
import sys
import json

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.check_complete_status import (
    check_all_status_files,
)


def check_running_dict(running_dict, grid_directory, logs_directory):

    for mass in running_dict:
        logs_path = os.path.join(logs_directory, str(mass))
        still_running = check_if_job_running(logs_path=logs_path)
        if still_running == False:
            print(f"{mass} has not changed, setting to FAILED")
            status_file_path = os.path.join(grid_directory, str(mass), "STATUS.txt")
            with open(status_file_path, "w") as f:
                f.write("FAILED")

    return


def check_if_job_running(logs_path, time_limit=1):

    still_running = False

    for logs_folder in os.listdir(logs_path):
        logs_file_path = os.path.join(logs_path, logs_folder)
        # print(logs_file_path)
        hours_since_last_modified = (
            time.time() - os.path.getmtime(logs_file_path)
        ) / 3600
        if hours_since_last_modified < time_limit:
            still_running = True

    return still_running


if __name__ == "__main__":

    grid_directory = sys.argv[1]

    # load config settings
    z, phase = grid_directory.split("/")[-2:]
    settings_path = os.path.join(
        grid_directory, "MINT_{}_{}_settings.json".format(phase, z[1:])
    )
    with open(settings_path, "r") as j:
        settings = json.loads(j.read())

    # get list of jobs still thought to be running
    status_dict = check_all_status_files(grid_directory=grid_directory)

    # check if still running and cinvert to fail if not
    running_dict = {
        mass: status_dict[mass]
        for mass in status_dict
        if status_dict[mass] == "RUNNING"
    }
    check_running_dict(
        running_dict=running_dict,
        grid_directory=grid_directory,
        logs_directory=logs_directory,
    )
