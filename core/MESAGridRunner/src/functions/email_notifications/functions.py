"""
Script to send an email when a function or simulation is finished.

https://leimao.github.io/blog/Python-Send-Gmail/
- I make use of the 'new authentication - less secure: app password' method

NOTE: use a environment variable named 'SIMULATION_NOTIFICATION_APP_PW' to store your app password
NOTE: change CORRESPONDING EMAIL to your email
"""

import os
import traceback
import datetime
import smtplib, ssl
from email.message import EmailMessage

CORRESPONDING_EMAIL = "mint.binary.c@gmail.com"


def send_email(config, body_text, subject):
    """
    Main function to handle sending the email
    """

    # Handle checking the configuration
    if not config["email_notifications_enabled"]:
        return

    # configuration
    port = 465  # For SSL
    smtp_server = "smtp.gmail.com"
    password = config["email_notifications_APP_password"]
    receiver_emails = [CORRESPONDING_EMAIL] + config[
        "email_notifications_recipients"
    ]  # Enter receiver address
    sender_email = CORRESPONDING_EMAIL  # Enter your address

    # set content
    datetimestring = datetime.datetime.utcnow().isoformat()
    body_text = (
        "Message from MINT grid sent on {}\n\n".format(datetimestring) + body_text
    )

    # Set up email object
    for receiver_email in receiver_emails:
        msg = EmailMessage()
        msg.set_content(body_text)
        msg["Subject"] = subject
        msg["From"] = sender_email
        msg["To"] = receiver_email

        print(
            "sending email from {} to {} with subject {}".format(
                sender_email, receiver_email, subject
            )
        )

        # Allow for testing
        if not config.get("email_notifications_testing"):
            # Send email
            try:
                context = ssl.create_default_context()
                with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
                    server.login(sender_email, password)
                    server.send_message(
                        msg, from_addr=sender_email, to_addrs=receiver_email
                    )
            except BaseException as error:
                print("An exception occurred: {}".format(exception))
                print(f"Exception Name: {type(exception).__name__}")
                print(f"Exception Desc: {exception}")
                print(f"Exception traceback: {traceback.print_exc()}")
        else:
            print("with body text:\n{}".format(body_text))


def send_error_email(config, error, traceback, class_object=None):
    """
    Function to send a general error email. Optionally uses class object
    """

    #
    class_object_text = (
        "class {} metallicity {}".format(
            class_object.__class__.__name__, config["metallicity"]
        )
        if class_object is not None
        else ""
    )

    subject = "MINT grid " + class_object_text + " failed"

    body_text = """
MINT component: {}

Exception Name: {}

An exception occurred: {}

Traceback: {}

""".format(
        class_object_text, error, type(error).__name__, traceback
    )

    # Send the email
    send_email(config=config, body_text=body_text, subject=subject)


def send_succes_email(config, message, class_object=None):
    """
    Function to send a general success email. Optionaly uses class object
    """

    #
    class_object_text = (
        "class {} metallicity {}".format(
            class_object.__class__.__name__, config["metallicity"]
        )
        if class_object is not None
        else ""
    )

    subject = "MINT grid " + class_object_text + " completed without errors!"

    body_text = """
MINT component: {}

Message: {}
""".format(
        class_object_text, message
    )

    # Send the email
    send_email(config=config, body_text=body_text, subject=subject)


class Email_context_manager:
    def __init__(self, config, class_object):
        self.config = config
        self.class_object = class_object

    def __enter__(self):
        self.start = datetime.datetime.now()
        # We don't return anything here because we don't use the class instance anyway

    def __exit__(self, exc, value, exc_traceback):
        if not exc:
            self.class_object.class_send_success_email()
        else:

            actual_traceback_obj = traceback.format_exc()

            self.class_object.class_send_error_email(value, actual_traceback_obj)


if __name__ == "__main__":

    print("yo")
