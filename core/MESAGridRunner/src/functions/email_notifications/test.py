"""
https://duongnt.com/context-manager/
"""


from main_config import main_config


from functions import send_email, send_error_email, Email_context_manager
from test_functions import succesful_function, error_function

import traceback, sys

from mint_general_interpolation_grid_builder.core.MESA.grid_runners.MS_MESA_gridbuilder import (
    MainSequenceMESATableBuilder,
)

#########
# some configuration
main_config["email_notifications_recipients"] = ["davidhendriks93@gmail.com"]
main_config["email_notifications_testing"] = False
main_config["metallicity"] = 0.02
#########
# Test to see if class has the correct functions


def test_Mesaclass_implementation():
    """
    Function to test the implementation in the mesa class
    """

    MainSequenceMESATableBuilder_instance = MainSequenceMESATableBuilder(
        settings=main_config
    )
    MainSequenceMESATableBuilder_instance.class_send_error_email()
    MainSequenceMESATableBuilder_instance.class_send_success_email()


from mint_general_interpolation_grid_builder.classes.MesaGridRunner_extensions.Email import (
    Email,
)


class dummy_class(Email):
    """
    dummy class
    """

    def __init__(self, config):
        self.config = config
        self.settings = config

        Email.__init__(self)


def text_contextmanager_succes(config):
    """
    Function to test the context manager
    """

    dummy_class_instance = dummy_class(config)

    with Email_context_manager(config=config, class_object=dummy_class_instance):
        print("hello")


def call_failing_function():
    """
    Function that calls a failing function
    """

    error_function()


def text_contextmanager_fail(config):
    """
    Function to test the context manager
    """

    dummy_class_instance = dummy_class(config)

    with Email_context_manager(config=config, class_object=dummy_class_instance):
        call_failing_function()


text_contextmanager_succes(config=main_config)
# text_contextmanager_fail(config=main_config)
