def succesful_function():
    """
    Dummy function to handle succes
    """

    print("succes!")


def error_function():
    """
    Dummy function to raise an error
    """

    print("error!")
    raise ValueError("error")
