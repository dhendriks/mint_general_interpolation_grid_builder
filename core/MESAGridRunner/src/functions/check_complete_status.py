#!/usr/bin/python3

"""
Checks all models to see what is still running
Checks if MESA run has produced end model for each
"""

import os
import sys

import numpy as np


def check_all_status_files(grid_directory):
    """Check all status files to get list of models waiting, running and finished"""

    ########################################################
    # loop through masses and store the contents of STATUS.txt in status_dict
    status_dict = {}
    for mass_dir in os.listdir(grid_directory):
        status_file_path = os.path.join(grid_directory, mass_dir, "STATUS.txt")
        if os.path.exists(status_file_path):
            with open(status_file_path, "r") as f:
                status = f.read().strip()
            status_dict[float(mass_dir)] = status

    status_dict = {m: status_dict[m] for m in sorted(status_dict)}
    # print(status_dict)

    return status_dict


def get_termination_code(file_path):

    # Open the file and read the lines
    with open(file_path, "r") as f:
        lines = f.readlines()

    # if no termination code then set equal to None
    term_code = None

    # Loop through each line and check if it contains the termination code
    for line in lines:
        if "termination code" in line:
            # get the rest of the line after the termination code
            term_code = line.split("termination code: ")[1].strip()

    return term_code


def check_complete_models_MS(finished_dict):

    complete_list = []
    incomplete_list = []

    # check termination code to see if MESA failed
    allowed_term_codes = ["xa_central_lower_limit", "eta_center_limit"]

    for mass in finished_dict:
        output_file_path = os.path.join(parent, str(mass), f"{mass}.out")
        term_code = get_termination_code(file_path=output_file_path)
        if term_code in allowed_term_codes:
            complete_list.append(mass)
        else:
            incomplete_list.append(mass)

    # write info on complete and incomplete models
    with open(status_all_file_path, "a") as f:
        f.write("\nCOMPLETE MODELS\n\n")
        for mass in complete_list:
            f.write(f"{mass}, ")
        f.write("\n\nINCOMPLETE MODELS\n\n")
        for mass in incomplete_list:
            f.write(f"{mass}, ")

    return


def check_complete_models(finished_dict):

    complete_dict = {}
    incomplete_dict = {}
    allowed_term_codes = [
        "xa_central_lower_limit",
        "HB_limit",
        "max_age",
        "eta_center_limit",
        "no_he_burn",
        "TP_started",
        "ne_ignition",
    ]

    term_codes = {}

    for mass in finished_dict:
        mass_dir = os.path.join(parent, str(mass))
        complete_list = []
        incomplete_list = []
        for file in os.listdir(mass_dir):
            try:
                M = float(file[:-4])
                output_file_path = os.path.join(mass_dir, file)
                term_code = get_termination_code(file_path=output_file_path)
                term_codes[f"{mass}_{M}"] = term_code
                if term_code in allowed_term_codes:
                    complete_list += [M]
                else:
                    incomplete_list += [M]
            except:
                None
        # add to dictionaries
        complete_dict[mass] = list(np.sort(complete_list))
        incomplete_dict[mass] = list(np.sort(incomplete_list))

    # write results
    with open(status_all_file_path, "a") as f:

        f.write("\nCOMPLETE MODELS\n")
        for mass in complete_dict:
            if len(complete_dict[mass]) != 0:
                f.write(f"\n{mass} " + str(complete_dict[mass]))

        f.write("\n\nINCOMPLETE MODELS\n")
        for mass in incomplete_dict:
            if len(incomplete_dict[mass]) != 0:
                # f.write(f'\n{mass} '+str(incomplete_dict[mass]))
                f.write(f"\n{mass}: ")
                for M in incomplete_dict[mass]:
                    term_code = term_codes["{}_{}".format(mass, M)]
                    f.write("{}({}), ".format(M, term_code))

    return


if __name__ == "__main__":

    ########################################################
    # get current path and parent directory
    path = os.getcwd()
    parent = os.path.dirname(path)
    status_all_file_path = os.path.join(parent, "STATUS_ALL.txt")

    status_dict = check_all_status_files(grid_directory=parent)

    ########################################################
    # write info about models running to STATUS_ALL.txt
    status_options = ["WAITING", "RUNNING", "FINISHED", "FAILED"]

    with open(status_all_file_path, "w") as f:
        for s in status_options:
            f.write(f"\n{s}\n")
            for mass in status_dict.keys():
                if status_dict[mass] == s:
                    f.write(f"{mass}, ")
            f.write("\n")

    ####################################################
    # report complete and incomplete models for each evol phase

    finished_dict = {
        mass: status_dict[mass]
        for mass in status_dict
        if status_dict[mass] == "FINISHED"
    }

    if os.path.split(parent)[-1] == "MS":
        check_complete_models_MS(finished_dict)
    elif os.path.split(parent)[-1] in ["MC", "GB", "CHeB", "EAGB"]:
        check_complete_models(finished_dict)
