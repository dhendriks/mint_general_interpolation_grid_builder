#!/usr/bin/python3

"""
Slurm class-extension for MESAGridRunner class
"""

import os
import random
import subprocess
import time
import json

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.check_complete_status import (
    check_all_status_files,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.check_for_failed_jobs import (
    check_running_dict,
)


class Slurm:
    """
    Slurm class-extension for MESAGridRunner class
    Allows running of grids on HPC using slurm
    """

    def __init__(self):
        pass

    def MESA_Slurm_run_grid(self):
        """
        Main function to have slurm handle the grid
        """

        ###########################################################
        # Loop over masses to create Slurm scripts for the MESA runs
        for mass in self.masses_list:
            # write slurm request file
            self.write_slurm_run_MESA_script(mass=mass)

        ###############################################################
        # create run_grid script to send off all slurm submission files
        self.write_run_MESA_grid_script_for_slurm(
            masses_list=self.masses_list, grid_dir=self.settings["grid_directory"],
        )

        if self.settings["auto_submit"]:
            # run all MESA models using run_grid
            os.chdir(self.settings["grid_directory"])
            os.system("./run_grid.sh")

        #################################################
        # Stay alive until slurm jobs finished

        print("\nRUNNING MESA\n")

        if self.settings["auto_submit"]:

            MESA_finished = False
            while not MESA_finished:
                time.sleep(600)

                # check all status files to see if finished
                status_dict = check_all_status_files(
                    grid_directory=self.settings["grid_directory"]
                )
                running_dict = {
                    mass: status_dict[mass]
                    for mass in status_dict
                    if status_dict[mass] == "RUNNING"
                }
                waiting_dict = {
                    mass: status_dict[mass]
                    for mass in status_dict
                    if status_dict[mass] == "WAITING"
                }

                # check if any jobs need to be converted to fail
                check_running_dict(
                    running_dict=running_dict,
                    grid_directory=self.settings["grid_directory"],
                    logs_directory=self.settings["logs_directory"],
                )

                # if no jobs are waiting or running then MESA finished
                num_jobs = len(waiting_dict.keys()) + len(running_dict.keys())
                if num_jobs == 0:
                    MESA_finished = True

            print("\nAll MESA RUNS FINISHED\n")
        else:
            print("\nYOU NEED TO SUBMIT run_grid.sh YOURSELF\n")

        return

    def write_slurm_run_MESA_script(self, mass):
        """generates script to be submitted on Eureka"""

        slurm_dic = {
            "slurm_partition": self.settings["slurm_partition"],
            "num_cores_for_MESA": self.settings["num_cores_for_MESA"],
            "hours": self.settings["MESA_run_max_hours"],
            "mass": mass,
            "metallicity": self.settings["metallicity"],
            "MESA_setup_code": self.settings["MESA_setup_code"],
            "extra_sbatch_commands": self.settings["extra_sbatch_commands"],
        }

        script = self.slurm_run_MESA_header(dic=slurm_dic)

        script += "./rn"

        filename = os.path.join(
            self.settings["grid_directory"], str(mass), "slurm_run.sub"
        )

        # write to slurm file
        with open(filename, "w") as f:
            f.write(script)

        return

    @staticmethod
    def slurm_run_MESA_header(dic):
        """return header for MESA slurm run file"""

        script = """#!/bin/bash

#SBATCH --partition={slurm_partition}
#SBATCH --job-name="{mass}_{metallicity}"
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={num_cores_for_MESA}
#SBATCH --time={hours}:00:00
#SBATCH -o slurm.out
#SBATCH -e slurm.err
{extra_sbatch_commands}

export OMP_NUM_THREADS={num_cores_for_MESA}

cd $SLURM_SUBMIT_DIR

{MESA_setup_code}

""".format(
            **dic
        )

        return script

    @staticmethod
    def write_run_MESA_grid_script_for_slurm(masses_list, grid_dir):
        # create run_grid script to run all models
        run_grid_script = os.path.join(grid_dir, "run_grid.sh")
        with open(run_grid_script, "w") as f:
            f.write("#!/bin/bash\n")
            for mass in masses_list:
                # mass_text = re.sub("\.", "p", str(mass)) + "M"
                mass_text = str(mass)
                f.write("cd {}\n".format(os.path.join(grid_dir, mass_text)))
                f.write("echo 'WAITING' > STATUS.txt\n")
                f.write("sbatch slurm_run.sub\n")
                # f.write("sleep 10\n")

        # make run file executable
        os.system("chmod +x " + run_grid_script)

        return
