#!/usr/bin/python3

"""
Multiprocessing class-extension for MESAGridRunner class
"""

import multiprocessing
import setproctitle
import os


class MultiProcessing:
    """
    MultiProcessing class-extension for MESAGridRunner class
    """

    def __init__(self):
        pass

    def MESA_multiprocessing_main(self):
        """
        Function to run the multiprocessing of a job queue and return or store the results

        Jobs are sourced from the user-determined self.job_list

        the output is stored (if put in the result_queue) in the self.job_results
        """

        # set MESA locations
        os.system(self.settings["MESA_setup_code"])

        # Set process name
        setproctitle.setproctitle("MESA grid runner parent process")

        # if max_queue_size is zero, calculate automatically
        if self.settings["max_queue_size"] == 0:
            self.settings["max_queue_size"] = 2 * self.settings["num_processes"]

        ############
        # Set up the manager object and queues
        manager = multiprocessing.Manager()
        job_queue = manager.Queue(maxsize=self.settings["max_queue_size"])
        result_queue = manager.Queue(maxsize=self.settings["max_result_queue_size"])

        ############
        # Create process instances to run the stars
        processes = []
        for ID in range(self.settings["num_processes"]):
            processes.append(
                multiprocessing.Process(
                    target=self.MESA_multiprocessing_worker_function,
                    args=(job_queue, result_queue, ID),
                )
            )

        ############
        # Activate the processes
        print("Starting subprocesses.")
        for p in processes:
            p.start()
        print("Started subprocesses.")

        # Set up the system_queue in the parent process
        self.MESA_multiprocessing_queue_filler(
            job_queue, processes, num_processes=self.settings["num_processes"],
        )

        ############
        # Join the processes after the queue filler has finished
        print("Do join of subprocesses ...")
        for p in processes:
            p.join()
        print("Joined all subprocesses.")

        ###########
        # Readout the result queue and store internally
        sentinel = object()
        for job_dict in iter(result_queue.get, sentinel):
            self.job_results.append(job_dict)
            if result_queue.empty():
                break

    def MESA_multiprocessing_worker_function(self, job_queue, result_queue, ID):
        """
        Function to execute the self.MESA_multiprocessing_run_mesa_model in parallel
        """

        ############################################################
        # Readout the job queue and loop continue until we hit STOP
        for job_number, job_dict in iter(job_queue.get, "STOP"):

            ################
            # Print some info
            process = multiprocessing.current_process()
            print(process.name, ID, job_number, job_dict)

            ###############
            # Call the function to process the detailed mesa models for a given track or star. This function is passed the job-info.
            self.MESA_multiprocessing_run_mesa_model(job_dict, result_queue)

    def MESA_multiprocessing_queue_filler(self, job_queue, processes, num_processes):
        """
        Function to fill the queue object for the multiprocessing
        """

        #######
        # Loop over the list and put these in the actual queue
        for job_number, job_dict in enumerate(self.job_list):

            ########
            # Put system in the job queue
            job_queue.put((job_number, job_dict), block=True)

        #######
        # Signal stop when the job_list has been exhausted
        for _ in range(num_processes):
            job_queue.put("STOP")

    def MESA_multiprocessing_run_grid(self):
        """
        Main function to have multiprocessing handle the grid
        """

        masses_list = [m for m in self.masses_dic]

        # Loop over masses to make job_list for the MESA runs
        for mass in masses_list:
            final_directory = os.path.join(self.settings["grid_directory"], str(mass))

            # TODO: fix job list
            job_dict = {"mesa_model_directory": final_directory}

            # add to job_list
            self.job_list.append(job_dict)

        # Call multiprocessing main
        self.MESA_multiprocessing_main()

    def MESA_multiprocessing_run_mesa_model(self, job_dict, result_queue):
        """
        Function to run the mesa model
        """

        # Create command
        mesa_script = "clean_make_run.script"
        command = "cd {} && ./{}".format(job_dict["mesa_model_directory"], mesa_script)

        # execute script
        os.system(command)
        # TODO: make output file for each of the star files into final

        # # TODO: clean below and handle running better
        # p = subprocess.run(make_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # stdout = p.stdout  # stdout = normal output
        # stderr = p.stderr  # stderr = error output

        # if p.returncode != 0:
        #     print("Something went wrong when executing the makefile:")
        #     print(stderr.decode("utf-8"))
        #     print("Aborting")
        #     sys.exit(-1)

        # else:
        #     print(stdout.decode("utf-8"))
        #     print("Successfully built the libbinary_c_api.so")
