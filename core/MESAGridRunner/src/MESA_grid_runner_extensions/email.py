"""
Email handling class extension for MesaGridrunner
"""

import traceback

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.email_notifications.functions import (
    send_error_email,
    send_succes_email,
)


class Email:
    """
    Email class-extension for MESAGridRunner class
    """

    def __init__(self):
        pass

    def class_send_error_email(self, exc, tb):
        """
        Function to send error email
        """

        send_error_email(
            config=self.settings, error=exc, traceback=tb, class_object=self
        )

    def class_send_success_email(self, extra_message=None):
        """
        Function to send an email about the grid success
        """

        message = "MesaGridRunner finished without errors."

        if not extra_message is None:
            message += "\n\n" + extra_message

        #
        send_succes_email(config=self.settings, message=message, class_object=self)
