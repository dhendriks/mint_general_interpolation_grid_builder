#!/usr/bin/python3

"""
Main Mesa grid runner class
"""

import os
import shutil
import sys, traceback
import json

from pathlib import Path
from datetime import date


from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner_extensions.slurm import (
    Slurm,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner_extensions.email import (
    Email,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner_extensions.multiprocessing import (
    MultiProcessing,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    conv_pen_alpha_step_overshoot,
    find_and_replace_text,
    write_inlists_from_inlists_dictionary,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.email_notifications.functions import (
    Email_context_manager,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.check_complete_status import (
    check_all_status_files,
)


class MESAGridRunner(Slurm, MultiProcessing, Email):
    def __init__(self, settings):

        # Init mixin classes
        Slurm.__init__(self)
        MultiProcessing.__init__(self)
        Email.__init__(self)

        self.settings = settings
        self.job_list = []
        self.job_results = []

        # set location of MESA output data
        if self.settings.get("grid_directory", None) is not None:

            self.settings["logs_directory"] = os.path.join(
                self.settings["MESA_output_directory"],
                self.settings["grid_directory"].replace(
                    self.settings["grids_root_directory"] + "/", ""
                ),
            )
            print(self.settings["MESA_output_directory"])
            print(
                self.settings["grid_directory"].replace(
                    self.settings["grids_root_directory"] + "/", ""
                )
            )
            print(self.settings["logs_directory"])

        # print(f"\nBuilding MESA grid\n")

        return

    def write_and_run_MESA_grid(self):
        """
        Main function for the MESA grid runner.
        """
        #
        with Email_context_manager(config=self.settings, class_object=self):

            if not self.settings["restart_unfinished_runs"]:

                ##################################
                # archive old grids
                self.archive_previous_MESA_grid()

                #################################
                # pre grid writing hook
                self.pre_MESA_grid_writing_hook()

                #################################
                # extra pre grid writing hook
                self.pre_MESA_grid_writing_hook_extra()

                ##################################
                # Determine masses
                self.decide_masses_dict()

                ##################################
                # Write all structure for MESA grid
                self.create_MESA_grid()

            else:
                print("RESTARTING UNFINISHED RUNS: SKIP CREATION OF MESA GRID")

            #################################
            # Run all MESA models
            self.run_MESA_grid()

    def archive_previous_MESA_grid(self):

        if self.settings.get("archive_previous_grid") and os.path.exists(
            self.settings["grid_directory"]
        ):
            suffix = os.path.basename(os.path.normpath(self.settings["grid_directory"]))
            print("moving previous grid to archive")
            archive_folder_loc = self.settings["grid_directory"].replace(
                suffix, "_" + suffix + "_archive"
            )
            archive_direc = os.path.join(
                archive_folder_loc, suffix + "_before_" + str(date.today())
            )
            shutil.move(self.settings["grid_directory"], archive_direc)

            # archive MESA output if in alternative location
            if not os.path.samefile(
                self.settings["MESA_output_directory"],
                self.settings["grids_root_directory"],
            ):
                archive_folder_loc = self.settings["logs_directory"].replace(
                    suffix, "_" + suffix + "_archive"
                )
                archive_direc = os.path.join(
                    archive_folder_loc, suffix + "_before_" + str(date.today())
                )
                shutil.move(self.settings["logs_directory"], archive_direc)

        return

    def pre_MESA_grid_writing_hook(self):

        ####################################################
        # create grid directory
        Path(self.settings["grid_directory"]).mkdir(parents=True, exist_ok=True)

        ######################################################
        # set rn line based on nice priority
        self.rn_line = "nice -n {} ./rn".format(self.settings["nice_priority"])

        ######################################################
        # set metallicity in inlists

        for inlist_name in self.settings["inlists"]:
            if (
                self.settings["inlists"][inlist_name]["kap"].get("Zbase", None)
                is not None
            ):
                self.settings["inlists"][inlist_name]["kap"]["Zbase"] = self.settings[
                    "metallicity"
                ]
            if (
                self.settings["inlists"][inlist_name]["controls"].get("initial_z", None)
                is not None
            ):
                self.settings["inlists"][inlist_name]["controls"][
                    "initial_z"
                ] = self.settings["metallicity"]

        ########################################################
        # write settings to json file
        f = os.path.join(
            self.settings["grid_directory"], self.format_settings_filename()
        )
        json.dump(self.settings, open(f, "w"))

        #################################################################
        # caches_dir

        self.caches_dir = os.path.join(
            self.settings["caches_dir"], self.settings["mesa_version"]
        )
        for inlist_name in self.settings["inlists"]:
            for cache_name in ["eosDT_cache_dir", "kap_cache_dir", "rates_cache_dir"]:
                if (
                    self.settings["inlists"][inlist_name]["star_job"].get(
                        cache_name, None
                    )
                    is not None
                ):
                    self.settings["inlists"][inlist_name]["star_job"][cache_name] = (
                        "'" + os.path.join(self.caches_dir, cache_name[:-4]) + "'"
                    )

        ################################################################
        # Set up path to src directory for location of MESA modules and routines

        this_file = os.path.abspath(__file__)
        this_file_dir = os.path.dirname(this_file)

        self.mesa_src_dir = os.path.join(
            this_file_dir.split("mint_general_interpolation_grid_builder")[0],
            "mint_general_interpolation_grid_builder/core/MESA/standard_{}/src".format(
                self.settings["mesa_version"]
            ),
        )

        ############################################################
        # check MESA environment variables set

        env_vars = ["MESA_DIR", "MESASDK_ROOT", "MESASDK_VERSION"]
        for v in env_vars:
            if os.popen(f"echo ${v}").read() == "\n":
                print(
                    "WARNING: ENSURE MESA ENVIRONMENT VARIABLES ARE SET AND MESA HAS BEEN SOURCED"
                )
                print("EXITING")
                sys.exit()

        return

    def pre_MESA_grid_writing_hook_extra(self):
        pass

    def decide_masses_dict(self):
        """
        Placeholder for function to determine masses dict
        This will vary for each evolutionary phase
        MUST set self.masses_dic in this function
        """

        print("Grid masses:")
        print(self.settings["masses"])

        self.masses_dic = {M: M for M in self.settings["masses"]}

        return

    def create_MESA_grid(self):
        """
        Handles writing the MESA grid for a dictionary of masses
        keys = initial masses
        values = list of final masses
        """

        ##########################################
        # copy src, make, clean files
        for path in ["mk", "clean", "make", "src"]:
            os.system(
                "cp -r $MESA_DIR/star/work/{} {} ".format(
                    path, self.settings["grid_directory"]
                )
            )

        # edit run_star_extras
        module_name = self.settings["module_name"]
        module_dirpath = os.path.join(self.mesa_src_dir, f"rse_modules/{module_name}")
        routines_dirpath = os.path.join(self.mesa_src_dir, "custom_routines")
        find_and_replace_text(
            file=os.path.join(
                self.settings["grid_directory"], "src/run_star_extras.f90"
            ),
            find_text="standard_run_star_extras.inc",
            replace_text=os.path.join(
                module_dirpath, f"{module_name}_run_star_extras.inc"
            ),
        )
        find_and_replace_text(
            file=os.path.join(
                self.settings["grid_directory"], "src/run_star_extras.f90"
            ),
            find_text="implicit none",
            replace_text="implicit none\n\ninclude '{}'".format(
                os.path.join(module_dirpath, f"{module_name}_params.inc")
            ),
        )
        find_and_replace_text(
            file=os.path.join(
                self.settings["grid_directory"], "src/run_star_extras.f90"
            ),
            find_text="implicit none",
            replace_text="implicit none\n\ninteger :: evol_phase".format(
                os.path.join(module_dirpath, f"{module_name}_params.inc")
            ),
        )
        include_routines_list = [
            "mixing_routines.inc",
            "convergence_routines.inc",
            "agb_routines.inc",
        ]
        include_routines_list = [
            "include '{}'".format(os.path.join(routines_dirpath, x))
            for x in include_routines_list
        ]
        include_routines_string = "\n".join(include_routines_list)
        find_and_replace_text(
            file=os.path.join(
                self.settings["grid_directory"], "src/run_star_extras.f90"
            ),
            find_text="contains",
            replace_text="contains\n\n{}\n".format(include_routines_string),
        )
        self.edit_run_star_extras()

        # build star executable
        os.chdir(self.settings["grid_directory"])
        print("Building star executable")
        os.system("./clean")
        os.system("./mk")

        ############################################
        # write run directory for each mass
        self.inlists_dict = self.settings["inlists"]
        for mass in self.masses_dic:
            self.write_run_directory(mass=mass, secondary_masses=self.masses_dic[mass])

        return

    def edit_run_star_extras(self):
        """Hook to enable editing of run_star_extras
        """
        pass

    def write_run_directory(self, mass, secondary_masses):
        """
        generates run directory for given initial mass and corresponding final masses
        """

        self.write_run_directory_general(mass=mass)

        self.write_run_directory_extras_hook(
            mass=mass, secondary_masses=secondary_masses, settings_dic=self.settings
        )

        write_inlists_from_inlists_dictionary(
            direc_path=self.final_directory, inlist_dict=self.inlists_dict
        )

        self.write_run_script(mass)

        return

    def write_run_directory_general(self, mass):

        ##########################################################
        # set up directory and paths

        # set logs directory to save data for this mass
        self.logs_dir = os.path.join(self.settings["logs_directory"], str(mass))

        # set path to final directory for this mass
        self.final_directory = os.path.join(self.settings["grid_directory"], str(mass))

        # make directory
        Path(self.final_directory).mkdir(parents=True, exist_ok=True)
        os.chdir(self.final_directory)

        #######################################################
        # copy MESA files

        # copy rn files
        for path in ["re", "rn"]:
            os.system(f"cp -r $MESA_DIR/star/work/{path} {self.final_directory}")

        # copy star executable
        os.system("cp ../star .")

        ######################################################
        # set mass in inlists

        for inlist_name in self.inlists_dict:
            if (
                self.inlists_dict[inlist_name]["controls"].get("initial_mass", None)
                is not None
            ):
                self.inlists_dict[inlist_name]["controls"]["initial_mass"] = mass

        #############################################################
        # check for mass dependent overshooting prescription
        if self.inlists_dict.get("inlist_MS", None) is not None:
            if self.settings["use_jermyn22_conv_pen_MS"]:
                (overshoot_scheme, overshoot_f,) = conv_pen_alpha_step_overshoot(mass)
                self.inlists_dict["inlist_MS"]["controls"][
                    "overshoot_scheme(1)"
                ] = f"'{overshoot_scheme}' !jermyn et al 2022"
                self.inlists_dict["inlist_MS"]["controls"][
                    "overshoot_zone_type(1)"
                ] = "'any'"
                self.inlists_dict["inlist_MS"]["controls"][
                    "overshoot_zone_loc(1)"
                ] = "'core'"
                self.inlists_dict["inlist_MS"]["controls"][
                    "overshoot_bdy_loc(1)"
                ] = "'any'"
                self.inlists_dict["inlist_MS"]["controls"]["overshoot_f(1)"] = str(
                    overshoot_f
                )
                self.inlists_dict["inlist_MS"]["controls"]["overshoot_f0(1)"] = str(
                    round(overshoot_f / 10, 4)
                )

        #############################################################
        # handle data output locations

        for inlist_name in self.inlists_dict.keys():
            if (
                self.inlists_dict[inlist_name]["controls"].get("log_directory", None)
                is not None
            ):
                suffix = os.path.basename(
                    os.path.normpath(
                        self.inlists_dict[inlist_name]["controls"][
                            "log_directory"
                        ].strip("'")
                    )
                )
                self.inlists_dict[inlist_name]["controls"][
                    "log_directory"
                ] = "'{}'".format(os.path.join(self.logs_dir, suffix,))
        for inlist_name in self.inlists_dict.keys():
            if (
                self.inlists_dict[inlist_name]["controls"].get("photo_directory", None)
                is not None
            ):
                suffix = os.path.basename(
                    os.path.normpath(
                        self.inlists_dict[inlist_name]["controls"][
                            "photo_directory"
                        ].strip("'")
                    )
                )
                self.inlists_dict[inlist_name]["controls"][
                    "photo_directory"
                ] = "'{}'".format(os.path.join(self.logs_dir, suffix,))

        return

    def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):
        """Placeholder for evol phase specific writing of run directory"""
        pass

    def write_run_script(self, mass):
        """Change ./rn file to control run.
        """

        # replace ./star in ./rn if script is specified
        if self.settings.get("rn_script", None) is not None:
            find_and_replace_text(
                file=os.path.join(self.final_directory, "rn"),
                find_text="./star",
                replace_text=self.settings["rn_script"],
            )
        return

    def run_MESA_grid(self):

        if self.settings["restart_unfinished_runs"]:

            ############################################################
            # Get masses list for restarting unfinished runs

            # check all status files to see what models didn't finish
            status_dict = check_all_status_files(
                grid_directory=self.settings["grid_directory"]
            )
            restart_list = [
                mass for mass in status_dict if status_dict[mass] != "FINISHED"
            ]
            self.masses_list = restart_list

            print("MASSES TO RESTART")
            print(restart_list)

        #############################################################
        # otherwise get mass list from masses_dic (set in decide_masses_dict)
        else:
            self.masses_list = [m for m in self.masses_dic]

        ############################################
        # Handle choice of running (or preparing the main script to run) the grid

        if self.settings["workload_manager"] == "slurm":
            self.MESA_Slurm_run_grid()
        elif self.settings["workload_manager"] == "multiprocessing":
            self.MESA_multiprocessing_run_grid()
        else:
            raise ValueError(
                "Choice for 'workload_manager': '{}' not supported".format(
                    self.settings["workload_manager"]
                )
            )

        return

    def format_settings_filename(self):
        """
        Function to format the settings filename
        """

        return "grid_Z%7.2e_settings.json" % (float(self.settings["metallicity"]))


if __name__ == "__main__":
    MesaGridRunner_instance = MesaGridRunner(settings={})
