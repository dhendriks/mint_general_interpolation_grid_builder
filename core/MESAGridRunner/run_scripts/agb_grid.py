"""
To run MINT grid
"""

import os

from mint_general_interpolation_grid_builder.eureka.check_nodes import (
    check_for_bad_nodes,
)

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner import (
    MESAGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.config.mint_inlists import inlists


# required for University of Surrey eureka HPC
bad_nodes = check_for_bad_nodes()
node_list = ",".join(bad_nodes)

# We then use this configuration and update it to reflect the settings that we want to use in this specific grid.
grid_config = {
    #################################
    # system properties
    "grids_root_directory": "/users/nr00492/test_agb_grid",
    "workload_manager": "slurm",
    "auto_submit": False,
    "archive_previous_grid": False,  # moves existing grid with same metallicity to archive
    #################################
    # email notifications
    "email_notifications_enabled": False,
    "email_notifications_APP_password": os.getenv("MINT_NOTIFICATION_APP_PW"),
    "email_notifications_recipients": ["nr00492@surrey.ac.uk"],
    ##################################
    # MESA setup
    "module_name": "agb",
    "mesa_version": "23051",
    "metallicity": 0.014,
    "num_cores_for_MESA": 12,  # number of processes used by MESA
    "MESA_output_directory": "/users/nr00492/parallel_scratch/test_agb_grid",
    "caches_dir": "/users/nr00492/parallel_scratch/caches/test_agb_grid",
    "MESA_setup_code": "export MESA_DIR=/users/nr00492/mesa-r23.05.1\nmodule load mesasdk/22.6.1",
    "use_jermyn22_conv_pen_MS": True,
    ################################
    # slurm options
    "slurm_partition": "shared",
    "MESA_run_max_hours": 168,
    "extra_sbatch_commands": f"#SBATCH --exclusive=user\n#SBATCH --mem=10GB\n#SBATCH --exclude={node_list}",
    ################################
    # other
    "save_photos_at_grid_points": False,
    "nice_priority": 0,
    "restart_unfinished_runs": False,
}

####################################################
# MESA inlists

# edit inlist options
# note mass and metallicity are handled automatically

# Method 1. target a specific inlist e.g.
inlists["inlist_TPAGB"]["controls"] = {
    **inlists["inlist_TPAGB"]["controls"],
    "use_other_alpha_mlt": ".true. ! prevent FeI (Rees & Izzard 2024)",
    "use_other_eps_grav": ".true. ! prevent HRI (Rees & Izzard 2024)",
    "log_directory": "LOGS_TPAGB",
    "photo_directory": "photos_TPAGB",
}
inlists["inlist_common"]["kap"] = {
    **inlists["inlist_common"]["kap"],
    "kap_lowT_prefix": "'AESOPUS'",
}
inlists["inlist_common"]["controls"] = {
    **inlists["inlist_common"]["controls"],
    "eta_center_limit": "200",
}

# Method 2. replace all occurences of chosen control in inlists

inlist_list = [
    "inlist_common",
    "inlist_MS",
    "inlist_GB",
    "inlist_CHeB",
    "inlist_EAGB",
    "inlist_TPAGB",
]
grid_config["inlists"] = {
    inlist_name: inlists[inlist_name] for inlist_name in inlist_list
}


####################################################
# replace star in ./rn for running MESA

grid_config[
    "rn_script"
] = """
# this provides the definition of do_one (run one part of test)
# do_one [inlist] [output model] [LOGS directory]
#MESA_DIR=../../..
source "${MESA_DIR}/star/test_suite/test_suite_helpers"

cp inlist_MS inlist
./star inlist_MS
cp inlist_GB inlist
./star inlist_GB
cp inlist_CHeB inlist
./star inlist_CHeB
cp inlist_EAGB inlist
./star inlist_EAGB
cp inlist_TPAGB inlist
./star inlist_TPAGB
"""

grid_config["grid_directory"] = os.path.join(
    grid_config["grids_root_directory"], "Z" + str(grid_config["metallicity"])
)

####################################################
# specify masses

grid_config["masses"] = [
    1.0,
    1.25,
    1.5,
    1.75,
    2.0,
    2.25,
    2.5,
    2.75,
    3.0,
    3.25,
    3.5,
    3.75,
    4.0,
    4.5,
    5.0,
    5.5,
    6.0,
    6.5,
    7.0,
    7.5,
    8.0,
]

####################################################
# run grid builder

grid_builder = MESAGridRunner(settings=grid_config)

grid_builder.write_and_run_MESA_grid()
