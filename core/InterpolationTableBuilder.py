"""
Main Interpolation table builder class
"""

from pathlib import Path
import os

import multiprocessing
import setproctitle
import pickle
import numpy as np

import pandas as pd
from PyPDF2 import PdfMerger
import mesaPlot as mp
from scipy.integrate import cumulative_trapezoid


from mint_general_interpolation_grid_builder.functions.track_intersection.functions import (
    track_intersection_main,
)

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.email_notifications.functions import (
    Email_context_manager,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner_extensions.email import (
    Email,
)


class InterpolationTableBuilder(Email):
    def __init__(self, settings):

        Email.__init__(self)

        # Main init
        self.settings = settings
        self.job_list = []
        self.job_results = []

        self.home = str(Path.home())
        self.max_result_queue_size = 10000

        return

    def __str__(self):
        return self.name

    def build_interpolation_grid(self):
        """
        Placeholder for Main function to build the interpolation grid.
        """

        print(f"\nBuilding {self.evol_phase} interpolation table\n")

        #
        with Email_context_manager(config=self.settings, class_object=self):
            ########
            # Pre-readout hook
            self.pre_readout_hook()

            ########
            # Retrieve the mesa model files and store them internally
            self.retrieve_detailed_mesa_models()

            ########
            # Pre-multiprocessing hook
            self.pre_multiprocessing_hook()

            ########
            # Multiprocess the job queue that was built in the retrieve_detailed_mesa_models() function.
            self.multiprocessing_main()

            ########
            # Pre-readout hook
            self.pre_gridbuilding_hook()

            ########
            # Track intersection checker
            self.track_intersection()

            ########
            # Build the interpolation table
            self.create_interpolation_table()

            ########
            # Pre-readout hook
            self.post_gridbuilding_hook()

        return

    def pre_readout_hook(self):

        pass

    def retrieve_detailed_mesa_models(self):
        """
        Provides job list from available masses
        """

        raise NotImplementedError

    def pre_multiprocessing_hook(self):
        """Quality checks on full MESA files before building the table"""

        pass

    def multiprocessing_main(self):
        """
        Function to run the multiprocessing of a job queue and return or store the results

        Jobs are sourced from the user-determined self.job_list

        the output is stored (if put in the result_queue) in the self.job_results
        """

        print("\nMULTIPROCESSING EXTRACTION OF MESA DATA\n")
        # Set process name
        setproctitle.setproctitle("Gridbuilder parent process")

        # if max_queue_size is zero, calculate automatically
        if self.settings["max_queue_size"] == 0:
            self.settings["max_queue_size"] = 2 * self.settings["num_processes"]

        ############
        # Set up the manager object and queues
        manager = multiprocessing.Manager()
        job_queue = manager.Queue(maxsize=self.settings["max_queue_size"])
        result_queue = manager.Queue(maxsize=self.max_result_queue_size)

        ############
        # Create process instances to run the stars
        processes = []
        for ID in range(self.settings["num_processes"]):
            processes.append(
                multiprocessing.Process(
                    target=self.multiprocessing_worker_function,
                    args=(job_queue, result_queue, ID),
                )
            )

        ############
        # Activate the processes
        print("Starting subprocesses.")
        for p in processes:
            p.start()
        print("Started subprocesses.")

        # Set up the system_queue in the parent process
        self.multiprocessing_queue_filler(
            job_queue, processes, num_processes=self.settings["num_processes"],
        )

        ############
        # Join the processes after the queue filler has finished
        print("Do join of subprocesses ...")
        for p in processes:
            p.join()
        print("Joined all subprocesses.")

        ###########
        # Readout the result queue and store internally
        sentinel = object()
        for job_dict in iter(result_queue.get, sentinel):
            self.job_results.append(job_dict)
            if result_queue.empty():
                break

        ##########
        # order job_results in total mass
        self.job_results = sorted(self.job_results, key=lambda x: x["total_mass"])

        ##########
        # load temporary files, order data and write to table
        self.tracks = []
        for job_dict in self.job_results:

            for filename in job_dict["result"]:

                # print(job_dict)
                try:

                    # print(filename)
                    with open(filename, "rb") as fn:
                        data_list = pickle.loads(fn.read())["results"]
                except Exception as e:
                    print(e)
                    print("no data for file ", filename)
                    continue

                current_df = pd.DataFrame.from_dict(data_list)
                self.tracks.append({"filename": filename, "df": current_df})

        return

    def multiprocessing_worker_function(self, job_queue, result_queue, ID):
        """
        Function to execute the self.process_detailed_mesa_models in parallel
        """

        ############################################################
        # Readout the job queue and loop continue until we hit STOP
        for job_number, job_dict in iter(job_queue.get, "STOP"):

            ################
            # Print some info
            process = multiprocessing.current_process()
            # print(process.name, ID, job_number, job_dict)

            ###############
            # Call the function to process the detailed mesa models for a given track or star. This function is passed the job-info.
            self.process_detailed_mesa_models(job_dict, result_queue)

        return

    def multiprocessing_queue_filler(self, job_queue, processes, num_processes):
        """
        Function to fill the queue object for the multiprocessing
        """

        #######
        # Loop over the list and put these in the actual queue
        for job_number, job_dict in enumerate(self.job_list):

            ########
            # Put system in the job queue
            job_queue.put((job_number, job_dict), block=True)

        #######
        # Signal stop when the job_list has been exhausted
        for _ in range(num_processes):
            job_queue.put("STOP")

        return

    def process_detailed_mesa_models(self, job_dict, result_queue):
        """
        This deals with individual jobs (i.e. tracks, stars etc)

        job_dict contains total mass and the corresponding initial_masses

        """

        raise NotImplementedError

    def pre_gridbuilding_hook(self):
        """
        Dummy function to do some pre-gridbuilding checks (i.e. see the results of the multiprocessing have been structured correctly)
        """
        pass

    def track_intersection(self):
        """
        Function to handle the track intersection checking.

        Requires:
        - self.tracks: a loaded list/dict with all the data per track
        - config
        """

        #
        if self.settings["handle_track_intersection"]:
            self.tracks = track_intersection_main(
                tracks=self.tracks,
                scalar_input_columns=self.scalar_input_columns,
                config=self.settings,
            )

        #

        # for track in self.tracks, access data with df.
        #   if we need to sort it, we should sort it by the second input scalar column
        #   when there are 2 interpolation variables, check
        #
        #   maybe we should not do this for the MS anyway, so lets skip interpolation variable number = 2
        #

    def create_interpolation_table(self):
        """
        Deals with combining all the results of the individual jobs and building up the interpolation table.

        Depending on how the results are stored, we can now build the interpolation table

        """

        raise NotImplementedError

    def post_gridbuilding_hook(self):
        """
        Output info on succesful and unsuccesful models and make plots

        """
        pass


if __name__ == "__main__":
    InterpolationTableBuilder_instance = InterpolationTableBuilder(settings={})
