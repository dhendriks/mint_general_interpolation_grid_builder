"""
Example subclass of the TableBuilder
"""

import os
import json

import numpy as np
import pandas as pd

from mint_general_interpolation_grid_builder.core.InterpolationTableBuilder import (
    InterpolationTableBuilder,
)


class ExampleTableBuilder(InterpolationTableBuilder):
    """
    Subclass to the TableBuilder
    """

    def __init__(self, settings):
        self.evol_phase = "EXAMPLE"

        # Init mixin classes
        InterpolationTableBuilder.__init__(self, settings=settings)
        # MINTGridBuilder.__init__(self)

        # grid description
        self.name = "EXAMPLE"
        self.longname = "example"
        self.description = (
            "General example of the setup of a interpolation table builder"
        )

        # self.tmp_dir = "/tmp/mesa_gridbuilder_example"
        # os.makedirs(self.tmp_dir, exist_ok=True)

        # #
        # self.name = "EXAMPLE"
        # self.longname = "Example stellar type"
        # self.description = "Description EXAMPLE stellar type"

        # self.vector_parameter_length = 10

    def retrieve_detailed_mesa_models(self):
        """
        Override of the placeholder retrieve_detailed_mesa_models

        Eventually this needs to create a list of jobs:
        """

        self.job_list = [
            {"job_number": i, "job_name": i, "job_argument": i ** 2} for i in range(100)
        ]

    def process_detailed_mesa_models(self, job_dict, result_queue):
        """
        Override of the placeholder process_detailed_mesa_models

        This deals with individual jobs (i.e. tracks, stars etc)

        The method we employ here is to write the results back into the job and write the dict to a file
        """

        # We can pass back the job dict with a result stored in it
        job_dict["result"] = {"mass": np.random.random(), "radius": np.random.random()}

        # write to file
        tmp_file = os.path.join(
            self.tmp_dir, "job_{}.json".format(job_dict["job_number"])
        )
        with open(tmp_file, "w") as f:
            f.write(json.dumps(tmp_file))

        # put the tmp file in the result list
        result_queue.put(tmp_file)

    def create_interpolation_table(self):
        """
        Override of the placeholder create_interpolation_table

        Deals with combining all the results of the individual jobs and building up the interpolation table.

        Depending on how the results are stored, we can now build the interpolation table.

        The call to handle the columns requires the class to have fixed definitions on the column that will be output
        """

        # Function to read out the temp files that are passed through the self.results_list and load these into a dataframe
        combined_df = self.combine_temp_datafiles_into_dataframe()

        # the handle_columns_interpolation_table requires the following column set to be defined:
        #   self.scalar_input_columns, self.scalar_output_columns.
        #   self.vector_output_columns is calculated automatically based on whether the columns have the CHEBYCHEV
        #   these columns should be present in the dataframe too.
        #   you can set the columns just before this function call, or in the init of the class or something
        combined_df = self.handle_columns_interpolation_table(df=combined_df)

        # Write the dataframe to a table
        self.write_dataframe_to_table_file(filename="example_grid.dat", df=combined_df)


if __name__ == "__main__":
    from mint_general_interpolation_grid_builder.MINT.config.table_columns import (
        table_columns,
    )

    #
    example_settings = {
        "max_queue_size": 10,
        "num_processes": 4,
        "EXAMPLE_scalar_input_columns": table_columns["EXAMPLE_scalar_input_columns"],
        "EXAMPLE_scalar_output_columns": table_columns["EXAMPLE_scalar_output_columns"],
        "vector_output_columns": table_columns["vector_output_columns"],
        "grid_directory": "/tmp",
        "metallicity": 0.02,
        "email_notifications_enabled": False,
    }

    grid_builder = ExampleTableBuilder(settings=example_settings)

    # grid_builder.build_interpolation_grid()
