!------- for use during AGB evolution ---------------------

! fixed quantities

  ! x_integer_ctrl(1) = evol phase

  ! x_logical_ctrl(1) = avoid HRI
  ! x_logical_ctrl(2) = save hdf5 files

! variable quantities- ENSURE THERE IS NO CONFLICT WITH OTHER USAGE OF THESE PARAMETERS

  ! xtra(1) = max_LHe

  ! ixtra(2) = TP_count

  ! lxtra(1) = keep_hydro_off
  ! lxtra(2) = hydro_on
  ! lxtra(3) = in_LHe_peak

character(len=20) :: envelope_overshooting_scheme
real(dp) :: VCT
real(dp) :: MXP
real(dp) :: overshoot_1_f
real(dp) :: overshoot_1_f0
real(dp) :: overshoot_2_f
real(dp) :: overshoot_2_f0
real(dp) :: alpha_mlt_start
real(dp) :: logP
integer :: num_extra_history_columns_eagb = 4
integer :: num_extra_history_columns_tpagb = 15
integer :: num_extra_profile_columns_tpagb = 2

! ----------------------------------------------------------------
