! Main Sequence Routines
! Author: Natalie Rees

subroutine data_for_extra_history_columns_ms(n,names,vals,s,j)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    type (star_info), pointer :: s
    integer, intent(out) :: j
    real(dp) :: burning_shell_thickness

    if (save_targets_MS_size>1) then
        if (s%center_h1<save_targets_list(save_targets_MS_size-1,1)) then
            call calc_burning_shell_thickness(s,burning_shell_thickness)
        else
            burning_shell_thickness = 0
        end if
    end if

    names(1) = 'burning_shell_thickness'
    vals(1) = burning_shell_thickness

    j = j+1

end subroutine data_for_extra_history_columns_ms
