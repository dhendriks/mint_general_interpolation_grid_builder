!=========================================================
! ROUTINES FOR AGB EVOLUTION
! AUTHOR: NATALIE REES
! REFERENCE: Rees et al. 2023, Rees & Izzard 2023
!=========================================================

subroutine extras_startup_agb(s, restart, id, ierr)
    logical, intent(in) :: restart
    integer, intent(in) :: id
    integer, intent(out) :: ierr
    type (star_info), pointer :: s
    ierr = 0

    if (.not. restart) then
        select case(evol_phase)
        ! case(4) ! EAGB
        !     s% ixtra(2) = 0 !TP_count
        !     s% lxtra(3) = .false. !in_LHe_peak
        !     ! save_target = ANINT(s% c_core_mass/s%he_core_mass*100)/100
        case(5) ! TP-AGB
            s% ixtra(2) = 1 ! TP_count
            s% lxtra(1) = .false. !keep_hydro_off
            s% lxtra(2) = .false. !hydro_on
            s% lxtra(3) = .true. !in_LHe_peak
        end select
    end if

    select case(evol_phase)

    case(5) !TP-AGB
        VCT = s% x_ctrl(1)
        MXP = s% x_ctrl(2)
        overshoot_1_f = s% overshoot_f(1) !
        overshoot_1_f0 = s% overshoot_f0(1) !
        overshoot_2_f = s% overshoot_f(2) !
        overshoot_2_f0 = s% overshoot_f0(2) !
        alpha_mlt_start = s%mixing_length_alpha !
        envelope_overshooting_scheme = s% overshoot_scheme(2)

    end select

end subroutine extras_startup_agb

subroutine extras_start_step_eagb(s)
    type (star_info), pointer :: s
    real(dp) :: min_beta

    ! relax timestep controls to get through carbon burning
    if (s%c_core_mass>1.0) then
        s% varcontrol_target = 1d-3
        s% delta_lgL_He_limit = 0.025
        s% lgL_He_burn_min = 2.5
    end if
    ! relax timestep controls for massive AGB stars
    if (s%c_core_mass>0.8) then
        s% dH_limit_min_H = 1d99
        s% dH_limit = 1d99
        s% dH_div_H_limit_min_H = 1d-3
        s% dH_div_H_limit = 0.9d0
    end if

    !relax varcontrol if getting post-AGB like structure due to envelope stripping
    call calc_min_beta(s,min_beta)
    if (min_beta <0.4) then
        s% varcontrol_target = 1d-3
    end if

end subroutine extras_start_step_eagb


subroutine data_for_extra_history_columns_eagb(n,names,vals,s)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    type (star_info), pointer :: s
    real(dp) :: alpha_mlt_max, min_beta

    ! names(1) = 'TP_count'
    ! vals(1) = s% ixtra(2)

    names(1) = 'alpha_mlt_surface'
    vals(1) = s% alpha_mlt(1)

    call calc_alpha_mlt_max(s,alpha_mlt_max)

    names(2) = 'alpha_mlt_max'
    vals(2) = alpha_mlt_max

    call calc_min_beta(s,min_beta)
    names(3) = 'min_beta'
    vals(3) = min_beta

    names(4) = 'varcontrol'
    vals(4) = s% varcontrol_target


end subroutine data_for_extra_history_columns_eagb

subroutine data_for_extra_history_columns_tpagb(n,names,vals,s,j)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    integer, intent(out) :: j
    type (star_info), pointer :: s
    real(dp) :: alpha_mlt_max, min_beta
    real(dp) :: c13_eff_pocket, c13_pocket_width
    real(dp):: frac
    integer :: i

    call calc_alpha_mlt_max(s,alpha_mlt_max)

    names(j+1) = 'alpha_mlt_max'
    vals(j+1) = alpha_mlt_max

    names(j+2) = 'alpha_mlt_surface'
    vals(j+2) = s% alpha_mlt(1)

    call calc_min_beta(s,min_beta)
    names(j+3) = 'min_beta'
    vals(j+3) = min_beta

    names(j+4) = 'TP_count'
    vals(j+4) = s% ixtra(2)

    names(j+5) = 'hydro_on'
    if (s% lxtra(2) .eqv. .true.) then
        vals(j+5) = 1
    else
        vals(j+5) = 0
    endif

    call calc_neutral_H_fraction_envelope(s,frac)

    names(j+6) = 'neutral_fraction_H_envelope'
    vals(j+6) = frac

    names(j+7) = 'PDCZ_overshoot'
    vals(j+7) = s% overshoot_f(1)

    names(j+8) = 'CE_overshoot'
    vals(j+8) = s% overshoot_f(2)

    names(j+9) = 'varcontrol'
    vals(j+9) = s% varcontrol_target

    names(j+10) = 'mesh_dlogX_dlogP_extra'
    vals(j+10) = s% mesh_dlogX_dlogP_extra(1)

    call calc_c13_pocket_mass(s,c13_eff_pocket,c13_pocket_width)

    names(j+11) = 'c13_eff_pocket_mass'
    vals(j+11) = c13_eff_pocket

    names(j+12) = 'c13_pocket_width'
    vals(j+12) = c13_pocket_width

    !coordinate at bottom of all mixing in envelope
    do i = s%num_mixing_regions,1,-1
        if (s%m(s%mixing_region_bottom(i))/Msun>s%he_core_mass) exit
    end do

    names(j+13) = 'env_mix_bot'
    vals(j+13) = s%m(s%mixing_region_bottom(i))/Msun

    names(j+14) = 'env_mix_bot_temp'
    vals(j+14) = s%T(s%mixing_region_bottom(i))

    names(j+15) = 'logP'
    vals(j+15) = logP

    j =j+15

end subroutine data_for_extra_history_columns_tpagb

subroutine data_for_extra_profile_columns_agb(n, nz, names, vals, s, j)
    integer, intent(in) :: n, nz
    integer, intent(out) :: j
    character (len=maxlen_profile_column_name) :: names(n)
    real(dp) :: vals(nz,n)
    type (star_info), pointer :: s
    integer :: k

    names(j+1) = 'beta'
    do k = 1, nz
        vals(k,j+1) = s% Pgas(k)/s% P(k)
    end do

    names(j+2) = 'alpha_mlt'
    do k = 1, nz
        vals(k,j+2) = s% alpha_mlt(k)
    end do

    j = j +2

end subroutine data_for_extra_profile_columns_agb


! CHANGE SO TERMINATES AS SOON AS PULSE STARTS
subroutine check_if_end_EAGB(s,do_termination,do_termination_code_str)
    logical, intent(out) :: do_termination
    character(len=50), intent(out) :: do_termination_code_str
    type (star_info), pointer :: s
    ! logical :: in_LHe_peak
    ! integer :: TP_count

    ! terminate if logLHe <-3
    if (s%power_he_burn<1d-3) then
        do_termination_code_str = 'no He burning'
        do_termination = .true.
    end if

    ! in_LHe_peak = s% lxtra(3)
    ! TP_count = s% ixtra(2)

    ! ! record first thermal pulse and terminate EAGB
    ! if (.not. in_LHe_peak) then
    !     ! check for thermal pulse, require higher he burning lum for higher core masses so not confused with carbon burning
    !     if ((s%he_core_mass-s%c_core_mass<0.1) .and. (((s% power_he_burn .gt. 10**(4)) .AND. (s%he_core_mass < 0.8)) .OR.((s% power_he_burn .gt. 10**(4.5)) .AND. (s%he_core_mass<= 1.2)) .OR. ((s% power_he_burn .gt. 10**(5.5)) .AND. (s%he_core_mass> 1.2)))) then
    !         in_LHe_peak = .true.
    !         TP_count = TP_count + 1
    !         s% need_to_update_history_now = .true.
    !         s% need_to_save_profiles_now = .true.
    !         write(*,*) 'starting thermal pulse', TP_count
    !     end if
    ! ! if thermal pulse started then check if ended and terminate if reached end of first thermal pusle
    ! else
    !     if (s% power_h_burn/s% power_he_burn .gt. 10) then
    !         in_LHe_peak = .false. ! pulse over
    !         write(*,*) 'thermal pulse ended'
    !         do_termination_code_str = 'first pulse over'
    !         do_termination = .true.
    !     end if
    ! end if

    ! check for thermal pulse, require higher he burning lum for higher core masses so not confused with carbon burning
    if ((s%he_core_mass-s%c_core_mass<0.1) .and. (((s% power_he_burn .gt. 10**(4)) .AND. (s%he_core_mass < 0.8)) .OR.((s% power_he_burn .gt. 10**(4.5)) .AND. (s%he_core_mass<= 1.2)) .OR. ((s% power_he_burn .gt. 10**(5.5)) .AND. (s%he_core_mass> 1.2)))) then
        s% need_to_update_history_now = .true.
        s% need_to_save_profiles_now = .true.
        write(*,*) 'starting thermal pulse'
        do_termination_code_str = 'TPs started'
        do_termination = .true.
    end if

    ! s% lxtra(3) = in_LHe_peak
    ! s% ixtra(2) = TP_count

end subroutine check_if_end_EAGB


subroutine control_hydro(id,s,ierr)
    ! HYDRO CONTROLS (Rees & Izzard 2023)
    ! turns hydro on for He shell flash to prevent convergence problems
    ! turns hydro on at start of thermal pulse
    ! turns hydro of once LHe drops below max value reached and is less than 1d6
    ! call subroutine in extras_finish_step
    ! must use with check_for_TP so that in_LHe_peak is updated
    integer, intent(in) :: id
    integer, intent(out) :: ierr
    integer :: i
    real(dp) :: max_LHe
    logical :: keep_hydro_off, hydro_on, in_LHe_peak
    type (star_info), pointer :: s
    ! ierr = 0
    ! call star_ptr(id, s, ierr)
    ! if (ierr /= 0) return

    max_LHe = s% xtra(1)
    keep_hydro_off = s% lxtra(1)
    hydro_on = s% lxtra(2)
    in_LHe_peak = s% lxtra(3)

    !write(*,*) 'hydro_on = ', hydro_on, 'in_LHe_peak = ', in_LHe_peak
    !write(*,*) 'max LHe = ', max_LHe, 'LHe = ', s%power_he_burn

    !routine for turning hydro on and off during He shell flash
    if (hydro_on .eqv. .false.) then
        call star_set_v_flag(id, .false., ierr)
        if ((in_LHe_peak) .AND. (.not. keep_hydro_off)) then
            hydro_on = .true.
            max_LHe = s% power_he_burn
        else if (.not. in_LHe_peak) then
            keep_hydro_off = .false.
        end if
    else
        call star_set_v_flag(id, .true., ierr)
        if (s% power_he_burn > max_LHe) then
            max_LHe = s% power_he_burn
        ! else if ((s% power_he_burn < (max_LHe/1.1))q .AND. (s% power_he_burn < 1d6)) then
        !     s%accel_factor = (s%power_he_burn-1d5)/(1d6-1d5)
        !     write(*,*) s%accel_factor
        else if ((s% power_he_burn < (max_LHe/1.1)) .AND. (s% power_he_burn < 1d6)) then
            hydro_on = .false.
            keep_hydro_off = .true.
        end if
    end if

    s% xtra(1) = max_LHe
    s% lxtra(1) = keep_hydro_off
    s% lxtra(2) = hydro_on

end subroutine control_hydro


subroutine check_for_TP(s)
    ! thermal pulse detection and counting
    ! call subroutine in extras_finish_step
    type (star_info), pointer :: s
    logical :: in_LHe_peak
    integer :: TP_count

    in_LHe_peak = s% lxtra(3)
    TP_count = s% ixtra(2)

    ! record thermal pulses
    if (.not. in_LHe_peak) then
        ! check for peak
        if (s% power_he_burn .gt. 1e4) then
            in_LHe_peak = .true.
            TP_count = TP_count + 1
            s% need_to_update_history_now = .true.
            s% need_to_save_profiles_now = .true.
            write(*,*) 'starting thermal pulse'
        end if
    else
        if ((s%power_he_burn .lt. 1d3) .and. (s% power_h_burn/s% power_he_burn .gt. 10)) in_LHe_peak = .false. ! pulse over
    end if

    s% lxtra(3) = in_LHe_peak
    s% ixtra(2) = TP_count

end subroutine check_for_TP


subroutine resolve_TDU(s,ierr)
    ! routine to increase temporal and spatial resolution during TDU (Rees et al. 2023)
    ! call subroutine in extras_finish_step
    integer, intent(out) :: ierr
    integer :: i,j
    type (star_info), pointer :: s
    ierr = 0
    ! call star_ptr(id, s, ierr)
    ! if (ierr /= 0) return

    ! write(*,*) 'VCT = ', VCT

    !find bottom of convective envelope for current and previous timestep
    do i = 1, s%num_mixing_regions
        if (s%cz_bot_mass_old(i)/Msun>s%he_core_mass_old) exit
    end do
    do j = 1, s%num_mixing_regions
        if (s%cz_bot_mass(j)/Msun>s%he_core_mass) exit
    end do

    ! write(*,*) s%he_core_mass_old-s%he_core_mass
    ! write(*,*) s%cz_bot_mass_old(i)-s%cz_bot_mass(j)

    ! if convective envelope moving inwards and not in He-shell flash increase resolution
    ! lxtra(2) = hydro_on
    if ((s%cz_bot_mass_old(i)>s%cz_bot_mass(j)) .and. (s% lxtra(2) .eqv. .false.)) then
        if (s%varcontrol_target>VCT+1d-8) then
            s%varcontrol_target = s%varcontrol_target -1d-6
            write(*,*) 'reduced varcontrol_target = ', s%varcontrol_target
        end if
        if (s%mesh_dlogX_dlogP_extra(1)>MXP+0.0001) then
            s%mesh_dlogX_dlogP_extra(1) = s%mesh_dlogX_dlogP_extra(1)-0.01
            write(*,*) 'reduced mesh_dlogX_dlogP_extra for he4 = ', s%mesh_dlogX_dlogP_extra(1)
        end if
    else
        if (s%varcontrol_target<0.999d-4) then
            s%varcontrol_target = s%varcontrol_target +1d-6
            write(*,*) 'reduced varcontrol_target = ', s%varcontrol_target
        end if
        if (s%mesh_dlogX_dlogP_extra(1)<0.999) then
            s%mesh_dlogX_dlogP_extra(1) = s%mesh_dlogX_dlogP_extra(1)+0.01
            write(*,*) 'reduced mesh_dlogX_dlogP_extra for he4 = ', s%mesh_dlogX_dlogP_extra(1)
        end if
    end if

end subroutine resolve_TDU

subroutine check_for_HTDU(s,ierr)
    ! turns of envelope overshooting when convective boundary hot during TDU to avoid convergence problems due to HTDU (Rees et al. 2023)
    ! turn envelope overshooting off during TDU if temp at base of convective envelope > 4e7
    ! must have envelope overshooting as overshooting controls group 2
    ! call subroutine in extras_finish_step
    integer, intent(out) :: ierr
    integer :: i
    type (star_info), pointer :: s
    ierr = 0
    ! call star_ptr(id, s, ierr)
    ! if (ierr /= 0) return

    !find zone number for bottom of all mixing in envelope
    do i = s%num_mixing_regions,1,-1
        if (s%m(s%mixing_region_bottom(i))/Msun>s%he_core_mass) exit
    end do

    ! change envelope overshooting
    if ((s%T(s%mixing_region_bottom(i))>0.4*1d8) .and. (s%varcontrol_target<9.9d-5)) then
        s% overshoot_scheme(2) = ' '
        write(*,*) 'no envelope overshooting, T=', s%T(s%mixing_region_bottom(i))
    else
        s% overshoot_scheme(2) = envelope_overshooting_scheme !'other' !'exponential'
    end if

end subroutine check_for_HTDU

subroutine avoid_HRI(s,envelope_overshooting_scheme,ierr)
    ! Rees & Izzard 2023
    ! phases out overshooting and increases alpha mlt as envelope decreases to avoid HRI
    ! call subroutine in extras_start_step
    integer, intent(out) :: ierr
    character(len=20),intent(out) :: envelope_overshooting_scheme
    integer :: i
    type (star_info), pointer :: s
    real(dp) :: Ms, Me, alpha_s, alpha_e, f, Teff_s, Teff_e
    ierr = 0
    ! call star_ptr(id, s, ierr)
    ! if (ierr /= 0) return

    !routine to phase out overshooting when star loosing mass to avoid HRI
    ! start to reduce overshooting when M=Ms
    ! overshooting reaches 0 when M = Me
    ! multiply overshooting by factor f = (M-Me)/(Ms-Me)


    Ms = 0.9*s% initial_mass
    Me = 0.5*s% initial_mass !s%he_core_mass+0.1 !initial_mass/3

    ! relax VCT as envelope removed, VCT = x_ctrl(1)
    ! if (s%star_mass<Ms) then
    !     VCT = (s% x_ctrl(1)-1d-4)*(s%star_mass-Ms)/(Ms-Me)+s% x_ctrl(1)
    ! end if

    if (s%star_mass<Me) then
        VCT = 1d-4 !VCT
    end if

    ! Ms = 0.9*s% initial_mass
    ! Me = s% initial_mass/2 !s%he_core_mass+0.1 !initial_mass/3

    if ((s%star_mass<Ms) .and. (s% star_mass >Me)) then
        f = (s%star_mass-Me)/(Ms-Me)
        s% overshoot_f(1) = overshoot_1_f*f !0.01
        s% overshoot_f0(1) = overshoot_1_f0*f !0.002
        ! write(*,*) s%overshoot_f(1)
        s% overshoot_f(2) = overshoot_2_f*f !0.0176 !0.014
        s% overshoot_f0(2) = overshoot_2_f0*f !0.0016 !0.002
        ! write(*,*) s%overshoot_f(2)
    else if (s% star_mass <= Me) then
        s% overshoot_scheme(1) = ' '
        s% overshoot_scheme(2) = ' '
        envelope_overshooting_scheme = ' '
    end if

    !routine to increase alpha mlt when star loosing mass
    ! start to increase alpha mlt when M=Ms
    ! alpha mlt reaches alpgha_e when M = Me
    ! y = [(ae-as)(x-Ms)+as(Me-Ms)]/(Me-Ms)

    !try increasing alpha linearly between M = 0.33*Mi and M = 0.15*Mi

    Ms = 0.7*s% initial_mass !3*s%he_core_mass
    Me = 0.3*s% initial_mass !s%he_core_mass+0.1
    alpha_s = alpha_mlt_start
    alpha_e = 3.0 !2.5

    Teff_s = 2800
    Teff_e = 2300

    if ((s% Teff <Teff_s) .and. (s%Teff>Teff_e)) then
        s% mixing_length_alpha = ((alpha_e-alpha_s)*(s% Teff-Teff_s)+alpha_s*(Teff_e-Teff_s))/(Teff_e-Teff_s)
    else if (s% Teff >= Teff_s) then
        s% mixing_length_alpha = alpha_s
    else
        s% mixing_length_alpha = alpha_e
    end if

    ! if ((s%star_mass<Ms) .and. (s% star_mass >Me)) then
    !     s% mixing_length_alpha = ((alpha_e-alpha_s)*(s%star_mass-Ms)+alpha_s*(Me-Ms))/(Me-Ms)
    ! else if (s% star_mass <=Me) then
    !     s% mixing_length_alpha =alpha_e
    ! end if

end subroutine avoid_HRI


subroutine alpha_mlt_beta(id, ierr)
    ! Rees & Izzard 2023
    ! increase mixing length parameter as a function of beta=Pgas/P to avoid the Fe-peak instability
    ! in extras_controls:
    !     s% other_alpha_mlt => alpha_mlt_beta
    ! in inlist:
    !     use_other_alpha_mlt = .true.

    integer, intent(in) :: id
    integer, intent(out) :: ierr
    type (star_info), pointer :: s
    integer :: i, inner_index
    real(dp) :: beta, logT, A, B, A1, A2
    ierr = 0
    call star_ptr(id, s, ierr)
    if (ierr /= 0) return

    select case (evol_phase)
    case(4)
        ! for use in stripped models undergoing he-shell burning (post-AGB like structure)
        if (s%he_core_mass/s%star_mass>0.7) then
            inner_index = s%c_core_k
            A = 40
            B = 0.3
        else
            A=0
        end if

    case(5)
        ! for use in TP-AGB models
        inner_index = s%he_core_k
        if (s%power_h_burn>1d0) then
            A1 = s% mixing_length_alpha
        else if (s% power_h_burn<1d-2) then
            A1 = 30
        else
            A1 = -14.0*LOG10(s% power_h_burn)+2
        end if
        A2=-9*(s%star_mass/s%he_core_mass-1)+20
        A = MAX(A1,A2)
        B=0.3

    case(6)
        ! for use in post-AGB models
        inner_index =s%he_core_k
        A = 30
        ! A = 10+40*cos(3.14/(instability_mass-s% he_core_mass)*(s% star_mass -s% he_core_mass))+40
    end select

    if (A>s%mixing_length_alpha) then
        do i=1,inner_index
            beta = s% Pgas(i)/s% P(i)
            s% alpha_mlt(i) = A -(A - s% mixing_length_alpha) /(1+10.0**(-6*(beta-B)))
        end do
    end if

end subroutine alpha_mlt_beta

! wind mass loss routine
subroutine vassilliadis_wood_wind(id, Lsurf, Msurf, Rsurf, Tsurf, X, Y, Z, w, ierr)
    ! Vassilliadis & Wood 1993 wind mass loss rate
    ! in extras_controls:
    !      s% other_wind => vassilliadis_wood_wind
    ! in inlist:
    !      use_other_wind = .true.

    use star_def
    use chem_def, only: ic12, io16

    integer, intent(in) :: id
    real(dp), intent(in) :: Lsurf, Msurf, Rsurf, Tsurf, X, Y, Z ! surface values (cgs)
    ! NOTE: surface is outermost cell. not necessarily at photosphere.
    ! NOTE: don't assume that vars are set at this point.
    ! so if you want values other than those given as args,
    ! you should use values from s% xh(:,:) and s% xa(:,:) only.
    ! rather than things like s% Teff or s% lnT(:) which have not been set yet.
    real(dp), intent(out) :: w ! wind in units of Msun/year (value is >= 0)
    integer, intent(out) :: ierr
    real(dp) :: Mdot1, Mdot2, vexp, P, factor
    real(dp) :: m, r, CO
    integer :: c12, o16
    !print*, 'Msurf', Msurf,'Rsurf', Rsurf

    type(star_info), pointer :: s
    call star_ptr(id,s,ierr)
    if(ierr/=0) return

    ! period from Vassilliadis & Wood
    !logP = -2.07+1.94*log10(Rsurf/(6.957D10))-0.9*log10(Msurf/(1.9885D33))
    !P = 10.0**logP

    ! fundamental period from Trabucchi et al 2018
    m = log10(Msurf/(1.9885D33))
    r = log10(Rsurf/(6.957D10))
    c12 = s% net_iso(ic12)
    o16 = s% net_iso(io16)
    CO = s% xa(c12,1)/ s% xa(o16,1)

    logP = -1.12166+1.24449*m+1.07886*r+0.87741*m**2-1.53239*m*r+0.10382*r**2-0.07659*m**3-0.26130*m**2*r+0.26867*m*r**2+0.03278*r**3-0.02713*log10(Z)+0.14872*Y-0.01455*log10(CO/0.4125541984736385)
    P = 10.0**logP

    vexp = -13.5 + 0.056*P

    if (vexp .lt. 3.0) then
        vexp = 3.0
    else if (vexp .gt. 15.0) then
        vexp = 15.0
    end if

    if (s% initial_mass >= 2.5) then
        Mdot1 = 10.0**(-11.4 + 0.0125*(P-100*(Msurf/1.9885D33-2.5)))
    else
        Mdot1 = 10.0**(-11.4 + 0.0123*P)
    end if

    Mdot2 = Lsurf*3.154D7/(2.9979D10*vexp*1D5*1.9885D33)

    select case(evol_phase)
    case(4)
        factor = 1.0
    case(5)
        factor = 1.0
    case(6)
        factor = 1.0
    end select
    !write(*,*) 'factor = ', factor
    w = factor*MIN(Mdot1, Mdot2)
    !write(*,*) w

    !mass-loss-period relation from Straniero et al 2006
    ! if (logP<2.5) then
    !    w = 10**(-7.7)
    ! else if (logP<=3.1) then
    !    w = 10**(-101.6+63.2*logP-10.282*logP**2)
    ! else
    !    w = 10**(-4.3)
    ! end if

    ierr = 0
end subroutine vassilliadis_wood_wind


subroutine calc_min_beta(s,min_beta)
    real(dp), intent(out) :: min_beta
    integer :: i
    type (star_info), pointer :: s
    !calculate minimum value of beta in envelope
    min_beta = 1.0
    do i=1, s% nz
        if (s% Pgas(i)/s% P(i) < min_beta) then
            min_beta = s% Pgas(i)/s% P(i)
        end if
    end do
end subroutine calc_min_beta


subroutine calc_alpha_mlt_max(s,alpha_mlt_max)
    real(dp),intent(out) :: alpha_mlt_max
    integer :: i
    type (star_info), pointer :: s

    alpha_mlt_max = s% mixing_length_alpha
    do i=1, s% nz
        if (s% alpha_mlt(i) > alpha_mlt_max) then
            alpha_mlt_max = s% alpha_mlt(i)
        end if
    end do
end subroutine calc_alpha_mlt_max

subroutine calc_neutral_H_fraction_envelope(s,frac)
    use chem_def, only: ih1
    use ionization_def, only: num_ion_vals, ion_ifneut_H
    real(dp), intent(out) :: frac
    real(dp) :: mHneut, mH, val, ionization_res(num_ion_vals)
    integer :: k, ionization_k
    type (star_info), pointer :: s

    mHneut = 0.0
    mH = 0.0
    do k = 1, s%he_core_k
        val = get_ion_info(ion_ifneut_H,k,chem_isos% Z(ih1))
        mHneut = mHneut + val*s%dm(k)*s% xa(s% net_iso(ih1),k)/1.989d33
        mH = mH + s%dm(k)*s% xa(s% net_iso(ih1),k)/1.989d33
    end do
    frac = mHneut/mH

    contains

    real(dp) function get_ion_info(id,k,Z)
        use ionization_lib, only: eval_ionization
        integer, intent(in) :: id, k, Z
        integer :: ierr
        ierr = 0
        if (ionization_k /= k) then
            call eval_ionization( &
                s% Z(k), s% X(k), s% Rho(k), s% lnd(k)/ln10, &
                s% T(k), s% lnT(k)/ln10, ionization_res, ierr)
            ionization_k = k
        end if
        if (ierr /= 0) then
            get_ion_info = Z
        else
            get_ion_info = ionization_res(id)
        end if
    end function get_ion_info

end subroutine calc_neutral_H_fraction_envelope

subroutine calc_c13_pocket_mass(s,c13_eff_pocket,c13_pocket_width)
    use chem_def, only: ih1, ic13, in14
    real(dp), intent(out) :: c13_pocket_width, c13_eff_pocket
    real(dp) :: c13_eff
    integer :: k
    type (star_info), pointer :: s

    !carbon 13 pocket mass defintion of Cristallo et al 2011
    c13_eff_pocket = 0
    do k = s%he_core_k, s%c_core_k
        c13_eff = s% xa(s% net_iso(ic13),k)-s% xa(s% net_iso(in14),k)*13/14
        if (c13_eff >0) then
            c13_eff_pocket = c13_eff_pocket + c13_eff* s% dm(k)/Msun
        end if
    end do


    !carbon 13 pocket mass definition of Battino et al 2016
    c13_pocket_width = 0
    do k = s%he_core_k, s%c_core_k
        if ((s% xa(s% net_iso(ic13),k)-s% xa(s% net_iso(in14),k)>0) .and. (s%xa(s% net_iso(ic13),k)>1d-3)) then
            c13_pocket_width = c13_pocket_width + s% dm(k)/Msun
        end if
    end do

end subroutine calc_c13_pocket_mass
