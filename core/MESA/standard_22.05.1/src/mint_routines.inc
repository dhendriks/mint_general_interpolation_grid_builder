! MINT routines


subroutine extras_startup_mint(s, restart, ierr)
    logical, intent(in) :: restart
    integer, intent(out) :: ierr
    type (star_info), pointer :: s
    ! save targets for each evol phase
    real, dimension(save_targets_MS_size) :: save_targets_MS
    real, dimension(save_targets_HG_size) :: save_targets_HG
    real, dimension(save_targets_RGB_size) :: save_targets_RGB
    real, dimension(save_targets_CHeB_size) :: save_targets_CHeB
    real, dimension(save_targets_EAGB_size) :: save_targets_EAGB

    integer :: i

    ierr = 0

    num_interp_variables = 1
    if (evol_phase_name=='RGB') num_interp_variables = 2

    ! set dimensions
    allocate(save_target_index_list(num_interp_variables))
    allocate(interp_variable_list(num_interp_variables))
    allocate(save_targets_list_real_lengths(num_interp_variables))
    allocate(interp_variable_increasing(num_interp_variables))
    allocate(max_delta_timestep_limit_list(num_interp_variables))
    allocate(num_retries_list(num_interp_variables))

    if (evol_phase_name == 'MS') then
        save_targets_MS = 0
        allocate(save_targets_list(save_targets_MS_size,1))
        save_targets_list(1:save_targets_MS_size,1) = save_targets_MS
        save_targets_list_real_lengths = save_targets_MS_size
        interp_variable_increasing = .False.
        max_delta_timestep_limit_list = s% delta_XH_cntr_limit

    else if (evol_phase_name == 'RGB') then
        save_targets_HG = 0
        save_targets_RGB = 0
        allocate(save_targets_list(MAX(save_targets_HG_size,save_targets_RGB_size),2))
        save_targets_list(1:save_targets_HG_size,1) = save_targets_HG
        save_targets_list(1:save_targets_RGB_size,2) = save_targets_RGB
        save_targets_list_real_lengths = (/save_targets_HG_size,save_targets_RGB_size/)
        interp_variable_increasing = (/.False., .True./)
        max_delta_timestep_limit_list = (/0.1d0,s%dH_div_H_limit/)

    else if (evol_phase_name == 'CHeB') then
        save_targets_CHeB = 0
        allocate(save_targets_list(save_targets_CHeB_size,1))
        save_targets_list(1:save_targets_CHeB_size,1) = save_targets_CHeB
        save_targets_list_real_lengths = save_targets_CHeB_size
        interp_variable_increasing = .False.
        max_delta_timestep_limit_list = s% delta_XHe_cntr_limit


    else if (evol_phase_name == 'EAGB') then
        save_targets_EAGB = 0
        allocate(save_targets_list(save_targets_EAGB_size,1))
        save_targets_list(1:save_targets_EAGB_size,1) = save_targets_EAGB
        save_targets_list_real_lengths = save_targets_EAGB_size
        interp_variable_increasing = .True.
        max_delta_timestep_limit_list = s% dHe_div_He_limit !FIX

    end if

    num_retries_list = 0

    if (.not. restart) then
        s% ixtra(1:num_interp_variables) = 1 ! save_target_index = 1
    end if

    do i = 1, num_interp_variables
        write(*,*) i,'. save targets = ', save_targets_list(1:save_targets_list_real_lengths(i), i)
    end do


end subroutine extras_startup_mint

subroutine extras_check_model_mint(s,do_retry)
    ! trigger retry if interp_variable overshoots save_target by more than max_error_save_target
    type (star_info), pointer :: s
    logical, intent(out) :: do_retry
    real(dp) :: central_he4, burning_shell_thickness

    ! reset to 0
    hit_interp_variable = 0

    if (evol_phase_name == 'MS') then
        interp_variable_list = s% center_h1

    else if (evol_phase_name == 'RGB') then
        call calc_burning_shell_thickness(s,burning_shell_thickness)
        interp_variable_list = (/burning_shell_thickness/s%star_mass,s%he_core_mass/s%star_mass/)

    else if (evol_phase_name == 'CHeB') then
        call calc_central_he4(s,central_he4)
        interp_variable_list = central_he4

    else if (evol_phase_name == 'EAGB') then
        interp_variable_list = s%c_core_mass/s%he_core_mass

    end if

    save_target_index_list = s% ixtra(1:num_interp_variables)

    call check_retry_for_save_target(do_retry)

end subroutine extras_check_model_mint

subroutine check_retry_for_save_target(do_retry)
    logical, intent(out) :: do_retry
    real(dp) :: save_target, interp_variable
    integer :: i

    do_retry = .false.

    do i = 1, num_interp_variables

        if (num_retries_list(i) < max_num_retries) then

            if (save_target_index_list(i) <= save_targets_list_real_lengths(i)) then
                !extract save target and interp variable for comparison
                save_target = save_targets_list(save_target_index_list(i),i)
                interp_variable = interp_variable_list(i)

                if (abs(interp_variable-save_target)/save_target>max_error_save_target) then
                    if (((interp_variable .lt. save_target) .and. (interp_variable_increasing(i) .eqv. .False.)) .or. ((interp_variable .gt. save_target) .and. (interp_variable_increasing(i) .eqv. .True.))) then
                        write(*,*) i, '. save_target = ', save_target, 'retry due to error in save target = ', (interp_variable-save_target)/save_target
                        do_retry = .true.
                        num_retries_list(i) = num_retries_list(i) + 1
                    end if
                end if
            end if

        else
            write(*,*) 'hit max_num_retries: ', num_retries_list(i)
        end if

    end do

end subroutine check_retry_for_save_target

subroutine extras_finish_step_mint(id,s)
    integer, intent(in) :: id
    real(dp), dimension(num_interp_variables) :: delta_timestep_limit
    type (star_info), pointer :: s

    ! write(*,*) 'finishing step, model number: ',s%model_number
    ! write(*,*) 'save_target_index_list= ', save_target_index_list

    call save_profiles_at_save_targets(id,s,delta_timestep_limit)

    if (evol_phase_name == 'MS') s% delta_XH_cntr_limit = delta_timestep_limit(1)

    ! if (evol_phase_name == 'RGB')

    if (evol_phase_name == 'CHeB') s% delta_XHe_cntr_limit = delta_timestep_limit(1)

    if (evol_phase_name == 'EAGB') s% dHe_div_He_limit = delta_timestep_limit(1) !FIX

    s% ixtra(1:num_interp_variables) = save_target_index_list
    num_retries_list=0 !reset num_retries

end subroutine extras_finish_step_mint

subroutine save_profiles_at_save_targets(id,s,delta_timestep_limit)
    integer, intent(in) :: id
    integer :: ierr
    real(dp), dimension(num_interp_variables), intent(out) :: delta_timestep_limit
    type (star_info), pointer :: s
    real(dp) :: save_target, interp_variable
    integer :: i
    character (len=strlen) :: filename, str_photo

    ierr = 0

    do i = 1, num_interp_variables

        if (save_target_index_list(i) <= save_targets_list_real_lengths(i)) then
            save_target = save_targets_list(save_target_index_list(i),i)
            interp_variable = interp_variable_list(i)

            ! write(*,*) save_target, interp_variable

            ! reduce timestep if near save target to hit target within max_error_save_target
            delta_timestep_limit(i) = MAX(MIN(abs(interp_variable-save_target)/4,max_delta_timestep_limit_list(i)),save_target*max_error_save_target/4)
            ! write(*,*) 'delta_XHe_cntr_limit = ', s%delta_XHe_cntr_limit

            if ((abs(interp_variable-save_target)/save_target<max_error_save_target) .or. (num_retries_list(i)>0)) then
                write(*,*) i, '. save_target = ', save_target, 'error in save target = ', (interp_variable-save_target)/save_target
                ! to save a profile,
                s% need_to_save_profiles_now = .true.
                s% save_profiles_model_priority = save_profile_priority_list(i)
                ! to update the star log,
                s% need_to_update_history_now = .true.
                hit_interp_variable = save_target
                if (s% x_logical_ctrl(3) .eqv. .True.) then
                    ! save photo
                    call string_for_model_number('t', save_target_index_list(i), 3, str_photo)
                    filename = trim(trim(s% photo_directory) // '/' // str_photo )
                    call star_save_for_restart(id, filename, ierr)
                end if
                ! move onto next target
                save_target_index_list(i)=save_target_index_list(i)+1
            end if

        else
            delta_timestep_limit(i) = max_delta_timestep_limit_list(i)

        end if

    end do

end subroutine save_profiles_at_save_targets

subroutine terminate_at_end_save_targets(s,do_termination,do_termination_code_str)
    logical, intent(out) :: do_termination
    character(len=50), intent(out) :: do_termination_code_str
    type (star_info), pointer :: s

    !terminate if interp_variable drops below final target
    ! currently only works for first interp variable if have more than one
    if (interp_variable_list(1)<save_targets_list(save_targets_list_real_lengths(1),1)*0.9) then
        do_termination_code_str = 'reached final save target'
        do_termination = .true.
    end if

end subroutine terminate_at_end_save_targets

subroutine data_for_extra_history_columns_mint(n,names,vals,s,j)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    integer, intent(out) :: j
    type (star_info), pointer :: s

    names(j+1) = 'interp_variable_hit'
    vals(j+1) = hit_interp_variable

    j =j+1

end subroutine data_for_extra_history_columns_mint
