subroutine extras_startup_change_mass(s)
   type (star_info), pointer :: s

   in_relax_phase = .False.
   ! set mass loss/ accretion rate and termination condition depending on final mass
   if (s%star_mass < s%x_ctrl(2)) then
      s% mass_change = mass_change_rate
      ! s% star_mass_max_limit = s%x_ctrl(2)
      ! s% max_years_for_timestep = 1d2
   else if (star_cut_turn_on_factor*s%he_core_mass< s%x_ctrl(2)) then
      s% mass_change = -mass_change_rate !-0.005/(s%dt/3d7)
      ! s% star_mass_min_limit = s%x_ctrl(2)
      ! s% max_years_for_timestep = 1d2
   else
      s% mass_change = 0
      ! s% max_years_for_timestep = 0.1
   end if

end subroutine extras_startup_change_mass

subroutine extras_check_model_change_mass(s,do_retry)
   logical, intent(out)::do_retry
   type (star_info), pointer :: s

   ! trigger retry if overshoots final mass by more than max_error_save_target
   ! x_ctrl(2) = final mass
   if (.not. in_relax_phase) then
      if ((s% star_mass>s%initial_mass) .and. (s%star_mass>s%x_ctrl(2)*(1+max_error_save_target))) then
         write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
         do_retry = .true.
      else if ((s% star_mass<s%initial_mass) .and. (s%star_mass<s%x_ctrl(2)*(1-max_error_save_target)) .and. (star_cut_turn_on_factor*s%he_core_mass < s%x_ctrl(2))) then
         write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
         do_retry = .true.
      end if
   end if

end subroutine extras_check_model_change_mass

subroutine extras_finish_step_mass_change(id,s, do_termination,do_termination_code_str,ierr)
   integer, intent(in) :: id
   logical, intent(out) :: do_termination
   character(len=50), intent(out) :: do_termination_code_str
   type (star_info), pointer :: s
   integer :: i, k
   integer :: ierr
   ierr = 0

   call terminate_if_degenerate(s,do_termination,do_termination_code_str)

   if (.not. in_relax_phase) then

      ! if final mass < star_cut_turn_on_factor*core mass then use cut method to remove mass
      if (star_cut_turn_on_factor*s%he_core_mass > s%x_ctrl(2)) then

         if (s%model_number == 1) then
            !find zone number where mass < final mass
            do i = 1,s%nz
               if (s% m(i) < (s%x_ctrl(2)*Msun)) then
                  k = i
                  exit
               end if
            end do
            call star_relax_to_star_cut(id, k, .false., .true., .false., ierr)

         else if (s% model_number==100) then
            in_relax_phase = .true.

         end if

      ! if using mass loss/ accretion method then end mass change if within allowed error of final mass
      else if (abs(s%star_mass-s%x_ctrl(2))<s%x_ctrl(2)*max_error_save_target) then
         in_relax_phase = .true.
         s% mass_change = 0

      end if

      ! if now in relax phase set termination condition
      if (in_relax_phase) then
         ! calculate kh timescale and add to current age to evolve for another 10 kh timescale during relax
         s%max_age = s% star_age + 10*s%kh_timescale
         write(*,*) 'kh timescale =',s% kh_timescale
         write(*,*) 'max age =',s% max_age
         s% max_years_for_timestep = 1d6

      end if

   end if

end subroutine extras_finish_step_mass_change
