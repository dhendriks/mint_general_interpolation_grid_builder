! Core Helium Burning Routines
! AUTHOR: Natalie Rees

! subroutine extras_startup_change_mass(s)
!     type (star_info), pointer :: s

!     select case(evol_phase)

!     case(10)
!         ! set mass loss/ accretion rate and termination condition depending on final mass
!         if (s%star_mass < s%x_ctrl(2)) then
!            s% mass_change = 1d-4
!            s% star_mass_max_limit = s%x_ctrl(2)
!            s% max_years_for_timestep = 1d2
!         else if (1.25*s%he_core_mass< s%x_ctrl(2)) then
!            s% mass_change =-1d-4
!            s% star_mass_min_limit = s%x_ctrl(2)
!            s% max_years_for_timestep = 1d2
!         else
!            s% mass_change =0
!            s% max_model_number = 100
!            s% max_years_for_timestep = 0.1
!         end if
!     case(20)
!         ! calculate kh timescale and add to current age to evolve for another kh timescale during relax
!         s%max_age = s% star_age + s%kh_timescale
!         write(*,*) 'kh timescale =',s% kh_timescale
!         write(*,*) 'max age =',s% max_age

!     end select

! end subroutine extras_startup_change_mass


! subroutine extras_check_model_change_mass(s,do_retry)
!     logical, intent(out)::do_retry
!     type (star_info), pointer :: s

!     ! trigger retry if overshoots final mass by more than max_error_save_target
!     ! x_ctrl(2) = final mass
!     if ((s% star_mass>s%initial_mass) .and. (s%star_mass>s%x_ctrl(2)*(1+max_error_save_target))) then
!         write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
!         do_retry = .true.
!     else if ((s% star_mass<s%initial_mass) .and. (s%star_mass<s%x_ctrl(2)*(1-max_error_save_target)) .and. (1.25*s%he_core_mass < s%x_ctrl(2))) then
!         write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
!         do_retry = .true.
!     end if

! end subroutine extras_check_model_change_mass


subroutine data_for_extra_history_columns_cheb(n,names,vals,s)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    type (star_info), pointer :: s

    names(1) = 'central_he4'
    vals(1) = interp_variable_list(1)

    names(2) = 'he_core_zone'
    vals(2) = s% he_core_k

    names(3) = 'varcontrol'
    vals(3) = s% varcontrol_target


end subroutine data_for_extra_history_columns_cheb

subroutine data_for_extra_profile_header_items_cheb(n, names, vals, s)
    integer, intent(in) :: n
    character (len=maxlen_profile_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    integer ::  i
    real(dp) :: total_mu
    type(star_info), pointer :: s

    total_mu = 0
    do i = 1, s%nz
        total_mu = total_mu + s% mu(i)* s% dm(i)/1.989D33
    end do

    names(1) = 'average_mu'
    vals(1) = total_mu/ s% star_mass

    names(2) = 'he_core_radius'
    vals(2) = s% he_core_radius

    names(3) = 'he_core_zone'
    vals(3) = s% he_core_k

end subroutine data_for_extra_profile_header_items_cheb

subroutine check_for_he_ignition(s,do_termination,do_termination_code_str)
    ! terminate evolution once log_LHe > 0 to ensure degenerate models which have center_he4 < 0.98 stop before He flash
    ! for higher mass stars with cores > 10M terminate once log_LHe > -2 to prevent crashing due to hitting eddington limit
    logical, intent(out) :: do_termination
    character(len=50), intent(out) :: do_termination_code_str
    type (star_info), pointer :: s

    if ((s%he_core_mass>10.0) .and. (s% power_he_burn >0.01)) then
        do_termination_code_str = 'He ignited'
        do_termination = .true.
    else if ((s%star_mass>2) .and. (s% power_he_burn >0.1)) then
        do_termination_code_str = 'He ignited'
        do_termination = .true.
    else if (s% power_he_burn >0.1) then
        do_termination_code_str = 'He ignited'
        do_termination = .true.
    end if

end subroutine check_for_he_ignition


! subroutine extras_finish_step_mass_change(id,s, do_termination,do_termination_code_str,ierr)
!     integer, intent(in) :: id
!     logical, intent(out) :: do_termination
!     character(len=50), intent(out) :: do_termination_code_str
!     type (star_info), pointer :: s
!     integer :: i, k
!     integer :: ierr
!     ierr = 0

!     select case(evol_phase)

!     case(10)

!         ! if final mass < 1.25*core mass then use cut method to remove mass
!         if ((s%model_number == 1) .AND. (1.25*s%he_core_mass > s%x_ctrl(2))) then
!            !find zone number where mass < final mass
!            do i = 1,s%nz
!               if (s% m(i) < (s%x_ctrl(2)*Msun)) then
!                  k = i
!                  exit
!               end if
!            end do
!            call star_relax_to_star_cut(id, k, .false., .true., .false., ierr)
!         end if

!         ! terminate if within allowed error of final mass
!         if ((1.25*s%he_core_mass < s%x_ctrl(2)) .and. (abs(s%star_mass-s%x_ctrl(2))<s%x_ctrl(2)*max_error_save_target)) then
!             do_termination_code_str = 'reached final mass'
!             do_termination = .true.
!         end if

!     case(20)

!         call terminate_if_degenerate(s,do_termination,do_termination_code_str)

!     end select

! end subroutine extras_finish_step_mass_change

subroutine save_profile_first_model(s)
    type (star_info), pointer :: s
    ! save a profile at start for element abundances pre-He burning
    if (s% model_number == 1) then
       s% need_to_save_profiles_now = .true.
       s% save_profiles_model_priority = 5
    end if
end subroutine save_profile_first_model


subroutine terminate_if_degenerate(s,do_termination,do_termination_code_str)
    logical, intent(out) :: do_termination
    character(len=50), intent(out) :: do_termination_code_str
    type (star_info), pointer :: s

    !terminate if degeneracy becomes too high
    if (s%eta(s%nz)>100.0) then
        do_termination_code_str = 'eta>100'
        do_termination = .true.
    end if

end subroutine terminate_if_degenerate

subroutine calc_central_he4(s,central_he4)
    real(dp),intent(out) :: central_he4
    integer :: ierr, i
    real(dp) :: total_he_mass_in_core
    type (star_info), pointer :: s
    ierr = 0

    !either store central he abundance if not degenerate or calculate average he4 abundance in he core if degenerate
    if ((s% initial_mass <= 2.4) .AND. (s% mlt_mixing_type(s% nz)==0) .AND. (s% center_he4 > 0.9)) then
       ! write(*,*) 'zone = ', core_k, ' mass = ', s% m(core_k)/1.989D33
       total_he_mass_in_core = 0
       do i = s%he_core_k, s%nz
          total_he_mass_in_core = total_he_mass_in_core + s% xa(s% net_iso(ihe4),i)* s% dm(i)/Msun
       end do
       central_he4 = total_he_mass_in_core/(s% m(s%he_core_k)/Msun)
       write(*,*) 'central he4 = ', central_he4
    else
       central_he4 = s% center_he4
    end if
end subroutine calc_central_he4
