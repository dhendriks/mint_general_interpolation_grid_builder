! parameters for binary grid

real(dp) :: star_cut_turn_on_factor = 1.25
real(dp) :: mass_change_rate = 1d-8
logical :: in_relax_phase
