#!/usr/bin/python3

import os


def write_inlist_common(dic):
    # writes inlist_common with options given in dic

    inlist = """
&star_job

      show_log_description_at_start = .false.

      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

      change_initial_net = .true.
      new_net_name = 'o18_and_ne22.net'
      ! new_net_name = 'cno_extras_o18_to_mg26_plus_fe56.net'

      change_v_flag = .true.
      change_initial_v_flag = .true.
      new_v_flag = .false.

      ! caches for use on eureka
      eosDT_cache_dir = '{caches_dir}/eosDT_cache'
      eosPT_cache_dir = '{caches_dir}/eosPT_cache'
      eosDE_cache_dir = '{caches_dir}/eosDE_cache'
      ionization_cache_dir = '{caches_dir}/ionization_cache'
      kap_cache_dir = '{caches_dir}/kap_cache'
      rates_cache_dir = '{caches_dir}/rates_cache'

/ ! end of star_job namelist

&eos
/ ! end of eos namelist

&kap
      use_Type2_opacities = .true.
      Zbase = {metallicity}

/ !end of kap namelist

&controls

! mesh
      mesh_delta_coeff = 2

! Mass dependant Timestep Conditions
      ! Below true for M>2Msun
      ! convergence_ignore_equL_residuals = .false.
      ! use_gold_tolerances = .true.
      ! use_gold2_tolerances = .true.
      ! varcontrol_target = 1d-3

      ! Below true for M<2Msun
      convergence_ignore_equL_residuals = .true.
      use_gold_tolerances = .false.
      use_gold2_tolerances = .false.
      varcontrol_target = 1d-3

! Timestep Conditions
      dX_nuc_drop_limit = 3d-2
      delta_lgRho_cntr_limit = 0.05
      delta_lgRho_cntr_hard_limit = 0.1
      delta_lgT_cntr_limit = 0.01
      delta_lgT_cntr_hard_limit = 0.02
      delta_lgTeff_limit = 0.01
      delta_lgTeff_hard_limit = 0.02
      delta_lgL_limit = 0.1
      delta_lgL_hard_limit = 0.2

! Output
      terminal_interval = 10
      write_header_frequency = 10
      history_interval = 1
      photo_digits = 6

! Mixing Parameters
      mixing_length_alpha = {mixing_length_alpha}

! Atmosphere
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! Energy
      energy_eqn_option = 'dedt'
      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

! diffusion
      ! show_diffusion_info = .true.
      ! diffusion_v_max = 1d-5
      diffusion_use_full_net = .true.
      diffusion_use_cgs_solver = .true.
      diffusion_use_isolve = .true.
      diffusion_rtol_for_isolve = 1d-4
      diffusion_atol_for_isolve = 1d-5
      diffusion_min_X_hard_limit = -1d-4
      diffusion_maxsteps_for_isolve = 1000
      diffusion_isolve_solver = 'ros2_solver'

/ ! end of controls namelist
      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_common"), "w") as f:
        f.write(inlist)

    return


def write_inlist_deflate_envelope(dic):

    inlist = """

&star_job

      show_log_description_at_start = .false.

      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

      change_initial_net = .true.
      new_net_name = 'o18_and_ne22.net'
      ! new_net_name = 'no_h_burning_net.net'
      ! new_net_name = 'cno_extras_o18_to_mg26_plus_fe56.net'

      change_v_flag = .true.
      change_initial_v_flag = .true.
      new_v_flag = .false.

      load_saved_model = .true.
      load_model_filename = '{deflate_envelope_load_model_filename}'

      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = 'model_after_deflated_envelope.data'

      save_model_when_terminate = .true.
      save_model_filename = '{deflate_envelope_save_model_filename}'

      set_initial_dt = .true.
      seconds_for_initial_dt = 3d8

      relax_opacity_max = .true.
      new_opacity_max = 0.1
      opacity_max_multiplier = 0.9

/ ! end of star_job namelist

&eos
/ ! end of eos namelist

&kap
      use_Type2_opacities = .true.
      Zbase = {metallicity}
/ !end of kap namelist

&controls

      mesh_delta_coeff = 2
      tol_correction_norm = 1d0
      tol_max_correction = 1d0
      varcontrol_target = 1d0
      max_years_for_timestep = 0.1
      min_timestep_limit = 1d-3
      opacity_max = 2
      power_nuc_burn_lower_limit = 5d-1
      dxdt_nuc_factor = 25

      log_directory = '{LOGS_deflate_envelope}'
      photo_directory = '{photos_deflate_envelope}'


      photo_interval = 50
      profile_interval = 10

! Mass loss
      x_integer_ctrl(1) = 3 ! part number
      x_ctrl(1) = 0.1d0 !start_of_He_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(2) = 0.1d0 !start_of_H_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(3) = 2d-2 !He_shell_mass_limit as compared to core mass - for stopping condition based on He shell mass
      x_ctrl(7) = 5d-3 ! H_shell_mass_limit as compared to core mass - for stopping condition based on H shell mass
      x_ctrl(8) = 2d3 ! mass loss eta parameter


! Mass dependant Timestep Conditions
      ! Below true for M>2Msun
      ! convergence_ignore_equL_residuals = .false.
      ! use_gold_tolerances = .true.
      ! use_gold2_tolerances = .true.
      ! varcontrol_target = 1d-3

      ! Below true for M<2Msun
      convergence_ignore_equL_residuals = .true.
      use_gold_tolerances = .false.
      use_gold2_tolerances = .false.


! Output
      terminal_interval = 10
      write_header_frequency = 10
      history_interval = 1
      photo_digits = 6

! Mixing Parameters
      mixing_length_alpha = {mixing_length_alpha}

! Atmosphere
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! Energy
      energy_eqn_option = 'dedt'
      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

! Diffusion and Mixing Parameters
      okay_to_reduce_gradT_excess = .true.
      use_ledoux_criterion = .false. !.true.
      alpha_semiconvection = -1 !1d-1
      semiconvection_option = 'Langer_85'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02
      min_overshoot_q = 1d-4
      overshoot_Delta0(1) = 1.0
      ! we use step overshooting
      overshoot_scheme(1) = 'step'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'top'
      overshoot_f(1) = 0.33
      overshoot_f0(1) = 0.05

      do_element_diffusion = .false.
      ! show_diffusion_info = .true.
      ! diffusion_v_max = 1d-5
      diffusion_use_full_net = .true.
      diffusion_use_cgs_solver = .true.
      diffusion_use_isolve = .true.
      diffusion_rtol_for_isolve = 1d-4
      diffusion_atol_for_isolve = 1d-5
      diffusion_min_X_hard_limit = -1d-4
      diffusion_maxsteps_for_isolve = 1000
      diffusion_isolve_solver = 'ros2_solver'

/ ! end of controls namelist
      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_deflate_envelope"), "w") as f:
        f.write(inlist)

    return


def write_inlist_preMS(dic):

    inlist = """

&star_job

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

      initial_zfracs = 7

      create_pre_main_sequence_model = .true.

      save_model_when_terminate = .true.
      save_model_filename = '{preMS_save_model_filename}'
      required_termination_code_string = 'max_age'
/ ! end of star_job namelist

&eos
      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'
/ ! end of eos namelist

&kap
      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'
/ ! end of kap namelist


&controls

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      initial_mass = {start_mass} !********* SPECIFIC SET VARIABLE *********

      log_directory = '{LOGS_preMS}'
      photo_directory = '{photos_preMS}'

      photo_interval = 100
      profile_interval = 10

      max_model_number = 150
      max_age = 0.5d0

      initial_z = {metallicity}

      do_element_diffusion = .false.

! Mass Loss
      cool_wind_full_on_T = 9.99d9
      hot_wind_full_on_T = 1d10
      cool_wind_GB_scheme = 'Reimers'
      cool_wind_AGB_scheme = 'Blocker'
      GB_to_AGB_wind_switch = 1d-4
      Reimers_scaling_factor = 0.5d0
      Blocker_scaling_factor = 0.0003d0

/ ! end of controls namelist

      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_preMS"), "w") as f:
        f.write(inlist)

    return


def write_inlist_restore_opacity(dic):

    inlist = """

&star_job

      show_log_description_at_start = .false.

      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

      change_initial_net = .true.
      new_net_name = 'o18_and_ne22.net'
      ! new_net_name = 'cno_extras_o18_to_mg26_plus_fe56.net'

      change_v_flag = .true.
      change_initial_v_flag = .true.
      new_v_flag = .true.

      load_saved_model = .true.
      load_model_filename = '{restore_opacity_load_model_filename}'

      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = '{restore_opacity_profile_filename}'

      save_model_when_terminate = .true.
      save_model_filename = '{restore_opacity_save_model_filename}'

      set_initial_dt = .true.
      seconds_for_initial_dt = 3d8

      relax_opacity_max = .true.
      new_opacity_max = 1d5
      opacity_max_multiplier = 1.04

/ ! end of star_job namelist

&eos
/ ! end of eos namelist

&kap
      use_Type2_opacities = .true.
      Zbase = {metallicity}
/ !end of kap namelist

&controls

      mesh_delta_coeff = 2
      tol_correction_norm = 1d0
      tol_max_correction = 1d0
      varcontrol_target = 1d0
      max_years_for_timestep = 0.1
      min_timestep_limit = 1d-3
      opacity_max = 0.1
      power_nuc_burn_lower_limit = 1d-1
      dxdt_nuc_factor = 25

      log_directory = '{LOGS_restore_opacity}'
      photo_directory = '{photos_restore_opacity}'

      photo_interval = 50
      profile_interval = 10

! Mass loss
      x_integer_ctrl(1) = 4 ! part number
      x_ctrl(1) = 0.1d0 !start_of_He_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(2) = 0.1d0 !start_of_H_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(3) = 2d-2 !He_shell_mass_limit as compared to core mass - for stopping condition based on He shell mass
      x_ctrl(7) = 5d-3 ! H_shell_mass_limit as compared to core mass - for stopping condition based on H shell mass
      x_ctrl(8) = 2d3 ! mass loss eta parameter
      ! mass_change = -1d-2
      ! use_other_adjust_mdot = .true.


! Mass dependant Timestep Conditions
      ! Below true for M>2Msun
      ! convergence_ignore_equL_residuals = .false.
      ! use_gold_tolerances = .true.
      ! use_gold2_tolerances = .true.
      ! varcontrol_target = 1d-3

      ! Below true for M<2Msun
      convergence_ignore_equL_residuals = .true.
      use_gold_tolerances = .false.
      use_gold2_tolerances = .false.


! Output
      terminal_interval = 10
      write_header_frequency = 10
      history_interval = 1
      photo_digits = 6

! Mixing Parameters
      mixing_length_alpha = {mixing_length_alpha}

! Atmosphere
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! Energy
      energy_eqn_option = 'dedt'
      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

! Diffusion and Mixing Parameters
      okay_to_reduce_gradT_excess = .true.
      use_ledoux_criterion = .false. !.true.
      alpha_semiconvection = -1 !1d-1
      semiconvection_option = 'Langer_85'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02
      min_overshoot_q = 1d-4
      overshoot_Delta0(1) = 1.0
      ! we use step overshooting
      overshoot_scheme(1) = 'step'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'top'
      overshoot_f(1) = 0.33
      overshoot_f0(1) = 0.05

      do_element_diffusion = .false.
      ! show_diffusion_info = .true.
      ! diffusion_v_max = 1d-5
      diffusion_use_full_net = .true.
      diffusion_use_cgs_solver = .true.
      diffusion_use_isolve = .true.
      diffusion_rtol_for_isolve = 1d-4
      diffusion_atol_for_isolve = 1d-5
      diffusion_min_X_hard_limit = -1d-4
      diffusion_maxsteps_for_isolve = 1000
      diffusion_isolve_solver = 'ros2_solver'

/ ! end of controls namelist

      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_restore_opacity"), "w") as f:
        f.write(inlist)

    return


def write_inlist_strip_envelope(dic):

    inlist = """
&star_job

      show_log_description_at_start = .false.

      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

      change_initial_net = .true.
      new_net_name = 'o18_and_ne22.net'
      ! new_net_name = 'no_h_burning_net.net'
      ! new_net_name = 'cno_extras_o18_to_mg26_plus_fe56.net'

      change_v_flag = .true.
      change_initial_v_flag = .true.
      new_v_flag = .false.

      load_saved_model = .true.
      load_model_filename = '{strip_envelope_load_model_filename}'

      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = '{strip_envelope_profile_filename}'

      save_model_when_terminate = .true.
      save_model_filename = '{strip_envelope_save_model_filename}'

      set_initial_dt = .true.
      seconds_for_initial_dt = 3d8

      relax_dxdt_nuc_factor = .true.
      new_dxdt_nuc_factor = 25
      dxdt_nuc_factor_multiplier = 1.1


/ ! end of star_job namelist

&eos
/ ! end of eos namelist

&kap
      use_Type2_opacities = .true.
      Zbase = {metallicity}
/ !end of kap namelist

&controls

      mesh_delta_coeff = 2
      tol_correction_norm = 1d0
      tol_max_correction = 1d0
      varcontrol_target = 1d0
      max_years_for_timestep = 0.1
      min_timestep_limit = 1d-3
      delta_lg_star_mass_limit = 5d-3
      use_other_adjust_mdot = .true.


      log_directory = '{LOGS_strip_envelope}'
      photo_directory = '{photos_strip_envelope}'

    ! report_solver_progress = .true.
    ! okay_to_remesh = .true.

      photo_interval = 50
      profile_interval = 10

! Mass loss
      x_integer_ctrl(1) = 2 ! part number
      x_ctrl(1) = 0.1d0 !start_of_He_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(2) = 0.1d0 !start_of_H_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(3) = 2d-2 !He_shell_mass_limit as compared to core mass - for stopping condition based on He shell mass
      x_ctrl(7) = 5d-3 ! H_shell_mass_limit as compared to core mass - for stopping condition based on H shell mass
      x_ctrl(8) = 2d3 ! mass loss eta parameter
      x_ctrl(9) = -1d-2 ! mass change in Msun/yr


! Mass dependant Timestep Conditions
      ! Below true for M>2Msun
      ! convergence_ignore_equL_residuals = .false.
      ! use_gold_tolerances = .true.
      ! use_gold2_tolerances = .true.
      ! varcontrol_target = 1d-3

      ! Below true for M<2Msun
      convergence_ignore_equL_residuals = .true.
      use_gold_tolerances = .false.
      use_gold2_tolerances = .false.


! Output
      terminal_interval = 10
      write_header_frequency = 10
      history_interval = 1
      photo_digits = 6

! Mixing Parameters
      mixing_length_alpha = {mixing_length_alpha}

! Atmosphere
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! Energy
      energy_eqn_option = 'dedt'
      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

! Diffusion and Mixing Parameters
      okay_to_reduce_gradT_excess = .true.
      use_ledoux_criterion = .false. !.true.
      alpha_semiconvection = -1 !1d-1
      semiconvection_option = 'Langer_85'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02
      min_overshoot_q = 1d-4
      overshoot_Delta0(1) = 1.0
      ! we use step overshooting
      overshoot_scheme(1) = 'step'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'top'
      overshoot_f(1) = 0.33
      overshoot_f0(1) = 0.05

      do_element_diffusion = .false.
      ! show_diffusion_info = .true.
      ! diffusion_v_max = 1d-5
      diffusion_use_full_net = .true.
      diffusion_use_cgs_solver = .true.
      diffusion_use_isolve = .true.
      diffusion_rtol_for_isolve = 1d-4
      diffusion_atol_for_isolve = 1d-5
      diffusion_min_X_hard_limit = -1d-4
      diffusion_maxsteps_for_isolve = 1000
      diffusion_isolve_solver = 'ros2_solver'

/ ! end of controls namelist
      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_strip_envelope"), "w") as f:
        f.write(inlist)

    return


def write_inlist_to_AGB(dic):

    inlist = """

&star_job


      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

      load_saved_model = .true.
      load_model_filename = '{to_AGB_load_model_filename}'

      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = '{to_AGB_profile_filename}'

      save_model_when_terminate = .true.
      save_model_filename = '{to_AGB_save_model_filename}'
      ! required_termination_code_string = 'xa_central_lower_limit'

      change_D_omega_flag = .true.
      new_D_omega_flag = .true.

/ ! end of star_job namelist

&eos
      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'
/ ! end of eos namelist

&kap
      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'
/ ! end of kap namelist


&controls

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{LOGS_to_AGB}'
      photo_directory = '{photos_to_AGB}'

      convergence_ignore_equL_residuals = .true.

      photo_interval = 100
      profile_interval = 10


! When to stop
      ! Used for the stopping condition check for a He shell mass relative to the core mass
      x_integer_ctrl(1) = 1 !for He_shell routine, 0 is off, 1 is on
      x_ctrl(1) = 0.1d0 !start_of_He_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(2) = 0.1d0 !start_of_H_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(3) = 2d-2 !He_shell_mass_limit as compared to core mass - for stopping condition based on He shell mass
      ! x_integer_ctrl(1) = 0
      ! xa_central_lower_limit_species(1) = 'he4'
      ! xa_central_lower_limit(1) = 1d-4

! Mixing Parameters
      do_element_diffusion = .false.
      use_ledoux_criterion = .true.
      alpha_semiconvection = 1d-1
      semiconvection_option = 'Langer_85'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02
      min_overshoot_q = 1d-4
      overshoot_Delta0(1) = 1.0
      ! we use step overshooting
      overshoot_scheme(1) = 'step'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'top'
      overshoot_f(1) = 0.33
      overshoot_f0(1) = 0.05

! Mass Loss
      ! cool_wind_full_on_T = 9.99d9
      ! hot_wind_full_on_T = 1d10
      ! cool_wind_GB_scheme = 'Reimers'
      ! cool_wind_AGB_scheme = 'Blocker'
      ! GB_to_AGB_wind_switch = 1d-4
      ! Reimers_scaling_factor = 0.5d0
      ! Blocker_scaling_factor = 0.0003d0

/ ! end of controls namelist
      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_to_AGB"), "w") as f:
        f.write(inlist)

    return


def write_inlist_WD_relax(dic):

    inlist = """
&star_job

      show_log_description_at_start = .false.

      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

      change_initial_net = .true.
      new_net_name = 'o18_and_ne22.net'
      ! new_net_name = 'cno_extras_o18_to_mg26_plus_fe56.net'

      change_v_flag = .true.
      change_initial_v_flag = .true.
      new_v_flag = .true.

      load_saved_model = .true.
      load_model_filename = '{WD_relax_load_model_filename}'

      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = '{WD_relax_profile_filename}'

      save_model_when_terminate = .true.
      save_model_filename = '{WD_relax_save_model_filename}'

      set_initial_age = .true.
      set_initial_model_number = .true.
      initial_age = 0
      initial_model_number = 0

      set_initial_dt = .true.
      seconds_for_initial_dt = 3d8


/ ! end of star_job namelist

&eos
/ ! end of eos namelist

&kap
      use_Type2_opacities = .true.
      Zbase = {metallicity}
/ !end of kap namelist

&controls

      mesh_delta_coeff = 2
      tol_correction_norm = 1d0
      tol_max_correction = 1d0
      varcontrol_target = 1d-3
      log_L_lower_limit = -3
      dxdt_nuc_factor = 1d0
      eps_nuc_factor = 1d0


      log_directory = '{LOGS_WD_relax}'
      photo_directory = '{photos_WD_relax}'

      photo_interval = 50
      profile_interval = 10

! Mass loss
      x_integer_ctrl(1) = 5 ! part number
      x_ctrl(1) = 0.1d0 !start_of_He_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(2) = 0.1d0 !start_of_H_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(3) = 2d-2 !He_shell_mass_limit as compared to core mass - for stopping condition based on He shell mass
      x_ctrl(7) = 5d-3 ! H_shell_mass_limit as compared to core mass - for stopping condition based on H shell mass
      x_ctrl(8) = 2d3 ! mass loss eta parameter
      ! mass_change = -1d-2
      ! use_other_adjust_mdot = .true.


! Mass dependant Timestep Conditions
      ! Below true for M>2Msun
      ! convergence_ignore_equL_residuals = .false.
      ! use_gold_tolerances = .true.
      ! use_gold2_tolerances = .true.
      ! varcontrol_target = 1d-3

      ! Below true for M<2Msun
      convergence_ignore_equL_residuals = .true.
      use_gold_tolerances = .false.
      use_gold2_tolerances = .false.

! ! Timestep Conditions
!       dX_nuc_drop_limit = 3d-2
!       delta_lgRho_cntr_limit = 0.05
!       delta_lgRho_cntr_hard_limit = 0.1
!       delta_lgT_cntr_limit = 0.01
!       delta_lgT_cntr_hard_limit = 0.02
!       delta_lgTeff_limit = 0.01
!       delta_lgTeff_hard_limit = 0.02
!       delta_lgL_limit = 0.1
!       delta_lgL_hard_limit = 0.2

! Output
      terminal_interval = 10
      write_header_frequency = 10
      history_interval = 1
      photo_digits = 6

! Mixing Parameters
      mixing_length_alpha = {mixing_length_alpha}

! Atmosphere
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! Energy
      energy_eqn_option = 'dedt'
      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

! Diffusion and Mixing Parameters
      okay_to_reduce_gradT_excess = .true.
      use_ledoux_criterion = .false. !.true.
      alpha_semiconvection = -1 !1d-1
      semiconvection_option = 'Langer_85'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02
      min_overshoot_q = 1d-4
      overshoot_Delta0(1) = 1.0
      ! we use step overshooting
      overshoot_scheme(1) = 'step'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'top'
      overshoot_f(1) = 0.33
      overshoot_f0(1) = 0.05

      do_element_diffusion = .true.
      ! show_diffusion_info = .true.
      ! diffusion_v_max = 1d-5
      diffusion_use_full_net = .true.
      diffusion_use_cgs_solver = .true.
      diffusion_use_isolve = .true.
      diffusion_rtol_for_isolve = 1d-4
      diffusion_atol_for_isolve = 1d-5
      diffusion_min_X_hard_limit = -1d-4
      diffusion_maxsteps_for_isolve = 1000
      diffusion_isolve_solver = 'ros2_solver'

/ ! end of controls namelist
      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_WD_relax"), "w") as f:
        f.write(inlist)

    return
