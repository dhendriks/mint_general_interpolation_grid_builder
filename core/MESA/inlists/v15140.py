#!/usr/bin/python3
"""
contains inlists for MESA 15140
"""
import os


def write_inlist_common(dic):
    # writes inlist_common with options given in dic

    inlist = """
&star_job

      ! opacities
      initial_zfracs = 6

      ! caches for use on eureka
      eosDT_cache_dir = '{caches_dir}/eosDT_cache'
      eosPT_cache_dir = '{caches_dir}/eosPT_cache'
      eosDE_cache_dir = '{caches_dir}/eosDE_cache'
      ionization_cache_dir = '{caches_dir}/ionization_cache'
      kap_cache_dir = '{caches_dir}/kap_cache'
      rates_cache_dir = '{caches_dir}/rates_cache'

/ !end of star_job namelist

&eos

! stop blending of OPAL and HELM tables which causes problems in Z=0.04 models
      Z_all_OPAL = 0.045d0
      Z_all_HELM = 0.045d0

      {eos_extra_controls}

/ ! end of eos namelist

&kap
      Zbase = {metallicity}

      ! show_info = .true.
      use_Type2_opacities = .true.
      kap_file_prefix = 'a09'
      kap_CO_prefix = 'a09_co'

/ ! end of kap namelist


&controls

! starting specifications
      initial_mass = {start_mass}
      initial_y = {initial_y}
      initial_z = {metallicity}

! MLT
      mixing_length_alpha = {mixing_length_alpha}
      MLT_option = 'Henyey'

      max_allowed_nz = 30000

! core mass definition
      he_core_boundary_h1_fraction = {he_core_boundary_h1_fraction}

! stop if degeneracy exceeds some value
      eta_center_limit = {eta_center_limit}

! for convergence
      convergence_ignore_equL_residuals = .true.

! atmosphere option
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! option to save photos at grid points
      x_logical_ctrl(3) = {save_photos_at_grid_points}.

! max number of profiles
      max_num_profile_models = 1000

!DEBUGING

      !report_why_dt_limits = .true.

      !report_solver_progress = .true. ! set true to see info about solver iterations
      !report_ierr = .true. ! if true, produce terminal output when have some internal error
      !stop_for_bad_nums = .true.

      !solver_save_photo_call_number = 104970
         ! Saves a photo when solver_call_number = solver_save_photo_call_number - 1
         ! e.g., useful for testing partials to set solver_call_number = solver_test_partials_call_number - 1

      !solver_test_partials_call_number = 4142
      !solver_test_partials_k = 2616
      !solver_test_partials_iter_number = 4
      !solver_test_partials_dx_0 = 1d-6
      !solver_test_partials_var_name = 'o16' ! 'all' or 'lnd', 'lnT', 'lnR', 'L', 'v', etc.    '' means code sets
      !solver_test_partials_equ_name = 'lnP' ! 'all' or 'dlnE_dt', 'dlnd_dt', 'dlnR_dt', 'equL', etc   '' means code sets
      !solver_test_partials_sink_name = 'si28' ! iso name to use for "sink" to keep sum = 1
      !solver_test_partials_show_dx_var_name = 'h1'


/ ! end of controls namelist

	""".format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_common"), "w") as f:
        f.write(inlist)

    return


def write_inlist_MS(dic):
    # writes inlist_MS with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with a pre-main sequence model
      !pre_ms_relax_to_start_radiative_core = .false.
      create_pre_main_sequence_model = .true.
      pre_ms_T_c = 9d5
      change_mass_years_for_dt = 1d-2

! set initial timestep
      set_initial_dt = .true.
      years_for_initial_dt = 1d-1

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{MS_save_model_filename}'
      required_termination_code_string = 'xa_central_lower_limit'

!history and profile columns listed in standard case
      history_columns_file = '{MS_hist_columns}'
      profile_columns_file = '{MS_prof_columns}'

! start counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

! network
      auto_extend_net = .true.
      h_he_net = 'basic.net'
      co_net = 'co_burn.net'
      adv_net = 'approx21.net'

! display on-screen plots
      pgstar_flag = .false.

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

      kap_lowT_prefix = '{kap_lowT_prefix}'
      AESOPUS_filename = '{aesopus_file_path}' !'AESOPUS_AGSS09.h5'


/ ! end of kap namelist

&controls

      x_integer_ctrl(1) = 1 ! part number

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{MS_logs_direc}'
      photo_directory = '{MS_photos_direc}'


      x_logical_ctrl(1) = {save_MS_model_at_core_formation}

! terminate if too slow
      max_model_number = 6000

      num_trace_history_values = 3
      trace_history_value_name(1) = 'log_L_div_Ledd'
      trace_history_value_name(2) = 'mass_conv_core'
      trace_history_value_name(3) = 'center_h1'

! stop at some hydrogen mass fraction
      xa_central_lower_limit_species(1) = 'h1'
      xa_central_lower_limit(1) = {MS_terminate_h1_mass_fraction}

! options for energy conservation (see MESA V, Section 3)
      use_dedt_form_of_energy_eqn = .true.
      limit_for_rel_error_in_energy_conservation = 1d99
      hard_limit_for_rel_error_in_energy_conservation = 1d99

! helps very low mass models at the TAMS
      convergence_ignore_equL_residuals = .true.

! timestep controls
      delta_XH_cntr_limit = {delta_XH_cntr_limit}
      delta_lg_XH_cntr_limit = {delta_lg_XH_cntr_limit}

! spatial resolution
      mesh_delta_coeff = {mesh_delta_coeff}

!set mass loss to essentially zero (apparently helps convergence)
      mass_change = {MS_mass_change}

! core overshooting
      overshoot_scheme(1) = '{MS_core_overshoot_scheme}'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'any'
      overshoot_f(1) = {MS_core_overshoot_f}
      overshoot_f0(1) = {MS_core_overshoot_f0}
      overshoot_D0(1) =  0d0
      overshoot_Delta0(1) = 1d0

! envelope overshooting
      overshoot_scheme(2) = '{MS_envelope_overshoot_scheme}'
      overshoot_zone_type(2) = 'nonburn'
      overshoot_zone_loc(2) = 'shell'
      overshoot_bdy_loc(2) = 'any'
      overshoot_f(2) = {MS_envelope_overshoot_f}
      overshoot_f0(2) = {MS_envelope_overshoot_f0}
      overshoot_D0(2) =  0d0
      overshoot_Delta0(2) = 1d0

! convective premixing
      do_conv_premix = {MS_do_conv_premix}
      conv_premix_avoid_increase = .true.
      conv_premix_time_factor = 0
      conv_premix_fix_pgas = .true.
      conv_premix_dump_snapshots = .false.

! semiconvection
      use_Ledoux_criterion = {MS_use_Ledoux_criterion}
      alpha_semiconvection = {MS_alpha_semiconvection}
      semiconvection_option = 'Langer_85 mixing; gradT = gradr'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02

! for massive models hiting Ledd
      use_superad_reduction = {use_superad_reduction}
      superad_reduction_Gamma_limit = {superad_reduction_Gamma_limit}
      superad_reduction_Gamma_limit_scale = {superad_reduction_Gamma_limit_scale}
      superad_reduction_Gamma_inv_scale = {superad_reduction_Gamma_inv_scale}
      superad_reduction_diff_grads_limit = {superad_reduction_diff_grads_limit}
      superad_reduction_limit = {superad_reduction_limit}

! create pre main sequence models
      relax_max_number_retries = 10000

{MS_extra_inlist_controls}

! output
      photo_interval = 500
      profile_interval = 10000
      history_interval = 10
      terminal_interval = 10
      write_header_frequency = 10


! Jermyn et al 2022: section 7.1.3

    timestep_factor_for_retries = 0.8
    min_timestep_factor = 0.9
    max_timestep_factor = 1.05d0
    retry_hold = 5
    redo_limit = -1
    relax_hard_limits_after_retry = .false.

    scale_max_correction = 0.01d0
    !scale_max_correction_for_negative_surf_lum = .true. !broken
    corr_coeff_limit = 1d-1
    ignore_min_corr_coeff_for_scale_max_correction = .true.
    ignore_species_in_max_correction = .true.
    max_resid_jump_limit = 1d99
    restore_mesh_on_retry = .true.
    num_steps_to_hold_mesh_after_retry = 5

    !extra spatial resolution
    !mesh_delta_coeff = 1d0
    mesh_delta_coeff_for_highT = 1.0d0
    mesh_Pgas_div_P_exponent = 0.5d0
    max_dq = 0.005
    min_dq_for_xa = 1d-5
    max_allowed_nz = 50000

! Fixing the position of the Lagrangian region of the mesh helps
! convergence near the Eddington limit
    max_logT_for_k_below_const_q = 100
    max_q_for_k_below_const_q = 0.995
    min_q_for_k_below_const_q = 0.995
    max_logT_for_k_const_mass = 100
    max_q_for_k_const_mass = 0.99
    min_q_for_k_const_mass = 0.99


/ ! end of controls namelist

&pgstar

/ ! end of pgstar namelist
	""".format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_MS"), "w") as f:
        f.write(inlist)

    return


def write_inlist_GB(dic):
    # writes inlist_GB with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with saved model
      load_saved_model = .true.
      saved_model_name = '{GB_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{GB_save_model_filename}'
      required_termination_code_string = 'HB_limit'

!history and profile columns listed in standard case
      history_columns_file = '{GB_hist_columns}'
      profile_columns_file = '{GB_prof_columns}'

! start counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

! set initial timestep
      set_initial_dt = .true.
      years_for_initial_dt = 1d1

! network
      auto_extend_net = .true.
      h_he_net = 'basic.net'
      co_net = 'co_burn.net'
      adv_net = 'approx21.net'

! turn on hydro
! helps degenerate models get through the He flash
      change_initial_v_flag = .true.
      change_v_flag = .true.
      new_v_flag = {GB_hydro_on}

! display on-screen plots
      pgstar_flag = .false.

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

      kap_lowT_prefix = '{kap_lowT_prefix}'
      AESOPUS_filename = '{aesopus_filename}' !'AESOPUS_AGSS09.h5'


/ ! end of kap namelist

&controls

      x_integer_ctrl(1) = 2 ! part number

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{GB_logs_direc}'
      photo_directory = '{GB_photos_direc}'

      num_trace_history_values = 6
      trace_history_value_name(1) = 'log_L_div_Ledd'
      trace_history_value_name(2) = 'min_beta'
      trace_history_value_name(3) = 'alpha_mlt_surface'
      trace_history_value_name(4) = 'alpha_mlt_max'
      trace_history_value_name(5) = 'mass_conv_core'
      trace_history_value_name(6) = 'hydrogen_exhausted_core_mass'


! stop if power of helium burning greater than some value
      !power_he_burn_upper_limit = {GB_terminate_helium_burning_limit}

! HB_limit
! ~~~~~~~~

! For detecting horizontal branch.
! Only applies when center abundance by mass of h1 is < 1d-4.
! Stop when the center abundance by mass of he4 drops below this limit.

      !HB_limit = {HB_limit}

! help convergence of stripped stars
      !use_other_alpha_mlt = .true.

! stop if can not converge
      min_timestep_limit = 1d0
      max_model_number = {GB_max_model_number}

! options for energy conservation (see MESA V, Section 3)
      use_dedt_form_of_energy_eqn = .true.
      limit_for_rel_error_in_energy_conservation = 1d99
      hard_limit_for_rel_error_in_energy_conservation = 1d99

! increase allowed error for degenerate ignition
      max_abs_rel_run_E_err = 0.1d0

! overshooting
      overshoot_scheme(1) = '{GB_envelope_overshoot_scheme}'
      overshoot_zone_type(1) = 'nonburn'
      overshoot_zone_loc(1) = 'shell'
      overshoot_bdy_loc(1) = 'any'
      overshoot_f(1) = {GB_envelope_overshoot_f}
      overshoot_f0(1) = {GB_envelope_overshoot_f0}
      overshoot_D0(1) =  0d0
      overshoot_Delta0(1) = 1d0

! convective premixing
      do_conv_premix = {GB_do_conv_premix}
      conv_premix_avoid_increase = .true.
      conv_premix_time_factor = 0
      conv_premix_fix_pgas = .true.
      conv_premix_dump_snapshots = .false.

! semiconvection
      use_Ledoux_criterion = {GB_use_Ledoux_criterion}
      alpha_semiconvection = {GB_alpha_semiconvection}
      semiconvection_option = 'Langer_85 mixing; gradT = gradr'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02

! for massive models hiting Ledd
      !okay_to_reduce_gradT_excess = .true.
      use_superad_reduction = {use_superad_reduction}
      superad_reduction_Gamma_limit = {superad_reduction_Gamma_limit}
      superad_reduction_Gamma_limit_scale = {superad_reduction_Gamma_limit_scale}
      superad_reduction_Gamma_inv_scale = {superad_reduction_Gamma_inv_scale}
      superad_reduction_diff_grads_limit = {superad_reduction_diff_grads_limit}
      superad_reduction_limit = {superad_reduction_limit}

! timestep controls
      !delta_HR_limit = 0.005

{GB_extra_inlist_controls}

! output
      photo_interval = 1000
      profile_interval = 0
      history_interval = 50
      terminal_interval = 10
      write_header_frequency = 100

! Jermyn et al 2022: section 7.1.3

    timestep_factor_for_retries = 0.8
    min_timestep_factor = 0.9
    max_timestep_factor = 1.05d0
    retry_hold = 5
    redo_limit = -1
    relax_hard_limits_after_retry = .false.

    scale_max_correction = 0.01d0
    !scale_max_correction_for_negative_surf_lum = .true. !broken
    corr_coeff_limit = 1d-1
    ignore_min_corr_coeff_for_scale_max_correction = .true.
    ignore_species_in_max_correction = .true.
    max_resid_jump_limit = 1d99
    restore_mesh_on_retry = .true.
    num_steps_to_hold_mesh_after_retry = 5

    !extra spatial resolution
    !mesh_delta_coeff = 1d0
    mesh_delta_coeff_for_highT = 1.0d0
    mesh_Pgas_div_P_exponent = 0.5d0
    max_dq = 0.005
    min_dq_for_xa = 1d-5
    max_allowed_nz = 50000

! Fixing the position of the Lagrangian region of the mesh helps
! convergence near the Eddington limit
    max_logT_for_k_below_const_q = 100
    max_q_for_k_below_const_q = 0.995
    min_q_for_k_below_const_q = 0.995
    max_logT_for_k_const_mass = 100
    max_q_for_k_const_mass = 0.99
    min_q_for_k_const_mass = 0.99

/ ! end of controls namelist

&pgstar

/ ! end of pgstar namelist
      """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_GB"), "w") as f:
        f.write(inlist)

    return


def write_inlist_CHeB(dic):
    # writes inlist_CHeB with options given in dic

    # if dic["mass"] < 2.2:
    #     degen = True
    # else:
    #     degen = False

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with saved pre-main sequence model
      load_saved_model = .true.
      saved_model_name = '{CHeB_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{CHeB_save_model_filename}'
      required_termination_code_string = 'xa_central_lower_limit'

!history and profile columns listed in standard case
      history_columns_file = '{CHeB_hist_columns}'
      profile_columns_file = '{CHeB_prof_columns}'

! start counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

! set initial timestep to make sure not too large now composition changes are turned on again
      !set_initial_dt = .true.
      !years_for_initial_dt = 1d3

! network
      auto_extend_net = .true.
      h_he_net = 'basic.net'
      co_net = 'co_burn.net'
      adv_net = 'approx21.net'

! display on-screen plots
      pgstar_flag = .false.

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

      kap_lowT_prefix = '{kap_lowT_prefix}'
      AESOPUS_filename = '{aesopus_filename}' !'AESOPUS_AGSS09.h5'


/ ! end of kap namelist

&controls

      x_integer_ctrl(1) = 3 ! evol_phase

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{CHeB_logs_direc}'
      photo_directory = '{CHeB_photos_direc}'

! stop at some helium mass fraction
      xa_central_lower_limit_species(1) = 'he4'
      xa_central_lower_limit(1) = {CHeB_terminate_he4_mass_fraction}

! stop if bad convergence
      max_model_number = 5000
      min_timestep_limit = 1d0

! terminal output
      num_trace_history_values = 6
      trace_history_value_name(1) = 'log_L_div_Ledd'
      trace_history_value_name(2) = 'min_beta'
      trace_history_value_name(3) = 'alpha_mlt_surface'
      trace_history_value_name(4) = 'alpha_mlt_max'
      trace_history_value_name(5) = 'mass_conv_core'
      trace_history_value_name(6) = 'hydrogen_exhausted_core_mass'

!save profile at start
      !profile_model = 1

      delta_XHe_cntr_limit = {delta_XHe_cntr_limit}
      delta_lg_XHe_cntr_limit = {delta_lg_XHe_cntr_limit}

! help convergence of stripped stars
      !use_other_alpha_mlt = .true.

! need Ledoux for massive stars
      use_Ledoux_criterion = .true.

! predictive mixing to aviod core breathing pulses from Paxton et al. 2018
      predictive_mix(1) = {CHeB_do_predictive_mix}
      predictive_zone_type(1) = 'any'
      predictive_zone_loc(1) = 'core'
      predictive_bdy_loc(1) = 'any'
      predictive_superad_thresh(1) = 0.01
      predictive_avoid_reversal = 'he4'

! envelope overshooting
      overshoot_scheme(2) = '{CHeB_envelope_overshoot_scheme}'
      overshoot_zone_type(2) = 'nonburn'
      overshoot_zone_loc(2) = 'shell'
      overshoot_bdy_loc(2) = 'any'
      overshoot_f(2) = {CHeB_envelope_overshoot_f}
      overshoot_f0(2) = {CHeB_envelope_overshoot_f0}
      overshoot_D0(2) =  0d0
      overshoot_Delta0(2) = 1d0

! for convergence
      convergence_ignore_equL_residuals = .true.

! for massive models hiting Ledd
      use_superad_reduction = {use_superad_reduction}
      superad_reduction_Gamma_limit = {superad_reduction_Gamma_limit}
      superad_reduction_Gamma_limit_scale = {superad_reduction_Gamma_limit_scale}
      superad_reduction_Gamma_inv_scale = {superad_reduction_Gamma_inv_scale}
      superad_reduction_diff_grads_limit = {superad_reduction_diff_grads_limit}
      superad_reduction_limit = {superad_reduction_limit}

{CHeB_extra_inlist_controls}

! output
      photo_interval = 500
      profile_interval = 0 ! custom saving routine in run_star_extras
      history_interval = 10 ! custom saving routine in run_star_extras
      terminal_interval = 1
      write_header_frequency = 10

/ ! end of controls namelist

&pgstar

/ ! end of pgstar namelist
	""".format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_CHeB"), "w") as f:
        f.write(inlist)

    return


def write_inlist_EAGB(dic):
    # writes inlist_EAGB with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! net
      !change_net = .true.
      !new_net_name = 'sagb_NeNa_MgAl.net'
      auto_extend_net = .true.
      h_he_net = 'basic.net'
      co_net = 'co_burn.net'
      adv_net = 'approx21.net'


! begin with saved model from end of core He burning
      load_saved_model = .true.
      saved_model_name = '{EAGB_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{EAGB_save_model_filename}'
      required_termination_code_string = 'TP_started'

!history and profile columns listed in standard case
      history_columns_file = '{EAGB_hist_columns}'
      profile_columns_file = '{EAGB_prof_columns}'

! restart counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'


/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

      kap_lowT_prefix = '{kap_lowT_prefix}'
      AESOPUS_filename = '{aesopus_filename}' !'AESOPUS_AGSS09.h5'


/ ! end of kap namelist

&controls

      x_integer_ctrl(1) = 4 ! part number

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{EAGB_logs_direc}'
      photo_directory = '{EAGB_photos_direc}'

!terminal output
      num_trace_history_values = 4
      trace_history_value_name(1) = 'min_beta'
      trace_history_value_name(2) = 'alpha_mlt_max'
      trace_history_value_name(3) = 'alpha_mlt_surface'
      trace_history_value_name(4) = 'center_he4'


! MLT alpha prescription to prevent Fe peak instability in run_star_extras
! alpha_mlt,base set in inlist_common
! only needed for stripped EAGB stars
      use_other_alpha_mlt = .true.

! terminate if exceed max_model_number
      max_model_number = 5000

! schwartzchild or ledoux
! keep ledoux criterion off because leads to tiny radiative region at he core boundary seperating convective envelope and convective region developed in intershell
! this issue prevents second dredge-up from happening when there is no overshooting and causes models to crash
      use_Ledoux_criterion = {EAGB_use_Ledoux_criterion}
      make_gradr_sticky_in_solver_iters = .true.

! semiconvection from Farmer et al 2015 (carbon burning)
      alpha_semiconvection = {EAGB_alpha_semiconvection}
      semiconvection_option = 'Langer_85'
      thermohaline_coeff = 1

! options for energy conservation (see MESA V, Section 3)
      use_dedt_form_of_energy_eqn = .true.

!time resolution- need to relax time resolution to get through carbon burning in SAGB models

!changed to 1d-3 for Mc>1.0 in run_star_extras to get through carbon burning
      varcontrol_target = 1d-4

!turned off in run_star_extras for Mc>1.0 to get through carbon burning
      delta_lgL_He_limit = {EAGB_delta_lgL_He_limit}
      lgL_He_burn_min = 2.0


! overshooting parameter doesn't affect 2DUP but will affect mixing in core during carbon burning
      overshoot_scheme(1) = '{EAGB_overshoot_scheme}'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'any'
      overshoot_bdy_loc(1) = 'bottom'
      overshoot_f(1) = {EAGB_overshoot_f}
      overshoot_f0(1) = {EAGB_overshoot_f0}

! vassilidadis & Wood 1993 wind in run_star_extras
      use_other_wind = {EAGB_use_vassilidadis_wood_wind}

!mesh controls from Farmer et al 2015 for carbon burning, don't use currently as stops getting through carbon burning

      !mesh_delta_coeff = 0.5

      !mesh_dlog_burn_c_dlogP_extra = 0.1d0
      !mesh_dlog_cc_dlogP_extra = 0.1d0
      !mesh_dlog_co_dlogP_extra = 0.1d0
      !mesh_dlog_oo_dlogP_extra = 0.1d0

      !mesh_logX_species(1) = 'c12'
      !mesh_logX_min_for_extra(1) = -2
      !mesh_dlogX_dlogP_extra(1) = 0.5
      !mesh_dlogX_dlogP_full_on(1) = 0
      !mesh_dlogX_dlogP_full_off(1) = -1

      !mesh_logX_species(2) = 'h1'
      !mesh_logX_min_for_extra(2) = -2
      !mesh_dlogX_dlogP_extra(2) = 0.5
      !mesh_dlogX_dlogP_full_on(2) = 0
      !mesh_dlogX_dlogP_full_off(2) = -1

      !mesh_logX_species(3) = 'he4'
      !mesh_logX_min_for_extra(3) = -2
      !mesh_dlogX_dlogP_extra(3) = 0.5
      !mesh_dlogX_dlogP_full_on(3) = 0
      !mesh_dlogX_dlogP_full_off(3) = -1

      !xa_mesh_delta_coeff(1) = 0.5
      !xa_mesh_delta_coeff(2) = 0.5
      !xa_mesh_delta_coeff(3) = 0.5


! Solver
      op_split_burn = .true.
      op_split_burn_min_T = 3d8

      op_split_burn_eps = 1d-7
      op_split_burn_odescal = 1d-8

{EAGB_extra_inlist_controls}

! output
      photo_interval = 100
      profile_interval = 0
      history_interval = 10
      terminal_interval = 10
      write_header_frequency = 10

/

&pgstar

      read_extra_pgstar_inlist1 = .true.
      extra_pgstar_inlist1_name = 'inlist_pgstar'

/ ! end of pgstar namelist
    """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_EAGB"), "w") as f:
        f.write(inlist)

    return


def write_inlist_TPAGB(dic):
    # writes inlist_TPAGB with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with saved model from end of core He burning
      load_saved_model = .true.
      saved_model_name = '{TPAGB_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{TPAGB_save_model_filename}'

!history and profile columns listed in standard case
      history_columns_file = '{TPAGB_hist_columns}'
      profile_columns_file = '{TPAGB_prof_columns}'

! restart counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      !set_initial_cumulative_energy_error = .true.
      !new_cumulative_energy_error = 0d0

! net
      change_net = .true.
      new_net_name = 'c13.net'

! turn on hydro
! hydro controllled in run_star_extras
      !change_initial_v_flag = .true.
      !change_v_flag = .true.
      !new_v_flag = .false.

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

      kap_lowT_prefix = '{kap_lowT_prefix}'
      AESOPUS_filename = '{aesopus_filename}' !'AESOPUS_AGSS09.h5'


/ ! end of kap namelist

&controls

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{TPAGB_logs_direc}'
      photo_directory = '{TPAGB_photos_direc}'

      x_integer_ctrl(1) = 5 ! evolutionary stage = TP-AGB

! parameters for resolve_TDU subroutine
      ! varcontrol target for resolving TDU (VCT from Rees et al. 2023)
      x_ctrl(1) = {VCT_target}
      ! mesh_dlogX_dlogP_extra for resolving TDU (MXP from Rees et al. 2023)
      x_ctrl(2) = {MXP_target}

! to prevent HRI
      use_other_eps_grav =  {avoid_HRI}

! MLT alpha prescription to prevent Fe peak instability in run_star_extras
! alpha_mlt,base set in inlist_common
      use_other_alpha_mlt = {use_alpha_mlt}

! to save hdf5 for post processing
      x_logical_ctrl(2) =  {save_hdf5_files}

!terminate when envelope mass drops below this in units Msun
      envelope_mass_limit = {envelope_mass_limit}

!terminal output
      num_trace_history_values = 5
      trace_history_value_name(1) = 'TP_count'
      trace_history_value_name(2) = 'alpha_mlt_max'
      trace_history_value_name(3) = 'alpha_mlt_surface'
      trace_history_value_name(4) = 'min_beta'
      trace_history_value_name(5) = 'lambda_DUP'

      ! min_timestep_limit = 300 !1d-6 ! in seconds


! eases equL convergence problems during He shell flash
      use_dPrad_dm_form_of_T_gradient_eqn = .true.

! prevent problems with converging equL during He shell flash
      convergence_ignore_equL_residuals = .true.

! options for energy conservation (see MESA V, Section 3)
! eps_grav form works better for AGB models with degenerate cores
      always_use_eps_grav_form_of_energy_eqn = .true.
      use_dedt_form_of_energy_eqn = .false.
      use_time_centered_eps_grav = .true.
      include_composition_in_eps_grav = .true.
      limit_for_rel_error_in_energy_conservation = 1d99
      hard_limit_for_rel_error_in_energy_conservation = 1d99

! schwartzchild or ledoux
! with no overshooting, ledoux criterion leads to more erratic peaks and troughs of thermal pulses which does affect subsequent evolution
! likely to be unimportant with overshooting as that dominates
      use_Ledoux_criterion = {TPAGB_use_Ledoux_criterion}

!spatial resolution
      mesh_delta_coeff = 1.0d0

! required to resolve TDU
      mesh_logX_species(1) = 'he4'
      mesh_logX_min_for_extra(1) = -2
      mesh_dlogX_dlogP_extra(1) = 1.0 !changed in run star extras to resolve TDU
      mesh_dlogX_dlogP_full_on(1) = 0
      mesh_dlogX_dlogP_full_off(1) = -1

! temporal resolution

      varcontrol_target = 1d-4 !changed in run star extras to resolve TDU
      min_allowed_varcontrol_target = 1d-5

!these controls ensure He-shell flash is resolved
      delta_lgL_He_limit = {TPAGB_delta_lgL_He_limit}
      lgL_He_burn_min = 2.0
      !relax timestep controls when He burning luminosity decreasing as small timesteps can cause convergence issues
      lgL_He_drop_factor = 2.0

! not sure if these actually have any effect on timestep as delta_lgL_He_limit dominates
      dHe_div_He_limit_min_He = 1d-4
      dHe_div_He_limit = 0.5d0

! change to limit timestep during interpulse period, we find defaults to be sufficient
      !dH_limit_min_H = 1d-2
      !dH_limit = 0.1
      !dH_div_H_limit_min_H = 1d-5
      !dH_div_H_limit = 0.5 !0.05 !0.5 !0.9

!overshooting

! IPCZ overshooting controls
      overshoot_scheme(1) = '{TPAGB_overshoot_scheme_IPCZ}'
      overshoot_zone_type(1) = 'burn_He'
      overshoot_zone_loc(1) = 'shell'
      overshoot_bdy_loc(1) = 'bottom'
      overshoot_f(1) = {TPAGB_overshoot_f_IPCZ}
      overshoot_f0(1) = {TPAGB_overshoot_f0_IPCZ} !0.002

! convective envelope overshooting controls,
! this is turned off in run_star_extras extras_startup for Tbce,tdu > 4e7K to stop unstable hot third dredge-up
      overshoot_scheme(2) = '{TPAGB_overshoot_scheme_CE}'
      overshoot_zone_type(2) = 'any'
      overshoot_zone_loc(2) = 'shell'
      overshoot_bdy_loc(2) = 'bottom'
      overshoot_f(2) = {TPAGB_overshoot_f_CE}
      overshoot_f0(2) = {TPAGB_overshoot_f0_CE}

!mass loss
! vassilidadis & Wood 1993 wind calculated in run_star_extras
      use_other_wind = {TPAGB_use_vassilidadis_wood_wind}

!artificial damping of shocks

    ! RSP_cq = 0d0 !4.0d0
    ! RSP_zsh = 0.1d0

    ! RSP_Qvisc_linear = 0d0
    ! RSP_Qvisc_quadratic = 0.05d0 !taken from Clayton 2018

{TPAGB_extra_inlist_controls}

! output
      photo_interval = 100
      profile_interval = 10000
      history_interval = 10
      terminal_interval = 10
      write_header_frequency = 10

/

&pgstar

      read_extra_pgstar_inlist1 = .true.
      extra_pgstar_inlist1_name = 'inlist_pgstar'

/ ! end of pgstar namelist
    """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_TPAGB"), "w") as f:
        f.write(inlist)

    return


def write_inlist_post_AGB(dic):
    # writes inlist_TPAGB with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with saved model from end of core He burning
      load_saved_model = .true.
      saved_model_name = '{postAGB_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{postAGB_save_model_filename}'

!history and profile columns listed in standard case
      history_columns_file = '{postAGB_hist_columns}'
      profile_columns_file = '{postAGB_prof_columns}'

! restart counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      !set_initial_cumulative_energy_error = .true.
      !new_cumulative_energy_error = 0d0

! net
      change_net = .true.
      new_net_name = 'basic.net'

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

      kap_lowT_prefix = '{kap_lowT_prefix}'
      AESOPUS_filename = '{aesopus_filename}' !'AESOPUS_AGSS09.h5'


/ ! end of kap namelist

&controls

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{postAGB_logs_direc}'
      photo_directory = '{postAGB_photos_direc}'

      x_integer_ctrl(1) = 6 ! evolutionary stage = postAGB

! to prevent HRI
      use_other_eps_grav =  {avoid_HRI}

! MLT alpha prescription to prevent Fe peak instability in run_star_extras
! alpha_mlt,base set in inlist_common
      use_other_alpha_mlt = {use_alpha_mlt}

!terminate when envelope mass drops below this in units Msun
      envelope_mass_limit = {envelope_mass_limit}

!terminal output
      num_trace_history_values = 3
      trace_history_value_name(1) = 'alpha_mlt_max'
      trace_history_value_name(2) = 'alpha_mlt_surface'
      trace_history_value_name(3) = 'min_beta'

! eases equL convergence problems during He shell flash
      use_dPrad_dm_form_of_T_gradient_eqn = .true.

! prevent problems with converging equL during He shell flash
      convergence_ignore_equL_residuals = .true.

! options for energy conservation (see MESA V, Section 3)
! eps_grav form works better for AGB models with degenerate cores
      always_use_eps_grav_form_of_energy_eqn = .true.
      use_dedt_form_of_energy_eqn = .false.
      use_time_centered_eps_grav = .true.
      include_composition_in_eps_grav = .true.
      limit_for_rel_error_in_energy_conservation = 1d99
      hard_limit_for_rel_error_in_energy_conservation = 1d99

! schwartzchild or ledoux
! with no overshooting, ledoux criterion leads to more erratic peaks and troughs of thermal pulses which does affect subsequent evolution
! likely to be unimportant with overshooting as that dominates
      use_Ledoux_criterion = {postAGB_use_Ledoux_criterion}

!spatial resolution
      mesh_delta_coeff = 1.0d0

! temporal resolution
      varcontrol_target = 1d-4 !changed in run star extras to resolve TDU
      min_allowed_varcontrol_target = 1d-5


! mass loss
! vassilidadis & Wood 1993 wind calculated in run_star_extras
      use_other_wind = {postAGB_use_vassilidadis_wood_wind}

{postAGB_extra_inlist_controls}

! output
      photo_interval = 100
      profile_interval = 10000
      history_interval = 10
      terminal_interval = 10
      write_header_frequency = 10

/

&pgstar


/ ! end of pgstar namelist
    """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_post_AGB"), "w") as f:
        f.write(inlist)

    return


def write_inlist_change_mass(dic):
    # writes inlist_change_mass with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with saved pre-main sequence model
      load_saved_model = .true.
      saved_model_name = '{change_mass_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{change_mass_save_model_filename}'
      required_termination_code_string = 'max_age'

! start counters
      set_initial_age = .true.
      initial_age = 0
      set_initial_model_number = .true.
      initial_model_number = 0
      !set_initial_cumulative_energy_error = .true.
      !new_cumulative_energy_error = 0d0

! set initial timestep
      !set_initial_dt = .true.
      !years_for_initial_dt = 1d1

! display on-screen plots
      pgstar_flag = .false.

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

/ ! end of kap namelist

&controls

! mass loss/ accretion rate, termination and timestep condition set in run_star_extras depending on final mass

      x_integer_ctrl(1) = 10 ! part number

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

! set target mass for use in run_star_extras
      x_ctrl(2) = final mass

      log_directory = '{change_mass_logs_direc}'
      photo_directory = '{change_mass_photos_direc}'

! extra terminal output
      num_trace_history_values = 3
      trace_history_value_name(1) = 'log_L_div_Ledd'
      trace_history_value_name(2) = 'mass_conv_core'
      trace_history_value_name(3) = 'center_h1'

! help convergence of stripped stars
      use_other_alpha_mlt = .true.

! mass accretion rate
      x_ctrl(1) = 1d-8

! star cut core mass multiplier
      x_ctrl(3) = 0.0

! turn off composition changes whilst mass is changing
      dxdt_nuc_factor = 0
      mix_factor = 0

! mass change
      accrete_same_as_surface = .true.

! stop if can not converge
      max_model_number = 5000

! allow timestep to increase more quickly
      max_timestep_factor = 2.0

!relax varcontrol_target
      !varcontrol_target = 1d-3

! to stop formation of small convective zones near the core boundary that cause DUP
! turned of in run_star_extras for low mass stars (<5Msun) and stripped stars?
      !prune_bad_cz_min_Hp_height = {prune_bad_cz_min_Hp_height}
      !prune_bad_cz_min_log_eps_nuc = 1d99
      !redo_conv_for_dr_lt_mixing_length = .true.

! for massive models hiting Ledd
      !okay_to_reduce_gradT_excess = .true.
      use_superad_reduction = {use_superad_reduction}
      superad_reduction_Gamma_limit = {superad_reduction_Gamma_limit}
      superad_reduction_Gamma_limit_scale = {superad_reduction_Gamma_limit_scale}
      superad_reduction_Gamma_inv_scale = {superad_reduction_Gamma_inv_scale}
      superad_reduction_diff_grads_limit = {superad_reduction_diff_grads_limit}
      superad_reduction_limit = {superad_reduction_limit}

! output
      photo_interval = 0
      profile_interval = 0
      history_interval = 10
      terminal_interval = 1
      write_header_frequency = 10

/ ! end of controls namelist

&pgstar

/ ! end of pgstar namelist
    """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_change_mass"), "w") as f:
        f.write(inlist)

    return


def write_inlist_relax(dic):
    # writes inlist_relax with options given in dic

    inlist = """
&star_job

      mesa_dir = ''

      read_extra_star_job_inlist1 = .true.
      extra_star_job_inlist1_name = 'inlist_common'

! begin with saved pre-main sequence model
      load_saved_model = .true.
      saved_model_name = '{relax_saved_model_name}'

! save a model at the end of the run
      save_model_when_terminate = .true.
      save_model_filename = '{relax_save_model_filename}'
      required_termination_code_string = 'max_age'

! start counters
      ! set_initial_age = .true.
      ! initial_age = 0
      ! set_initial_model_number = .true.
      ! initial_model_number = 0
      !set_initial_cumulative_energy_error = .true.
      !new_cumulative_energy_error = 0d0

! display on-screen plots
      pgstar_flag = .false.

/ !end of star_job namelist

&eos

      read_extra_eos_inlist1 = .true.
      extra_eos_inlist1_name = 'inlist_common'

/ ! end of eos namelist

&kap

      read_extra_kap_inlist1 = .true.
      extra_kap_inlist1_name = 'inlist_common'

/ ! end of kap namelist

&controls

      x_integer_ctrl(1) = 20 ! part number

      read_extra_controls_inlist1 = .true.
      extra_controls_inlist1_name = 'inlist_common'

      log_directory = '{relax_logs_direc}'
      photo_directory = '{relax_photos_direc}'

      max_years_for_timestep = 1d6

! turn off composition changes whilst relaxing
      dxdt_nuc_factor = 0

! output
      photo_interval = 0
      profile_interval = 0
      history_interval = 10
      terminal_interval = 10
      write_header_frequency = 10

/ ! end of controls namelist

&pgstar

/ ! end of pgstar namelist
    """.format(
        **dic
    )

    with open(os.path.join(dic["directory"], "inlist_relax"), "w") as f:
        f.write(inlist)

    return


def write_inlist_overshoot_dbl_exp(dic):
    # writes inlist_overshoot_dbl_exp with options given in dic

    inlist = """
&overshoot_dbl_exp

            !### overshoot_D2
            !### overshoot_f2

      !overshoot_scheme(2) = 'other' ! 'double_exponential'
      !overshoot_zone_type(2) = 'any' ! 'burn_Z'
      !overshoot_zone_loc(2) = 'any'
      !overshoot_bdy_loc(2) = 'any' ! 'bottom'
      !overshoot_f(2) = 0.014
      !overshoot_f0(2) = 0.014

! parameters from Battino et al 2016
      overshoot_f2(2) = 0.25
      overshoot_D2(2) = 1d11

/ ! end of overshoot_dbl_exp
            ````````````````````````````````````````````````````````````````````````````````````````````````````````
    """

    with open(os.path.join(dic["directory"], "inlist_overshoot_dbl_exp"), "w") as f:
        f.write(inlist)

    return
