! Hertzsprung gap routines
! AUTHORS: Natalie Rees, Giovanni Mirouh

subroutine calc_burning_shell_thickness(s,burning_shell_thickness)
    type (star_info), pointer :: s
    real(dp), intent(out) :: burning_shell_thickness
    real(dp) :: burning_shell_lower_mass, burning_shell_higher_mass
    real(dp) :: max_eps_nuc_shell, max_eps_nuc_shell_m

    call calc_burning_shell_boundaries(s,burning_shell_lower_mass,burning_shell_higher_mass,max_eps_nuc_shell, max_eps_nuc_shell_m)
    burning_shell_thickness = burning_shell_higher_mass- burning_shell_lower_mass

end subroutine calc_burning_shell_thickness

subroutine calc_burning_shell_boundaries(s,burning_shell_lower_mass,burning_shell_higher_mass,max_eps_nuc_shell, max_eps_nuc_shell_m)
    type (star_info), pointer :: s
    real(dp), intent(out) :: burning_shell_lower_mass, burning_shell_higher_mass
    real(dp), intent(out) :: max_eps_nuc_shell, max_eps_nuc_shell_m
    real(dp) :: cutoff_energy, slope
    integer :: i_count, max_eps_nuc_shell_i

    ! find location of maximum nuclear burning
    max_eps_nuc_shell = -100
    max_eps_nuc_shell_m = -20
    max_eps_nuc_shell_i = -20

    ! return 0 if he star with no burning shell
    if (s%he_core_k==1) then
        burning_shell_higher_mass = s% star_mass
        burning_shell_lower_mass = s% star_mass
        RETURN
    end if

    do i_count=1, s% he_core_k !if not in core
        if (s% eps_nuc(i_count) > max_eps_nuc_shell) then
            max_eps_nuc_shell   = s% eps_nuc(i_count)
            max_eps_nuc_shell_m = s% m(i_count)/Msun
            max_eps_nuc_shell_i = i_count
        endif
    enddo

    ! write(*,*) 'BST peak at m=', max_eps_nuc_shell_m,  &
    ! 'eps_nuc =',      max_eps_nuc_shell,    &
    ! 'he_core_mass=',  s% he_core_mass,      &
    ! 'model_number=',  s% model_number,      &
    ! 'brain left m[1]=', s%m(1)/Msun,        &
    ! 'zone of max=', max_eps_nuc_shell_i

    !Compute the energy at which the cutoff goes (1/e*max)
    cutoff_energy = 0.5*max_eps_nuc_shell !0.36787944117*max_eps_nuc_shell
    !write(*,*) s% max_eps_nuc, cutoff_energy, s%max_eps_nuc/cutoff_energy

    ! Locate where that energy is reached
    ! Start from peak at max_eps_nuc_shell_i and increase
    ! This will give burning-shell lower bound (as m is decreasing)
    do i_count = max_eps_nuc_shell_i, s%nz !nz is core

        if (((s%eps_nuc(i_count-1)-cutoff_energy)>0) .and. ((s%eps_nuc(i_count)-cutoff_energy)<0)) then
            !s%m is in cm, Msun is defined in const_def
            !Imposing the sign of that inequation forces the
            !derivative sign

            !vals(1) = s%m(i_count-1)/Msun !simple solution
            slope = (s%eps_nuc(i_count-1)-s%eps_nuc(i_count))/(s%m(i_count-1)-s%m(i_count))
            burning_shell_lower_mass = s%m(i_count) + (cutoff_energy-s%eps_nuc(i_count))/slope
            burning_shell_lower_mass = burning_shell_lower_mass/Msun

            ! write(*,*) 'Burning shell lower bound',             &
            ! s% m(i_count-1)/Msun, s% m(i_count)/Msun,           &
            ! s% eps_nuc(i_count-1), ' >',cutoff_energy,          &
            ! s% eps_nuc(i_count), ' <',cutoff_energy, slope,'>0'

            exit

        endif
    enddo


    !location of outer limit of burning shell
    do i_count =max_eps_nuc_shell_i, 2,-1    !1 is surface
        if (((s%eps_nuc(i_count-1)-cutoff_energy)<0) .and. ((s%eps_nuc(i_count)-cutoff_energy)>0)) then
            !Imposing the sign of that inequation forces the derivative sign

            ! write(*,*) 'Burning shell upper bound',             &
            ! 'Burning shell lower bound',             &
            ! s% m(i_count-1)/Msun, s% m(i_count)/Msun,           &
            ! s% eps_nuc(i_count-1), s% eps_nuc(i_count)

            slope = (s%eps_nuc(i_count-1)-s%eps_nuc(i_count))/(s%m(i_count-1)-s%m(i_count))
            burning_shell_higher_mass = s%m(i_count) + (cutoff_energy-s%eps_nuc(i_count))/slope
            burning_shell_higher_mass = burning_shell_higher_mass/Msun

            exit
        end if
    enddo

end subroutine calc_burning_shell_boundaries


subroutine data_for_extra_history_columns_hg(n,names,vals,s,j)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    type (star_info), pointer :: s
    integer, intent(out) :: j

    real(dp) :: burning_shell_lower_mass, burning_shell_higher_mass
    real(dp) :: max_eps_nuc_shell, max_eps_nuc_shell_m
    call calc_burning_shell_boundaries(s,burning_shell_lower_mass,burning_shell_higher_mass,max_eps_nuc_shell, max_eps_nuc_shell_m)

    names(j+1) = 'burning_shell_lower_mass'
    vals(j+1) = burning_shell_lower_mass

    names(j+2) = 'burning_shell_higher_mass'
    vals(j+2) = burning_shell_higher_mass

    names(j+3) = 'burning_shell_max_eps_nuc_m'
    vals(j+3) = max_eps_nuc_shell_m

    names(j+4) = 'burning_shell_max_eps_nuc'
    vals(j+4) = max_eps_nuc_shell

    names(j+5) = 'burning_shell_thickness'
    vals(j+5) = burning_shell_higher_mass-burning_shell_lower_mass

    j = j+5

end subroutine data_for_extra_history_columns_hg
