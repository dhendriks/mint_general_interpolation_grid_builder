!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!! These are the parameters that define the location/names/size of the HDF5 file !!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!! hdf5_prefix         -> basename for each HDF5 file
!!!!!!!                        default: M3.00000Z0.020 for a 3 Msun star with Z = 0.02
!!!!!!! hdf5_codev          -> some extra string attribute for each HDF5 file
!!!!!!!                        default: mesa rev `revision number`
!!!!!!! hdf5_modname        -> folder in which to write out the HDF5 files
!!!!!!! head_cycle_name     -> basename for each cycle
!!!!!!! dataset_cycle_name  -> name of the dataset containing the profiles
!!!!!!! dataset_cycle_name2 -> name of the dataset containing the abundances
!!!!!!! format_file         -> suffix format in the HDF5 name
!!!!!!! format_cycle        -> suffix format in the cycle name
!!!!!!! hdf5_num_mod_output -> number of cycles per HDF5 file
!!!!!!! change_names        -> whether you want to change the name of some quantities,
!!!!!!!			       for instance 'star_mass' into 'stellar_mass'.
!!!!!!!			       See subroutines change_history_names and change_profile_names
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ! If left as empty strings, these parameters will take default values
      ! determined at runtime (see header).
      character (len=256) :: hdf5_prefix  = ''
      character (len=256) :: hdf5_codev   = ''
      character(len=256), parameter :: hdf5_modname      = 'HDF5'
      character(len=256), parameter :: hdf5_profile_list = 'hdf5_profile_columns.list'

      character(len=*), parameter :: head_cycle_name     = 'cycle'
      character(len=*), parameter :: dataset_cycle_name  = 'SE_DATASET'
      character(len=*), parameter :: dataset_cycle_name2 = 'ISO_MASSF'

      integer, parameter :: hdf5_num_mod_output = 100

      logical, parameter :: change_names = .true.

! following originally in run_star_extras
      character(len=*), parameter :: format_file  = "(I7.7)"
      character(len=*), parameter :: format_cycle = "(I10)"

      integer :: num_history_columns, num_profile_columns
      character (len=maxlen_profile_column_name), pointer :: profile_names(:) ! num_profiles_columns
      double precision, pointer :: profile_vals(:,:) ! (nz,num_profile_columns)
      logical, pointer :: profile_is_int(:) ! true if the values in the profile column are integers

      ! These variables are just for the logs to check the profile names for the HDF5 file
      integer :: num_profile_columns_logs
      character (len=maxlen_profile_column_name), pointer :: profile_names_logs(:) ! num_profiles_columns_logs
      double precision, pointer :: profile_vals_logs(:,:) ! (nz,num_profile_columns)
      logical, pointer :: profile_is_int_logs(:) ! true if the values in the profile column are integers

      character(len=256) :: filename, prefix
      integer(HID_T)     :: file_id                   ! File identifier
      integer            :: error                     ! Error flag
      integer            :: firstmodel

      integer :: num_extra_history_columns_hdf5 = 2
      integer :: num_extra_profile_columns_hdf5 = 3
