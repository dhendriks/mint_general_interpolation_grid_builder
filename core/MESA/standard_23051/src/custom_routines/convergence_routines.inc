
subroutine converge_eagb(s)
   type (star_info), pointer :: s
   real(dp) :: min_beta

   ! relax timestep controls to get through carbon burning
   if (s%co_core_mass>1.0) then
       s% varcontrol_target = 1d-3
       s% delta_lgL_He_limit = 0.025
       s% lgL_He_burn_min = 2.5
   end if

   ! relax timestep controls for massive AGB stars
   if (s%co_core_mass>0.8) then
       s% dX_limit_species(1) = 'h1'
       s% dX_limit_min_X(1) = 1d99
       s% dX_limit(1) = 1d99
       s% dX_div_X_limit_min_X(1) = 1d99
       s% dX_div_X_limit(1) = 1d99
       s% dX_limit_species(2) = 'he4'
       s% dX_limit_min_X(2) = 1d99
       s% dX_limit(2) = 1d99
       s% dX_div_X_limit_min_X(2) = 1d99
       s% dX_div_X_limit(2) = 1d99
       s% varcontrol_target = 1d-3
   end if

   !relax varcontrol if getting post-AGB like structure due to envelope stripping
   call calc_min_beta(s,min_beta)
   if (min_beta <0.4) then
       s% varcontrol_target = 1d-3
   end if

end subroutine converge_eagb

subroutine alpha_mlt_stripped_stars(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr
    real(dp) :: A, B
    integer :: inner_index
    type (star_info), pointer :: s
    ierr = 0
    call star_ptr(id, s, ierr)
    if (ierr /= 0) return
    s% alpha_mlt(:) = s% mixing_length_alpha

    if (s%he_core_mass/s%star_mass>0.6) then
       ! write(*,*) 'using alpha mlt'
       inner_index = s%he_core_k
       A = 40
       B = 0.3
       call alpha_mlt_beta(id, ierr, A, B, inner_index)
    end if

 end subroutine alpha_mlt_stripped_stars

 subroutine alpha_mlt_stripped_eagb(id, ierr)
   integer, intent(in) :: id
   integer, intent(out) :: ierr
   real(dp) :: A, B
   integer :: inner_index
   type (star_info), pointer :: s
   ierr = 0
   call star_ptr(id, s, ierr)
   if (ierr /= 0) return
   s% alpha_mlt(:) = s% mixing_length_alpha

   if (s%he_core_mass/s%star_mass>-1) then
      ! write(*,*) 'using alpha mlt'
      inner_index = s%co_core_k
      A = 40
      B = 0.2
      call alpha_mlt_beta(id, ierr, A, B, inner_index)
   end if

end subroutine alpha_mlt_stripped_eagb

 subroutine other_alpha_mlt_change_mass(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr
    real(dp) :: A, B
    integer :: inner_index
    type (star_info), pointer :: s
    ierr = 0
    call star_ptr(id, s, ierr)
    if (ierr /= 0) return

    s% alpha_mlt(:) = s% mixing_length_alpha

    ! write(*,*) 'using alpha mlt'
    inner_index = s%he_core_k
    A = 30
    B = 0.2
    call alpha_mlt_beta(id, ierr, A, B, inner_index)

 end subroutine other_alpha_mlt_change_mass

 subroutine evolve_through_helium_flash(s)
    type (star_info), pointer :: s

    ! controls to convergence degenerate stars through He flash
    if ((s%center_degeneracy > 5.0) .and. (s% use_gold_tolerances .eqv. .true.)) then
       write(*,*) 'easing controls to get through degenerate ignition'
       s% convergence_ignore_equL_residuals = .true.
       s% use_gold_tolerances = .false.
       s% use_gold2_tolerances = .false.
       s% varcontrol_target = 1d-3
    end if

 end subroutine evolve_through_helium_flash

 subroutine turn_off_hydro_to_prevent_pulsations(id,s)
    integer, intent(in) :: id
    type (star_info), pointer :: s
    integer :: ierr = 0

    ! turn off hydro if pulsations get too large
    if ((s%he_core_mass/s% star_mass <0.6) .and. (s% center_degeneracy<5.0) .and. (ABS(s% v_div_csound(1))>1d-3)) then
       ! write(*,*) 'turn of hydro'
       call star_set_v_flag(id, .false., ierr)
    end if

 end subroutine turn_off_hydro_to_prevent_pulsations

subroutine prevent_burning_shell_mixing(id,lower_mass_lim)
    integer, intent(in) :: id
    real(dp), intent(in) :: lower_mass_lim
    integer :: i, ierr
    type (star_info), pointer :: s
    ierr = 0
    call star_ptr(id, s, ierr)

    ! prevent mixing in H-burning shell in high mass stars
    if (s%star_mass>lower_mass_lim) then
       s% D_mix_zero_region_top_q = s% q(MAX(s%he_core_k-1,1))
       do i = s%he_core_k, s%nz
          if (s% X(i) < MAX(1d-8,s%center_h1*1d2)) then
             s% D_mix_zero_region_bottom_q = s%q(i+1)
             ! write(*,*) s% D_mix_zero_region_bottom_q
             exit
          end if
       end do
       write(*,*) 'No mixing between m = ', s% D_mix_zero_region_bottom_q*s% star_mass, '-', s% D_mix_zero_region_top_q*s% star_mass
    end if

 end subroutine prevent_burning_shell_mixing
