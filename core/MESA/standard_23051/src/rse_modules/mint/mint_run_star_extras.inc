!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Standard run_star_extra routines for MINT grid running
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine extras_controls(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         ! this is the place to set any procedure pointers you want to change
         ! e.g., other_wind, other_mixing, other_energy  (see star_data.inc)


         ! the extras functions in this file will not be called
         ! unless you set their function pointers as done below.
         ! otherwise we use a null_ version which does nothing (except warn).

         s% extras_startup => extras_startup
         s% extras_start_step => extras_start_step
         s% extras_check_model => extras_check_model
         s% extras_finish_step => extras_finish_step
         s% extras_after_evolve => extras_after_evolve
         s% how_many_extra_history_columns => how_many_extra_history_columns
         s% data_for_extra_history_columns => data_for_extra_history_columns
         s% how_many_extra_profile_columns => how_many_extra_profile_columns
         s% data_for_extra_profile_columns => data_for_extra_profile_columns

         s% how_many_extra_history_header_items => how_many_extra_history_header_items
         s% data_for_extra_history_header_items => data_for_extra_history_header_items
         s% how_many_extra_profile_header_items => how_many_extra_profile_header_items
         s% data_for_extra_profile_header_items => data_for_extra_profile_header_items

         evol_phase = s% x_integer_ctrl(1)

         select case(evol_phase)
         case(1)
            s% xtra(x_superad_val) = s% superad_reduction_diff_grads_limit
            s% xtra(x_overshoot_val) = s% overshoot_f(1)

            ! turn of MLT++ during create_pre_main_sequence_model
            s% superad_reduction_diff_grads_limit = s% superad_reduction_diff_grads_limit*1d4
            ! turn of MLT++ for low mass models
            if (s% initial_mass < 10.0) then
               s% use_superad_reduction = .false.
            end if

            ! convective penetration
            s% other_adjust_mlt_gradT_fraction => adjust_mlt_gradT_fraction_conv_pen

         case(2,3)
            s% other_alpha_mlt => alpha_mlt_stripped_stars

         case(4)
            s% other_alpha_mlt => alpha_mlt_stripped_eagb

         case(5)
            s% other_alpha_mlt => other_alpha_mlt_prevent_FeI
            s% other_eps_grav => other_eps_grav_prevent_HRI

         case(10)
            s% other_alpha_mlt => other_alpha_mlt_change_mass

         end select

      end subroutine extras_controls


      subroutine extras_startup(id, restart, ierr)
         integer, intent(in) :: id
         logical, intent(in) :: restart
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         select case(evol_phase)
         case(1)
            evol_phase_name = 'MS'
            call startup_mint(s,restart,ierr)
         case(2) ! GB
            evol_phase_name = 'GB'
            call startup_mint(s,restart,ierr)
            if (s% star_mass<3d0) s% energy_eqn_option = 'eps_grav'
            if (s% star_mass>20) s% Pextra_factor=2d0
         case(3) ! CHeB
            evol_phase_name = 'CHeB'
            call startup_mint(s,restart,ierr)
            if (s% star_mass>20) s% Pextra_factor=2d0
         case(4)
            evol_phase_name = 'EAGB'
            call startup_mint(s,restart,ierr)
            M_eff = s% star_mass
         case(5)
            evol_phase_name = 'TPAGB'
            call startup_mint(s,restart,ierr)

            if (.not. restart) then
               s% ixtra(ix_TP_count) = 1
               s% lxtra(lx_in_LHe_peak) = .true.
               s% xtra(x_min_core_mass_after_TDU) = s% he_core_mass
               s% xtra(x_max_core_mass_before_TDU) = s% he_core_mass
               s% xtra(x_interpulse_core_mass_growth) = 1.0
            end if
            VCT = s% x_ctrl(1)
            MXP = s% x_ctrl(2)
            envelope_overshooting_scheme = s% overshoot_scheme(2)
            call star_set_v_flag(id, .false., ierr)
         case(10) ! mass change
             call extras_startup_change_mass(id,s,ierr)
         end select


      end subroutine extras_startup


      integer function extras_start_step(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         real(dp) :: lower_mass_lim
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         select case (evol_phase)
         case(1)
             call phase_in_superad_reduction(id)
             call phase_out_convective_premixing_and_overshooting(id)
         case(2)
             lower_mass_lim = 15.0
             call prevent_burning_shell_mixing(id,lower_mass_lim)
             call evolve_through_helium_flash(s)
             call turn_off_hydro_to_prevent_pulsations(id,s)
         case(3)
             lower_mass_lim = 10.0
             call prevent_burning_shell_mixing(id,lower_mass_lim)
         case(4)
             TP_flag = 0
             call converge_eagb(s)
         end select

         extras_start_step = 0
      end function extras_start_step


      ! returns either keep_going, retry, or terminate.
      integer function extras_check_model(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         logical :: do_retry
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_check_model = keep_going

         do_retry = .false.
         select case(evol_phase)
         case(1,2,3,4,5)
            call check_model_mint(s,do_retry)
         case(10)
            call extras_check_model_change_mass(s,do_retry)
         end select
         if (do_retry .eqv. .true.) extras_check_model = retry


         ! if you want to check multiple conditions, it can be useful
         ! to set a different termination code depending on which
         ! condition was triggered.  MESA provides 9 customizeable
         ! termination codes, named t_xtra1 .. t_xtra9.  You can
         ! customize the messages that will be printed upon exit by
         ! setting the corresponding termination_code_str value.
         ! termination_code_str(t_xtra1) = 'my termination condition'

         ! by default, indicate where (in the code) MESA terminated
         if (extras_check_model == terminate) s% termination_code = t_extras_check_model
      end function extras_check_model


      integer function how_many_extra_history_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_history_columns = 6

         if (evol_phase_name == 'MS') then
            how_many_extra_history_columns = 7
         else if (evol_phase_name == 'GB') then
            how_many_extra_history_columns = 8 
         else if (evol_phase_name == 'CHeB') then
            how_many_extra_history_columns = 7
         else if (evol_phase_name == 'EAGB') then
            how_many_extra_history_columns = 10
         else if (evol_phase_name == 'TPAGB') then
            how_many_extra_history_columns = 12
         end if

         end function how_many_extra_history_columns


      subroutine data_for_extra_history_columns(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         real(dp) :: alpha_mlt_max, min_beta, core_he4
         integer :: k
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         ! all evol phases

         names(1) = 'interp_variable_hit'
         vals(1) = hit_interp_variable

         call calc_min_beta(s,min_beta)
         names(2) = 'min_beta'
         vals(2) = min_beta

         call calc_alpha_mlt_max(s,alpha_mlt_max)

         names(3) = 'alpha_mlt_max'
         vals(3) = alpha_mlt_max

         names(4) = 'alpha_mlt_surface'
         vals(4) = s% alpha_mlt(1)

         names(5) = 'hydrogen_exhausted_core_mass'

         if (s% center_h1 < 1d-4) then
             do k=1, s% nz
                 if (s% X(k) < 1d-4) exit
             end do
             vals(5) = s%q(k)*s%star_mass
         else
             vals(5) = 0
         end if

         names(6) = 'nuclear_burning_core_mass'
         do k = 1, s%nz
            if (s% T(k)>4d6) exit
         end do
         vals(6) = s%q(k)*s%star_mass

         ! extra columns for specific phases 

         if (evol_phase_name == 'MS') then

            names(7) = 'he_core_mass_1d-2'
            if (s% center_h1 < 1d-2) then
                do k=1, s% nz
                    if (s% X(k) < 1d-2) exit
                end do
                vals(7) = s%q(k)*s%star_mass
            else
                vals(7) = 0
            end if

         else if (evol_phase_name == 'GB') then

            call calc_core_he4(s,core_he4)
            names(7) = 'core_he4'
            vals(7) = core_he4

            names(8) = 'ignited_helium'
            vals(8) = ignited_helium

         else if (evol_phase_name == 'CHeB') then
            call calc_core_he4(s,core_he4)
            names(7) = 'core_he4'
            vals(7) = core_he4

         else if (evol_phase_name == 'EAGB') then

            names(7) = 'TP_flag'
            vals(7) = TP_flag
            
            names(8) = 'ignited_neon'
            vals(8) = ignited_neon

            names(9) = 'envelope_lost_flag'
            vals(9) = envelope_lost_flag

            names(10) = 'M_eff'
            vals(10) = M_eff

         else if (evol_phase_name == 'TPAGB') then

            names(7) = 'PDCZ_overshoot'
            vals(7) = s% overshoot_f(1)

            names(8) = 'CE_overshoot'
            vals(8) = s% overshoot_f(2)

            names(9) = 'lambda_DUP'
            if (s% lxtra(lx_in_LHe_peak) .eqv. .true.) then
                vals(9) = (s% xtra(x_max_core_mass_before_TDU)-s%he_core_mass)/ s% xtra(x_interpulse_core_mass_growth)
            else
                vals(9) = 0
            end if

            names(10) = 'varcontrol'
            vals(10) = s% varcontrol_target

            names(11) = 'conv_env_bot'
            vals(11) = conv_env_bot

            names(12) = 'TP_count'
            vals(12) = s% ixtra(ix_TP_count)

         end if

         ! note: do NOT add the extras names to history_columns.list
         ! the history_columns.list is only for the built-in history column options.
         ! it must not include the new column names you are adding here.


      end subroutine data_for_extra_history_columns


      integer function how_many_extra_profile_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_profile_columns = 0
      end function how_many_extra_profile_columns


      subroutine data_for_extra_profile_columns(id, n, nz, names, vals, ierr)
         integer, intent(in) :: id, n, nz
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(nz,n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         integer :: k
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         ! note: do NOT add the extra names to profile_columns.list
         ! the profile_columns.list is only for the built-in profile column options.
         ! it must not include the new column names you are adding here.

         ! here is an example for adding a profile column
         !if (n /= 1) stop 'data_for_extra_profile_columns'
         !names(1) = 'beta'
         !do k = 1, nz
         !   vals(k,1) = s% Pgas(k)/s% P(k)
         !end do

      end subroutine data_for_extra_profile_columns


      integer function how_many_extra_history_header_items(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_history_header_items = 0
      end function how_many_extra_history_header_items


      subroutine data_for_extra_history_header_items(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         type(star_info), pointer :: s
         integer, intent(out) :: ierr
         ierr = 0
         call star_ptr(id,s,ierr)
         if(ierr/=0) return

         ! here is an example for adding an extra history header item
         ! also set how_many_extra_history_header_items
         ! names(1) = 'mixing_length_alpha'
         ! vals(1) = s% mixing_length_alpha

      end subroutine data_for_extra_history_header_items


      integer function how_many_extra_profile_header_items(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_profile_header_items = 0
      end function how_many_extra_profile_header_items


      subroutine data_for_extra_profile_header_items(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(n)
         type(star_info), pointer :: s
         integer, intent(out) :: ierr
         ierr = 0
         call star_ptr(id,s,ierr)
         if(ierr/=0) return

         ! here is an example for adding an extra profile header item
         ! also set how_many_extra_profile_header_items
         ! names(1) = 'mixing_length_alpha'
         ! vals(1) = s% mixing_length_alpha

      end subroutine data_for_extra_profile_header_items


      ! returns either keep_going or terminate.
      ! note: cannot request retry; extras_check_model can do that.
      integer function extras_finish_step(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         logical :: do_termination
         character(len=50) :: do_termination_code_str
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_finish_step = keep_going

         ! to save a profile,
            ! s% need_to_save_profiles_now = .true.
         ! to update the star log,
            ! s% need_to_update_history_now = .true.

         select case(evol_phase)
         case(1,2,3,4)
            call finish_step_mint(id,s,do_termination,do_termination_code_str)
         case(5)
            call check_for_TP(s)
            call resolve_TDU(s,ierr)
            call check_for_HTDU(s,ierr)
            call finish_step_mint(id,s,do_termination,do_termination_code_str)
         case(10)
            call finish_step_mass_change(id,s,do_termination,do_termination_code_str,ierr)
         end select

         if (do_termination .eqv. .True.) then
            termination_code_str(t_xtra1) = do_termination_code_str
            s% termination_code = t_xtra1
            extras_finish_step = terminate
         end if

         ! see extras_check_model for information about custom termination codes
         ! by default, indicate where (in the code) MESA terminated
         ! if (extras_finish_step == terminate) s% termination_code = t_extras_finish_step
      end function extras_finish_step


      subroutine extras_after_evolve(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
      end subroutine extras_after_evolve


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Extra routines for MINT functionality
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine startup_mint(s, restart, ierr)
         logical, intent(in) :: restart
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         integer :: i

         ierr = 0

         ! number of interpolation variables to use to save profiles
         num_interp_variables = 1

         ! set dimensions
         allocate(save_target_index_list(num_interp_variables))
         allocate(interp_variable_list(num_interp_variables))
         allocate(save_targets_list_real_lengths(num_interp_variables))
         allocate(interp_variable_increasing(num_interp_variables))
         allocate(max_delta_timestep_limit_list(num_interp_variables))
         allocate(num_retries_list(num_interp_variables))

         allocate(save_targets_list(save_targets_length,1))
         save_targets_list(1:save_targets_length,1) = save_targets
         save_targets_list_real_lengths = save_targets_length

         interp_variable_increasing = .True.

         if (evol_phase_name == 'MS') then
             interp_variable_increasing = .False.
             max_delta_timestep_limit_list = s% delta_XH_cntr_limit
         else if (evol_phase_name == 'CHeB') then
             interp_variable_increasing = .False.
             max_delta_timestep_limit_list = s% delta_XHe_cntr_limit
         end if

         num_retries_list = 0

         ! set first target index
         if (.not. restart) then

             s% lxtra(lx_hit_first_target) = .false.

             s% ixtra(ix_save_target_index:ix_save_target_index+num_interp_variables-1) = 1 ! save_target_index = 1

             ! set first target index based on initial degeneracy
             if ((evol_phase_name == 'GB') .or.  (evol_phase_name == 'EAGB')) then
                 do i = 1, save_targets_length
                     if (save_targets_list(i,1)>s% center_degeneracy) exit
                 end do
                 s% ixtra(ix_save_target_index) = i
             end if

         end if

         do i = 1, num_interp_variables
             write(*,*) i,'. save targets = ', save_targets_list(1:save_targets_list_real_lengths(i), i)
         end do


     end subroutine startup_mint

     subroutine check_model_mint(s,do_retry)
      ! trigger retry if interp_variable overshoots save_target by more than max_error_save_target
      type (star_info), pointer :: s
      logical, intent(out) :: do_retry
      real(dp) :: core_he4
      integer :: i

      ! reset to 0
      hit_interp_variable = 0

      if (evol_phase_name == 'MS') then
          interp_variable_list = s% center_h1

      else if (evol_phase_name == 'GB') then
          interp_variable_list = s% center_degeneracy

      else if (evol_phase_name == 'CHeB') then
          call calc_core_he4(s,core_he4)
          interp_variable_list = core_he4
         ! interp_variable_list = s% center_he4

      else if (evol_phase_name == 'EAGB') then
          !interp_variable_list = s%co_core_mass/s%he_core_mass
          interp_variable_list = s% center_degeneracy

      else if (evol_phase_name == 'TPAGB') then
          interp_variable_list = s% ixtra(ix_TP_count)
      end if

      save_target_index_list = s% ixtra(ix_save_target_index:ix_save_target_index+num_interp_variables-1)

      ! allow save target index to go back to smaller values if interp variable decreases before first target hit
      if ((evol_phase_name == 'GB') .and. (s% lxtra(lx_hit_first_target) .eqv. .false.)) then
          if (interp_variable_list(1) .lt. save_targets_list(save_target_index_list(1)-1,1)) then
              do i = 1, save_targets_length
                  if (save_targets_list(i,1)>interp_variable_list(1)) exit
              end do
              save_target_index_list(1) = i
          end if
      end if

      call check_retry_for_save_target(do_retry)

  end subroutine check_model_mint

  subroutine check_retry_for_save_target(do_retry)
   logical, intent(out) :: do_retry
   real(dp) :: interp_variable
   integer :: i

   do_retry = .false.

   do i = 1, num_interp_variables

       if (num_retries_list(i) < max_num_retries) then

           if (save_target_index_list(i) <= save_targets_list_real_lengths(i)) then
               !extract save target and interp variable for comparison
               save_target = save_targets_list(save_target_index_list(i),i)
               interp_variable = interp_variable_list(i)

               if (abs((interp_variable-save_target)/save_target)>max_error_save_target) then
                   if (((interp_variable .lt. save_target) .and. (interp_variable_increasing(i) .eqv. .False.)) .or. ((interp_variable .gt. save_target) .and. (interp_variable_increasing(i) .eqv. .True.))) then
                       write(*,*) i, '. save_target = ', save_target, 'retry due to error in save target = ', (interp_variable-save_target)/save_target
                       do_retry = .true.
                       num_retries_list(i) = num_retries_list(i) + 1
                   end if
               end if
           end if

       else
           write(*,*) 'hit max_num_retries: ', num_retries_list(i)
       end if

   end do

end subroutine check_retry_for_save_target

subroutine finish_step_mint(id, s, do_termination, do_termination_code_str)
   integer, intent(in) :: id
   real(dp) :: min_beta, core_he4
   logical, intent(out) :: do_termination
   character(len=50), intent(out) :: do_termination_code_str
   real(dp), dimension(num_interp_variables) :: delta_timestep_limit
   type (star_info), pointer :: s
   real(dp) :: he_rich_layer_mass
   real(dp) :: mass, radius, logP, P, CO, Y, vexp, Mdot
   real(dp) :: min_vexp = 3.0, max_vexp = 15.0


   ! write(*,*) 'finishing step, model number: ',s%model_number
   ! write(*,*) 'save_target_index_list= ', save_target_index_list

   call save_profiles_at_save_targets(id,s,delta_timestep_limit)

   if (evol_phase_name == 'MS') then
       ! set timestep limit
       s% delta_XH_cntr_limit = delta_timestep_limit(1)

       ! save final model at h=1e-4
       if ((s% center_h1<1d-4) .and. (s% job% save_model_number==-111)) then
           s% job% save_model_number = s% model_number
           s% job% save_model_when_terminate = .false.
           ! s% job% save_model_filename = 'testing.mod'
           write(*,*) 'saving model: ', s% job% save_model_filename
       end if

       call terminate_at_end_save_targets(s,do_termination,do_termination_code_str)

   else if (evol_phase_name == 'GB') then

       ! near He ignition
       call calc_core_he4(s,core_he4)

       write(*,*) 'max_center_he4 = ', s% xtra(x_center_he4_max), &
                  'center_he4 = ', s% center_he4, &
                  'core_he4 = ', core_he4, &
                  'saved model: ', .not. s% job% save_model_when_terminate
       if (s% center_he4 > s% xtra(x_center_he4_max)) s% xtra(x_center_he4_max) = s% center_he4

       ! save model when central helium drops by small amount
       if (((s% center_he4 < s% xtra(x_center_he4_max)-0.0001) .or. (core_he4<1-s%initial_z-0.001)) .and. (s% center_h1<1d-12)) then
         if (s% job% save_model_number==-111) then
            s% job% save_model_number = s% model_number
            s% job% save_model_when_terminate = .false.
            write(*,*) 'saving model: ', s% job% save_model_filename
         end if
       end if

      ! terminate after significant amount of helium burning
      if (s% center_he4<1-s%initial_z-0.01) then
         write(*,*) 'terminating as Yc < ', 1-s%initial_z-0.01
         ignited_helium = 1
         do_termination = .true.
         do_termination_code_str = 'HB_limit'
      end if


   else if (evol_phase_name == 'CHeB') then

       ! set timestep limit
       s% delta_XHe_cntr_limit = delta_timestep_limit(1)

       call terminate_at_end_save_targets(s,do_termination,do_termination_code_str)

       ! save model when he4=1e-4
       if ((s% center_he4<1d-4) .and. (s% job% save_model_number==-111)) then
           s% job% save_model_number = s% model_number
           s% job% save_model_when_terminate = .false.
           ! s% job% save_model_filename = 'testing.mod'
           write(*,*) 'saving model: ', s% job% save_model_filename
       end if

   else if (evol_phase_name == 'EAGB') then

         ! check for neon ignition
         if ((s% center_ne20 < 0.01) .and. (s% center_c12 < 0.01)) then
            ignited_neon = 1
            do_termination_code_str = 'ne_ignition'
            do_termination = .true.
         else
            s% xtra(x_center_ne20_old) = s%center_ne20
         end if

         ! check for thermal pulse, require higher he burning lum for higher core masses so not confused with carbon burning
         if ((s%he_core_mass-s%co_core_mass<0.1) .and. (s% power_h_burn .gt. 1d2) .and. (((s% power_he_burn .gt. 10**(4)) .AND. (s%he_core_mass < 0.8)) .OR.((s% power_he_burn .gt. 10**(4.5)) .AND. (s%he_core_mass<= 1.1)) .OR. ((s% power_he_burn .gt. 10**(5.5)) .AND. (s%he_core_mass> 1.1)))) then
            TP_flag = 1
            s% need_to_update_history_now = .true.
            s% need_to_save_profiles_now = .true.
            write(*,*) 'starting thermal pulse'
            do_termination_code_str = 'TP_started'
            do_termination = .true.
         end if

         ! calculate M_eff including mass loss
         mass = safe_log10(s% star_mass)
         radius = s% log_surface_radius
         CO = 16/12 * s% surface_c12/ s% surface_o16
         Y = 0.3
         
         logP = -1.12166+1.24449*mass+1.07886*radius+0.87741*mass**2-1.53239*mass*radius+0.10382*radius**2-0.07659*mass**3-0.26130*mass**2*radius+0.26867*mass*radius**2+0.03278*radius**3-0.02713*LOG10(0.02)+0.14872*Y-0.01455*LOG10(CO/0.4125541984736385)
         P = 10**logP
         vexp = MIN(MAX(-13.5 + 0.056*P, min_vexp), max_vexp)
         Mdot = MIN(10.0**(-11.4 + 0.0123*P),3.8E33*10**s% log_surface_luminosity*3.154E7/(2.9979E10*vexp*1E5*1.9885E33))
         
         M_eff = M_eff - Mdot * s% dt_years
         write(*,*) 'Effective mass due to wind mass-loss = ', M_eff
         if (M_eff - s% he_core_mass < 0.01) then
            envelope_lost_flag = 1
            s% need_to_update_history_now = .true.
            s% need_to_save_profiles_now = .true.
            write(*,*) 'envelope lost'
            do_termination_code_str = 'envelope_lost'
            do_termination = .true.
         end if

   else if (evol_phase_name == 'TPAGB') then

        if (s% ixtra(ix_TP_count)==s% x_integer_ctrl(2)) then
            do_termination_code_str = 'max_NTP'
            do_termination = .true.
        end if

   end if

   s% ixtra(ix_save_target_index:ix_save_target_index+num_interp_variables-1) = save_target_index_list
   num_retries_list=0 !reset num_retries

end subroutine finish_step_mint

subroutine save_profiles_at_save_targets(id,s,delta_timestep_limit)
   integer, intent(in) :: id
   integer :: ierr
   real(dp), dimension(num_interp_variables), intent(out) :: delta_timestep_limit
   type (star_info), pointer :: s
   real(dp) :: interp_variable
   integer :: i
   character (len=strlen) :: filename, str_photo

   ierr = 0

   do i = 1, num_interp_variables

       if (save_target_index_list(i) <= save_targets_list_real_lengths(i)) then
           save_target = save_targets_list(save_target_index_list(i),i)
           interp_variable = interp_variable_list(i)

         !   write(*,*) save_target, interp_variable

           ! reduce timestep if near save target to hit target within max_error_save_target
           delta_timestep_limit(i) = MAX(MIN(abs(interp_variable-save_target)/4,max_delta_timestep_limit_list(i)),save_target*max_error_save_target/4)
           ! write(*,*) 'delta_XHe_cntr_limit = ', s%delta_XHe_cntr_limit

           if ((abs((interp_variable-save_target)/save_target)<max_error_save_target) .or. (num_retries_list(i)>0)) then
               write(*,*) i, '. save_target = ', save_target, 'error in save target = ', (interp_variable-save_target)/save_target
               ! set hit_first_target = .true.
               s% lxtra(lx_hit_first_target) = .true.
               ! to save a profile,
               s% need_to_save_profiles_now = .true.
               s% save_profiles_model_priority = save_profile_priority_list(i)
               ! to update the star log,
               s% need_to_update_history_now = .true.
               hit_interp_variable = save_target
               if (s% x_logical_ctrl(3) .eqv. .True.) then
                   ! save photo
                   call string_for_model_number('t', save_target_index_list(i), 3, str_photo)
                   filename = trim(trim(s% photo_directory) // '/' // str_photo )
                   call star_save_for_restart(id, filename, ierr)
               end if
               ! move onto next target
               save_target_index_list(i)=save_target_index_list(i)+1
           end if

       else
           delta_timestep_limit(i) = max_delta_timestep_limit_list(i)

       end if

   end do

end subroutine save_profiles_at_save_targets

subroutine terminate_at_end_save_targets(s,do_termination,do_termination_code_str)
   logical, intent(out) :: do_termination
   character(len=50), intent(out) :: do_termination_code_str
   type (star_info), pointer :: s

   !terminate if interp_variable drops below final target
   ! currently only works for first interp variable if have more than one
   if (interp_variable_list(1)<save_targets_list(save_targets_list_real_lengths(1),1)*0.9) then
       do_termination_code_str = 'xa_central_lower_limit'
       do_termination = .true.
   end if

end subroutine terminate_at_end_save_targets

subroutine phase_in_superad_reduction(id)
   integer, intent(in) :: id
   integer :: ierr
   type (star_info), pointer :: s
   call star_ptr(id, s, ierr)

   ! slowly turn on superad reduction to make it easier to produce pre-MS models
   ! s% xtra(x_superad_val) must be set in extras controls
   if (s% model_number == 200) then
       s% superad_reduction_diff_grads_limit = s% xtra(x_superad_val)
       if (s% star_mass>100) s% Pextra_factor = 2d0
   else if (s%model_number < 200) then
       s% superad_reduction_diff_grads_limit = 10**(safe_log10(s% xtra(x_superad_val))+4-4d0*(s% model_number)/200.0)
       write(*,*) 'superad_reduction_diff_grads_limit = ', s% superad_reduction_diff_grads_limit
   end if
end subroutine phase_in_superad_reduction

subroutine phase_out_convective_premixing_and_overshooting(id)
   integer, intent(in) :: id
   integer :: ierr
   type (star_info), pointer :: s
   call star_ptr(id, s, ierr)

   ! turn of convective premixing and overshooting if he core has formed to prevent late stage rejevunation
   ! only works for overshoot_scheme(1)
   if (s%center_h1 < s% he_core_boundary_h1_fraction) then
       ! write(*,*) 'turning of convective premixing'
       if (s%center_h1 < s% he_core_boundary_h1_fraction/10) then
          s% overshoot_scheme(1) = ''
          s% do_conv_premix = .false.
       else
          s% overshoot_f(1) = s% xtra(x_overshoot_val)/(11-10*(s% center_h1/s% he_core_boundary_h1_fraction))
          write(*,*) 'overshoot_f = ', s% overshoot_f(1)
       end if
    end if

end subroutine phase_out_convective_premixing_and_overshooting




subroutine extras_startup_change_mass(id,s,ierr)
   integer, intent(in) :: id
   integer, intent(out) :: ierr
   type (star_info), pointer :: s
   real(dp) :: mass_change_rate = 1d-8

   in_relax_phase = .False.
   start_relax_model_number = 0

   ! set mass loss/ accretion rate and termination condition depending on final mass

   ! mass accretion
   if (s%star_mass < s%x_ctrl(2)) then
      s% mass_change = s%x_ctrl(1)
      s% convergence_ignore_equL_residuals = .true.
      s% use_gold_tolerances = .false.
      s% use_gold2_tolerances = .false.
      s% varcontrol_target = 1d-3
      s% min_timestep_limit = 3
      ! s% use_other_alpha_mlt = .true.
      !s% use_other_rate_get = .true.

   ! mass loss by wind
   else if (s%x_ctrl(3)*s%he_core_mass< s%x_ctrl(2)) then
      s% mass_change = -mass_change_rate
      s% min_timestep_limit = 3
      s% use_other_alpha_mlt = .true.

   ! mass loss by star cut
   else
      s% mass_change = 0
      s% use_other_alpha_mlt = .false.
      ! prevent mixing reducing core size
      ! s% xtra(x_alpha_mlt) = s% mixing_length_alpha
      ! s% mixing_length_alpha = 0.0
      ! s% max_years_for_timestep = 0.1
   end if

   ! ! turn of size limit of convectve regions if stripped star
   ! if (s% he_core_mass*1.3> s%x_ctrl(2)) then
   !    s% prune_bad_cz_min_Hp_height = 0
   !    ! s% prune_bad_cz_min_log_eps_nuc = -99
   ! end if


end subroutine extras_startup_change_mass

subroutine extras_check_model_change_mass(s,do_retry)
   logical, intent(out)::do_retry
   type (star_info), pointer :: s

   select case(evol_phase)
   case(10)
      ! trigger retry if overshoots final mass by more than max_error_save_target
      ! x_ctrl(2) = final mass
      if (.not. in_relax_phase) then
         if ((s% mass_change>0) .and. (s%star_mass>s%x_ctrl(2)*(1+max_error_save_target))) then
            write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
            do_retry = .true.
         else if ((s% mass_change<0) .and. (s%star_mass<s%x_ctrl(2)*(1-max_error_save_target)) .and. (s%x_ctrl(3)*s%he_core_mass < s%x_ctrl(2))) then
            write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
            ! write(*,*) s% star_mass, s%initial_mass, s% he_core_mass, s%x_ctrl(2), s%x_ctrl(3), max_error_save_target
            do_retry = .true.
         end if
      end if
   end select

end subroutine extras_check_model_change_mass

subroutine finish_step_mass_change(id,s, do_termination,do_termination_code_str,ierr)
   integer, intent(in) :: id
   logical, intent(out) :: do_termination
   character(len=50), intent(out) :: do_termination_code_str
   type (star_info), pointer :: s
   integer :: i, k
   integer :: ierr
   real(dp) :: star_cut_turn_on_factor = 1.25
   ierr = 0

   ! call terminate_if_degenerate(s,do_termination,do_termination_code_str)

   if (.not. in_relax_phase) then

      if (s% model_number == start_relax_model_number) then
         in_relax_phase = .true.
         ! call star_set_v_flag(id, .true., ierr)

      ! if final mass < star_cut_turn_on_factor*core mass then use cut method to remove mass
      else if (s% mass_change == 0) then
         if (s%model_number == 1) then
            !find zone number where mass < final mass
            do i = 1,s%nz
               if (s% m(i) < (s%x_ctrl(2)*Msun)) then
                  k = i
                  exit
               end if
            end do
            call star_relax_to_star_cut(id, k, .false., .true., .false., ierr)
            start_relax_model_number = 100
            s% use_other_alpha_mlt = .true.
            ! s% mixing_length_alpha = s% xtra(x_alpha_mlt)
         end if

      ! if wind mass loss is taking too long then terminate
      else if ((s%mass_change < 0) .and. (s%model_number==3000)) then
         do_termination = .true.
         do_termination_code_str = 'mass_loss_too_slow'

         ! s% mass_change = 0
         ! s% use_other_alpha_mlt = .false.
         ! s% min_timestep_limit = 1d-6
         ! !find zone number where mass < final mass
         ! do i = 1,s%nz
         !    if (s% m(i) < (s%x_ctrl(2)*Msun)) then
         !       k = i
         !       exit
         !    end if
         ! end do
         ! call star_relax_to_star_cut(id, k, .false., .true., .false., ierr)
         ! s% MLT_option = 'Henyey'
         ! start_relax_model_number = s% model_number+100
         ! s% use_other_alpha_mlt = .true.

      ! if mass accretion not getting anywhere then terminate
      else if ((s%mass_change>0) .and. (s%model_number==2000)) then
         ! write(*,*) s% initial_mass
         do_termination = .true.
         do_termination_code_str = 'accretion_too_slow'

         ! if (s%star_mass<s%initial_mass*1.01) then
         !    do_termination = .true.
         !    do_termination_code_str = 'accretion_too_slow'
         ! end if

      ! if using mass loss/ accretion method then end mass change if within allowed error of final mass
      else if (abs(s%star_mass-s%x_ctrl(2))<s%x_ctrl(2)*max_error_save_target) then
         in_relax_phase = .true.
         s% mass_change = 0

      end if

      ! if now in relax phase set termination condition
      if (in_relax_phase) then
         ! calculate kh timescale and add to current age to evolve for another 10 kh timescale during relax
         s%max_age = s% star_age + 10*s%kh_timescale
         write(*,*) 'kh timescale =',s% kh_timescale
         write(*,*) 'max age =',s% max_age
         s% min_timestep_limit = 3
         ! s% max_years_for_timestep = 1d8

      end if

   end if

end subroutine finish_step_mass_change

subroutine calc_core_he4(s,core_he4)
   use chem_def, only: ihe4
   real(dp),intent(out) :: core_he4
   integer :: ierr, i
   real(dp) :: total_he_mass_in_core
   type (star_info), pointer :: s
   ierr = 0

   !either store central he abundance if not degenerate or calculate average he4 abundance in he core if degenerate
   if ((s% initial_mass <= 2.4) .AND. (s% mlt_mixing_type(s% nz)==0) .AND. (s% center_he4 > 0.9)) then
      ! write(*,*) 'zone = ', core_k, ' mass = ', s% m(core_k)/1.989D33
      total_he_mass_in_core = 0
      do i = s%he_core_k, s%nz
         total_he_mass_in_core = total_he_mass_in_core + s% xa(s% net_iso(ihe4),i)* s% dm(i)/Msun
      end do
      core_he4 = total_he_mass_in_core/(s% m(s%he_core_k)/Msun)
      ! write(*,*) 'core he4 = ', core_he4
   else
      core_he4 = s% center_he4
   end if
end subroutine calc_core_he4
