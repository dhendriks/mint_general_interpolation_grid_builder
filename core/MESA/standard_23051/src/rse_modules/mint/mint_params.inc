!--------- for MINT grid -----------------------------------------
! for use in saving profile data for MINT grid
! e.g. for MS, interp_variable = center_h1

! integer :: evol_phase
! evolutionary phases:
!   1 = MS
!   2 = HG & GB
!   3 = CHeB
!   4 = EAGB
!   5 = TPAGB
!   6 = post-AGB
!   10 = mass change


character(len=10) :: evol_phase_name !MS, HG, GB, CHeB, EAGB, TPAGB

! works with any number of interpolation variables
integer :: num_interp_variables ! number of interpolation variables for save targets
real(dp), dimension(:), allocatable :: interp_variable_list ! list of current values of interpolation variables, set in extras_check_model_mint for each step
real(dp), dimension(:,:), allocatable :: save_targets_list ! ! list of arrays of target values for interp_variable, set in extras_startup_mint (rectangular array of save_targets, fill in dead spaces with 0)
integer, dimension(:), allocatable :: save_target_index_list !list of current index of save_targets_list, set equal to 1 in extras_startup when not restarting, also stored in s% ixtra(1) to be compatible with restarts
! ixtra(1:num_intep_variables) = save_target_index_list
integer, dimension(:), allocatable :: save_targets_list_real_lengths !list of actual lengths of save_targets to consider so that zeros are ignored

logical, dimension(:), allocatable :: interp_variable_increasing ! list of directions for interp variables, = True if increasing with evolution
real(dp), dimension(:), allocatable :: max_delta_timestep_limit_list ! list of max allowed values of relevant delta timestep controls, read from inlist in extras_startup_mint
integer, dimension(:), allocatable :: num_retries_list ! list containing number of retries for each interp variable
integer, dimension(2) :: save_profile_priority_list = (/4, 5/) ! list of values for save profile priority for identifing profile files
integer :: max_num_retries = 7
real(dp) :: max_error_save_target = 0.001 ! max allowed error in interp_variable compared to save_target
real(dp) :: hit_interp_variable ! equal to interp variable target if hit during that step, otherwise = 0
real(dp) :: save_target ! store current save target

! quantities saved in photos, ensure no cross over with run_star_extras
integer, parameter :: lx_hit_first_target = 2
integer, parameter :: ix_save_target_index = 20

! MS
integer, parameter :: x_superad_val = 1 !s% xtra(x_superad_val)
integer, parameter :: x_overshoot_val = 2 !s% xtra(x_overshoot_val)

! GB
integer, parameter :: x_lum_max = 1 !s% xtra(x_lum_max)
integer, parameter :: lx_lum_peak = 1 ! s% lxtra(lx_lum_peak)
integer, parameter :: x_degen_max = 2 ! s% xtra(x_degen_max)
integer, parameter :: x_center_he4_max = 3 ! s% xtra(x_center_he4_max)
integer :: ignited_helium ! 1 = helium ignited, 0 otherwise


! EAGB
integer, parameter :: x_center_ne20_old = 1 ! s% xtra(x_center_ne20_old)
integer, parameter :: x_he_rich_layer_mass_old = 2 ! s% xtra(x_he_rich_layer_mass_old)
integer :: TP_flag
integer :: ignited_neon 
integer :: envelope_lost_flag
real(dp) :: M_eff ! mass when wind mass loss included

! TPAGB


! mass change

logical :: in_relax_phase
integer :: start_relax_model_number
integer :: num_extra_history_columns_mass_change = 1
integer, parameter :: x_alpha_mlt = 1 !s% xtra(x_alpha_mlt)
!------------------------------------------------------------------
