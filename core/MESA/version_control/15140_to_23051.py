#!/usr/bin/python3

########################################################################
# Creates inlists and src folder from version 15140 to version 23.05.1
########################################################################

import os
import re


def find_and_replace_text(file, find_text, replace_text):
    with open(file, "r+") as f:
        text = f.read()
        text = re.sub(find_text, replace_text, text)
        f.seek(0)
        f.write(text)
        f.truncate()

    return


file_path = os.path.dirname(os.path.abspath(__file__))
MESA_dir = os.path.split(file_path)[0]

old_version = "15140"
new_version = "23051"

########################################################################
# INLISTS

inlist_dir = os.path.join(MESA_dir, "inlists")
os.system(f"rm -rf {inlist_dir}/v{new_version}.py")
os.system(f"cp -r  {inlist_dir}/v{old_version}.py {inlist_dir}/v{new_version}.py")

inlist_changes = {
    "read_extra_star_job_inlist1": "read_extra_star_job_inlist(1)",
    "extra_star_job_inlist1_name": "extra_star_job_inlist_name(1)",
    "read_extra_eos_inlist1": "read_extra_eos_inlist(1)",
    "extra_eos_inlist1_name": "extra_eos_inlist_name(1)",
    "read_extra_kap_inlist1": "read_extra_kap_inlist(1)",
    "extra_kap_inlist1_name": "extra_kap_inlist_name(1)",
    "read_extra_controls_inlist1": "read_extra_controls_inlist(1)",
    "extra_controls_inlist1_name": "extra_controls_inlist_name(1)",
    "eosPT_cache_dir": "!eosPT_cache_dir",
    "eosDE_cache_dir": "!eosDE_cache_dir",
    "ionization_cache_dir": "!ionization_cache_dir",
    "use_dedt_form_of_energy_eqn = .true.": "energy_eqn_option = 'dedt'",
    "use_dedt_form_of_energy_eqn = .false.": "energy_eqn_option = 'eps_grav'",
    "dH_limit_min_H": "dX_limit_species(1) = 'h1'\ndX_limit_min_X(1)",
    "dH_limit": "dX_limit(1)",
    "dH_div_H_limit_min_H": "dX_div_X_limit_min_X(1)",
    "dH_div_H_limit": "dX_div_X_limit(1)",
    "dHe_div_He_limit_min_He": "dX_limit_species(2) = 'he4'\ndX_limit_min_X(2)",
    "dHe_div_He_limit": "dX_div_X_limit(2)",
    "saved_model_name = ": "load_model_filename = ",
    "always_use_eps_grav_form_of_energy_eqn = .true.": "",
}

for key, value in inlist_changes.items():
    find_and_replace_text(
        file=os.path.join(inlist_dir, f"v{new_version}.py"),
        find_text=key,
        replace_text=value,
    )


########################################################################
# SRC

os.system(f"rm -rf {MESA_dir}/standard_{new_version}")
os.system(f"cp -rf {MESA_dir}/standard_{old_version} {MESA_dir}/standard_{new_version}")

src_changes = {
    "c_core_mass": "co_core_mass",
    "c_core_k": "co_core_k",
    "he_core_mass_old": "he_core_mass",
    r"s% P\(i\)": "s% Peos(i)",
    r"s% P\(k\)": "s% Peos(k)",
    r"s% dH_limit_min_H": "s% dX_limit_species(1) = 'h1'\ns% dX_limit_min_X(1)",
    r"s% dH_limit": "s% dX_limit(1)",
    r"s% dH_div_H_limit_min_H": "s% dX_div_X_limit_min_X(1)",
    r"s% dH_div_H_limit": "s% dX_div_X_limit(1)",
    r"s% dHe_div_He_limit": "s% dX_div_X_limit(2)",
    r"! s% other_rate_get": "s% other_rate_get",
    r"! s% use_other_rate_get": "s% use_other_rate_get",
    r"s% extra_heat": "!s% extra_heat",
}

# loop through src files and make changes
src_path = os.path.join(MESA_dir, f"standard_{new_version}/src")
file_list = [
    os.path.join(dp, f)
    for dp, dn, fn in os.walk(os.path.expanduser(src_path))
    for f in fn
]
# print(file_list)
for file_dir in file_list:
    for key, value in src_changes.items():
        find_and_replace_text(
            file=file_dir, find_text=key, replace_text=value,
        )
