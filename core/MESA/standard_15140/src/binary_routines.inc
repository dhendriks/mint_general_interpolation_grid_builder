subroutine other_alpha_mlt_change_mass(id, ierr)
   integer, intent(in) :: id
   integer, intent(out) :: ierr
   real(dp) :: A, B
   integer :: inner_index
   type (star_info), pointer :: s
   ierr = 0
   call star_ptr(id, s, ierr)
   if (ierr /= 0) return

   s% alpha_mlt(:) = s% mixing_length_alpha

   ! write(*,*) 'using alpha mlt'
   inner_index = s%he_core_k
   A = 30
   B = 0.2
   call alpha_mlt_beta(id, ierr, A, B, inner_index)

end subroutine other_alpha_mlt_change_mass


subroutine extras_startup_change_mass(id,s,ierr)
   integer, intent(in) :: id
   integer, intent(out) :: ierr
   type (star_info), pointer :: s

   in_relax_phase = .False.
   start_relax_model_number = 0

   ! set mass loss/ accretion rate and termination condition depending on final mass

   ! mass accretion
   if (s%star_mass < s%x_ctrl(2)) then
      s% mass_change = s%x_ctrl(1)
      s% convergence_ignore_equL_residuals = .true.
      s% use_gold_tolerances = .false.
      s% use_gold2_tolerances = .false.
      s% varcontrol_target = 1d-3
      s% min_timestep_limit = 3
      ! s% use_other_alpha_mlt = .true.
      !! s% use_other_rate_get = .true.

   ! mass loss by wind
   else if (s%x_ctrl(3)*s%he_core_mass< s%x_ctrl(2)) then
      s% mass_change = -mass_change_rate
      s% min_timestep_limit = 3
      s% use_other_alpha_mlt = .true.

   ! mass loss by star cut
   else
      s% mass_change = 0
      s% use_other_alpha_mlt = .false.
      ! prevent mixing reducing core size
      ! s% xtra(x_alpha_mlt) = s% mixing_length_alpha
      ! s% mixing_length_alpha = 0.0
      ! s% max_years_for_timestep = 0.1
   end if

   ! ! turn of size limit of convectve regions if stripped star
   ! if (s% he_core_mass*1.3> s%x_ctrl(2)) then
   !    s% prune_bad_cz_min_Hp_height = 0
   !    ! s% prune_bad_cz_min_log_eps_nuc = -99
   ! end if


end subroutine extras_startup_change_mass

subroutine extras_check_model_change_mass(s,do_retry)
   logical, intent(out)::do_retry
   type (star_info), pointer :: s

   ! trigger retry if overshoots final mass by more than max_error_save_target
   ! x_ctrl(2) = final mass
   if (.not. in_relax_phase) then
      if ((s% mass_change>0) .and. (s%star_mass>s%x_ctrl(2)*(1+max_error_save_target))) then
         write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
         do_retry = .true.
      else if ((s% mass_change<0) .and. (s%star_mass<s%x_ctrl(2)*(1-max_error_save_target)) .and. (s%x_ctrl(3)*s%he_core_mass < s%x_ctrl(2))) then
         write (*,*) 'retry due to error in final mass', (s%star_mass-s%x_ctrl(2))/s%x_ctrl(2)
         ! write(*,*) s% star_mass, s%initial_mass, s% he_core_mass, s%x_ctrl(2), s%x_ctrl(3), max_error_save_target
         do_retry = .true.
      end if
   end if

end subroutine extras_check_model_change_mass

subroutine extras_finish_step_mass_change(id,s, do_termination,do_termination_code_str,ierr)
   integer, intent(in) :: id
   logical, intent(out) :: do_termination
   character(len=50), intent(out) :: do_termination_code_str
   type (star_info), pointer :: s
   integer :: i, k
   integer :: ierr
   ierr = 0

   ! call terminate_if_degenerate(s,do_termination,do_termination_code_str)

   if (.not. in_relax_phase) then

      if (s% model_number == start_relax_model_number) then
         in_relax_phase = .true.
         ! call star_set_v_flag(id, .true., ierr)

      ! if final mass < star_cut_turn_on_factor*core mass then use cut method to remove mass
      else if (s% mass_change == 0) then
         if (s%model_number == 1) then
            !find zone number where mass < final mass
            do i = 1,s%nz
               if (s% m(i) < (s%x_ctrl(2)*Msun)) then
                  k = i
                  exit
               end if
            end do
            call star_relax_to_star_cut(id, k, .false., .true., .false., ierr)
            start_relax_model_number = 100
            s% use_other_alpha_mlt = .true.
            ! s% mixing_length_alpha = s% xtra(x_alpha_mlt)
         end if

      ! if wind mass loss is taking too long then terminate
      else if ((s%mass_change < 0) .and. (s%model_number==3000)) then
         do_termination = .true.
         do_termination_code_str = 'mass_loss_too_slow'

         ! s% mass_change = 0
         ! s% use_other_alpha_mlt = .false.
         ! s% min_timestep_limit = 1d-6
         ! !find zone number where mass < final mass
         ! do i = 1,s%nz
         !    if (s% m(i) < (s%x_ctrl(2)*Msun)) then
         !       k = i
         !       exit
         !    end if
         ! end do
         ! call star_relax_to_star_cut(id, k, .false., .true., .false., ierr)
         ! s% MLT_option = 'Henyey'
         ! start_relax_model_number = s% model_number+100
         ! s% use_other_alpha_mlt = .true.

      ! if mass accretion not getting anywhere then terminate
      else if ((s%mass_change>0) .and. (s%model_number==2000)) then
         ! write(*,*) s% initial_mass
         do_termination = .true.
         do_termination_code_str = 'accretion_too_slow'

         ! if (s%star_mass<s%initial_mass*1.01) then
         !    do_termination = .true.
         !    do_termination_code_str = 'accretion_too_slow'
         ! end if

      ! if using mass loss/ accretion method then end mass change if within allowed error of final mass
      else if (abs(s%star_mass-s%x_ctrl(2))<s%x_ctrl(2)*max_error_save_target) then
         in_relax_phase = .true.
         s% mass_change = 0

      end if

      ! if now in relax phase set termination condition
      if (in_relax_phase) then
         ! calculate kh timescale and add to current age to evolve for another 10 kh timescale during relax
         s%max_age = s% star_age + 10*s%kh_timescale
         write(*,*) 'kh timescale =',s% kh_timescale
         write(*,*) 'max age =',s% max_age
         s% min_timestep_limit = 3
         ! s% max_years_for_timestep = 1d8

      end if

   end if

end subroutine extras_finish_step_mass_change

subroutine data_for_extra_history_columns_mass_change(n,names,vals,s,j)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    real(dp) :: min_beta
    integer, intent(out) :: j
    type (star_info), pointer :: s

    call calc_min_beta(s,min_beta)
    names(j+1) = 'min_beta'
    vals(j+1) = min_beta

    j =j+1

end subroutine data_for_extra_history_columns_mass_change


subroutine prevent_burning_shell_mixing(id,lower_mass_lim)
   integer, intent(in) :: id
   real(dp), intent(in) :: lower_mass_lim
   integer :: i, ierr
   type (star_info), pointer :: s
   ierr = 0
   call star_ptr(id, s, ierr)

   ! prevent mixing in H-burning shell in high mass stars
   if (s%star_mass>lower_mass_lim) then
      s% D_mix_zero_region_top_q = s% q(MAX(s%he_core_k-1,1))
      do i = s%he_core_k, s%nz
         if (s% X(i) < MAX(1d-8,s%center_h1*1d2)) then
            s% D_mix_zero_region_bottom_q = s%q(i+1)
            ! write(*,*) s% D_mix_zero_region_bottom_q
            exit
         end if
      end do
      write(*,*) 'No mixing between m = ', s% D_mix_zero_region_bottom_q*s% star_mass, '-', s% D_mix_zero_region_top_q*s% star_mass
   end if

end subroutine prevent_burning_shell_mixing
