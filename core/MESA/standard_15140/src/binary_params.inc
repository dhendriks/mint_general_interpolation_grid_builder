! parameters for binary grid

real(dp) :: star_cut_turn_on_factor = 1.25
real(dp) :: mass_change_rate = 1d-8
logical :: in_relax_phase
integer :: start_relax_model_number
integer :: num_extra_history_columns_mass_change = 1

integer, parameter :: x_alpha_mlt = 1 !s% xtra(x_alpha_mlt)
