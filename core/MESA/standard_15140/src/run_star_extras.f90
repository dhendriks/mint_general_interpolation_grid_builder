
! ***********************************************************************
!
!   Copyright (C) 2010-2020  Bill Paxton & The MESA Team
!
!   this file is part of mesa.
!
!   mesa is free software; you can redistribute it and/or modify
!   it under the terms of the gnu general library public license as published
!   by the free software foundation; either version 2 of the license, or
!   (at your option) any later version.
!
!   mesa is distributed in the hope that it will be useful,
!   but without any warranty; without even the implied warranty of
!   merchantability or fitness for a particular purpose.  see the
!   gnu library general public license for more details.
!
!   you should have received a copy of the gnu library general public license
!   along with this software; if not, write to the free software
!   foundation, inc., 59 temple place, suite 330, boston, ma 02111-1307 usa
!
! ***********************************************************************


      module run_star_extras

      use star_lib
      use star_def
      use const_def
      use math_lib
      use eos_def
      use eos_lib
      use chem_def

!----------- for hdf5 output for MPPNP nugrid post processing  -----------
      use run_star_support, only: failed
      use utils_lib, only: utils_OMP_GET_MAX_THREADS
      use HDF5
!-------------------------------------------

      implicit none

      include "test_suite_extras_def.inc"

      integer :: evol_phase
      ! evolutionary phases:
      !   1 = MS
      !   2 = HG & GB
      !   3 = CHeB
      !   4 = EAGB
      !   5 = TPAGB
      !   6 = post-AGB
      !   10 = mass change before CHeB
      !   11 = mass change before EAGB
      !   12 = mass change before TP-AGB
      !   20 = relax before CHeB

      ! MS
      integer, parameter :: x_superad_val = 1 !s% xtra(x_superad_val)
      integer, parameter :: x_overshoot_val = 2 !s% xtra(x_overshoot_val)

      ! EAGB
      integer, parameter :: x_center_ne20_old = 1 ! s% xtra(x_center_ne20_old)
      integer, parameter :: x_he_rich_layer_mass_old = 2 ! s% xtra(x_he_rich_layer_mass_old)

      ! TPAGB: see agb_params.inc

!------- for use during AGB evolution ---------------------
      include 'agb_params.inc'
! ----------------------------------------------------------------

!--------- for MINT grid -----------------------------------------
      include 'mint_params.inc'
      include 'binary_params.inc'
!------------------------------------------------------------------

!--------- for hdf5 output for MPPNP nugrid post processing ---------
      include 'mesa_hdf5_params.inc'
!-------------------------------------------------------------------

!--------- for double exponential overshoot (s-process) -----------------------
      ! include 'overshoot_dbl_exp/overshoot_dbl_exp_def.inc'
! ------------------------------------------------------------------------

      contains

!--------- for hdf5 output for MPPNP nugrid post processing ---------
      ! include 'mesa_hdf5_routines.inc'
!-------------------------------------------------------------------

! !--------for use during HG evolution--------------------
!       include 'hg_routines.inc'
! !------------------------------------------------------

!--------for use during CHeB evolution--------------------
      include 'cheb_routines.inc'
!------------------------------------------------------

!------- for use during AGB evolution ---------------------
      include 'agb_routines.inc'
! ----------------------------------------------------------------

!--------- for MINT grid -----------------------------------------
      include 'mint_routines.inc'
      include 'binary_routines.inc'
!------------------------------------------------------------------

      include "test_suite_extras.inc"

      subroutine extras_photo_read(id, iounit, ierr)
        integer, intent(in) :: id, iounit
        integer, intent(out) :: ierr
        type (star_info), pointer :: s
        ierr = 0

        call star_ptr(id, s, ierr)
        if (ierr /= 0) return

      end subroutine extras_photo_read

      subroutine extras_photo_write(id, iounit)
        integer, intent(in) :: id, iounit
        integer :: ierr
        type (star_info), pointer :: s
        ierr = 0

        call star_ptr(id, s, ierr)
        if (ierr /= 0) return

      end subroutine extras_photo_write

      subroutine extras_controls(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         select case(s% x_integer_ctrl(1))

         case(1)
            s% xtra(x_superad_val) = s% superad_reduction_diff_grads_limit
            s% xtra(x_overshoot_val) = s% overshoot_f(1)

            ! turn of MLT++ during create_pre_main_sequence_model
            s% superad_reduction_diff_grads_limit = s% superad_reduction_diff_grads_limit*1d4
            ! turn of MLT++ for low mass models
            if (s% initial_mass < 10.0) then
               s% use_superad_reduction = .false.
            end if

            ! convective penetration
            s% other_adjust_mlt_gradT_fraction => other_adjust_mlt_gradT_fraction

         case(2,3)
            s% other_alpha_mlt => other_alpha_mlt_GB

         case(4)
            s% other_wind => vassilliadis_wood_wind
            s% other_alpha_mlt => other_alpha_mlt_prevent_FeI

         case(5,6)
!--------- for double exponential overshoot (s-process) -----------------------
            ! include 'overshoot_dbl_exp/overshoot_dbl_exp_extras_controls.inc'
!-------------------------------------------------------------------------------
!------- for use during AGB evolution ---------------------
            s% other_wind => vassilliadis_wood_wind
            s% other_alpha_mlt => other_alpha_mlt_prevent_FeI
            s% other_eps_grav => other_eps_grav_prevent_HRI
!----------------------------------------------------------
         case(10)
            s% other_alpha_mlt => other_alpha_mlt_change_mass

         end select

         ! this is the place to set any procedure pointers you want to change
         ! e.g., other_wind, other_mixing, other_energy  (see star_data.inc)
         s% other_photo_read => extras_photo_read
         s% other_photo_write => extras_photo_write

         ! the extras functions in this file will not be called
         ! unless you set their function pointers as done below.
         ! otherwise we use a null_ version which does nothing (except warn).

         s% extras_startup => extras_startup
         s% extras_start_step => extras_start_step
         s% extras_check_model => extras_check_model
         s% extras_finish_step => extras_finish_step
         s% extras_after_evolve => extras_after_evolve
         s% how_many_extra_history_columns => how_many_extra_history_columns
         s% data_for_extra_history_columns => data_for_extra_history_columns
         s% how_many_extra_profile_columns => how_many_extra_profile_columns
         s% data_for_extra_profile_columns => data_for_extra_profile_columns

         s% how_many_extra_history_header_items => how_many_extra_history_header_items
         s% data_for_extra_history_header_items => data_for_extra_history_header_items
         s% how_many_extra_profile_header_items => how_many_extra_profile_header_items
         s% data_for_extra_profile_header_items => data_for_extra_profile_header_items


      end subroutine extras_controls

      ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      ! Andrassy et al. 2023
      !
      ! Adjust temperature gradient in the penetration zone to be adiabatic.
      ! Only works if conv_premix = .true. since the last iteration in a time
      ! step has NaNs in D_mix if conv_premix = .false.
      !
      ! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      subroutine other_adjust_mlt_gradT_fraction(id, ierr)
         use utils_lib, only: is_bad
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type(star_info), pointer :: s
         integer :: k, max_mt

         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         if (s%num_conv_boundaries < 1) return

         if (s%model_number<10) return

         if(is_bad(s% D_mix(1))) return

         if (s% overshoot_scheme(1) == 'step') then
            do k= s%nz, 1, -1
               if (s%D_mix(k) <= s% min_D_mix) exit

               ! f is the fraction such that grad_T = f*grad_ad + (1-f)*grad_rad
               s% adjust_mlt_gradT_fraction(k) = 1.0_dp
            end do
         end if

      end subroutine other_adjust_mlt_gradT_fraction

      subroutine other_alpha_mlt_GB(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         real(dp) :: A, B
         integer :: inner_index
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         s% alpha_mlt(:) = s% mixing_length_alpha

         if (s%he_core_mass/s%star_mass>0.6) then
            ! write(*,*) 'using alpha mlt'
            inner_index = s%he_core_k
            A = 40
            B = 0.3
            call alpha_mlt_beta(id, ierr, A, B, inner_index)
         end if

      end subroutine other_alpha_mlt_GB

!--------- for double exponential overshoot (s-process) -----------------------
      ! include 'overshoot_dbl_exp/overshoot_dbl_exp.inc'
!-------------------------------------------------------------------------------

      ! subroutine allocate_ptr(ptr, nz)
      !    real(dp), pointer :: ptr(:)
      !    integer, intent(in) :: nz
      !    integer :: ierr
      !    ierr = 0
      !    allocate(ptr(nz), stat=ierr)
      !    if (ierr /= 0) then
      !       write(*,*) 'ERROR -- failed to allocate pointer' ; return
      !    end if
      !    !print*, 'diff_coeff allocated within subrotuine' !for debugging
      ! end subroutine allocate_ptr


      subroutine extras_startup(id, restart, ierr)
         use kap_lib
         use kap_def

         integer, intent(in) :: id
         logical, intent(in) :: restart
         integer, intent(out) :: ierr
         integer :: handle, i

         type (Kap_General_Info), pointer :: rq
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         call test_suite_startup(s, restart, ierr)

         ! handle = alloc_kap_handle(ierr)
         ! if (ierr /= 0) return

         ! call kap_ptr(handle,rq,ierr)
         ! if (ierr /= 0) return

         ! rq% kap_lowT_option = 4
         ! call kap_setup_tables(handle, ierr)

         evol_phase = s% x_integer_ctrl(1)


         if (.not. restart) then
         end if

         select case(evol_phase)
         case(1)
            evol_phase_name = 'MS'
            call extras_startup_mint(s,restart,ierr)
         case(2) ! GB
            evol_phase_name = 'GB'
            call extras_startup_mint(s,restart,ierr)
            if (s% star_mass<3d0) s% energy_eqn_option = 'eps_grav'
            if (s% star_mass>20) s% Pextra_factor=2d0
         case(3) ! CHeB
            evol_phase_name = 'CHeB'
            call extras_startup_mint(s,restart,ierr)
            if (s% star_mass>20) s% Pextra_factor=2d0
         case(10,20) ! mass change
            call extras_startup_change_mass(id,s,ierr)
         case(4)
            evol_phase_name = 'EAGB'
            call extras_startup_agb(s,restart,id,ierr)
            call extras_startup_mint(s,restart,ierr)
         case(5)
            evol_phase_name = 'TPAGB'
            call extras_startup_agb(s,restart,id,ierr)
!--------- for hdf5 output for MPPNP nugrid post processing ---------
            if (s%x_logical_ctrl(2) .eqv. .true.) then
                ! call extras_startup_hdf5(id,restart,ierr)
            end if
!---------------------------------------------------------------------
         case(6)
            evol_phase_name = 'postAGB'
         end select

      end subroutine extras_startup

      integer function extras_start_step(id)
        use chem_def, only: ihe4
        integer, intent(in) :: id
        integer :: i, ierr, k
        real(dp) :: min_beta
        type (star_info), pointer :: s
        real(dp) :: Ledd
        real(dp) :: lower_mass_lim
        ierr = 0
        call star_ptr(id, s, ierr)

        select case (evol_phase)
        case(1)

            !slowly turn on superad reduction to make it easier to produce pre-MS models
            if (s% model_number == 200) then
              s% superad_reduction_diff_grads_limit = s% xtra(x_superad_val)
              if (s% star_mass>100) s% Pextra_factor = 2d0
            else if (s%model_number < 200) then
              s% superad_reduction_diff_grads_limit = 10**(safe_log10(s% xtra(x_superad_val))+4-4d0*(s% model_number)/200.0)
              write(*,*) 'superad_reduction_diff_grads_limit = ', s% superad_reduction_diff_grads_limit
            end if

            ! turn of convective premixing and overshooting if he core has formed to prevent late stage rejevunation
            if (s%center_h1 < 0.1) then
               ! write(*,*) 'turning of convective premixing'
               if (s%center_h1<0.01) then
                  s% overshoot_scheme(1) = ''
                  s% do_conv_premix = .false.
               else
                  s% overshoot_f(1) = s% xtra(x_overshoot_val)/1*(safe_log10(s% center_h1)+2)
                  write(*,*) 'overshoot_f = ', s% overshoot_f(1)
               end if
            end if

        case(2)
            lower_mass_lim = 15.0
            call prevent_burning_shell_mixing(id,lower_mass_lim)
        case(3)
            lower_mass_lim = 10.0
            call prevent_burning_shell_mixing(id,lower_mass_lim)
        case(4)
            call extras_start_step_eagb(s)
        end select

        if (ierr /= 0) return
        extras_start_step = 0
      end function extras_start_step


      ! returns either keep_going, retry, or terminate.
      integer function extras_check_model(id)
         integer, intent(in) :: id
         integer :: ierr, k
         type (star_info), pointer :: s
         logical :: do_retry
         real(dp) :: excess_he4, dm_to_remove
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_check_model = keep_going

         do_retry = .false.

         select case (evol_phase)

         case(1,2,3,4)
            call extras_check_model_mint(s,do_retry)

         case(5)
            ! call extras_check_model_resolve_TDU(s,do_retry)

         case(10)
            call extras_check_model_change_mass(s,do_retry)

         end select

         if (do_retry .eqv. .true.) extras_check_model = retry

         ! by default, indicate where (in the code) MESA terminated
         if (extras_check_model == terminate) s% termination_code = t_extras_check_model
      end function extras_check_model


      integer function how_many_extra_history_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         how_many_extra_history_columns = 0

         select case(evol_phase)
         case(1)
            how_many_extra_history_columns = 0 + num_extra_history_columns_mint
         case(2)
            how_many_extra_history_columns = 0 + num_extra_history_columns_mint
         case(3)
            how_many_extra_history_columns = 0 + num_extra_history_columns_mint
         case(4)
            how_many_extra_history_columns = num_extra_history_columns_eagb + num_extra_history_columns_mint
         case(5)
            how_many_extra_history_columns = num_extra_history_columns_tpagb !+ num_extra_history_columns_hdf5
         case(6)
            how_many_extra_history_columns = num_extra_history_columns_post_agb
         end select
      end function how_many_extra_history_columns


      subroutine data_for_extra_history_columns(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         integer :: j
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         j = 0

         select case(evol_phase)
         case(1,2,3,10)
            call data_for_extra_history_columns_mint(n,names,vals,s,j)
         case(4)
            call data_for_extra_history_columns_mint(n,names,vals,s,j)
            call data_for_extra_history_columns_eagb(n,names,vals,s,j)
         case(5)
            call data_for_extra_history_columns_tpagb(n,names,vals,s,j)
            ! call data_for_extra_history_columns_hdf5(n,names,vals,s,j)
         case(6)
            call data_for_extra_history_columns_post_agb(n,names,vals,s,j)
         end select

         ! note: do NOT add the extras names to history_columns.list
         ! the history_columns.list is only for the built-in history column options.
         ! it must not include the new column names you are adding here.


      end subroutine data_for_extra_history_columns


      integer function how_many_extra_profile_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         how_many_extra_profile_columns = 0

         select case(evol_phase)
         case(5,6)
            how_many_extra_profile_columns = num_extra_profile_columns_tpagb !+ num_extra_profile_columns_hdf5
         end select

      end function how_many_extra_profile_columns


      subroutine data_for_extra_profile_columns(id, n, nz, names, vals, ierr)
         integer, intent(in) :: id, n, nz
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(nz,n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         integer :: j
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         ! note: do NOT add the extra names to profile_columns.list
         ! the profile_columns.list is only for the built-in profile column options.
         ! it must not include the new column names you are adding here.

         ! here is an example for adding a profile column
         ! if (n /= 2) stop 'data_for_extra_profile_columns'
         j = 0
         select case(evol_phase)
         case(5,6)
            call data_for_extra_profile_columns_tpagb(n, nz, names, vals, s, j)
            ! call data_for_extra_profile_columns_hdf5(n, nz, names, vals, s, j)
         end select

      end subroutine data_for_extra_profile_columns


      integer function how_many_extra_history_header_items(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_history_header_items = 0
      end function how_many_extra_history_header_items


      subroutine data_for_extra_history_header_items(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         type(star_info), pointer :: s
         integer, intent(out) :: ierr
         ierr = 0
         call star_ptr(id,s,ierr)
         if(ierr/=0) return

         ! here is an example for adding an extra history header item
         ! also set how_many_extra_history_header_items
         ! names(1) = 'mixing_length_alpha'
         ! vals(1) = s% mixing_length_alpha

      end subroutine data_for_extra_history_header_items


      integer function how_many_extra_profile_header_items(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         how_many_extra_profile_header_items = 0

      end function how_many_extra_profile_header_items


      subroutine data_for_extra_profile_header_items(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(n)
         type(star_info), pointer :: s
         integer, intent(out) :: ierr
         ierr = 0
         call star_ptr(id,s,ierr)
         if(ierr/=0) return

         ! here is an example for adding an extra profile header item
         ! also set how_many_extra_profile_header_items
         ! names(1) = 'mixing_length_alpha'
         ! vals(1) = s% mixing_length_alpha

      end subroutine data_for_extra_profile_header_items


      ! returns either keep_going or terminate.
      ! note: cannot request retry; extras_check_model can do that.
      integer function extras_finish_step(id)
         use chem_def, only: ic13
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         integer :: c13, k_max_c13, he4
         logical :: in_LHe_peak
         integer :: TP_count
         integer :: k
         logical :: do_termination
         character(len=50) :: do_termination_code_str
         character(len=256) :: save_file_extension = '_end'
         real(dp) :: he_rich_layer_mass

         do_termination = .false.

         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_finish_step = keep_going

         ! to save a profile,
            ! s% need_to_save_profiles_now = .true.
         ! to update the star log,
            ! s% need_to_update_history_now = .true.

         select case (evol_phase)

         case(1)

            call extras_finish_step_mint(id,s,do_termination,do_termination_code_str)

         case(10)

            call extras_finish_step_mass_change(id,s,do_termination,do_termination_code_str,ierr)

         case(2)

            call extras_finish_step_mint(id,s,do_termination,do_termination_code_str)

            ! controls to convergence degenerate stars through He flash
            if ((s%center_degeneracy > 5.0) .and. (s% use_gold_tolerances .eqv. .true.)) then
               write(*,*) 'easing controls to get through degenerate ignition'
               s% convergence_ignore_equL_residuals = .true.
               s% use_gold_tolerances = .false.
               s% use_gold2_tolerances = .false.
               s% varcontrol_target = 1d-3
            end if

            ! terminate if core mass decreases
            if (s%he_core_mass<s%he_core_mass_old) then
               do_termination = .true.
               do_termination_code_str = 'core_mass_decreased'
            end if

            ! turn off hydro if pulsations get too large
            if ((s%he_core_mass/s% star_mass <0.6) .and. (s% center_degeneracy<5.0) .and. (ABS(s% v_div_csound(1))>1d-3)) then
               ! write(*,*) 'turn of hydro'
               call star_set_v_flag(id, .false., ierr)
            end if

         case(3)

            call extras_finish_step_mint(id,s,do_termination,do_termination_code_str)

         case(4)

            call extras_finish_step_mint(id,s,do_termination,do_termination_code_str)

            ! call check_if_end_EAGB(s,do_termination,do_termination_code_str)

            if ((s% center_ne20 < s% xtra(x_center_ne20_old)) .and. (s% co_core_mass> 1.37)) then
               do_termination_code_str = 'ne_ignition'
               do_termination = .true.
            else
               s% xtra(x_center_ne20_old) = s%center_ne20
            end if

            he_rich_layer_mass = s% he_core_mass - s% co_core_mass
            if ((he_rich_layer_mass > s% xtra(x_he_rich_layer_mass_old)) .and. (he_rich_layer_mass<0.1) .and. (s% power_he_burn .gt. 10**(4))) then
               do_termination_code_str = 'TP_started'
               do_termination = .true.
            else
               s% xtra(x_he_rich_layer_mass_old) = he_rich_layer_mass
            end if

         case(5)

            call check_for_TP(s)

            ! turn hydro on or off
            !call control_hydro(id,s,ierr)

            ! resolve TDU
            call resolve_TDU(s,ierr)

            ! turn off envelope overshooting for HTDU
            call check_for_HTDU(s,ierr)

            !terminate TP-AGB when H-rich envelope falls below 20% of current stellar mass (Dotter et al 2016)
            ! if ((s%star_mass-s%he_core_mass)/s%star_mass<0.2) then
            !     do_termination_code_str = 'terminate TP-AGB'
            !     extras_finish_step = terminate
            ! end if

!--------- for hdf5 output for MPPNP nugrid post processing ---------
            if (s%x_logical_ctrl(2) .eqv. .true.) then
                ! call extras_finish_step_hdf5(id)
            end if
!--------------------------------------------------------------------

         end select

         if (do_termination .eqv. .True.) then
            termination_code_str(t_xtra1) = do_termination_code_str
            s% termination_code = t_xtra1
            extras_finish_step = terminate
         end if

      end function extras_finish_step


      subroutine extras_after_evolve(id, ierr)
         use chem_def, only: ic13
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s

         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         include 'formats'

         call test_suite_after_evolve(s, ierr)

      end subroutine extras_after_evolve

      end module run_star_extras
