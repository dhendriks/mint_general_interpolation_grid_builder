! Core Helium Burning Routines
! AUTHOR: Natalie Rees


subroutine data_for_extra_history_columns_cheb(n,names,vals,s,j)
    integer, intent(in) :: n
    character (len=maxlen_history_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    type (star_info), pointer :: s
    integer, intent(out) :: j

    names(j+1) = 'central_he4'
    vals(j+1) = interp_variable_list(1)

    names(j+2) = 'varcontrol'
    vals(j+2) = s% varcontrol_target

    j=j+2

end subroutine data_for_extra_history_columns_cheb

subroutine data_for_extra_profile_header_items_cheb(n, names, vals, s)
    integer, intent(in) :: n
    character (len=maxlen_profile_column_name),intent(out) :: names(n)
    real(dp), intent(out) :: vals(n)
    integer ::  i
    real(dp) :: total_mu
    type(star_info), pointer :: s

    total_mu = 0
    do i = 1, s%nz
        total_mu = total_mu + s% mu(i)* s% dm(i)/1.989D33
    end do

    names(1) = 'average_mu'
    vals(1) = total_mu/ s% star_mass

    names(2) = 'he_core_radius'
    vals(2) = s% he_core_radius

    names(3) = 'he_core_zone'
    vals(3) = s% he_core_k

end subroutine data_for_extra_profile_header_items_cheb

! subroutine check_for_he_ignition(s,do_termination,do_termination_code_str)
!     ! terminate evolution once log_LHe > 0 to ensure degenerate models which have center_he4 < 0.98 stop before He flash
!     ! for higher mass stars with cores > 10M terminate once log_LHe > -2 to prevent crashing due to hitting eddington limit
!     logical, intent(out) :: do_termination
!     character(len=50), intent(out) :: do_termination_code_str
!     type (star_info), pointer :: s

!     if ((s%he_core_mass>10.0) .and. (s% power_he_burn >0.01)) then
!         do_termination_code_str = 'He ignited'
!         do_termination = .true.
!     else if ((s%star_mass>2) .and. (s% power_he_burn >0.1)) then
!         do_termination_code_str = 'He ignited'
!         do_termination = .true.
!     else if (s% power_he_burn >0.1) then
!         do_termination_code_str = 'He ignited'
!         do_termination = .true.
!     end if

! end subroutine check_for_he_ignition


subroutine save_profile_first_model(s)
    type (star_info), pointer :: s
    ! save a profile at start for element abundances pre-He burning
    if (s% model_number == 1) then
       s% need_to_save_profiles_now = .true.
       s% save_profiles_model_priority = 5
    end if
end subroutine save_profile_first_model


! subroutine terminate_if_degenerate(s,do_termination,do_termination_code_str)
!     logical, intent(out) :: do_termination
!     character(len=50), intent(out) :: do_termination_code_str
!     type (star_info), pointer :: s

!     !terminate if degeneracy becomes too high
!     if (s%eta(s%nz)>100.0) then
!         do_termination_code_str = 'eta>100'
!         do_termination = .true.
!     end if

! end subroutine terminate_if_degenerate

subroutine calc_central_he4(s,central_he4)
    real(dp),intent(out) :: central_he4
    integer :: ierr, i
    real(dp) :: total_he_mass_in_core
    type (star_info), pointer :: s
    ierr = 0

    !either store central he abundance if not degenerate or calculate average he4 abundance in he core if degenerate
    if ((s% initial_mass <= 2.4) .AND. (s% mlt_mixing_type(s% nz)==0) .AND. (s% center_he4 > 0.9)) then
       ! write(*,*) 'zone = ', core_k, ' mass = ', s% m(core_k)/1.989D33
       total_he_mass_in_core = 0
       do i = s%he_core_k, s%nz
          total_he_mass_in_core = total_he_mass_in_core + s% xa(s% net_iso(ihe4),i)* s% dm(i)/Msun
       end do
       central_he4 = total_he_mass_in_core/(s% m(s%he_core_k)/Msun)
       write(*,*) 'central he4 = ', central_he4
    else
       central_he4 = s% center_he4
    end if
end subroutine calc_central_he4
