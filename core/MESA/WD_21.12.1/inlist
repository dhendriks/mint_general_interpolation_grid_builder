&star_job

      show_log_description_at_start = .false.

      set_initial_cumulative_energy_error = .true.
      new_cumulative_energy_error = 0d0

      change_initial_net = .true.
      new_net_name = 'o18_and_ne22.net'
      ! new_net_name = 'cno_extras_o18_to_mg26_plus_fe56.net'

      change_v_flag = .true.
      change_initial_v_flag = .true.
      new_v_flag = .true.

      load_saved_model = .true.
      load_model_filename = 'model_with_opacity_restored.mod'

      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = 'WD_relaxed.data'

      save_model_when_terminate = .true.
      save_model_filename = 'WD_relaxed.mod'

      set_initial_age = .true.
      set_initial_model_number = .true.
      initial_age = 0
      initial_model_number = 0

      set_initial_dt = .true.
      seconds_for_initial_dt = 3d8


/ ! end of star_job namelist

&eos
/ ! end of eos namelist

&kap
      use_Type2_opacities = .true.
      Zbase = 0.02
/ !end of kap namelist

&controls

      mesh_delta_coeff = 2
      tol_correction_norm = 1d0
      tol_max_correction = 1d0
      varcontrol_target = 1d-3
      log_L_lower_limit = -3
      dxdt_nuc_factor = 1d0
      eps_nuc_factor = 1d0


      log_directory = 'LOGS_WD_relax'
      photo_directory = 'photos_WD_relax'

      photo_interval = 50
      profile_interval = 10

! Mass loss
      x_integer_ctrl(1) = 5 ! part number
      x_ctrl(1) = 0.1d0 !start_of_He_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(2) = 0.1d0 !start_of_H_shell_xa_limit - for stopping condition based on He shell mass
      x_ctrl(3) = 2d-2 !He_shell_mass_limit as compared to core mass - for stopping condition based on He shell mass
      x_ctrl(7) = 5d-3 ! H_shell_mass_limit as compared to core mass - for stopping condition based on H shell mass
      x_ctrl(8) = 2d3 ! mass loss eta parameter
      ! mass_change = -1d-2
      ! use_other_adjust_mdot = .true.


! Mass dependant Timestep Conditions
      ! Below true for M>2Msun
      ! convergence_ignore_equL_residuals = .false.
      ! use_gold_tolerances = .true.
      ! use_gold2_tolerances = .true.
      ! varcontrol_target = 1d-3

      ! Below true for M<2Msun
      convergence_ignore_equL_residuals = .true.
      use_gold_tolerances = .false.
      use_gold2_tolerances = .false.

! ! Timestep Conditions
!       dX_nuc_drop_limit = 3d-2
!       delta_lgRho_cntr_limit = 0.05
!       delta_lgRho_cntr_hard_limit = 0.1
!       delta_lgT_cntr_limit = 0.01
!       delta_lgT_cntr_hard_limit = 0.02
!       delta_lgTeff_limit = 0.01
!       delta_lgTeff_hard_limit = 0.02
!       delta_lgL_limit = 0.1
!       delta_lgL_hard_limit = 0.2

! Output
      terminal_interval = 10
      write_header_frequency = 10
      history_interval = 1
      photo_digits = 6

! Mixing Parameters
      mixing_length_alpha = 2

! Atmosphere
      atm_option = 'T_tau'
      atm_T_tau_relation = 'Eddington'
      atm_T_tau_opacity = 'fixed'

! Energy
      energy_eqn_option = 'dedt'
      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

! Diffusion and Mixing Parameters
      okay_to_reduce_gradT_excess = .true.
      use_ledoux_criterion = .false. !.true.
      alpha_semiconvection = -1 !1d-1
      semiconvection_option = 'Langer_85'
      num_cells_for_smooth_gradL_composition_term = 20
      threshold_for_smooth_gradL_composition_term = 0.02
      min_overshoot_q = 1d-4
      overshoot_Delta0(1) = 1.0
      ! we use step overshooting
      overshoot_scheme(1) = 'step'
      overshoot_zone_type(1) = 'any'
      overshoot_zone_loc(1) = 'core'
      overshoot_bdy_loc(1) = 'top'
      overshoot_f(1) = 0.33
      overshoot_f0(1) = 0.05

      do_element_diffusion = .true.
      ! show_diffusion_info = .true.
      ! diffusion_v_max = 1d-5
      diffusion_use_full_net = .true.
      diffusion_use_cgs_solver = .true.
      diffusion_use_isolve = .true.
      diffusion_rtol_for_isolve = 1d-4
      diffusion_atol_for_isolve = 1d-5
      diffusion_min_X_hard_limit = -1d-4
      diffusion_maxsteps_for_isolve = 1000
      diffusion_isolve_solver = 'ros2_solver'

/ ! end of controls namelist
