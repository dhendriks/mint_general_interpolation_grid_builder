! ***********************************************************************
!
!   Copyright (C) 2011  The MESA Team
!
!   this file is part of mesa.
!
!   mesa is free software; you can redistribute it and/or modify
!   it under the terms of the gnu general library public license as published
!   by the free software foundation; either version 2 of the license, or
!   (at your option) any later version.
!
!   mesa is distributed in the hope that it will be useful,
!   but without any warranty; without even the implied warranty of
!   merchantability or fitness for a particular purpose.  see the
!   gnu library general public license for more details.
!
!   you should have received a copy of the gnu library general public license
!   along with this software; if not, write to the free software
!   foundation, inc., 59 temple place, suite 330, boston, ma 02111-1307 usa
!
! ***********************************************************************

      module run_star_extras

      use star_lib
      use star_def
      use const_def
      use math_lib
      use auto_diff

      implicit none


      real(dp) :: mass_loss_rate = 0d0

      include "test_suite_extras_def.inc"


      ! these routines are called by the standard run_star check_model
      contains

      include "test_suite_extras.inc"


      subroutine extras_controls(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         s% extras_startup => extras_startup
         s% extras_check_model => extras_check_model
         s% extras_finish_step => extras_finish_step
         s% extras_after_evolve => extras_after_evolve
         s% how_many_extra_history_columns => how_many_extra_history_columns
         s% data_for_extra_history_columns => data_for_extra_history_columns
         s% how_many_extra_profile_columns => how_many_extra_profile_columns
         s% data_for_extra_profile_columns => data_for_extra_profile_columns
         s% other_adjust_mdot => adjust_mdot
         ! s% other_remove_surface => remove_ejecta_one_cell_per_step

      end subroutine extras_controls


      subroutine extras_startup(id, restart, ierr)
         integer, intent(in) :: id
         logical, intent(in) :: restart
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         call test_suite_startup(s, restart, ierr)
      end subroutine extras_startup


      subroutine extras_after_evolve(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         real(dp) :: dt
         real(dp) :: start_of_He_shell_xa_limit, start_of_H_shell_xa_limit, core_mass, start_of_He_shell_mass, start_of_H_shell_mass, He_shell_mass, H_shell_mass
         integer :: k, start_of_He_shell_cell
         character (len=strlen) :: test
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         call test_suite_after_evolve(s, ierr)

         start_of_He_shell_xa_limit = s% x_ctrl(1)
         start_of_H_shell_xa_limit = s% x_ctrl(2)
         core_mass = 0d0
         start_of_He_shell_mass = 0d0
         start_of_H_shell_mass = 0d0

         do k = s% nz, 1, -1
            if (s% Y(k) > start_of_He_shell_xa_limit) then
               core_mass = s% m(k)
               start_of_He_shell_mass = s% m(k)
               start_of_He_shell_cell = k
               exit
            end if
         end do

         do k = start_of_He_shell_cell, 1, -1
            if (s% X(k) > start_of_H_shell_xa_limit) then
               start_of_H_shell_mass = s% m(k)
               exit
            end if
         end do

         He_shell_mass = (start_of_H_shell_mass - start_of_He_shell_mass)/start_of_He_shell_mass
         H_shell_mass = s% mstar - start_of_H_shell_mass
         print*, 'Star mass', s% mstar/msun, 'H_shell_mass/mstar:', H_shell_mass/s% mstar, 'He_shell_mass', He_shell_mass ; print*, '' ; print*, ' '

      end subroutine extras_after_evolve


      ! returns either keep_going, retry, or terminate.
      integer function extras_check_model(id)
         integer, intent(in) :: id
         integer :: ierr, k, start_of_He_shell_cell, model_number_target = 0
         type (star_info), pointer :: s
         real(dp) :: start_of_He_shell_xa_limit, start_of_H_shell_xa_limit, He_shell_mass_limit, &
                     core_mass, start_of_He_shell_mass, start_of_H_shell_mass, He_shell_mass, &
                     H_shell_mass_limit, H_shell_mass, mass_prev_step = 0d0, previous_power_nuc_burn = 0d0
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_check_model = keep_going

         if (s% x_integer_ctrl(1) == 1) then

            start_of_He_shell_xa_limit = s% x_ctrl(1)
            start_of_H_shell_xa_limit = s% x_ctrl(2)
            He_shell_mass_limit = s% x_ctrl(3)
            core_mass = 0d0
            start_of_He_shell_mass = 0d0
            start_of_H_shell_mass = 0d0

            do k = s% nz, 1, -1
               if (s% Y(k) > start_of_He_shell_xa_limit) then
                  core_mass = s% m(k)
                  start_of_He_shell_mass = s% m(k)
                  start_of_He_shell_cell = k
                  exit
               end if
            end do

            do k = start_of_He_shell_cell, 1, -1
               if (s% X(k) > start_of_H_shell_xa_limit) then
                  start_of_H_shell_mass = s% m(k)
                  exit
               end if
            end do

            He_shell_mass = start_of_H_shell_mass - start_of_He_shell_mass
            ! print*, 'He_shell_mass', He_shell_mass, 'core_mass', core_mass, 'He_shell_mass/core_mass', He_shell_mass/core_mass
            if (mod(s% model_number,100) == 0) print*, 'He_shell_mass / rest of star mass', He_shell_mass/start_of_H_shell_mass
            if (((He_shell_mass/start_of_H_shell_mass) <= He_shell_mass_limit) .and. (He_shell_mass/start_of_H_shell_mass) > 0d0) then
               print*, 'Termination: He_shell_mass/core_mass) <= He_shell_mass_limit, He_shell_mass/core_mass:', He_shell_mass/core_mass, 'He_shell_mass_limit:', He_shell_mass_limit ; print*, '' ; print*, ' '
               extras_check_model = terminate
            end if
         end if

         if (s% x_integer_ctrl(1) == 2) then
            start_of_He_shell_xa_limit = s% x_ctrl(1)
            start_of_H_shell_xa_limit = s% x_ctrl(2)
            He_shell_mass_limit = s% x_ctrl(3)
            H_shell_mass_limit = s% x_ctrl(7)
            core_mass = 0d0
            start_of_He_shell_mass = 0d0
            start_of_H_shell_mass = 0d0

            do k = s% nz, 1, -1
               if (s% Y(k) > start_of_He_shell_xa_limit) then
                  core_mass = s% m(k)
                  start_of_He_shell_mass = s% m(k)
                  start_of_He_shell_cell = k
                  exit
               end if
            end do

            do k = start_of_He_shell_cell, 1, -1
               if (s% X(k) > start_of_H_shell_xa_limit) then
                  start_of_H_shell_mass = s% m(k)
                  exit
               end if
            end do
            He_shell_mass = (start_of_H_shell_mass - start_of_He_shell_mass)/start_of_He_shell_mass

            H_shell_mass = s% mstar - start_of_H_shell_mass
            if (mod(s% model_number,100) == 0) print*, 'H_shell_mass / rest of star mass', H_shell_mass/s% mstar
            if (((H_shell_mass/s% mstar) <= H_shell_mass_limit) .and. (H_shell_mass/s% mstar) > 0d0) then
               print*, 'Termination: H_shell_mass/mstar <= H_shell_mass_limit, H_shell_mass/mstar:', H_shell_mass/s% mstar, 'H_shell_mass_limit:', H_shell_mass_limit, 'He_shell_mass', He_shell_mass ; print*, '' ; print*, ' '
               extras_check_model = terminate
            end if

            ! if (mass_prev_step == 0d0) then
            !    mass_prev_step = s% mstar
            ! else
            !    if (((mass_prev_step - s% mstar)/msun < 1d-10) .and. mass_prev_step - s% mstar > 0d0) then
            !       print*, 'Termination: mass_prev_step - s% mstar < 1d-10, H_shell_mass/mstar:', H_shell_mass/s% mstar, 'H_shell_mass_limit:', H_shell_mass_limit ; print*, '' ; print*, ' '
            !       extras_check_model = terminate
            !    else
            !       mass_prev_step = s% mstar
            !    end if
            ! end if
         end if

         if (s% x_integer_ctrl(1) == 3) then
            if (s% power_nuc_burn - previous_power_nuc_burn < 0d0 .and. previous_power_nuc_burn > 0d0 .and. s% max_years_for_timestep /= 1d5) then
               print*, 'changing max_years_for_timestep from ', s% max_years_for_timestep, 'to ', 1d5
               s% max_years_for_timestep = 1d5
            else
               previous_power_nuc_burn = s% power_nuc_burn
            end if
         end if

         if (s% x_integer_ctrl(1) == 4) then
            s% max_years_for_timestep = 0 ! this is activated after the relax steps
         end if

      end function extras_check_model


      subroutine adjust_mdot(id, ierr)
         use star_def
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         real(dp) :: mdot, eta, A, C, Rl, alt, mdot_exp
         integer :: model_number_target = 0
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

         ! eta = s% x_ctrl(8)
         ! A = 1d-14
         ! C = 50d0/Rsun
         ! Rl = 24.5 !*Rsun
         ! mdot = eta*(s% photosphere_L*s% photosphere_r/s% photosphere_m)/(Lsun*Rsun/Msun) !L_phot already in Lsun units
         ! alt = A*exp(C*(s% photosphere_r-Rl))
         ! mdot_exp = min(mdot, alt)
         ! ! print*, 'mdot', mdot

         ! s% mstar_dot = -mdot*(msun/secyer)

         if (s% x_integer_ctrl(1) == 2) then
            if (s% doing_relax) return
            if (model_number_target == 0) model_number_target = s% model_number + 100 ! mass stripping activated 100 timesteps after dxdt_nuc_factor relax steps finished to allow model to settle
            if (s% model_number >= model_number_target .and. model_number_target /= 0) then
               s% mstar_dot = s% x_ctrl(9)*(msun/secyer)
            end if
         end if

      end subroutine adjust_mdot


      integer function how_many_extra_history_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_history_columns = 0
      end function how_many_extra_history_columns


      subroutine data_for_extra_history_columns(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
      end subroutine data_for_extra_history_columns


      integer function how_many_extra_profile_columns(id)
         use star_def, only: star_info
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_profile_columns = 0
      end function how_many_extra_profile_columns


      subroutine data_for_extra_profile_columns(id, n, nz, names, vals, ierr)
         use star_def, only: star_info, maxlen_profile_column_name
         use const_def, only: dp
         integer, intent(in) :: id, n, nz
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(nz,n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         integer :: k
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
      end subroutine data_for_extra_profile_columns


      ! returns either keep_going or terminate.
      integer function extras_finish_step(id)
         integer, intent(in) :: id
         integer :: ierr, k, start_of_He_shell_cell, start_of_H_shell_cell, cutting_cell
         real(dp) :: start_of_He_shell_xa_limit, start_of_H_shell_xa_limit, core_mass, &
                     start_of_H_shell_mass, He_shell_mass, H_shell_mass, H_shell_mass_limit, &
                     mass_left, time_over_to_lose = 31557600d4, starting_time = 0d0, percent_lose
         type (star_info), pointer :: s
         include 'formats'
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_finish_step = keep_going


         ! mass_loss_rate = 0d0
         ! if (s% x_integer_ctrl(1) == 2) then ! part2
         !    start_of_He_shell_xa_limit = 0.1d0   !The upper He mass fraction limit for start of He shell
         !    start_of_H_shell_xa_limit = 0.1d0    !The upper H mass fraction limit for start of H shell
         !    H_shell_mass_limit = 1d-2           !H shell mass as a fraction of the CO core mass
         !    core_mass = 0d0
         !    start_of_H_shell_mass = 0d0

         !    do k = s% nz, 1, -1
         !       if (s% Y(k) > start_of_He_shell_xa_limit) then
         !          core_mass = s% m(k)
         !          start_of_He_shell_cell = k
         !          exit
         !       end if
         !    end do

         !    do k = start_of_He_shell_cell, 1, -1
         !       if (s% X(k) > start_of_H_shell_xa_limit) then
         !          start_of_H_shell_mass = s% m(k)
         !          start_of_H_shell_cell = k
         !          exit
         !       end if
         !    end do

         !    do k = start_of_H_shell_cell, 1, -1
         !       if (((s% m(k)-start_of_H_shell_mass)/core_mass) > H_shell_mass_limit) then
         !          H_shell_mass = s% m(k) - start_of_H_shell_mass
         !          cutting_cell = k
         !          exit
         !       else if (k==1) then
         !          extras_finish_step = terminate
         !          return
         !       end if
         !    end do

         !    mass_left = 0d0
         !    do k = cutting_cell, 1, -1
         !       mass_left = mass_left + s% dm(k)
         !    end do

         !    ! if (starting_time == 0d0) starting_time = s% time
         !    ! percent_lose =  1 - ((s% time - starting_time)/time_over_to_lose)
         !    ! percent_lose = 1d-2
         !    ! if (percent_lose > 1d0) percent_lose = 1d0
         !    ! mass_loss_rate = (mass_left)*percent_lose
         !    mass_loss_rate = 1d23*s% dt
         !    if (s% mstar <= 2*1.9884098706980504d33) extras_finish_step = terminate
         !    ! print*, 'mass_loss_rate', -mass_loss_rate, 's% mstar_dot', mass_loss_rate/s% dt, 'percent_lose', percent_lose, 'mass_left', mass_left
         ! end if


         ! if (s% x_integer_ctrl(1) == 3 .and. .not. s% lxtra(1)) then ! part2
         !    start_of_He_shell_xa_limit = s% x_ctrl(4)    !The upper He mass fraction limit for start of He shell
         !    start_of_H_shell_xa_limit = s% x_ctrl(5)     !The upper H mass fraction limit for start of H shell
         !    H_shell_mass_limit = s% x_ctrl(6)            !H shell mass as a fraction of the CO core mass
         !    core_mass = 0d0
         !    start_of_H_shell_mass = 0d0

         !    do k = s% nz, 1, -1
         !       if (s% Y(k) > start_of_He_shell_xa_limit) then
         !          core_mass = s% m(k)
         !          start_of_He_shell_cell = k
         !          exit
         !       end if
         !    end do

         !    do k = start_of_He_shell_cell, 1, -1
         !       if (s% X(k) > start_of_H_shell_xa_limit) then
         !          start_of_H_shell_mass = s% m(k)
         !          start_of_H_shell_cell = k
         !          exit
         !       end if
         !    end do

         !    do k = start_of_H_shell_cell, 1, -1
         !       if (((s% m(k)-start_of_H_shell_mass)/core_mass) > H_shell_mass_limit .or. k==1) then
         !          H_shell_mass = s% m(k) - start_of_H_shell_mass
         !          cutting_cell = k
         !          exit
         !       end if
         !    end do

         !    write(*,2) 'call star_remove_surface_at_cell_k', cutting_cell, s% m(cutting_cell)/Msun
         !    call star_remove_surface_at_cell_k(s% id, cutting_cell, ierr)
         !    if (ierr /= 0) then
         !       write(*,*) 'failed in star_remove_surface_at_cell_k', cutting_cell
         !       extras_finish_step = terminate
         !       return
         !    end if
         !    s% lxtra(1) = .true.
         ! end if

      end function extras_finish_step


      ! subroutine remove_ejecta_one_cell_per_step(id, ierr, j)
      !    integer, intent(in) :: id
      !    integer, intent(out) :: ierr, j
      !    type (star_info), pointer :: s
      !    integer :: k, start_of_He_shell_cell, start_of_H_shell_cell, cutting_cell
      !    real(dp) :: start_of_He_shell_xa_limit, start_of_H_shell_xa_limit, core_mass, &
      !                start_of_H_shell_mass, He_shell_mass, H_shell_mass, H_shell_mass_limit
      !    include 'formats'
      !    ierr = 0
      !    call star_ptr(id, s, ierr)
      !    if (ierr /= 0) return

      !    if (s% x_integer_ctrl(1) == 3 .and. .not. s% lxtra(1)) then ! part2
      !       start_of_He_shell_xa_limit = s% x_ctrl(4)    !The upper He mass fraction limit for start of He shell
      !       start_of_H_shell_xa_limit = s% x_ctrl(5)     !The upper H mass fraction limit for start of H shell
      !       H_shell_mass_limit = s% x_ctrl(6)            !H shell mass as a fraction of the CO core mass
      !       core_mass = 0d0
      !       start_of_H_shell_mass = 0d0

      !       do k = s% nz, 1, -1
      !          if (s% Y(k) > start_of_He_shell_xa_limit) then
      !             core_mass = s% m(k)
      !             start_of_He_shell_cell = k
      !             exit
      !          end if
      !       end do

      !       do k = start_of_He_shell_cell, 1, -1
      !          if (s% X(k) > start_of_H_shell_xa_limit) then
      !             start_of_H_shell_mass = s% m(k)
      !             start_of_H_shell_cell = k
      !             exit
      !          end if
      !       end do

      !       do k = start_of_H_shell_cell, 1, -1
      !          if (((s% m(k)-start_of_H_shell_mass)/core_mass) > H_shell_mass_limit .or. k==1) then
      !             H_shell_mass = s% m(k) - start_of_H_shell_mass
      !             cutting_cell = k
      !             exit
      !          end if
      !       end do

      !       if (cutting_cell > 2) then
      !          print*, 'call star_remove_surface_at_cell_k', 'mass left', (s% mstar-s% m(cutting_cell)), 'mass left / mstar', (s% mstar-s% m(cutting_cell))/s% mstar
      !          call star_remove_surface_at_cell_k(s% id, 2, ierr)
      !          if (ierr /= 0) then
      !             write(*,*) 'failed in star_remove_surface_at_cell_k'
      !             return
      !          end if
      !       else
      !          s% lxtra(1) = .true.
      !       end if
      !    end if

      ! end  subroutine remove_ejecta_one_cell_per_step

      end module run_star_extras
