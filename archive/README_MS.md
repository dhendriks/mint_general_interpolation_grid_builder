Main Sequence Grid Builder
----------------------------------------------------------

Grid Builder Structure
----------------------------------------------------------
- MainSequenceTableBuilder in MS_table_builder.py builds an interpolation table from MESA output data.
- The MESA models are run and output data produced by MainSequenceMESATableBuilder in MS_MESA_gridbuilder.py which is a subclass of MainSequenceTableBuilder.
- MainSequenceTableBuilder is a subclass of NatalieTableBuilder which itself is a sublcass of the main TableBuilder
- The data extraction is multiprocessed to reduce computation time

Required settings
----------------------------------------------------------
To run MainSequenceTableBuilder you must provide a dictionary with the following settings:
- max_queue_size: maximum number of multiprocessing jobs to be in queue at any one time e.g. 10
- num_processes: number of cores used for multiprocessing e.g. 10
- metallicity: the initial metallcity e.g. 0.02
- table_name: filename for interpolation table (without .dat) e.g. grid_Z2.00e-02
- grid_directory: path to MESA grid with respect to home e.g. /ms_grid/Z0p02_grid_v3/
- eureka_version: version of eureka being used i.e. eureka or eureka2

Interpolation Variables
----------------------------------------------------------
The interpolation variables for the Main Sequence table are the total stellar mass, M, and the central hydrogen mass fraction, Xc. These are the first and second columns of the interpolation table respectively. This table is completely orthogonal in the interpolation variables.

1. Total Stellar Mass
- Mass loss is turned off during MESA evolution to keep the stellar mass constant
- The table contains masses in the approximate range 0.32-250Msun, equally spaced in log space
	- (A few MESA runs may fail due to computation difficulties and thus those masses do not make it into the interpolation table)

2. The central hydrogen mass fraction decreases as the model evolves along the main sequence
- The first value of Xc is that of PMS-epsilon, Xc,1 = Xc,zams - epsilon where epsilon = 0.0015 following advice from Aaron Dotter on MESA mailing list
- The final value of Xc is at TAMS e.g. Xc,final = 1e-6
- Rejuvenation can cause an increase in Xc, see below for discussion

Rejuvenation
----------------------------------------------------------
For masses in the approximate range 1.2-1.4 Msun, the convective core is unstable to growth. Rejuvenation can occur when the convective region rapidly increases in mass and mixes unprocessed, hydrogen rich into the core. This causes the central hydrogen mass fraction, Xc to suddenly increase. This is related to the transition between the pp chain and cno cycle in these low mass models.

Rejuvenation means that the central hydrogen mass fraction is not always decreasing. We use the MESA model data when Xc first reaches each target value.

Rejuvenation leads to error in the MS lifetime when it is integrated using the first derivative from the interpolation table. There are two solutions to this problem...

1. Prevent rejuvenation by NOT using convective premixing
	- This leads to a h exhausted core with approx half the mass at TAMS
2. Reduce the MS lifetime integration error by fitting the Xc-time relationship with a signoid during rejuvenation
	- See 1.2M_rejuvenation_solutions.pdf
	- Increasing the Xc resolution for the relevant range in Xc reduces the error further (if fitting with a signoid)


Description of Scalar Quantities:
---------------------------------
MASS
Stellar mass in solar units. Stellar mass is a lookup parameter.  Mass loss
effects have been turned off for the stellar evolution so that mass is
constant.  In the original grid, the mass range covered is 0.32 to 100 Msun,
with logarithmic steps
(in MESA, this is the "mass" variable).

CENTRAL_HYDROGEN
Central mass fraction in hydrogen-1. It is a lookup parameter, serving as a
proxy for time as it decreases monotonically along the main-sequence evolution.
In the original grid, it goes from 0.6985 to 1e-6, for a ZAMS/envelope hydrogen
mass fraction of 0.7
(in MESA, this is the "center_h1" variable).

RADIUS
Stellar radius in solar units, taken at the photosphere (defined as optical
depth tau=2/3).
(in MESA, this is the "photosphere_r" variable)

LUMINOSITY
Stellar luminosity in solar units
(in MESA, this is the "photosphere_L" variable).

AGE
Model age, when evolved as a single star. The ZAMS age is subtracted, so that
the age printed in the grid should be 0 for the maximum value of the central
hydrogen.
Note that age is not a lookup parameter of the grid: the proxy for
time spent on the MS is now the central h1 mass fraction (column 2). The age is
output in order to allow for comparisons with Hurley et al. (2000) and other
prescriptions.

CONVECTIVE_CORE_MASS
Relative convective core mass, i.e. core mass to stellar mass ratio (where
stellar mass can be found in column 1). This is computed based on the
Ledoux stability criterion in which we remove the impact of near surface
ionization zones (this is done by assigning to the stellar outer 2% in radius
the value at the bottom of the layer). If the central point of the model is
Ledoux-unstable, then the mass of the outer boundary of that convective
zone is saved. If the central point is Ledoux-stable then the value is
set to 0, if the model is fully convective then it is set to the stellar mass.

CONVECTIVE_CORE_RADIUS
Relative convective core radius, i.e. core radius to stellar photosphere radius
ratio (where stellar radius can be found in column 3). Just like the relative
convective core mass, it is based on the Ledoux criterion with
near-surface layers ignored. If the central point of the model is
Ledoux-unstable, then the radius of the outer boundary of that
convective zone is saved. If the central point is Ledoux-stable then the
value is set to 0, if the model is fully convective then it is set to the
stellar radius.

CONVECTIVE_CORE_MASS_OVERSHOOT
Similar to CONVECTIVE_CORE_MASS, including core overshooting.

CONVECTIVE_CORE_RADIUS_OVERSHOOT
Similar to CONVECTIVE_CORE_RADIUS, including core overshooting.

CONVECTIVE_ENVELOPE_MASS
Relative mass included in the convective envelope (where stellar mass can be
found in column 1).  Just like the relative convective core mass, it is based
on the Ledoux criterion with near-surface layers ignored. If the surface
point of the model is Ledoux-unstable, then the mass of that convective
zone is saved.  If the surface is Ledoux-stable then the value is set to
0, if the model is fully convective then it is set to the stellar mass (just
like the core equivalent).

CONVECTIVE_ENVELOPE_RADIUS
Relative radius to stellar photosphere radius ratio evaluated at the bottom of
the convective envelope (where stellar radius can be found in column 3).  Just
like the relative convective core mass, it is based on the Ledoux
criterion with near-surface layers ignored.  If the surface point of the model
is Ledoux-unstable, then the radius of the inner boundary of that
convective zone is saved. If the surface point is Ledoux-stable then the
value is set to 1, if the model is fully convective then it is set to 0. Note
that this is different from the envelope mass estimate

CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED
Relative mass included in the convective envelope, i.e. envelope mass to
stellar mass ratio, estimated from a simplified Ledoux criterion.  The Ledoux
criterion is simplified in order to neglect small (semi)convective zones: for a
given profile, if a (semi)convective zone appears in between two radiative
zones and its mass is less than a threshold, it is set to "radiative" to assess
the convective envelope mass. The threshold is set to 1% of the total mass.
Changes in stability in the top layers are ignored: layers above a given mass
coordinate are set to the stability of that layer. The relative mass coordinate
out of which we ignore changes in the Ledoux stability criterion is 0.997 if
M<=100Msun, and 0.995 for M>100Msun.  If the star is fully convective, this is
equal to the stellar mass, if the surface is radiative it is equal to 0.

CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED
Relative radius at the base of the convective radius, i.e. radius to stellar
photosphere radius ratio evaluated at the bottom of the convective envelope,
using the same simplification as for CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED.
If the surface point is Ledoux-stable then the
value is set to 1, if the model is fully convective then it is set to 0. Note
that this is different from the envelope mass estimate above.

K2
Apsidal constant k2, calculated assuming a spherical non-rotating star
(in MESA, this is the "apsidal_constant_k2").

TIDAL_E2
Tidal dissipation factor E2 from Zahn (1975), for stars with a radiative envelope.

TIDAL_E_FOR_LAMBDA
Tidal dissipation factor E from Zahn (1989), for stars with a convective envelope.
This coefficient is meant to be multiplied by integrals of the radius into the
lambda01 and lambda22 coefficients (this operation is done analytically in binary_c).

TIMESCALE_KELVIN_HELMHOLTZ
Kelvin-Helmholtz timescale (in years). As defined by GM^2/(2RL)
(in MESA, this is the "kh_timescale").

TIMESCALE_DYNAMICAL
Dynamical timescale (in years). As defined by 2*pi*sqrt(r^3/(G*m))
(in MESA, this is the "dynamic_timescale").

TIMESCALE_NUCLEAR
Nuclear timescale (in years)
(in MESA, this is the "nuclear_timescale").

FIRST_DERIVATIVE_CENTRAL_HYDROGEN
dX/dt, first derivative of the central h1 mass fraction with respect to time
(in yr^-1). To ensure consistency between gridpoints, and to get rid of the
dependence on the stellar evolution timestep, it is computed by fitting a
polynomial function to the X-t function near the point where the gridpoint is
saved.

SECOND_DERIVATIVE_CENTRAL_HYDROGEN
d^2 X/ dt^2 second derivative of the central h1 mass fraction with respect to
time (in yr^-2). This is yet to be added. It is also obtained from the
polynomial fit used for the first derivative.


Description of Vector Quantities:
---------------------------------------
Vector quantities give the structure of the stellar interior. These are essential for nucleosynthesis calculations in binary_c. They are interpolated on a Chebyshev grid, that can be denser near the core, the surface, or both, for a given number of points. The number of points is given by the column numbers assigned to each quantity and the mass coordinates of these points is given in the header.

The Chebyshev-interpolated quantities are:

CHEBYSHEV_TEMPERATURE
Temperature inside the star, in Kelvin.

CHEBYSHEV_DENSITY
Density inside the star, in cgs units (g cm^-3)

CHEBYSHEV_TOTAL_PRESSURE
Pressure inside the star, including both gas (electron and ions) and radiation pressures, in cgs units.

CHEBYSHEV_GAS_PRESSURE
Gas pressure inside the star, in cgs units. The ratio of CHEBYSHEV_GAS_PRESSURE to CHEBYSHEV_TOTAL_PRESSURE yields the beta coefficient.

CHEBYSHEV_RADIUS
Radius inside the star, in solar radius units.


Chebyshev Interpolation Mass Coordinates:
-----------------------------------------
After the list of quantities present in the table, one has to specify the mass coordinates where vector quantities are estimated.
It should be introduced with the line
'# CHEBYSHEV_GRID'
and the list of relative mass coordinates should be ordered in ascending order from 0 (stellar center) to 1 (stellar surface), introduced by a hash and whitespace.

# CHEBYSHEV_GRID                           m values for the interpolations (in units of stellar mass)
# +0.000000000000000E+00 ... +9.368943686873262E-01 +9.526570100284418E-01 +9.684314502351893E-01 +1.000000000000000E+00
#
