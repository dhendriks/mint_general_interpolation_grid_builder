#!/usr/bin/python3

import os
import json

import numpy as np

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner import (
    MESAGridRunner,
)


class PostProcessingMesaGridRunner(MESAGridRunner):
    def __init__(self, settings):

        self.evol_phase = settings["evol_phase"]
        settings["grid_directory"] = os.path.join(
            settings["grids_root_directory"],
            "Z{}/{}".format(
                settings["metallicity"],
                self.evol_phase,
            ),
        )

        MESAGridRunner.__init__(self, settings=settings)

        original_grid_settings_filepath = os.path.join(
            settings["MESA_CME_grid_directory"],
            "Z{}/{}/{}".format(
                settings["metallicity"],
                self.evol_phase,
                "MINT_Z%7.2e_%s_settings.json"
                % (float(self.settings["metallicity"]), self.evol_phase),
            ),
        )
        original_grid_settings = json.load(open(original_grid_settings_filepath))
        settings["photos_save_directory"] = original_grid_settings[
            "MESA_output_directory"
        ]
        self.settings = {**original_grid_settings, **settings}

        return

    def decide_masses_dict(self):
        masses_dict = {0.08: 0.08, 1.13: 1.13}

        print(" Masses to run = \n", masses_dict)

        self.masses_dic = masses_dict

        return

    def pre_MESA_grid_writing_hook_extra(self):

        self.settings["inlists"][f"inlist_{self.evol_phase}"]["controls"] = {
            **self.settings["inlists"][f"inlist_{self.evol_phase}"]["controls"],
            "log_directory": f"'LOGS_{self.evol_phase}_target_num'",
            "photo_directory": f"'photos_{self.evol_phase}_target_num'",
            "dxdt_nuc_factor": "0",
        }

        return

    def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):

        # add inlist for restarts
        self.inlists_dict["inlist"] = self.inlists_dict[f"inlist_{self.evol_phase}"]

        copy_to_path = os.path.join(self.final_directory, "photos")
        os.makedirs(copy_to_path, exist_ok=True)

        this_file = os.path.abspath(__file__)
        run_filepath = os.path.join(
            this_file.split("post_processing_grid_builder")[0],
            "functions/run_accretion_response.py",
        )

        photo_dir = self.get_photos_directory(mass=mass)
        print(photo_dir)
        photos_list = np.sort(
            [int(p[1:]) for p in os.listdir(photo_dir) if p[0] == "t"]
        )
        photos_list = ["t" + str(p).rjust(3, "0") for p in photos_list]

        script = ""

        for photo in photos_list[:5]:
            photo_path = os.path.join(photo_dir, photo)

            script += """
    cp {} {}
    sed -i 's/target_num/{}/g' inlist
    ./re {}
    sed -i 's/{}/target_num/g' inlist

""".format(
                photo_path, copy_to_path, photo, photo, photo
            )

        self.settings["rn_script"] = script

        return

    def get_photos_directory(self, mass):
        suffix = "Z{}/{}/{}/photos_{}".format(
            str(self.settings["metallicity"]),
            self.evol_phase,
            str(mass),
            self.evol_phase,
        )
        return os.path.join(self.settings["photos_save_directory"], suffix)


if __name__ == "__main__":
    MesaGridRunner_instance = PostProcessingMesaGridRunner(settings={})
