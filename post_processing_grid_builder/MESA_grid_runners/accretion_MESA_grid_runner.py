#!/usr/bin/python3

import os
import numpy as np

from mint_general_interpolation_grid_builder.post_processing_grid_builder.PostProcessingMESAGridRunner import (
    PostProcessingMesaGridRunner,
)
from MESAGridRunner.src.functions.inlist_writing import (
    find_and_replace_text,
    write_inlists,
)
from mint_general_interpolation_grid_builder.MINT.config.mint_inlists import inlists
from mint_general_interpolation_grid_builder.eureka.check_nodes import (
    check_for_bad_nodes,
)


class AccretionMESATableBuilder(PostProcessingMesaGridRunner):
    def __init__(self, settings):

        PostProcessingMesaGridRunner.__init__(self, settings=settings)

        return


if __name__ == "__main__":

    # required for University of Surrey eureka HPC
    bad_nodes = check_for_bad_nodes()
    node_list = ",".join(bad_nodes)

    settings = {
        #################################
        # system properties
        "grids_root_directory": "/users/nr00492/MINT_accretion_response",
        "MESA_CME_grid_directory": "/users/nr00492/MINT_grids_23051",
        "workload_manager": "slurm",
        "auto_submit": False,
        "archive_previous_grid": False,  # moves existing grid with same metallicity to archive
        #################################
        # email notifications
        "email_notifications_enabled": False,
        "email_notifications_APP_password": os.getenv("MINT_NOTIFICATION_APP_PW"),
        "email_notifications_recipients": ["nr00492@surrey.ac.uk"],
        ##################################
        # MESA setup
        "module_name": "accretion",
        "mesa_version": "23051",
        "metallicity": 0.02,
        "num_cores_for_MESA": 12,  # number of processes used by MESA
        "MESA_output_directory": "/users/nr00492/parallel_scratch/MINT_accretion_response",
        "caches_dir": "/users/nr00492/parallel_scratch/caches/agb_grid_23051",
        "MESA_setup_code": "export MESA_DIR=/users/nr00492/mesa-r23.05.1\nmodule load mesasdk/22.6.1",
        "use_jermyn22_conv_pen_MS": True,
        ################################
        # slurm options
        "slurm_partition": "shared",
        "MESA_run_max_hours": 168,
        "extra_sbatch_commands": f"#SBATCH --exclusive=user\n#SBATCH --mem=10GB\n#SBATCH --exclude={node_list}",
        ################################
        # other
        "save_photos_at_grid_points": False,
        "nice_priority": 0,
        "restart_unfinished_runs": False,
    }

    evol_phase = "MS"
    inlist_list = ["inlist_common", f"inlist_{evol_phase}"]

    settings = {
        **settings,
        "evol_phase": evol_phase,
        "inlists": {inlist_name: inlists[inlist_name] for inlist_name in inlist_list},
    }

    MesaGridRunner_instance = AccretionMESATableBuilder(settings=settings)

    MesaGridRunner_instance.write_and_run_MESA_grid()
