"""
test script for david
"""

import os
import json

import numpy as np

from mint_general_interpolation_grid_builder.MINT.src.run_grid_runners_and_table_builders import (
    run_grid,
)
from mint_general_interpolation_grid_builder.MINT.config.mint_inlists import inlists
from mint_general_interpolation_grid_builder.MINT.config.mint_settings import (
    mint_defaults,
)
from mint_general_interpolation_grid_builder.eureka.check_nodes import (
    check_for_bad_nodes,
)

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    set_control_value_in_all_inlists,
)


run_mesa = True
build_interpolation_tables = False


#############
# Configure
MESA_MINT_data_root = os.path.join(os.getenv("PROJECT_DATA_ROOT"), "MESA_MINT_data")

grid_config = {
    **mint_defaults,
    "grids_root_directory": os.path.join(
        MESA_MINT_data_root, "MESA_output", "david_test_grid"
    ),
    "MESA_output_directory": os.path.join(
        MESA_MINT_data_root, "MESA_output", "david_test_grid_mesa"
    ),
    "caches_dir": os.path.join(MESA_MINT_data_root, "caches", "MINT"),
    # "extra_sbatch_commands": f"#SBATCH --exclusive=user\n#SBATCH --mem=10GB\n#SBATCH --exclude={node_list}",
    "initial_masses": [1],
    "save_photos_at_grid_points": True,
    "tabulation_setup_code": "",
    "workload_manager": "multiprocessing",
    "MESA_setup_code": "export MESA_DIR=/home/david/projects/MESA/mesa-r23.05.1; export MESASDK_ROOT=/home/david/projects/MESA/mesasdk; source $MESASDK_ROOT/bin/mesasdk_init.sh",  # TODO: call to load mesa sdk
}


print(inlists.keys())
print(json.dumps(inlists["inlist_MS"], indent=4))


# print(json.dumps(inlists['inlist_GB'], indent=4))


# print(json.dumps(grid_config, indent=4))
quit()

################
# set inlists
grid_config["inlists"] = inlists

grid_config["metallicity_directory"] = os.path.join(
    grid_config["grids_root_directory"], "Z" + str(grid_config["metallicity"])
)

################
# set MS grid runner and run
grid_config["inlists"]["inlist_MS"]["controls"]["delta_XH_cntr_limit"] = "0.05d0"

# add table columns to config
grid_config = {**grid_config, **table_columns}

#############################################
# MS:
if True:
    ms_config = {
        **grid_config,
    }

    print("Main Sequence")

    ms_config["grid_directory"] = os.path.join(
        grid_config["metallicity_directory"], "MS"
    )

    # Handle MESA
    if run_mesa:

        # Set up object
        MS_MESA_grid_builder = MainSequenceGridRunner(settings=ms_config)

        # Run mesa
        MS_MESA_grid_builder.write_and_run_MESA_grid()

    # Build interpolation table
    if build_interpolation_tables:

        if ms_config["workload_manager"] == "slurm":
            slurm_path = os.path.join(ms_config["grid_directory"], "slurm_tabulate.sub")
            os.system("sbatch {}".format(slurm_path))
        else:
            # Set up object
            MS_interpolation_grid_builder = MainSequenceTableBuilder(settings=ms_config)

            # Build interpolation table
            MS_interpolation_grid_builder.build_interpolation_grid()
