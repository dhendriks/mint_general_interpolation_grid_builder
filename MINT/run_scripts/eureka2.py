"""
To run MINT grid
"""

import os

import numpy as np

from mint_general_interpolation_grid_builder.MINT.src.run_grid_runners_and_table_builders import (
    run_grid,
)
from mint_general_interpolation_grid_builder.MINT.config.mint_inlists import inlists
from mint_general_interpolation_grid_builder.MINT.config.mint_settings import (
    mint_defaults,
)
from mint_general_interpolation_grid_builder.eureka.check_nodes import (
    check_for_bad_nodes,
)

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    set_control_value_in_all_inlists,
)


def mint_grid(config, inlists):
    """
    Example grid. This function controls which phases are executed and with which settings this happens
    """

    # The input config should contain some global configuration, and probably default value for all the parameters

    # We then use this configuration and update it to reflect the settings that we want to use in this specific grid.
    grid_config = {
        **config,
        # for eureka
        # "grids_root_directory": "/users/nr00492/MINT_grids_23051",
        # "MESA_output_directory": "/users/nr00492/parallel_scratch/MINT_grids_23051",
        # "caches_dir": "/users/nr00492/parallel_scratch/caches/MINT",
        # for eureka2
        "grids_root_directory": "/parallel_scratch/nr00492/MINT_grids",
        "MESA_output_directory": "/parallel_scratch/nr00492/MINT_grids",
        "caches_dir": "/parallel_scratch/nr00492/caches/MINT",
        "tabulation_setup_code": "source /users/nr00492/mint/bin/activate\nexport PYTHONPATH='${PYTHONPATH}:/users/nr00492'",
        "MESA_setup_code": """
export MESA_DIR=/users/nr00492/mesa-r23.05.1
module load mesasdk/22.6.1
source /users/nr00492/mint/bin/activate
export PYTHONPATH='${PYTHONPATH}:/users/nr00492'
""",
        #  "archive_previous_grid":True,
    }

    # for eureka
    # bad_nodes = check_for_bad_nodes()
    # node_list = ",".join(bad_nodes)
    # grid_config["extra_sbatch_commands"] = f"#SBATCH --exclusive=user\n#SBATCH --mem=10GB\n#SBATCH --exclude={node_list}"

    ####################################################
    # MESA inlists

    # edit inlist options
    # note mass and metallicity are handled automatically

    # Method 1. target a specific inlist e.g.
    inlists["inlist_EAGB"]["controls"] = {
        **inlists["inlist_EAGB"]["controls"],
        "use_other_wind": ".false.",
    }

    # Method 2. replace all occurences of chosen control in inlists
    inlists = set_control_value_in_all_inlists(
        inlists=inlists, control_name="superad_reduction_diff_grads_limit", value="1d-3"
    )

    grid_config["inlists"] = inlists

    # Execute the grid with the appropriate control flags
    run_grid(
        grid_config=grid_config,
        stellar_type_handle_dict={
            #'MS':True,
            #'TAMS_MASS_CHANGE': True,
            # "GB": True,
            "CHeB": True,
            #'EAGB':True,
            #'TPAGB':True,
        },
        run_mesa=True,
        # build_interpolation_tables=True,
    )


if __name__ == "__main__":
    mint_grid(config=mint_defaults, inlists=inlists)
