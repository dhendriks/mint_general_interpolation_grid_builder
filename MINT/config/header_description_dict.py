"""
File containing the general header description dictionary

Each parameter (can/must) have the following properties:
- "desc": description of parameter
- "unit": astropy-based physical unit
- "mesa_history_dependency": dependency parameters in the mesa history files
- "mesa_profile_dependency": dependency parameters in the mesa profile files
- "exclude_from_test": (optional) flag to exclude from test. Assumed to be False if absent. Use to disable testing
- "exclude_from_test_phase_specific": (optional) list of stellar phases where we want to exclude this parameter from testing.
- "diff_threshold_default": testing difference threshold default value. Not required if "exclude_from_test" is True.
- "diff_function": function used to calculate the difference. Not required if "exclude_from_test" is True.
- "threshold_dict": (optional) dictionary to specify custom threshold values for a specific stellar phase. Overrides "diff_threshold_default".
- "transform_function": (optional) function applied to the data before the difference is calculated. This only parameter to this function should be the array of values.
"""

import astropy.units as u
import numpy as np

# header scalars
header_description_dict_scalars = {
    "MASS": {
        "desc": "total stellar mass",
        "unit": u.Msun,
        "mesa_history_dependency": ["star_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CENTRAL_HYDROGEN": {
        "desc": "central hydrogen mass fraction Xc",
        "plot_scale": "linear",
        "mesa_history_dependency": ["center h1"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CENTRAL_HELIUM": {
        "desc": "central helium mass fraction XHe",
        "plot_scale": "linear",
        "mesa_history_dependency": ["center he4"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CENTRAL_CARBON": {
        "desc": "central carbon-12 mass fraction XC",
        "plot_scale": "linear",
        "mesa_history_dependency": ["center c12"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CENTRAL_OXYGEN": {
        "desc": "central oxygen-16 mass fraction XO",
        "plot_scale": "linear",
        "mesa_history_dependency": ["center o16"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CENTRAL_NEON": {
        "desc": "central neon-20 mass fraction XNe",
        "plot_scale": "linear",
        "mesa_history_dependency": ["add_center_abundances"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.05,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_CORE_MASS_FRACTION": {
        "desc": "fractional He core mass (Mc/M), Xc<1d-2",
        "plot_scale": "linear",
        "mesa_history_dependency": ["he_core_mass", "star_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.05,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HYDROGEN_EXHUASTED_CORE_MASS_FRACTION": {
        "desc": "fractional H exhuasted core mass (Mc/M), Xc<1d-6",
        "plot_scale": "linear",
        "mesa_history_dependency": ["star_mass", "he_core_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "INTERSHELL_MASS_FRACTION": {
        "desc": "He-rich intershell mass as a fraction of core mass",
        "plot_scale": "linear",
        "mesa_history_dependency": ["co_core_mass", "he_core_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "AGE": {
        "desc": "model age",
        "unit": u.yr,
        "plot_scale": "log",
        "mesa_history_dependency": ["star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "AGE_FRACTION": {
        "desc": "model age fraction",
        "unit": u.yr,
        "plot_scale": "log",
        "mesa_history_dependency": ["star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "RADIUS": {
        "desc": "Photospheric radius",
        "unit": u.Rsun,
        "plot_scale": "log",
        "mesa_history_dependency": ["log_R"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.2,
        "diff_function": "abs_frac_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "LOG_RADIUS": {
        "desc": "log10 Photospheric radius",
        "unit": u.Rsun,
        "plot_scale": "log",
        "mesa_history_dependency": ["log_R"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.2,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "LUMINOSITY": {
        "desc": "photosphere luminosity",
        "unit": u.Lsun,
        "plot_scale": "log",
        "mesa_history_dependency": ["log_L"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.2,
        "diff_function": "abs_frac_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "LOG_LUMINOSITY": {
        "desc": "log10 photosphere luminosity",
        "unit": u.Lsun,
        "plot_scale": "log",
        "mesa_history_dependency": ["log_L"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.2,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "NEUTRINO_LUMINOSITY": {
        "desc": "power emitted in neutrinos, nuclear and thermal",
        "unit": u.Lsun,
        "plot_scale": "log",
        "mesa_history_dependency": ["log_Lneu"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "LUMINOSITY_DIV_EDDINGTON_LUMINOSITY": {
        "desc": "luminosity divided by the eddington luminosity",
        "plot_scale": "log",
        "mesa_history_dependency": ["log_L_div_Ledd"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HYDROGEN_LUMINOSITY": {
        "desc": "total thermal power from hydrogen burning",
        "unit": u.Lsun,
        "mesa_history_dependency": ["log_LH"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_LUMINOSITY": {
        "desc": "total thermal power from triple-alpha, excluding neutrinos",
        "unit": u.Lsun,
        "mesa_history_dependency": ["log_LHe"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CENTRAL_DEGENERACY": {
        "desc": "the electron chemical potential at the central mesh point, in units of k*T",
        "unit": u.joule,
        "plot_scale": "linear",
        "mesa_history_dependency": ["center_degeneracy"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_CORE_RADIUS_FRACTION": {
        "desc": "relative core radius (Rcore/Rstar)",
        "mesa_history_dependency": ["he_core_radius", "star_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CARBON_CORE_MASS_FRACTION": {
        "desc": "co core mass (Msun)",
        "plot_scale": "linear",
        "mesa_history_dependency": ["co_core_mass", "star_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 0.05,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CARBON_CORE_RADIUS_FRACTION": {
        "desc": "co core mass (Msun)",
        "plot_scale": "linear",
        "mesa_history_dependency": ["co_core_radius", "log_R"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CARBON_TO_HELIUM_CORE_MASS_FRACTION": {
        "desc": "co core mass/ he core mass ratio",
        "mesa_history_dependency": ["co_core_mass", "he_core_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "INTERSHELL_MASS": {
        "desc": "he core mass - co core mass (Msun)",
        "unit": u.Msun,
        "mesa_history_dependency": ["co_core_mass", "he_core_mass", "star_mass"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_CORE_MASS_FRACTION": {
        "desc": "relative convective core mass (Mcore/Mstar)",
        "plot_scale": "log",
        "mesa_history_dependency": ["mass_conv_core", "star_mass"],
        "mesa_profile_dependency": ["radius", "ledoux_stable", "mass"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_CORE_RADIUS_FRACTION": {
        "desc": "relative convective core radius (Rcore/Rstar)",
        "plot_scale": "log",
        "mesa_history_dependency": [
            "mass_conv_core",
            "radius",
            "conv_mx1_top",
            "he_core_mass",
            "mixing_regions 10",
            "mix_relr_regions 10",
            "conv_mx1_top_r",
            "conv_mx2_top_r",
        ],
        "mesa_profile_dependency": ["radius", "ledoux_stable", "mass"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION": {
        "desc": "relative mass cord of main conevctive region in core including overshooting & semiconvection mixing",
        "plot_scale": "log",
        "mesa_history_dependency": [
            "mixing_regions 10",
            "mix_relr_regions 10",
            "mx1_top",
            "mx2_top",
        ],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION": {
        "desc": "relative radius cord of main conevctive region in core including overshooting & semiconvection mixing",
        "plot_scale": "log",
        "mesa_history_dependency": [
            "mixing_regions 10",
            "mix_relr_regions 10",
            "mx1_top_r",
            "mx2_top_r",
        ],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_ENVELOPE_MASS_FRACTION": {
        "desc": "relative convective envelope mass (Menv/Mstar)",
        "plot_scale": "log",
        "mesa_history_dependency": [
            "conv_mx1_top",
            "conv_mx2_top",
            "conv_mx1_bot",
            "conv_mx2_bot",
        ],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_ENVELOPE_RADIUS_FRACTION": {
        "desc": "relative convective envelope radius (bottom envelope coordinate) (Renv/Rstar)",
        "plot_scale": "log",
        "mesa_history_dependency": [
            "conv_mx1_top_r",
            "conv_mx2_top_r",
            "conv_mx1_bot_r",
            "conv_mx2_bot_r",
        ],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_ENVELOPE_MASS_TOP_FRACTION": {
        "desc": "relative mass coord of top of convective envelope  (Menv,top/Mstar)",
        "mesa_history_dependency": ["conv_mx1_top", "conv_mx2_top"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_ENVELOPE_RADIUS_TOP_FRACTION": {
        "desc": "relative radius coord of top of convective envelope (Renv,top/Rstar)",
        "mesa_history_dependency": ["conv_mx1_top_r", "conv_mx2_top_r"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED_FRACTION": {
        "desc": "relative envelope mass from simplified Ledoux (Mcore/Mstar)",
        "plot_scale": "log",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["radius", "mass", "gradr", "gradL"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED_FRACTION": {
        "desc": "relative envelope radius from simplified Ledoux (Rcore/Rstar)",
        "plot_scale": "log",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["radius", "mass", "gradr", "gradL"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "TIMESCALE_KELVIN_HELMHOLTZ": {
        "desc": "Kelvin-Helmholtz timescale",
        "unit": u.yr,
        "plot_scale": "log",
        "mesa_history_dependency": ["kh_timescale"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "TIMESCALE_DYNAMICAL": {
        "desc": "Dynamical timescale",
        "unit": u.yr,
        "plot_scale": "log",
        "mesa_history_dependency": ["dynamic_timescale"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "TIMESCALE_NUCLEAR": {
        "desc": "Nuclear timescale",
        "unit": u.yr,
        "plot_scale": "log",
        "mesa_history_dependency": ["nuc_timescale"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "MEAN_MOLECULAR_WEIGHT_CORE": {
        "desc": "mean molecular weight at central mesh point",
        "plot_scale": "log",
        "mesa_history_dependency": ["center_mu"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "MEAN_MOLECULAR_WEIGHT_AVERAGE": {
        "desc": "mean molecular weight average through star",
        "plot_scale": "log",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["mu", "mass"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_CENTRAL_HYDROGEN": {
        "desc": "dXc/dt",
        "unit": 1 / u.yr,
        "plot_scale": "symlog",
        "mesa_history_dependency": ["center h1", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_CENTRAL_HYDROGEN": {
        "desc": "d2Xc/dt2",
        "unit": 1 / (u.yr * u.yr),
        "plot_scale": "symlog",
        "mesa_history_dependency": ["center h1", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_CENTRAL_DEGENERACY": {
        "desc": "first derivative of the electron chemical potential at the central mesh point with respect to time",
        "unit": u.joule / u.yr,
        "plot_scale": "symlog",
        "mesa_history_dependency": ["center_degeneracy", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_frac_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_CENTRAL_DEGENERACY": {
        "desc": "second derivative of the electron chemical potential at the central mesh point with respect to time",
        "unit": u.joule / (u.yr * u.yr),
        "plot_scale": "symlog",
        "mesa_history_dependency": ["center_degeneracy", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_CENTRAL_HELIUM": {
        "desc": "dYcore/dt",
        "unit": 1 / u.yr,
        "mesa_history_dependency": ["center he4", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_CENTRAL_HELIUM": {
        "desc": "d2Ycore/dt2",
        "unit": 1 / (u.yr * u.yr),
        "mesa_history_dependency": ["center he4", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_CENTRAL_CARBON": {
        "desc": "dXCcore/dt",
        "unit": 1 / u.yr,
        "mesa_history_dependency": ["center c12", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_CENTRAL_CARBON": {
        "desc": "d2XCcore/dt2",
        "unit": 1 / (u.yr * u.yr),
        "mesa_history_dependency": ["center c12", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_CENTRAL_OXYGEN": {
        "desc": "dXOcore/dt",
        "unit": 1 / u.yr,
        "mesa_history_dependency": ["center o16", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_CENTRAL_OXYGEN": {
        "desc": "d2XOcore/dt2",
        "unit": 1 / (u.yr * u.yr),
        "mesa_history_dependency": ["center o16", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION": {
        "desc": "first derivative of the helium core mass fraction with respect to time",
        "unit": 1 / u.yr,
        "mesa_history_dependency": ["he_core_mass", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_frac_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_HELIUM_CORE_MASS_FRACTION": {
        "desc": "second derivative of the helium core mass fraction with respect to time",
        "unit": 1 / (u.yr * u.yr),
        "mesa_history_dependency": ["he_core_mass", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "FIRST_DERIVATIVE_CARBON_CORE_MASS_FRACTION": {
        "desc": "first derivative of the carbon core mass fraction with respect to time",
        "unit": 1 / u.yr,
        "mesa_history_dependency": ["co_core_mass", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_frac_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "SECOND_DERIVATIVE_CARBON_CORE_MASS_FRACTION": {
        "desc": "second derivative of the carbon core mass fraction with respect to time",
        "unit": 1 / (u.yr * u.yr),
        "mesa_history_dependency": ["co_core_mass", "star_age"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "K2": {
        "desc": "apsidal constant NOT THE moment of inertia factor BEWARE",
        "plot_scale": "log",
        "mesa_history_dependency": ["apsidal_constant_k2"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "TIDAL_E2": {
        "desc": "tidal E2 from Zahn",
        "plot_scale": "log",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [
            "radius",
            "ledoux_stable",
            "mass",
            "gradr",
            "gradL",
            "logRho",
            "logP",
            "brunt_N2",
        ],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "TIDAL_E_FOR_LAMBDA": {
        "desc": "tidal E for Zahns lambda",
        "plot_scale": "log",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [
            "radius",
            "ledoux_stable",
            "mass",
            "gradr",
            "gradL",
            "logRho",
            "logP",
        ],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "MOMENT_OF_INERTIA_FACTOR": {
        "desc": "beta^2 from Claret AA 541, A113 (2012)= I/(MR^2)",
        "plot_scale": "log",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["mass", "radius", "logRho"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_CORE_MOMENT_OF_INERTIA_FACTOR": {
        "desc": "beta^2 from Claret AA 541, A113 (2012)= I/(MR^2) up to the helium core boundary",
        "plot_scale": "log",
        "mesa_history_dependency": ["he_core_k"],
        "mesa_profile_dependency": ["mass", "radius", "logRho"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "CARBON_CORE_MOMENT_OF_INERTIA_FACTOR": {
        "desc": "beta^2 from Claret AA 541, A113 (2012)= I/(MR^2) up to the carbon core boundary",
        "plot_scale": "log",
        "mesa_history_dependency": ["co_core_k"],
        "mesa_profile_dependency": ["mass", "radius", "logRho"],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "DELTA_CENTRAL_HELIUM": {
        "desc": "change in central helium mass fraction XHe between two points e.g. for degenerate ignition",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "DELTA_AGE": {
        "desc": "change in stellar age (yrs) between two points e.g. for degenerate ignition",
        "unit": u.yr,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "DELTA_HELIUM_CORE_MASS": {
        "desc": "change in helium core mass (Msun) between two points e.g. for degenerate ignition",
        "unit": u.Msun,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "WARNING_FLAG": {
        "desc": "warning flag for binary_c, flag = 1 if data warning, flag = 0 if data reliable",
        "plot_scale": "linear",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_IGNITED_FLAG": {
        "desc": "flag for the ignition of helium, = 1 if core helium burning started",
        "plot_scale": "linear",
        "mesa_history_dependency": ["center he4"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_IGNITION_PARAMETER": {
        "desc": "log10 of the helium luminosity divided by the max helium luminsoity reached at helium ignition",
        "plot_scale": "linear",
        "mesa_history_dependency": ["log_LHe"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "HELIUM_FLASH_FLAG": {
        "desc": "flag for future or past helium flash, 1 = Helium flash, 0 = no Helium flash",
        "plot_scale": "linear",
        "mesa_history_dependency": ["center_degeneracy"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "fAE": {
        "desc": "flag for the ignition of neon, = 1 if core neon burning started",
        "plot_scale": "linear",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "ONSET_THERMAL_PULSES_FLAG": {
        "desc": "flag for the onset of thermal pulses, = 1 if pulses started",
        "plot_scale": "linear",
        "mesa_history_dependency": ["log_LHe"],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
    "INITIAL_MASS": {
        "desc": "initial mass used to form model, allows reconstruction of tracks",
        "plot_scale": "linear",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": [],
        # Testing config
        # 'exclude_from_test': True,
        # 'exclude_from_test_phase_specific': []
        "threshold_default": 1,
        "diff_function": "abs_frac_diff",
        # 'threshold_dict': {'MS': 1, "GB": 1},
        # 'transform_function': np.log10,
    },
}

# header vectors
header_description_dict_vectors = {
    "CHEBYSHEV_MASS": {
        "desc": "mass on Chebyshev grid",
        "unit": u.Msun,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["mass"],
    },
    "CHEBYSHEV_TEMPERATURE": {
        "desc": "temperature on Chebyshev grid",
        "unit": u.K,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["logT"],
    },
    "CHEBYSHEV_DENSITY": {
        "desc": "density on Chebyshev grid",
        "unit": u.g / (u.cm ** 3),
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["logRho"],
    },
    "CHEBYSHEV_TOTAL_PRESSURE": {
        "desc": "total pressure on Chebyshev grid",
        "unit": u.dyne / (u.cm ** 2),
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["logP"],
    },
    "CHEBYSHEV_GAS_PRESSURE": {
        "desc": "gas pressure on Chebyshev grid",
        "unit": u.dyne / (u.cm ** 2),
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["pgas"],
    },
    "CHEBYSHEV_RADIUS": {
        "desc": "radius on Chebyshev grid",
        "unit": u.Rsun,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["radius"],
    },
    "CHEBYSHEV_GAMMA1": {
        "desc": "adiabatic Gamma1 on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["gamma1"],
    },
    "CHEBYSHEV_PRESSURE_SCALE_HEIGHT": {
        "desc": "pressure scale height on Chebyshev grid",
        "unit": u.Rsun,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["pressure_scale_height"],
    },
    "CHEBYSHEV_DIFFUSION_COEFFICIENT": {
        "desc": "Eulerian diffusion coefficient on Chebyshev grid",
        "unit": u.cm ** 2 / u.s,
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["log_D_mix"],
    },
    "CHEBYSHEV_HYDROGEN_MASS_FRACTION": {
        "desc": "hydrogen-1 mass fraction on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["h1"],
    },
    "CHEBYSHEV_HELIUM_MASS_FRACTION": {
        "desc": "helium-4 mass fraction on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["he4"],
    },
    "CHEBYSHEV_DELTA_HELIUM": {
        "desc": "change in helium mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["he4"],
    },
    "CHEBYSHEV_DELTA_CARBON": {
        "desc": "change in carbon mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["c12"],
    },
    "CHEBYSHEV_DELTA_NITROGEN": {
        "desc": "change in nitrogen mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["n14"],
    },
    "CHEBYSHEV_DELTA_OXYGEN": {
        "desc": "change in oxygen mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["o16"],
    },
    "CHEBYSHEV_DELTA_FLUORINE19": {
        "desc": "change in fluorine19 mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["f19"],
    },
    "CHEBYSHEV_DELTA_CARBON13": {
        "desc": "change in carbon13 mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["c13"],
    },
    "CHEBYSHEV_DELTA_NITROGEN15": {
        "desc": "change in nitrogen15 mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["n15"],
    },
    "CHEBYSHEV_DELTA_OXYGEN17": {
        "desc": "change in oxygen17 mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["o17"],
    },
    "CHEBYSHEV_DELTA_OXYGEN18": {
        "desc": "change in oxygen18 mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["o18"],
    },
    "CHEBYSHEV_DELTA_NEON22": {
        "desc": "change in neon22 mass fraction between two profiles on Chebyshev grid",
        "mesa_history_dependency": [],
        "mesa_profile_dependency": ["ne22"],
    },
}

if __name__ == "__main__":
    print(
        header_description_dict_scalars["FIRST_DERIVATIVE_CENTRAL_HYDROGEN"][
            "unit"
        ].unit
    )
