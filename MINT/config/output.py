from typing import Any


def format_logs_foldername(evol_phase: str, initial_mass: Any = "Mi") -> str:
    if evol_phase == "MS":
        return "LOGS_MS"
    else:
        return f"LOGS_{evol_phase}_{initial_mass}"


def format_photos_foldername(evol_phase: str, initial_mass: Any = "Mi") -> str:
    if evol_phase == "MS":
        return "photos_MS"
    else:
        return f"photos_{evol_phase}_{initial_mass}"


def format_save_model_filename(evol_phase: str, mass: Any, initial_mass: Any) -> str:
    save_model_filename_dict = {
        "MS": f"{mass}_TAMS.mod",
        "MC": f"{mass}_MC_Mi_{initial_mass}.mod",
        "GB": f"{mass}_ZACHeB_Mi_{initial_mass}.mod",
        "CHeB": f"{mass}_TACHeB_Mi_{initial_mass}.mod",
        "EAGB": f"{mass}_TPAGB_Mi_{initial_mass}.mod",
    }
    return save_model_filename_dict[evol_phase]
