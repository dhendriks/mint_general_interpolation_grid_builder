import numpy as np
import os

mint_defaults = {
    #################################
    # system properties
    "grids_root_directory": "/users/nr00492/MINT_grids_23051",
    # "grids_root_directory": "/users/nr00492/MINT_grids_test",
    "workload_manager": "slurm",
    "auto_submit": False,
    "archive_previous_grid": False,  # moves existing grid with same metallicity to archive
    "binary_grid": True,
    #################################
    # email notifications
    "email_notifications_enabled": False,
    "email_notifications_APP_password": os.getenv("MINT_NOTIFICATION_APP_PW"),
    "email_notifications_recipients": ["nr00492@surrey.ac.uk"],
    ##################################
    # MESA setup
    "module_name": "mint",
    "mesa_version": "23051",
    "metallicity": 0.02,
    "initial_masses": list(
        np.append(
            np.arange(0.08, 2.6, 0.03),
            np.logspace(np.log10(2.6), np.log10(1000), num=100),
        )
    ),
    "num_cores_for_MESA": 6,  # number of processes used by MESA
    "MESA_output_directory": "/users/nr00492/parallel_scratch/MINT_grids_23051",
    "caches_dir": "/users/nr00492/parallel_scratch/caches/MINT",
    "MESA_setup_code": "export MESA_DIR=/users/nr00492/mesa-r23.05.1\nmodule load mesasdk/22.6.1",
    "use_jermyn22_conv_pen_MS": True,
    ################################
    # slurm options
    "slurm_partition": "shared",
    "MESA_run_max_hours": 168,
    "extra_sbatch_commands": "",
    "tabulation_setup_code": "module load anaconda3/4.3.1\nsource /users/nr00492/mint/bin/activate",
    ################################
    # other
    "save_photos_at_grid_points": False,
    "nice_priority": 0,
    "restart_unfinished_runs": False,
    #################################
    # for multiprocessing during table building
    "max_queue_size": 10,
    "max_result_queue_size": 10,
    "num_processes": 20,
    #################################
    # plotting and post-processing
    "plot_all_models": False,
    "handle_track_intersection": False,
}
