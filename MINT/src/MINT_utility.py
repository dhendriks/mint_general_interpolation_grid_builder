#!/usr/bin/python3

import os
from pathlib import Path
import numpy as np

from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.cheby import (
    cheby_points,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.functions.ms_routines import (
    chemical_composition,
    central_hydrogen_range_to_compute,
)
from mint_general_interpolation_grid_builder.MINT.config.time_proxy_targets.GB import (
    targets as GB_targets,
)
from mint_general_interpolation_grid_builder.MINT.config.time_proxy_targets.CHeB import (
    targets as CHeB_targets,
)
from mint_general_interpolation_grid_builder.MINT.config.time_proxy_targets.EAGB import (
    targets as EAGB_targets,
)

"""
class for MINT utility shared between grid runner and table builder
"""


class MINTUtility:
    def __init__(self, settings) -> None:

        self.home = str(Path.home())

        self.evol_phase_list = ["MS", "GB", "CHeB", "EAGB", "TPAGB"]

        if self.evol_phase is not None:

            self.previous_evol_phase = self.evol_phase_list[
                self.evol_phase_list.index(self.evol_phase) - 1
            ]

            print("setting saved_models_directory")
            self.saved_models_directory = os.path.join(
                self.settings["grid_directory"], "saved_models"
            )
            Path(self.saved_models_directory).mkdir(parents=True, exist_ok=True)

            self.prev_evol_phase_saved_models_directory = os.path.join(
                os.path.dirname(self.settings["grid_directory"]),
                self.previous_evol_phase,
                "saved_models",
            )

            self.set_cheby_coords()

            self.scalar_input_columns = settings[
                f"{self.evol_phase}_scalar_input_columns"
            ]
            self.scalar_output_columns = settings[
                f"{self.evol_phase}_scalar_output_columns"
            ]
            self.vector_output_columns = settings["vector_output_columns"]

        # set save targets
        if self.settings.get("metallicity", None) is not None:
            self.set_save_targets()

        return

    def set_save_targets(self):
        ############################################################
        # set save targets for each evol phase

        # set h targets list for MS
        dic = {"zams_z": self.settings["metallicity"]}
        dic = chemical_composition(dictionary=dic)

        self.time_proxy_save_targets_all_phases = {
            "MS": np.flip(
                np.unique(
                    [
                        self.format_table_value(t)
                        for t in central_hydrogen_range_to_compute(dic["zams_x"])
                    ]
                )
            ),
            "GB": GB_targets,
            "CHeB": np.flip(CHeB_targets),
            "EAGB": EAGB_targets,
            "TPAGB": np.arange(1, 10, 1),
            "MC": [1],
        }

        self.time_proxy_save_targets = self.time_proxy_save_targets_all_phases[
            self.evol_phase
        ]
        self.time_proxy_column_name = self.scalar_input_columns[1]
        self.time_proxy_save_targets_list_str = (
            "(/"
            + str([round(i, 15) for i in self.time_proxy_save_targets])[1:-1]
            + "/)"
        )

        return

    def set_cheby_coords(self):
        # set chebyshev relative mass coordinates for each evol phase

        if self.evol_phase == "MS":
            self.cheby_coords = cheby_points(0, 1, N=100)
        else:
            self.cheby_coords = np.append(
                cheby_points(0, 1, N=50), cheby_points(1, 2, N=50)
            )

        self.vector_parameter_length = len(self.cheby_coords)

        # print(self.cheby_coords)
        return

    def cheby_points(self, a, b, N=50):
        """
        Function call to cheby point calculation
        """

        return cheby_points(a=a, b=b, N=N)

    def format_table_value(self, value):

        if isinstance(value, (float)):
            formatted_table_value = float("%.4g" % (value))
        elif isinstance(value, (int)):
            formatted_table_value = value
        elif isinstance(value, (list, np.ndarray)):
            formatted_table_value = np.array(
                [self.format_table_value(val) for val in value]
            )
        else:
            raise ValueError(
                "Unexpected table quanitity type for: ", value, ", type: ", type(value)
            )

        return formatted_table_value


if __name__ == "__main__":
    instance = MINTUtility(settings={})
