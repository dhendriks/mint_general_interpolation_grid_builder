#!/usr/bin/python3

import os
import sys
import re
import json
import multiprocessing
import math

from pathlib import Path
import numpy as np
import pandas as pd
import mesaPlot as mp
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from scipy.integrate import cumulative_trapezoid
from scipy.interpolate import interp1d


from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder import (
    MINTTableBuilder,
)


class EarlyAsymptoticGiantBranchTableBuilder(MINTTableBuilder):
    def __init__(self, settings):

        self.evol_phase = "EAGB"

        # Init mixin classes
        MINTTableBuilder.__init__(self, settings=settings)

        # grid description
        self.name = "EAGB"
        self.longname = "Early-Asymptotic Giant Branch"
        self.description = "Testing"

    def check_table_specific(self, table_df):
        """
        Function to do specific check of the table
        """

        return table_df

    def create_interpolation_table_hook(self, df):

        # df_remapped = self.remap_data_to_orthogonal_grid_parallel(df=df)
        df_remapped = df

        df_checked = self.check_table(table_df=df_remapped)

        # add table to MINT tables dict
        self.MINT_tables = {
            self.format_interpolation_table_output_filename(): df_checked
        }

        return


if __name__ == "__main__":

    # Get the command line argument containing the JSON string
    json_str = sys.argv[1]

    # Load the JSON string into a dictionary
    my_dict = json.loads(json_str)

    grid_builder = EarlyAsymptoticGiantBranchTableBuilder(settings=my_dict)

    grid_builder.build_interpolation_grid()
