#!/usr/bin/python3

import numpy as np
import pandas as pd
import multiprocessing
from scipy.interpolate import griddata


class InterpolationCoordinateRemapping:
    def __init__(self):

        self.delta_Mc_div_M = 0.001
        self.time_proxy_skip_factor = 1

        pass

    def remap_data_to_orthogonal_grid(self, df, varX, varY, multiprocesses=False):

        x_grid, y_grid = np.meshgrid(
            df[varX].unique()[:: self.time_proxy_skip_factor],
            np.arange(
                df[varY].min(),
                df[varY].max() + self.delta_Mc_div_M,
                self.delta_Mc_div_M,
            ),
        )
        df_remapped = pd.DataFrame()
        unique_mass_values = df["MASS"].unique()[:]

        if multiprocesses:
            with multiprocessing.Pool(processes=self.settings["num_processes"]) as pool:
                results = pool.starmap(
                    self.remap_data_to_orthogonal_grid_mass,
                    [
                        (M, df[df["MASS"] == M], varX, varY, x_grid, y_grid)
                        for M in unique_mass_values
                    ],
                )
        else:
            results = []
            for M in unique_mass_values:
                results += [
                    self.remap_data_to_orthogonal_grid_mass(
                        M, df[df["MASS"] == M], varX, varY, x_grid, y_grid
                    )
                ]

        df_remapped = pd.concat(results)

        return df_remapped

    def remap_data_to_orthogonal_grid_mass(self, M, df_sec, varX, varY, x_grid, y_grid):
        # print('Remapping mass = ', M)

        if varX == 'CENTRAL_DEGENERACY':
            #  Curved boundary of tracks can lead to odd extrapolation effects so duplicate lowest core mass track to slightly lower core mass
            track = df_sec.groupby([varX]).first().reset_index()
            track.loc[:, varY] = track[varY].min()
            if "WARNING_FLAG" in df_sec.columns:
                track.loc[:, "WARNING_FLAG"] = 0
            df_sec = pd.concat([df_sec, track]).sort_values([varX, varY]).reset_index(drop=True)

        points = df_sec[[varX, varY]].values
        data = np.column_stack(
            (np.full(len(x_grid.flatten()), M), x_grid.flatten(), y_grid.flatten())
        )

        output_columns = [
            col for col in df_sec.columns if col not in ["MASS", varX, varY]
        ]
        scalar_output_columns = [col for col in output_columns if col[:4] != "CHEB"]
        vector_output_columns = [col for col in output_columns if col[:4] == "CHEB"]

        for quantity in scalar_output_columns:
            values = df_sec[quantity].values
            remapped_data, nan_mask = self.griddata_interpolation(
                points, values, x_grid, y_grid
            )
            data = np.column_stack((data, remapped_data))

        data = np.column_stack((data, nan_mask))

        df_mass_remapped = pd.DataFrame(
            data,
            columns=["MASS", varX, varY]
            + scalar_output_columns
            + ["EXTRAPOLATION_FLAG"],
        )

        for quantity in vector_output_columns:
            cheby_quantity_2D_array = np.stack(df_sec["CHEBYSHEV_MASS"].values)
            cheby_quantity_data = np.empty((len(nan_mask), 0))

            for cheby_index in range(cheby_quantity_2D_array.shape[-1]):
                values = cheby_quantity_2D_array[:, cheby_index]
                remapped_data, nan_mask = self.griddata_interpolation(
                    points, values, x_grid, y_grid
                )
                cheby_quantity_data = np.column_stack(
                    (cheby_quantity_data, remapped_data)
                )

            df_mass_remapped[quantity] = list(cheby_quantity_data)

        df_mass_remapped = df_mass_remapped.sort_values(by=["MASS", varX, varY])

        return df_mass_remapped

    def griddata_interpolation(self, points, values, x_grid, y_grid):

        # Use scipy.interpolate.griddata to interpolate the data onto the grid
        interpolated_values = griddata(
            points, values, (x_grid, y_grid), method="linear", rescale=False
        )

        # Create a mask for nan values
        nan_mask = np.isnan(interpolated_values)

        # Nearest-neighbor extrapolation
        interpolated_points = np.column_stack(
            (x_grid[~nan_mask].flatten(), y_grid[~nan_mask].flatten())
        )
        extrapolated_values = griddata(
            interpolated_points,
            interpolated_values[~nan_mask].flatten(),
            (x_grid, y_grid),
            method="nearest",
        )

        # Format values correctly
        remapped_values = np.array(
            [self.format_table_value(t) for t in extrapolated_values.flatten()]
        )

        return remapped_values, nan_mask.flatten()
