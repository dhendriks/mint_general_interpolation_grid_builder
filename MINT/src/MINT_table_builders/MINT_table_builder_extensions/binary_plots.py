#!/usr/bin/python3
"""
Binary plots class extension for InterpolationTableBuilder
"""

import os

from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from PyPDF2 import PdfMerger
import mesaPlot as mp

from mint_general_interpolation_grid_builder.functions_natalie.custom_mpl_settings import (
    load_mpl_rc,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.derivatives import (
    integrate_euler_2D,
)
from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)


class BinaryPlots:
    def __init__(self):
        pass

    def binary_history_file_plots(self):

        print("\nPlotting interpolation variables and HR diagrams")

        # set up
        m = mp.MESA()

        ###################################################
        # plots of interpolation variables

        x_label = self.scalar_input_columns[2]
        y_label = self.scalar_input_columns[1]

        # set directory for plots
        plots_dir = os.path.join(
            self.settings["grid_directory"], "plots/interpolation_variables"
        )
        Path(plots_dir).mkdir(parents=True, exist_ok=True)

        # plot for each final masses
        for job_dict in self.job_list:
            Mt = job_dict["total_mass"]
            # print(Mt)
            fig, ax = plt.subplots()
            ax.set(xlabel=x_label, ylabel=y_label)
            # loop through initial masses
            for Mi in job_dict["initial_masses"].keys():
                if job_dict["initial_masses"][Mi]["use_model"] == True:

                    # load history file
                    target_locs = job_dict["initial_masses"][Mi]["target_locs"]
                    try:
                        m, direc = self.load_history_file(Mt=Mt, Mi=Mi)
                    except:
                        continue

                    # set interp variables
                    y = m.hist.interp_variable_hit[target_locs]

                    ax.plot(
                        (m.hist.he_core_mass / m.hist.star_mass)[target_locs],
                        y,
                        label=f"{Mi}",
                    )

            # # add lines for c core mass = 1.05 and 1.2 Msun
            # if self.evol_phase == 'EAGB':
            #     x = np.linspace(0.1,1)
            #     ax.plot(x,1.05/(Mt*x), color = 'black', label='C ignition')
            #     ax.plot(x,1.37/(Mt*x),color='black',label= 'O ignition')
            #     ax.set_ylim(self.target_eagb[0],self.target_eagb[-1])

            ax.legend(title=f"M = {Mt}", loc="center left", bbox_to_anchor=(1, 0.5))
            plt.savefig(f"{plots_dir}/{Mt}.pdf", dpi=200)
            plt.close()

        self.combine_plots_into_multiplot_pdf(
            list_of_values=[job_dict["total_mass"] for job_dict in self.job_list],
            direc=plots_dir,
        )

        #######################################################
        # HR diagram

        plots_dir = os.path.join(self.settings["grid_directory"], "plots/HR_diagram")
        Path(plots_dir).mkdir(parents=True, exist_ok=True)

        # plot for each final masses
        for job_dict in self.job_list:
            Mt = job_dict["total_mass"]
            # print(Mt)
            fig, ax = plt.subplots()
            ax.set(xlabel="log_Teff", ylabel="log_L")
            # loop through initial masses
            for Mi in job_dict["initial_masses"].keys():
                if job_dict["initial_masses"][Mi]["use_model"] == True:
                    target_locs = job_dict["initial_masses"][Mi]["target_locs"]
                    try:
                        m, direc = self.load_history_file(Mt=Mt, Mi=Mi)
                    except:
                        continue
                    ax.plot(
                        m.hist.log_Teff[target_locs],
                        m.hist.log_L[target_locs],
                        label=f"{Mi}",
                    )

            ax.invert_xaxis()
            ax.legend(title=f"M = {Mt}", loc="center left", bbox_to_anchor=(1, 0.5))
            plt.savefig(f"{plots_dir}/{Mt}.pdf", dpi=200)
            plt.close()

        self.combine_plots_into_multiplot_pdf(
            list_of_values=[job_dict["total_mass"] for job_dict in self.job_list],
            direc=plots_dir,
        )

        return

    def integrate_to_check_stellar_age_resolved(self):
        # integrate stellar age for each stellar track
        # plot comparison of integration and mesa time

        print("plotting time integration calculations")

        file_save_dir = os.path.join(
            self.settings["grid_directory"], "plots/time_integration"
        )
        Path(file_save_dir).mkdir(parents=True, exist_ok=True)

        # set integration variables
        X_name = self.scalar_input_columns[1]
        dXdt_name = "FIRST_DERIVATIVE_" + X_name
        Z_name = "HELIUM_CORE_MASS_FRACTION"
        dZdt_name = "FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION"

        # values of delta_t to use
        delta_t_div = 0.0005

        # for calculating the mean error
        total_error = 0

        # calculate integration error for each mass
        for t in self.tracks:

            df_sec = t["df"]
            filename = t["filename"]
            label = filename.split("/")[-1]
            Mt, Mi = label.split("_")
            print(f"{Mt}, {Mi}")

            # extract quantities from dataframe
            no_warning = np.array(df_sec["WARNING_FLAG"]) == 0
            mesa_time = np.array(df_sec["AGE"])[no_warning]
            dX_dt = np.array(df_sec[dXdt_name])[no_warning]
            X = np.array(df_sec[X_name])[no_warning]
            dMc_dt = np.array(df_sec[dZdt_name])[no_warning]
            Mc = np.array(df_sec[Z_name])[no_warning]

            # ensure first point is at time = 0
            mesa_time = mesa_time - mesa_time[0]

            # set integration timestep
            delta_t = mesa_time[-1] * delta_t_div
            # integrate using forward euler
            int_time, int_Mc = integrate_euler_2D(X, dX_dt, delta_t, dMc_dt)
            int_Mc = int_Mc + Mc[0]
            # calculate error at end of MS
            time_error = round(
                abs((int_time[-1] - mesa_time[-1]) / mesa_time[-1] * 100), 2
            )
            Mc_error = round(abs((int_Mc[-1] - Mc[-1]) / Mc[-1] * 100), 2)

            # add error for smallest timestep to total error
            total_error += time_error

            ############################################################
            # plot time integration and compare to MESA time using smallest timestep for each mass
            fig, ax = plt.subplots(1, 2, sharey=True)

            ax[0].scatter(mesa_time, X, label="mesa time")
            ax[0].scatter(int_time, X, label="integrated")
            ax[0].legend(title=f"Mt = {Mt}, Mi = {Mi},\n$\%$ error = {time_error}")

            ax[1].scatter(Mc, X, label="mesa")
            ax[1].scatter(int_Mc, X, label="integrated")
            ax[1].legend(title=f"$\%$ error = {Mc_error}")

            ax[0].set_ylabel(X_name)
            ax[0].set_xlabel("Time (yrs)")
            ax[1].set_xlabel(Z_name)
            ax[1].yaxis.set_label_coords(1.05, 0.5)
            ax[1].yaxis.tick_right()
            plt.savefig(os.path.join(file_save_dir, f"{label}.pdf"), dpi=200)
            # ax.set_xscale("log")
            # plt.savefig(os.path.join(file_save_dir, f"{label}_log.pdf"), dpi=200)
            plt.close()

        ###################################################
        # combine all time integration plots into one pdf
        # Set up the pdf stuff
        merger = PdfMerger()
        pdf_page_number = 0

        labels = [t["filename"].split("/")[-1] for t in self.tracks]
        for label in labels:
            output_name = os.path.join(file_save_dir, f"{label}.pdf")
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger=merger,
                pdf_filename=output_name,
                page_number=pdf_page_number,
                bookmark_text=label,
            )

        # wrap up the pdf
        combined_pdf_filename = file_save_dir + "_all.pdf"
        merger.write(combined_pdf_filename)
        merger.close()

        return
