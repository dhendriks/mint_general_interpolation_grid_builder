#!/usr/bin/python3
"""
MESA model extraction class extension for InterpolationTableBuilder
"""
import os
import math

import mesaPlot as mp
import numpy as np
import matplotlib.pyplot as plt
from PyPDF2 import PdfMerger
from pathlib import Path

from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)


class MESAModelExtraction:
    def __init__(self):
        pass

    ##################################################################################
    # Extracting MESA data

    def get_job_list(self, masses_dict):

        print("Retrieving job list from available MESA runs")

        self.job_list = []

        rse_file = os.path.join(
            self.settings["grid_directory"], "src/run_star_extras.f90"
        )
        with open(rse_file, "r") as file:
            for line_number, line in enumerate(file, 1):
                if "save_targets = " in line:
                    target_vals = np.array(
                        [float(t) for t in line.split("/")[-2].split(",")]
                    )

        print("\nTime proxy targets = ", target_vals)

        for Mt in masses_dict:

            print("Getting M = ", Mt)

            # dictionary to store info for each job
            job_dict = {"total_mass": Mt, "initial_masses": {}}

            for Mi in masses_dict[Mt]:

                job_dict["initial_masses"][Mi] = {"use_model": False}

                ###################################################
                # load history file

                try:
                    m, direc = self.load_history_file(Mt=Mt, Mi=Mi)
                except:
                    # if can't load history file set no_LOGS to True and skip
                    # print("M = " + str(Mt) + " does not exist")
                    job_dict["initial_masses"][Mi]["no_LOGS"] = True
                    continue

                ###################################################
                # set interp variables

                if self.evol_phase == "MS":
                    interp_variable = m.hist.center_h1
                    # target_vals = self.target_h
                elif self.evol_phase == "GB":
                    interp_variable = m.hist.center_degeneracy
                    # target_vals = self.target_eta
                elif self.evol_phase == "CHeB":
                    interp_variable = m.hist.center_he4
                    # target_vals = self.target_he
                elif self.evol_phase == "EAGB":
                    # interp_variable = m.hist.co_core_mass/m.hist.he_core_mass
                    interp_variable = m.hist.center_degeneracy
                    # target_vals = self.target_eagb

                ####################################################
                # get target locs

                # find indexes of m.hist for interp variable targets
                try:
                    target_locs = self.find_target_locs(m=m, evol_phase=self.evol_phase)
                    job_dict["initial_masses"][Mi]["target_locs"] = target_locs
                except:
                    raise ValueError(
                        f"Failed to find time-proxy targets for M = {Mt}, Mi = {Mi}"
                    )

                # case for less than two target_locs
                if len(target_locs) < 2:
                    job_dict["initial_masses"][Mi]["no_evolve"] = True
                    job_dict["initial_masses"][Mi][
                        "last_interp_variable_val"
                    ] = interp_variable[-1]
                    continue

                #########################################################
                # check for evol phase specific issues

                if self.evol_phase == "MS":
                    # check model has non-zero he core mass at TAMS
                    if m.hist.he_core_mass[target_locs[-1]] == 0:
                        # job_dict["no_he_core"] = job_dict["no_he_core"] + [{"initial_mass": Mi}]
                        job_dict["initial_masses"][Mi]["no_he_core"] = True
                        continue

                # if self.evol_phase == 'GB':
                # check model reaches he ignition
                # if m.hist.center_he4[-1] > 1-self.settings['metallicity']-0.001:
                #     job_dict["initial_masses"][Mi]["no_evolve"] = True
                #     job_dict["initial_masses"][Mi]["last_interp_variable_val"] = interp_variable[-1]
                #     continue

                # check track starts at TAMS
                # if m.hist.center_h1[target_locs[0]]<1e-7:
                #     continue

                ##############################################################
                if self.settings["plot_all_models"]:

                    #####################################################
                    # plot time proxy with targets
                    self.plot_save_targets(
                        m=m,
                        Mi=Mi,
                        Mt=Mt,
                        target_locs=target_locs,
                        interp_variable=interp_variable,
                    )

                    ####################################################
                    # plot conv regions
                    self.plot_conv_regions(m=m, Mi=Mi, Mt=Mt)

                #########################################################################
                # check all targets have been hit for phases with same targets vals

                if self.evol_phase in ["MS"]:
                    if len(target_locs) != len(target_vals):
                        if len(target_locs) < len(target_vals):
                            job_dict["initial_masses"][Mi]["no_evolve"] = True
                            job_dict["initial_masses"][Mi][
                                "last_interp_variable_val"
                            ] = interp_variable[-1]
                        else:
                            job_dict["initial_masses"][Mi][
                                "too_many_target_locs"
                            ] = True
                        continue

                job_dict["initial_masses"][Mi]["use_model"] = True

            self.job_list.append(job_dict)

        if self.settings["plot_all_models"]:
            ###########################################################
            # combine plots into one pdf
            save_targets_file_save_dir = os.path.join(
                self.plots_directory, "save_targets"
            )
            self.combine_plots(
                file_save_dir=save_targets_file_save_dir, masses_dict=masses_dict
            )
            conv_regions_file_save_dir = os.path.join(
                self.plots_directory, "conv_regions"
            )
            self.combine_plots(
                file_save_dir=conv_regions_file_save_dir, masses_dict=masses_dict
            )

        return

    def get_masses_list_from_available_directories(self):
        masses = []

        # read out masses from logs_directory
        # print(self.settings['logs_directory'])
        for direc in os.listdir(self.settings["logs_directory"]):
            # print(direc)
            try:
                masses += [float(direc)]
            except:
                None

        masses = sorted(masses)

        return masses

    def get_binary_masses_dict_from_available_directories(self, masses):
        # looks in logs directory for available models in binary grid

        masses_dict = {}
        for mass in masses:
            logs_dir = os.path.join(self.settings["logs_directory"], str(mass))
            final_masses = []
            for direc in os.listdir(logs_dir):
                # print(direc)
                try:
                    final_masses.append(float(direc.split("_")[-1]))
                except:
                    None
            masses_dict[mass] = sorted(set(final_masses))

        return masses_dict

    def load_history_file(self, Mt, Mi=0):

        m = mp.MESA()

        direc = os.path.join(
            self.settings["logs_directory"], str(Mt), f"LOGS_{self.evol_phase}"
        )

        if self.settings["binary_grid"]:
            if self.evol_phase == "MC":
                direc = direc + f"_{Mt}Mf"
            elif self.evol_phase in ["GB", "CHeB", "EAGB"]:
                direc = direc + f"_{Mi}"

        m.loadHistory(f=direc)

        # check more than one data entry
        if len(m.hist.model_number) == 1:
            raise ValueError

        return m, direc

    def plot_save_targets(self, m, Mi, Mt, target_locs, interp_variable):

        save_targets_file_save_dir = os.path.join(self.plots_directory, "save_targets")
        Path(save_targets_file_save_dir).mkdir(parents=True, exist_ok=True)

        fig, ax = plt.subplots()
        ax.plot(m.hist.star_age, interp_variable, label=f"Mi = {Mi}, Mt = {Mt}")
        if self.evol_phase in ["GB"]:
            ax.scatter(
                m.hist.star_age[target_locs],
                np.log10(m.hist.interp_variable_hit[target_locs]),
            )
        else:
            ax.scatter(
                m.hist.star_age[target_locs], m.hist.interp_variable_hit[target_locs]
            )
        ax.set_xlabel("Time (yrs)")
        ax.set_ylabel("Interpolation Time Proxy")
        ax.legend(title="Z = {}".format(self.settings["metallicity"]))
        plot_name = os.path.join(save_targets_file_save_dir, f"{Mi}_{Mt}.pdf")
        plt.savefig(plot_name, dpi=200)
        plt.close()

        return

    def plot_conv_regions(self, m, Mi, Mt):

        conv_regions_file_save_dir = os.path.join(self.plots_directory, "conv_regions")
        Path(conv_regions_file_save_dir).mkdir(parents=True, exist_ok=True)

        fig, ax = plt.subplots()
        type_dict = {0: "-", 1: "--", 2: "-."}
        color_dict = {0: "b", 1: "r", 2: "g"}
        name_dict = {0: "no mixing", 1: "conv", 2: "overshoot"}

        for i, t in enumerate(m.hist.star_age):
            boundary_bottom = 0
            for j in range(1, 11):
                mix_type = m.hist[f"mix_type_{j}"][i]
                boundary_top = m.hist[f"mix_qtop_{j}"][i]
                if mix_type != -1:
                    ax.vlines(
                        x=t,
                        ymin=boundary_bottom,
                        ymax=boundary_top,
                        linestyles=type_dict[mix_type],
                        colors=color_dict[mix_type],
                    )
                boundary_bottom = boundary_top

        # add core mass
        ax.plot(
            m.hist.star_age,
            m.hist.he_core_mass / m.hist.star_mass,
            color="black",
            label="he core mass",
        )
        ax.plot(
            m.hist.star_age,
            m.hist.hydrogen_exhausted_core_mass / m.hist.star_mass,
            color="blue",
            label="hydrogen exhausted core mass",
        )
        if "co_core_mass" in m.hist.keys():
            ax.plot(
                m.hist.star_age,
                m.hist.co_core_mass / m.hist.star_mass,
                label="c core mass",
            )
        ax.set_xlabel("Time")
        ax.set_ylabel("Relative Mass")
        ax.set_title(
            "Z = {}, Mi = {}, Mt = {}".format(self.settings["metallicity"], Mi, Mt)
        )
        ax.legend()

        plot_name = os.path.join(conv_regions_file_save_dir, f"{Mi}_{Mt}.pdf")
        plt.savefig(plot_name, dpi=200)
        plt.close()

        return

    @staticmethod
    def find_target_locs(m, evol_phase):

        if evol_phase in ["MS", "CHeB", "EAGB"]:
            target_locs = list(np.where(m.hist.interp_variable_hit != 0)[0])

        elif evol_phase == "GB":

            # only include targets after TAMS relaxation phase
            max_eta_loc = np.argmax(m.hist.center_degeneracy)
            min_eta_loc = np.argmin(m.hist.center_degeneracy[: max_eta_loc + 1])
            target_locs = [
                t
                for t in np.where(m.hist.interp_variable_hit != 0)[0]
                if t >= min_eta_loc
            ]
            degen_values = m.hist.interp_variable_hit[target_locs]

            # remove first duplicate value
            duplicate = np.where(np.diff(degen_values) == 0)[0]
            target_locs = np.delete(target_locs, duplicate)

            # remove targets after Lmax if He-flash (sudden change in luminosity not interpolated well)
            index_Lmax = np.where(m.hist.log_L > np.max(m.hist.log_L) - 0.2)[0][-1]
            if (np.max(m.hist.center_degeneracy) > 5) & (
                m.hist.center_he4[-1] < np.max(m.hist.center_he4) - 0.001
            ):
                target_locs = [t for t in target_locs if t <= index_Lmax]

            # # remove targets with zero core mass
            # zero_core_mass = np.where(m.hist.hydrogen_exhausted_core_mass[target_locs]==0)[0]
            # target_locs = np.delete(target_locs,zero_core_mass)

        return target_locs

    @staticmethod
    def read_profiles_index(m, directory, profiles_index_num=4):
        # reads profiles.index to find model numbers for interp variable targets
        model_nums = []
        with open(directory + "profiles.index", "r") as profiles_index:
            profiles_index.readline()
            for line in profiles_index.readlines():
                if int(line.split()[1]) == profiles_index_num:
                    model_nums += [line.split()[0]]

        indexes = []
        for n in model_nums:
            indexes += [np.where(m.hist.model_number == int(n))[0][0]]

        return indexes

    @staticmethod
    def combine_plots(file_save_dir, masses_dict):
        merger = PdfMerger()
        pdf_page_number = 0

        for Mt in masses_dict:
            for Mi in masses_dict[Mt]:
                output_name = os.path.join(file_save_dir, f"{Mi}_{Mt}.pdf")
                if os.path.exists(output_name):
                    merger, pdf_page_number = add_pdf_and_bookmark(
                        merger=merger,
                        pdf_filename=output_name,
                        page_number=pdf_page_number,
                        bookmark_text=f"{Mi}_{Mt}",
                    )

        # wrap up the pdf
        combined_pdf_filename = file_save_dir + "_all.pdf"
        merger.write(combined_pdf_filename)
        merger.close()

        return

    ##################################################################################
    # Table info output

    def print_models_in_grid_info(self):

        ###########################################
        # print good models
        print("\nMODELS IN GRID")

        for job_dict in self.job_list:
            Mt = job_dict["total_mass"]
            for Mi in job_dict["initial_masses"].keys():
                if job_dict["initial_masses"][Mi]["use_model"] == True:
                    print(f"{Mt} {Mi}")

        ##########################################
        # print bad models

        print("\nNO LOGS")
        for job_dict in self.job_list:
            Mt = job_dict["total_mass"]
            for Mi in job_dict["initial_masses"].keys():
                if job_dict["initial_masses"][Mi].get("no_LOGS") == True:
                    print(f"{Mt} {Mi}")

        print("\nNO EVOLVE")
        for job_dict in self.job_list:
            Mt = job_dict["total_mass"]
            for Mi in job_dict["initial_masses"].keys():
                if job_dict["initial_masses"][Mi].get("no_evolve") == True:
                    interp_variable = job_dict["initial_masses"][Mi][
                        "last_interp_variable_val"
                    ]
                    print(f"{Mt} {Mi} {interp_variable}")

        if self.evol_phase == "MS":
            print("\nNO HE CORE")
            for job_dict in self.job_list:
                Mt = job_dict["total_mass"]
                for Mi in job_dict["initial_masses"].keys():
                    if job_dict["initial_masses"][Mi].get("no_he_core") == True:
                        print(f"{Mt} {Mi}")

        # job_dict = job_results[0]
        # info_headers = [
        #     k
        #     for k in job_dict.keys()
        #     if k not in ("total_mass", "initial_masses", "result")
        # ]

        # for info_header in info_headers:
        #     non_empty_value = False
        #     print("\n", info_header)
        #     for job_dict in job_results:
        #         Mt = job_dict["total_mass"]
        #         info_list = job_dict[info_header]
        #         if info_list:
        #             if non_empty_value == False:
        #                 print(["total_mass"] + list(info_list[0].keys()))
        #                 non_empty_value = True
        #             for entry in info_list:
        #                 print([Mt] + list(entry.values()))

        return
