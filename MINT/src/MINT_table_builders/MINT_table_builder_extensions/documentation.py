"""
Documentation class extension for InterpolationTableBuilder
"""

from mint_general_interpolation_grid_builder.functions.description_documentation_functions.build_description_page_functions import (
    build_description_table,
)

from mint_general_interpolation_grid_builder.MINT.config.header_description_dict import (
    header_description_dict_scalars,
    header_description_dict_vectors,
)


class InterpolationTableDocumentation:
    def __init__(self):
        pass

    #############################
    # Documentation functions

    def generate_parameter_description_table(self):
        """
        Function to generate a static webpage for the mint interpolation grid.
        We can use this to build pages with sphinx.

        This function makes use of the class properties,
        - self.scalar_input_columns
        - self.scalar_output_columns
        and generates one single table from that

        If that does not work for a given subclass, please override this function

        TODO: add index numbers at table entry
        """

        ###############
        # Create section header
        description_table_rst = """{} MINT_GRID\n{}
{}\n\n
""".format(
            self.name, "=" * (len(self.name) + len(" MINT_GRID")), self.description
        )

        #
        description_table_rst += "\n\n"

        #############
        # Build input scalars description table
        scalar_input_columns_description_table = build_description_table(
            "Input scalars", self.scalar_input_columns, header_description_dict_scalars
        )

        # Build title
        scalar_input_columns_subsection_name = "Input scalars {} table\n".format(
            self.name
        )
        scalar_input_columns_subsection_name += "-" * (
            len(scalar_input_columns_subsection_name) - 1
        )

        # Write description
        scalar_input_columns_subsection_description = "Below we list the input scalar parameters for the {} MINT grid.".format(
            self.name
        )

        # Add to total string
        description_table_rst += (
            scalar_input_columns_subsection_name
            + "\n"
            + scalar_input_columns_subsection_description
            + "\n"
            + scalar_input_columns_description_table
            + "\n"
        )

        #############
        # Build output scalars description table
        scalar_output_columns_description_table = build_description_table(
            "Output scalars",
            self.scalar_output_columns,
            header_description_dict_scalars,
        )

        # Build title
        scalar_output_columns_subsection_name = "Output scalars {} table\n".format(
            self.name
        )
        scalar_output_columns_subsection_name += "-" * (
            len(scalar_output_columns_subsection_name) - 1
        )

        # Write description
        scalar_output_columns_subsection_description = "Below we list the output scalar parameters for the {} MINT grid.".format(
            self.name
        )

        # Add to total string
        description_table_rst += (
            scalar_output_columns_subsection_name
            + "\n"
            + scalar_output_columns_subsection_description
            + "\n"
            + scalar_output_columns_description_table
            + "\n"
        )

        #############
        # Build output vector description table
        vector_output_columns_description_table = build_description_table(
            "Output vectors",
            self.vector_output_columns,
            header_description_dict_vectors,
        )

        # Build title
        vector_output_columns_subsection_name = "Output vectors {} table\n".format(
            self.name
        )
        vector_output_columns_subsection_name += "-" * (
            len(vector_output_columns_subsection_name) - 1
        )

        # Write description
        vector_output_columns_subsection_description = "Below we list the output vector parameters for the {} MINT grid. These structural quantities are determined on a chebychev mass grid.".format(
            self.name
        )

        # Add to total string
        description_table_rst += (
            vector_output_columns_subsection_name
            + "\n"
            + vector_output_columns_subsection_description
            + "\n"
            + vector_output_columns_description_table
            + "\n"
        )

        #
        return description_table_rst
