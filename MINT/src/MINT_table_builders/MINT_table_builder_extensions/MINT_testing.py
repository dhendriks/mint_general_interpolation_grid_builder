#!/usr/bin/python3

import os
import math
import pprint
import shutil
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d, RegularGridInterpolator
import multiprocessing

from mint_general_interpolation_grid_builder.functions_natalie.custom_mpl_settings import (
    load_mpl_rc,
)
from mint_general_interpolation_grid_builder.functions.testing.track_comparison_tests import (
    compare_track,
)
from mint_general_interpolation_grid_builder.functions.testing.track_comparison_config import (
    track_comparison_difference_functions_dict,
)
from mint_general_interpolation_grid_builder.MINT.config.header_description_dict import (
    header_description_dict_scalars,
)


class MINTTester:
    def __init__(self) -> None:

        self.df_MESA_dict = {}
        self.df_evol_dict = {}
        self.df_remapped = {}

        self.stopping_conditions_dict = {
            "MS": {"CENTRAL_HYDROGEN": {"type": "decreasing", "critical_value": 1e-6,}},
            "GB": {"HELIUM_IGNITED_FLAG": {"type": "boolean", "critical_value": 0.9,}},
            "CHeB": {"CENTRAL_HELIUM": {"type": "decreasing", "critical_value": 1e-4,}},
            "EAGB": {
                "CENTRAL_NEON": {
                    "type": "decreasing",
                    "critical_value": 0.01,
                },
                "ONSET_THERMAL_PULSES_FLAG": {
                    "type": "boolean",
                    "critical_value": 1,
                },
                "INTERSHELL_MASS": {
                    "type": "decreasing",
                    "critical_value": 0.01,
                },
            },
        }

        self.num_integration_steps = 1000

        self.verbose = False

        self.standard_test_columns = [
            "MASS",
            "INITIAL_MASS",
            "LOG_LUMINOSITY",
            "LUMINOSITY",
            "LOG_RADIUS",
            "RADIUS",
            "LOG_EFFECTIVE_TEMPERATURE",
            "EFFECTIVE_TEMPERATURE",
            "HELIUM_CORE_MASS_FRACTION",
            "CARBON_CORE_MASS_FRACTION",
            "CENTRAL_DEGENERACY",
            "INITIAL_MASS",
        ]

        self.testing_output_dir = os.path.join(
            self.settings["grid_directory"], "test_results"
        )

    def load_table(self, evol_phase: str) -> pd.DataFrame:
        table_filepath = os.path.join(
            self.settings["grid_directory"].replace(self.evol_phase, evol_phase),
            "tracks_Z%7.2e_%s.csv" % (self.settings["metallicity"], evol_phase),
        )
        print(table_filepath)
        df = pd.read_csv(table_filepath, index_col=[0])
        # df.reset_index(inplace=True)
        df.drop_duplicates(
            subset=self.settings[f"{evol_phase}_scalar_input_columns"], inplace=True
        )
        sigma = 5.67e-8
        df["EFFECTIVE_TEMPERATURE"] = df.apply(
            lambda row: math.pow(
                row["LUMINOSITY"]
                * 3.8e26
                / (4 * math.pi * sigma * math.pow(row["RADIUS"] * 6.96e8, 2)),
                0.25,
            ),
            axis=1,
        )
        df["LOG_LUMINOSITY"] = np.log10(df["LUMINOSITY"])
        df["LOG_RADIUS"] = np.log10(df["RADIUS"])
        df["LOG_EFFECTIVE_TEMPERATURE"] = np.log10(df["EFFECTIVE_TEMPERATURE"])

        for input_column in self.settings[f"{evol_phase}_scalar_input_columns"][1:]:
            df[f"LOG_FIRST_DERIVATIVE_{input_column}"] = np.log10(
                abs(df[f"FIRST_DERIVATIVE_{input_column}"])
            )

        if ("HELIUM_CORE_MASS_FRACTION" in df.columns) and (
            "CARBON_CORE_MASS_FRACTION" in df.columns
        ):
            df["INTERSHELL_MASS"] = (
                df["HELIUM_CORE_MASS_FRACTION"] - df["CARBON_CORE_MASS_FRACTION"]
            )

        if evol_phase == "MS":
            df["INITIAL_MASS"] = df["MASS"]

        return df

    def test_MINT_CME(self) -> None:
        """ """

        self.create_or_empty_directory(self.testing_output_dir)

        df = self.load_table(evol_phase=self.evol_phase)
        unique_masses = [
            M for M in df["MASS"].unique() if M in df["INITIAL_MASS"].unique()
        ]

        # 1. Reproduce tracks for current evol phase
        self.set_and_make_testing_output_subdirectory(direc_name="MINT_CME_tracks")
        with multiprocessing.Pool(processes=self.settings["num_processes"]) as pool:
            pool.starmap(self.reproduce_MESA_track, [(df, M, M) for M in unique_masses])
        self.set_and_make_testing_output_subdirectory(
            direc_name="MINT_CME_tracks/largest_difference"
        )
        self.plot_CME_tracks_largest_difference()

        # 2. compute all evolution up to current evol phase to test transitions
        self.set_and_make_testing_output_subdirectory(direc_name="MINT_CME_HR")
        with multiprocessing.Pool(processes=self.settings["num_processes"]) as pool:
            pool.map(self.save_MINT_test_SSE_plot, [(M) for M in unique_masses])
        plt.close()
        self.combine_plots_into_multiplot_pdf(
            list_of_values=df["MASS"].unique(), direc=self.save_dir
        )

        return

    def reproduce_MESA_track(
        self,
        df: pd.DataFrame,
        M: float,
        Mi: float,
        remove_track_from_table: bool = False,
        remove_mass_from_table: bool = False,
    ) -> None:

        # Extract initial conditions from MESA track

        if self.evol_phase in ["CHeB", "EAGB"]:
            df_he_flash, df_no_he_flash = self.split_tracks_post_he_ignition(df=df)
            if Mi in df_he_flash[df_he_flash["MASS"] == M]["INITIAL_MASS"].unique():
                df = df_he_flash
            else:
                df = df_no_he_flash

        track = df[(df["MASS"] == M) & (df["INITIAL_MASS"] == Mi)]
        if track.empty:
            raise ValueError(f"Track not in dataframe, M = {M}, Mi = {Mi}")

        # options for testing
        if remove_track_from_table:
            df_table = df.drop(track.index)
        elif remove_mass_from_table:
            df_table = df[df["MASS"] != M]
        else:
            df_table = df.copy()

        initial_state_vector = {"MASS": M}
        varX = self.scalar_input_columns[1]
        if self.evol_phase in ["MS", "CHeB"]:
            initial_state_vector[varX] = track[varX].max()
        else:
            initial_state_vector[varX] = track[varX].min()
        if self.evol_phase != "MS":
            varY = self.scalar_input_columns[2]
            initial_state_vector[varY] = track[varY].min()

        self.integrate_evol_phase(
            df=df_table,
            evol_phase=self.evol_phase,
            state_vector=initial_state_vector,
            delta_t=track["AGE"].max() / self.num_integration_steps,
        )

        if self.evol_phase not in self.df_evol_dict:
            raise ValueError("Integration failed for M = " + str(M))

        df_MESA = track
        df_MINT = self.df_evol_dict[self.evol_phase]

        comparison_dicts = compare_track(
            time_proxy_parameter=varX,
            align_parameter="",
            align_tracks=False,
            stellar_phase=self.evol_phase,
            MINT_df=df_MINT,
            MESA_df=df_MESA,
            header_description_dict_scalars=header_description_dict_scalars,
            track_comparison_difference_functions_dict=track_comparison_difference_functions_dict,
            plot_comparison=True,
            plots_output_dir=os.path.join(self.save_dir, str(M)),
        )

        # save results
        for summary_result in [
            "largest_difference",
            "time_proxy_largest_difference",
            "passed",
        ]:
            summary_result_dict = {
                key: value[summary_result] for key, value in comparison_dicts.items()
            }
            df_summary_result = pd.DataFrame([summary_result_dict], index=[M])
            df_summary_result.index.name = "track_label"
            filepath = os.path.join(self.save_dir, f"{summary_result}.csv")
            df_summary_result.to_csv(
                filepath, mode="a", header=not os.path.exists(filepath)
            )

        return comparison_dicts

    def plot_CME_tracks_largest_difference(self) -> None:

        df_largest_difference = pd.read_csv(self.save_dir + ".csv", index_col=[0])
        df_largest_difference.sort_index(inplace=True)

        x = df_largest_difference.index
        for column in df_largest_difference.columns:
            fig, ax = plt.subplots(figsize=(10, 10))
            ax.plot(x, df_largest_difference[column], marker="o")
            ax.set(
                xlabel="INITIAL_MASS",
                ylabel=header_description_dict_scalars[column]["diff_function"],
            )
            threshold_val = header_description_dict_scalars[column]["threshold_default"]
            ax.axhline(
                threshold_val, c="black", label="threshold = {}".format(threshold_val),
            )
            ax.set_xscale("log")
            ax.legend(title=f"{self.evol_phase}, {column}")
            plt.savefig(os.path.join(self.save_dir, f"{column}.pdf"), dpi=200)
            plt.close()

        return

    def integrate_CME(self, mass: float) -> None:

        # 1. MS
        if self.evol_phase in ["MS", "GB", "CHeB", "EAGB"]:
            df_MS = self.load_table(evol_phase="MS")
            initial_state_vector = {
                "MASS": mass,
                "CENTRAL_HYDROGEN": df_MS["CENTRAL_HYDROGEN"].max(),
            }
            self.integrate_evol_phase(
                df=df_MS,
                evol_phase="MS",
                state_vector=initial_state_vector,
                delta_t=self.get_delta_t_for_track(df_MS, mass, mass),
            )

        # 2. GB
        if (
            self.evol_phase in ["GB", "CHeB", "EAGB"]
            and "MS" in self.df_evol_dict.keys()
        ):
            df_GB = self.load_table(evol_phase="GB")
            varX, varY = self.settings[f"GB_scalar_input_columns"][1:]
            initial_state_vector = {
                "MASS": mass,
                varX: self.df_evol_dict["MS"][varX].values[-1],
                varY: self.df_evol_dict["MS"][varY].values[-1],
            }
            self.integrate_evol_phase(
                df=df_GB,
                evol_phase="GB",
                state_vector=initial_state_vector,
                delta_t=self.get_delta_t_for_track(df_GB, mass, mass),
            )

        # 3. CHeB
        if self.evol_phase in ["CHeB", "EAGB"] and "GB" in self.df_evol_dict.keys():
            df_CHeB = self.load_table(evol_phase="CHeB")
            varX, varY = self.settings[f"CHeB_scalar_input_columns"][1:]

            df_he_flash, df_no_he_flash = self.split_tracks_post_he_ignition(df=df_CHeB)
            if self.df_evol_dict["GB"]["CENTRAL_DEGENERACY"].max() > 5:
                df = df_he_flash
            else:
                df = df_no_he_flash

            initial_state_vector = {
                "MASS": mass,
                varX: df_CHeB[varX].max(),
                varY: self.df_evol_dict["GB"][varY].values[-1],
            }
            self.integrate_evol_phase(
                df=df,
                evol_phase="CHeB",
                state_vector=initial_state_vector,
                delta_t=self.get_delta_t_for_track(df_CHeB, mass, mass),
            )

        # 4. EAGB
        if self.evol_phase in ["EAGB"] and "CHeB" in self.df_evol_dict.keys():
            df_EAGB = self.load_table(evol_phase="EAGB")
            varX, varY = self.settings[f"EAGB_scalar_input_columns"][1:]

            df_he_flash, df_no_he_flash = self.split_tracks_post_he_ignition(df=df_EAGB)
            if self.df_evol_dict["GB"]["CENTRAL_DEGENERACY"].max() > 5:
                df = df_he_flash
            else:
                df = df_no_he_flash

            initial_state_vector = {
                "MASS": mass,
                varX: self.df_evol_dict["CHeB"][varX].values[-1],
                varY: self.df_evol_dict["CHeB"][varY].values[-1],
            }
            self.integrate_evol_phase(
                df=df,
                evol_phase="EAGB",
                state_vector=initial_state_vector,
                delta_t=self.get_delta_t_for_track(df_GB, mass, mass),
            )

        return

    def integrate_evol_phase(
        self,
        df: pd.DataFrame,
        evol_phase: str,
        state_vector: dict,
        delta_t: float,
    ) -> None:

        test_columns = list(
            set(
                self.standard_test_columns
                + list(state_vector.keys())
                + list(self.stopping_conditions_dict[evol_phase].keys())
                + [
                    f"FIRST_DERIVATIVE_{param}"
                    for param in state_vector.keys()
                    if param != "MASS"
                ]
                + [
                    f"LOG_FIRST_DERIVATIVE_{param}"
                    for param in state_vector.keys()
                    if param != "MASS"
                ]
            )
        )
        test_columns = list(set(test_columns).intersection(df.columns))

        # Extract slice of df containing mass being tested
        mass = state_vector["MASS"]
        idx = np.searchsorted(df["MASS"].unique(), mass)
        mass_tuple = df["MASS"].unique()[[idx - 1, idx]]
        df_sec = df[df["MASS"].isin(mass_tuple)][test_columns]
        self.df_MESA_dict[evol_phase] = df_sec

        # Remap tracks to orthongonal grid
        if evol_phase == "MS":
            self.df_remapped[evol_phase] = df_sec
        else:
            self.df_remapped[evol_phase] = self.prepare_table_for_mint(
                df=df_sec, evol_phase=evol_phase, multiprocesses=False
            )

        # Set up interpolation functions
        df_evol = pd.DataFrame(columns=test_columns + ["INT_TIME"])
        interp_dict = self.setup_interpolation_functions(
            df=self.df_remapped[evol_phase],
            interpolation_parameters=state_vector.keys(),
        )

        # Integrate
        continue_integration = True
        integration_time = 0
        print(f"Starting {evol_phase}: " + self.state_vector_string(state_vector))

        while continue_integration:

            if self.verbose:
                print(self.state_vector_string(state_vector))

            # interpolate from table
            try:
                evol_row = {
                    col: float(interp_function(tuple(state_vector.values())))
                    for col, interp_function in interp_dict.items()
                }
            except:
                break
            self.evol_row = {
                **evol_row,
                **state_vector,
                "INT_TIME": integration_time,
                "FIRST_DERIVATIVE_MASS": 0,
            }
            df_evol = pd.concat(
                [df_evol, pd.DataFrame([self.evol_row])], ignore_index=True
            )
            integration_time += delta_t

            # evolve interpolation parameters
            for interp_param, interp_param_value in state_vector.items():
                state_vector[interp_param] = (
                    state_vector[interp_param]
                    + delta_t * self.evol_row[f"FIRST_DERIVATIVE_{interp_param}"]
                )

            # Check if stopping conditions met
            for (
                stopping_condition_name,
                stopping_condition,
            ) in self.stopping_conditions_dict[evol_phase].items():
                if stopping_condition["type"] == "increasing":
                    if (
                        self.evol_row[stopping_condition_name]
                        >= stopping_condition["critical_value"]
                    ):
                        continue_integration = False
                elif stopping_condition["type"] == "decreasing":
                    if (
                        self.evol_row[stopping_condition_name]
                        <= stopping_condition["critical_value"]
                    ):
                        continue_integration = False
                elif stopping_condition["type"] == "boolean":
                    if (
                        self.evol_row[stopping_condition_name]
                        == stopping_condition["critical_value"]
                    ):
                        continue_integration = False

        print(f"Finished {evol_phase}: " + self.state_vector_string(state_vector))

        if not df_evol.empty:
            self.df_evol_dict[evol_phase] = df_evol

        return

    def get_delta_t_for_track(self, df: pd.DataFrame, M: float, Mi: float) -> float:
        track = self.find_closest_rows(df, M, Mi)
        # track = df[(df["MASS"] == M) & (df["INITIAL_MASS"] == Mi)]
        return track["AGE"].max() / self.num_integration_steps

    def set_and_make_testing_output_subdirectory(self, direc_name: str) -> None:
        self.save_dir = os.path.join(self.testing_output_dir, direc_name)
        os.makedirs(self.save_dir, exist_ok=True)
        return

    def save_MINT_test_SSE_plot(self, M: float, save: bool = True) -> None:
        load_mpl_rc()

        self.integrate_CME(mass=M)

        # Create a figure and a grid of subplots
        fig = plt.figure(figsize=(16, 16))
        ax1 = plt.subplot2grid((2, 3), (0, 0))  # First row, first column
        ax2 = plt.subplot2grid((2, 3), (0, 1), sharey=ax1)  # First row, second column
        ax3 = plt.subplot2grid((2, 3), (0, 2), sharey=ax1)  # First row, third column

        ax4 = plt.subplot2grid(
            (2, 3), (1, 0), colspan=3
        )  # Second row, spans all columns
        fig.tight_layout()

        if "GB" in self.df_evol_dict.keys():
            ax1 = self.plot_interp_params_SSE(ax1, M, "GB")
        if "CHeB" in self.df_evol_dict.keys():
            ax2 = self.plot_interp_params_SSE(ax2, M, "CHeB")
        if "EAGB" in self.df_evol_dict.keys():
            ax3 = self.plot_interp_params_SSE(ax3, M, "EAGB")
        ax1.set(ylabel="CORE_MASS_FRACTION")
        ax2.invert_xaxis()
        ax4 = self.plot_HR_SSE(self.df_MESA_dict, self.df_evol_dict, ax4, M)

        if save:
            plt.savefig(os.path.join(self.save_dir, f"{M}.pdf"), dpi=200)
        # plt.close()

        return

    def plot_interp_params_SSE(self, ax, M: float, evol_phase: str):

        varX, varY = self.settings[f"{evol_phase}_scalar_input_columns"][1:]

        track = self.df_MESA_dict[evol_phase][
            (self.df_MESA_dict[evol_phase]["INITIAL_MASS"] == M)
            & (self.df_MESA_dict[evol_phase]["MASS"] == M)
        ]
        ax.scatter(
            track[varX], track["HELIUM_CORE_MASS_FRACTION"], alpha=0.5, label="He, MESA"
        )
        ax.plot(
            self.df_evol_dict[evol_phase][varX],
            self.df_evol_dict[evol_phase]["HELIUM_CORE_MASS_FRACTION"],
            alpha=1.0,
            label="He, MINT",
        )

        if "CARBON_CORE_MASS_FRACTION" in self.df_evol_dict[evol_phase].columns:
            ax.scatter(
                track[varX],
                track["CARBON_CORE_MASS_FRACTION"],
                alpha=0.5,
                label="CO, MESA",
            )
            ax.plot(
                self.df_evol_dict[evol_phase][varX],
                self.df_evol_dict[evol_phase]["CARBON_CORE_MASS_FRACTION"],
                alpha=1.0,
                label="CO, MINT",
            )

        ax.set(xlabel=varX)
        ax.legend(title=evol_phase)

        return ax

    @staticmethod
    def plot_HR_SSE(df_MESA_dict: dict, df_evol_dict: dict, ax, M: float):

        for evol_phase, MESA_dict in df_MESA_dict.items():
            track = MESA_dict[
                (MESA_dict["INITIAL_MASS"] == M) & (MESA_dict["MASS"] == M)
            ]
            ax.scatter(
                track["LOG_EFFECTIVE_TEMPERATURE"],
                track["LOG_LUMINOSITY"],
                alpha=0.3,
                label=f"{evol_phase}, MESA",
            )

        for evol_phase, evol_dict in df_evol_dict.items():
            ax.plot(
                evol_dict["LOG_EFFECTIVE_TEMPERATURE"],
                evol_dict["LOG_LUMINOSITY"],
                alpha=1.0,
                label=f"{evol_phase}, MINT",
            )

        ax.invert_xaxis()
        ax.set(ylabel="$\log(L~/\mathrm{L}_\odot)$")
        ax.set(xlabel="$\log (T_\mathrm{eff}~/\mathrm{K})$")
        ax.legend(title=f"M={M}")

        return ax

    @staticmethod
    # SETUP INTERPOLATION FUNCTIONS
    def setup_interpolation_functions(
        df: pd.DataFrame,
        interpolation_parameters: list,
    ) -> dict:
        """
        WARNING: interpolation fails if only one value of an interpolation variable
        TODO: add failsafe for this scenario
        """

        output_columns = [
            col for col in df.columns if col not in interpolation_parameters
        ]
        interp_dict = {}

        # Extract unique values for interpolation parameters
        interpolation_values = tuple(
            df[param].unique() for param in interpolation_parameters
        )

        for col in output_columns:
            values = np.reshape(
                df[col].values, tuple(len(val) for val in interpolation_values)
            )
            interp_dict[col] = RegularGridInterpolator(interpolation_values, values)

        return interp_dict

    @staticmethod
    def state_vector_string(state_vector: dict) -> str:
        return ", ".join([f"{key}: {value}" for key, value in state_vector.items()])

    @staticmethod
    def create_or_empty_directory(directory):
        # Create the directory if it doesn't exist
        if not os.path.exists(directory):
            os.makedirs(directory)
        else:
            # If the directory exists, delete everything inside it
            shutil.rmtree(directory)
            # Recreate the directory
            os.makedirs(directory)

    @staticmethod
    def find_closest_rows(df, M, Mi):
        """
        Finds the rows in the DataFrame df that have the closest values to M in the "MASS" column 
        and Mi in the "INITIAL_MASS" column.

        Parameters:
        df (pd.DataFrame): The DataFrame to search.
        M (float): The target value for the "MASS" column.
        Mi (float): The target value for the "INITIAL_MASS" column.

        Returns:
        pd.DataFrame: A DataFrame containing the rows with the closest values to M and Mi.
        """
        # Calculate absolute differences
        df["diff_mass"] = (df["MASS"] - M).abs()
        df["diff_initial_mass"] = (df["INITIAL_MASS"] - Mi).abs()

        # Sum of absolute differences
        df["total_diff"] = df["diff_mass"] + df["diff_initial_mass"]

        # Find the row(s) with the minimum total difference
        closest_rows = df[df["total_diff"] == df["total_diff"].min()]

        # Drop the auxiliary columns
        closest_rows = closest_rows.drop(
            columns=["diff_mass", "diff_initial_mass", "total_diff"]
        )

        return closest_rows
