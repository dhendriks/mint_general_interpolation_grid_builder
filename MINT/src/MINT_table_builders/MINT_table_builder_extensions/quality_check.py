"""
Quality checking class extension for InterpolationTableBuilder
"""

from mint_general_interpolation_grid_builder.functions.table_functions.table_checker_functions import (
    check_bad_vals,
    check_negative,
    check_positive,
    check_bounded,
    check_for_duplicate_interpolation_variables,
    check_for_non_single_last_interpolation_variable,
    check_orthogonality,
)


class QualityChecking:
    def __init__(self):
        pass

    #############################
    # Quality checking functions

    def check_table_specific(self, table_df):
        """
        Function to check the table for a specific stellar type / subclass.

        The user has to supply this function, but could fill it with a pass statement.

        A selection of functions to check for certain properties is present in the mint_general_interpolation_grid_builder.functions.table_checker_functions file
        """

        raise NotImplementedError

    def _check_table_global(self, table_df):
        """
        Function to do a global quality check on the table. In this function we have implemented the following checks:


        We have implemented the following checks:
        - boundedness: a check whether <bounded_parameters> are bounded between 0 and 1
        - bad_vals: a checker whether bad values are present in the table (np.inf, np.nan)
        """

        ########################
        # set up
        all_scalar_columns = self.scalar_input_columns + self.scalar_output_columns

        ########################
        # quality check for all the numbers in the table
        check_bad_vals(
            df=table_df,
            required_columns=all_scalar_columns + self.vector_output_columns,
        )

        ########################
        # Check for boundedness
        bounded_parameters = [
            "CENTRAL_HYDROGEN",
            "CENTRAL_HELIUM",
            "HELIUM_CORE_MASS_FRACTION",
            "HELIUM_CORE_RADIUS_FRACTION",
            "CARBON_CORE_MASS_FRACTION",
            "CONVECTIVE_CORE_MASS_FRACTION",
            "CONVECTIVE_CORE_RADIUS_FRACTION",
            "CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION",
            "CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION",
            "CONVECTIVE_ENVELOPE_MASS_FRACTION",
            "CONVECTIVE_ENVELOPE_RADIUS_FRACTION",
            "CONVECTIVE_ENVELOPE_MASS_SIMPLIFIED",
            "CONVECTIVE_ENVELOPE_RADIUS_SIMPLIFIED",
        ]
        check_bounded(df=table_df, required_columns=bounded_parameters)

        ##############################################
        # Check first derivatives have required sign

        negative_derivatives = [
            "FIRST_DERIVATIVE_CENTRAL_HYDROGEN",
            "FIRST_DERIVATIVE_CENTRAL_HELIUM",
        ]
        check_negative(
            df=table_df, required_columns=negative_derivatives, allow_zero=False
        )

        positive_derivatives = [
            "FIRST_DERIVATIVE_CENTRAL_DEGENERACY",
            "FIRST_DERIVATIVE_CARBON_TO_HELIUM_CORE_MASS_FRACTION",
        ]
        check_positive(
            df=table_df, required_columns=positive_derivatives, allow_zero=False
        )

        ##################################################
        # check for duplicates of interpolation quantities

        table_df = check_for_duplicate_interpolation_variables(
            df=table_df, input_columns=self.scalar_input_columns
        )

        #####################################################
        # check the last interpolation quantity has at least two values

        # table_df = check_for_non_single_last_interpolation_variable(
        #     df=table_df,
        #     input_columns=self.scalar_input_columns,
        #     )

        ##########################################################
        table_df = check_orthogonality(
            df=table_df,
            input_columns=self.scalar_input_columns,
            remove_non_orthogonal_values=False,
        )

        # TODO: monotonic increasing function
        # TODO: derivative_behaviour

        return table_df

    def check_table(self, table_df):
        """
        Main function to perform a quality check on the table. There are two parts to this. A global check that is done for every table: _check_table_global, and a specific check that is configured on a per-stellar-type basis.

        Currently, the code is configured to fail hard, i.e. we raise a ValueError upon any fail of the checks.

        Problems should be prevented by improving the interpolation table compilation, instead of removing elements that are bad.
        """

        ########################
        # Perform global check
        table_df = self._check_table_global(table_df=table_df)

        #########################
        # Perform specific check
        table_df = self.check_table_specific(table_df=table_df)

        return table_df
