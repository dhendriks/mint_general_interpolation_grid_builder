#!/usr/bin/python3
"""
Table writing class extension for InterpolationTableBuilder
"""
import pickle
import os
import gzip

from datetime import datetime
import pandas as pd


class TableWriting:
    def __init__(self):
        pass

    #############################
    # Table writing functions

    def combine_temp_datafiles_into_dataframe(self):

        # print('JOB RESULTS:', self.job_results)

        # Set up combined-df
        combined_df = pd.DataFrame()

        for track in self.tracks:
            combined_df = pd.concat([combined_df, track["df"]])

        return combined_df

    def handle_columns_interpolation_table(self, df):
        """
        Function to handle re-ordering the columns of the dataframe
        """

        # combine column names in order of desired interpolation table
        combined_new_columns = (
            self.scalar_input_columns
            + self.scalar_output_columns
            + self.vector_output_columns
        )

        # sort data to be in chosen column order and rows in ascending order of interpolation quantities
        df = df.reindex(columns=combined_new_columns)
        print(self.scalar_input_columns)
        df = df.sort_values(by=self.scalar_input_columns)

        return df

    def write_dataframe_to_table_file(self, file_path, df):
        """
        Function to write the dataframe to a file with the appropriate heading and formatting
        """

        # write header
        self.write_header(
            p_list_scalars=self.scalar_input_columns + self.scalar_output_columns,
            p_list_vectors=self.vector_output_columns,
            filename=file_path,
        )

        separator = " "
        for row_index, row in df.iterrows():
            output_string = ""

            # Write input scalar columns:
            for scalar_input_column in self.scalar_input_columns:
                output_string += "%.10g" % (row[scalar_input_column]) + separator

            # Write output scalar columns:
            for scalar_output_column in self.scalar_output_columns:
                output_string += "%.10g" % (row[scalar_output_column]) + separator

            # Write output vector columns:
            for vector_column in self.vector_output_columns:
                output_string += (
                    separator.join(["%.10g" % (el) for el in row[vector_column]])
                    + separator
                )

            # Clean string
            output_string = output_string.strip() + "\n"

            # Write to file:
            with open(file_path, "a") as f:
                f.write(output_string)

        return

    def compress_table_file(self, file_path):

        # specify the path and name of the compressed file
        compressed_file_path = file_path + ".gz"

        # open the .dat file in binary mode
        with open(file_path, "rb") as f_in:
            # read the contents of the file
            file_contents = f_in.read()

            # compress the file contents using gzip
            compressed_contents = gzip.compress(file_contents)

            # write the compressed contents to the compressed file
            with open(compressed_file_path, "wb") as f_out:
                f_out.write(compressed_contents)

        # remove old file
        # os.remove(file_path)

        return

    def write_header(self, p_list_scalars, p_list_vectors, filename):
        """
        Function to write the header of the interpolation table.

        Step:
        - writes some metadata
        - writes the scalar parameter header info
        - writes chebychev
        """

        # writes header for data file

        #########
        # Set up meta data dictionary for the header
        local_dic = {}
        local_dic["date"] = datetime.date(datetime.now())
        # local_dic["label"] = filename
        local_dic["metallicity"] = self.settings["metallicity"]
        local_dic["evol_phase"] = self.evol_phase

        header = """# MINT DATAFILE MINT_{evol_phase}
# MESA version 15140 - Generated on {date}
# METALLICITY {metallicity}
#
# START MINT COLUMN DESCRIPTORS
#
""".format(
            **local_dic
        )

        #################
        # Write scalars
        for i, p in enumerate(p_list_scalars):
            if self.p_dict_scalars[p].get("unit", None) is not None:
                unit = "(" + str(self.p_dict_scalars[p]["unit"]) + ")"
            else:
                unit = " "
            header += (
                f'# {p:<50} {i+1:<7} {self.p_dict_scalars[p]["desc"]:<110} {unit}\n'
            )

        #################
        #
        for j, p in enumerate(p_list_vectors):
            if self.p_dict_vectors[p].get("unit", None) is not None:
                unit = "(" + str(self.p_dict_vectors[p]["unit"]) + ")"
            else:
                unit = " "
            num_string = (
                str(i + 2 + j * len(self.cheby_coords))
                + "-"
                + str(i + 1 + (j + 1) * len(self.cheby_coords))
            )
            header += f'# {p:<50} {num_string:<7} {self.p_dict_vectors[p]["desc"]:<110} {unit}\n'

        header += """#
# END MINT COLUMN DESCRIPTORS
#
# CHEBYSHEV_GRID                           m values for the interpolations,  0 = core, 1 = he core boundary 2 = stellar surface
"""
        print("writing header")

        #############
        # Write to file
        f = open(filename, "w")
        f.write(header)

        # save chebyshev mass coordinates to header
        f.write("#" + " ".join(self.cheby_coords.astype(str)) + "\n")
        f.write("#\n# END HEADER\n")
        f.close()

        print("Finished header")

        return

    def format_interpolation_table_output_filename(self):
        """
        Function to format the output file
        """

        return "MINT_Z%7.2e_%s.dat" % (
            float(self.settings["metallicity"]),
            self.evol_phase,
        )
