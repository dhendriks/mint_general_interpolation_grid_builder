#!/usr/bin/python3

import os
import pandas as pd
import numpy as np


class PhaseTransitions:
    def __init__(self) -> None:
        return

    def add_helium_ignition_boundary(self, df):

        # TODO: loading table everytime is slow, speed up somehow
        table_filepath = os.path.join(
            self.settings["grid_directory"].replace(self.evol_phase, "GB"),
            "tracks_Z%7.2e_%s.csv" % (self.settings["metallicity"], "GB"),
        )
        df_tracks = pd.read_csv(table_filepath, index_col=[0])

        df_tracks["HELIUM_CORE_MASS"] = (
            df_tracks["MASS"] * df_tracks["HELIUM_CORE_MASS_FRACTION"]
        )
        df["HELIUM_CORE_MASS"] = df["MASS"] * df["HELIUM_CORE_MASS_FRACTION"]

        df_ign = df_tracks[df_tracks["HELIUM_IGNITED_FLAG"] == 1][
            ["HELIUM_CORE_MASS", "CENTRAL_DEGENERACY"]
        ].sort_values(["CENTRAL_DEGENERACY"])
        df_ign_smoothed = df_ign.rolling(window=10).mean()
        df["HELIUM_IGNITED_FLAG"] = df.apply(
            lambda x: self.set_helium_ignition_flag(x, df_ign_smoothed), axis=1
        )
        return df

    @staticmethod
    def set_helium_ignition_flag(x, df_ign_smoothed):
        if x["CENTRAL_DEGENERACY"] > df_ign_smoothed["CENTRAL_DEGENERACY"].max():
            return 0
        Mc_at_ign_boundary = np.interp(
            x["CENTRAL_DEGENERACY"],
            df_ign_smoothed["CENTRAL_DEGENERACY"],
            df_ign_smoothed["HELIUM_CORE_MASS"],
        )
        if x["HELIUM_CORE_MASS"] > Mc_at_ign_boundary:
            return 1
        else:
            return 0

    @staticmethod
    def extrapolate_GB_tracks(df):

        # TODO: extend to include more columns
        columns_to_extrapolate = [
            "RADIUS",
            "LUMINOSITY",
            "LOG_LUMINOSITY",
            "LOG_RADIUS",
            "EFFECTIVE_TEMPERATURE",
            "LOG_EFFECTIVE_TEMPERATURE",
            "HELIUM_CORE_MASS_FRACTION",
        ]
        # columns_to_extrapolate = [col for col in df.columns if col[:4] != "CHEB" and col not in ['MASS','INITIAL_MASS','CENTRAL_DEGENERACY','HELIUM_IGNITED_FLAG','HELIUM_CORE_MASS_FRACTION']]

        for M in df["MASS"].unique():
            for Mi in df[df["MASS"] == M]["INITIAL_MASS"].unique():

                track = df[(df["MASS"] == M) & (df["INITIAL_MASS"] == Mi)]
                Mc = max(track["HELIUM_CORE_MASS_FRACTION"].values)
                x = track["CENTRAL_DEGENERACY"].values
                max_degen = min(max(x) + 0.5, 5)
                xnew = [
                    eta
                    for eta in df["CENTRAL_DEGENERACY"].unique()
                    if eta < max_degen and eta > max(x)
                ]

                if len(xnew) != 0 and track["HELIUM_IGNITED_FLAG"].values[-1] == 1:

                    new_rows = {
                        "MASS": [M] * len(xnew),
                        "INITIAL_MASS": [Mi] * len(xnew),
                        "CENTRAL_DEGENERACY": xnew,
                        "HELIUM_IGNITED_FLAG": [1] * len(xnew),
                    }

                    for col_name in [
                        col for col in columns_to_extrapolate if col in df.columns
                    ]:
                        y = track[col_name].values
                        dy_deta = (y[-4] - y[-1]) / (x[-4] - x[-1])
                        ynew = y[-1] + (np.array(xnew) - x[-1]) * dy_deta
                        new_rows[col_name] = list(ynew)

                    df = pd.concat([df, pd.DataFrame(new_rows)], ignore_index=True)

        return df
