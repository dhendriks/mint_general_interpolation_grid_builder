#!/usr/bin/python3


import os
import re
import math
import sys
import json

import numpy as np
import mesaPlot as mp
import matplotlib.pyplot as plt
from PyPDF2 import PdfMerger
from pathlib import Path


from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder import (
    MINTTableBuilder,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.derivatives import (
    integrate_euler,
)
from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)


class MainSequenceTableBuilder(MINTTableBuilder):
    def __init__(self, settings):

        self.evol_phase = "MS"

        # Init mixin classes
        MINTTableBuilder.__init__(self, settings=settings)

        # grid description
        self.name = "MS"
        self.longname = "Main Sequence"
        self.description = "Testing main sequence"

        return

    @staticmethod
    def calc_rel_mass(p):
        return np.flip(p.mass / p.star_mass)

    def check_table_specific(self, table_df):
        """
        Function to do specific check of the table
        """

        return table_df

    def fill_table_edges_with_dummy_data(self):

        new_job_list = []

        #######################################################################
        # pad edges with dummy data to make regular

        # find min and max masses to hit all target locs so can fill outside of this with dummy data
        complete_masses = []
        for job_dict in self.job_list:
            # if use_model = True then add mass to list of complete models
            if job_dict["initial_masses"][job_dict["total_mass"]]["use_model"]:
                complete_masses.append(job_dict["total_mass"])

        min_mass_for_complete_evol = min(complete_masses)
        max_mass_for_complete_evol = max(complete_masses)
        print(
            f"\nThere are complete evol tracks for {min_mass_for_complete_evol}-{max_mass_for_complete_evol} Msun"
        )

        # for masses outside complete region force use of dummy data
        for job_dict in self.job_list:
            Mt = job_dict["total_mass"]
            if (
                (Mt < min_mass_for_complete_evol) or (Mt > max_mass_for_complete_evol)
            ) and (job_dict["initial_masses"][Mt].get("no_LOGS") != True):
                target_locs = job_dict["initial_masses"][Mt]["target_locs"]
                if len(target_locs) > 0:
                    print(f"adding dummy data for {Mt}Msun")
                    num_missing_targets = len(self.time_proxy_save_targets) - len(
                        target_locs
                    )
                    new_target_locs = (
                        target_locs + [target_locs[-1]] * num_missing_targets
                    )
                    job_dict["initial_masses"][Mt]["target_locs"] = new_target_locs
                    job_dict["initial_masses"][Mt]["use_model"] = True
                # print(job_dict)
            new_job_list.append(job_dict)

        self.job_list = new_job_list

        return

    def integrate_to_check_stellar_age_resolved(self):

        print("plotting time integration calculations")

        file_save_dir = os.path.join(
            self.settings["grid_directory"], "plots/time_integration"
        )
        Path(file_save_dir).mkdir(parents=True, exist_ok=True)

        # list of masses
        # y = np.sort(np.fromiter(df.keys(), dtype=float))
        y = [t["df"]["MASS"].iloc[0] for t in self.tracks]
        df_list = [t["df"] for t in self.tracks]

        # values of delta_t to use
        delta_t_div = [0.05, 0.01, 0.005, 0.001, 0.0005]
        x = np.array(delta_t_div)

        # set up error array (Z axis)
        Z = np.zeros((len(y), len(x)))
        # for calculating the mean error
        total_error = 0

        # calculate integration error for each mass
        for yi, mass in enumerate(y):
            print(mass)
            # df_mass_sec = df[str(mass)]
            df_mass_sec = df_list[yi]

            # extract quantities from dataframe
            no_warning = np.array(df_mass_sec["WARNING_FLAG"]) == 0
            mesa_time = np.array(df_mass_sec["AGE"])[no_warning]
            dXc_dt = np.array(df_mass_sec["FIRST_DERIVATIVE_CENTRAL_HYDROGEN"])[
                no_warning
            ]
            Xc = np.array(df_mass_sec["CENTRAL_HYDROGEN"])[no_warning]

            # ensure first point is at time = 0
            mesa_time = mesa_time - mesa_time[0]

            # calculate error for each timestep value
            for xi, div in enumerate(x):
                # set integration timestep
                delta_t = mesa_time[-1] * div
                # integrate using forward euler
                int_time = integrate_euler(Xc, dXc_dt, delta_t)
                # calculate error at end of MS
                error = abs((int_time[-1] - mesa_time[-1]) / mesa_time[-1] * 100)

                Z[yi, xi] = error

            # add error for smallest timestep to total error
            total_error += error

            ############################################################
            # plot time integration and compare to MESA time using smallest timestep for each mass
            fig, ax = plt.subplots()
            ax.scatter(Xc, mesa_time, label="mesa time")
            ax.scatter(Xc, int_time, label="np gradient")

            ax.legend(title=f"M = {mass} Msun, $\%$ error = " + str(round(error, 2)))

            ax.set_xlabel("Xc")
            ax.set_ylabel("Time (yrs)")
            plt.savefig(os.path.join(file_save_dir, "%.2f" % mass + ".pdf"), dpi=200)
            ax.set_xscale("log")
            plt.savefig(
                os.path.join(file_save_dir, "%.2f" % mass + "_log.pdf"), dpi=200
            )
            plt.close()

        ###################################################
        # combine all time integration plots into one pdf
        # Set up the pdf stuff
        merger = PdfMerger()
        pdf_page_number = 0

        for mass in y:
            output_name = os.path.join(file_save_dir, "%.2f" % mass + ".pdf")
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger=merger,
                pdf_filename=output_name,
                page_number=pdf_page_number,
                bookmark_text="{}".format(str(mass)),
            )

        # wrap up the pdf
        combined_pdf_filename = file_save_dir + "_all.pdf"
        merger.write(combined_pdf_filename)
        merger.close()

        ##########################################################
        # contour plot of error for all masses and timesteps
        fig, ax = plt.subplots(1, 1)
        X, Y = np.meshgrid(x, y)
        cp = ax.contourf(np.log10(X), np.log10(Y), np.log10(Z))
        fig.colorbar(cp)  # Add a colorbar to a plot
        ax.set_title(
            "log(percentage error in MS lifetime)\nmean error = "
            + "%.2f" % (total_error / len(y))
        )
        ax.set_xlabel("log(delta_t/ MS lifetime)")
        ax.set_ylabel("log(Mass)")
        plt.savefig(file_save_dir + "_error.pdf", dpi=200)

        return

    def resolution_comparison(self, masses):
        ###############################################################
        # comparison for resolution study
        m = mp.MESA()
        plots_dir = "{}{}plots/mesh_coeff_1.0_0.5".format(
            self.home, self.settings["grid_directory"]
        )
        Path(plots_dir).mkdir(parents=True, exist_ok=True)
        comparison_logs_directory = (
            "/users/nr00492/parallel_scratch/ms_grid/Z0p02_grid_v4/"
        )

        for Mt in masses:
            direc_1 = "{}{}/LOGS_{}/".format(
                self.settings["logs_directory"], Mt, self.evol_phase
            )
            direc_2 = f"{comparison_logs_directory}{Mt}/LOGS_{self.evol_phase}/"
            if os.path.exists(f"{direc_1}history.data") and os.path.exists(
                f"{direc_2}history.data"
            ):
                fig, ax = plt.subplots()
                m.loadHistory(f=direc_1)
                ax.plot(m.hist.star_age, m.hist.log_L, label="delta_mesh_coeff = 0.5")
                m.loadHistory(f=direc_2)
                ax.plot(m.hist.star_age, m.hist.log_L, label="delta_mesh_coeff = 1.0")
                ax.set(xlabel="Age", ylabel="logL")
                ax.legend(title=f"M = {Mt}")
                plt.savefig(f"{plots_dir}/{Mt}.pdf", dpi=200)
                plt.close()

        self.combine_plots_into_multiplot_pdf(list_of_values=masses, direc=plots_dir)

        return


if __name__ == "__main__":

    # Get the command line argument containing the JSON string
    json_str = sys.argv[1]

    # Load the JSON string into a dictionary
    my_dict = json.loads(json_str)

    grid_builder = MainSequenceTableBuilder(settings=my_dict)

    grid_builder.build_interpolation_grid()
