#!/usr/bin/python3

import os
import re
import json
import multiprocessing
import math
import sys

from pathlib import Path
import numpy as np
import pandas as pd
import mesaPlot as mp
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from scipy.integrate import cumulative_trapezoid
from scipy.interpolate import interp1d


from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder import (
    MINTTableBuilder,
)


class CoreHeliumBurningTableBuilder(MINTTableBuilder):
    def __init__(self, settings):

        self.evol_phase = "CHeB"

        # Init mixin classes
        MINTTableBuilder.__init__(self, settings=settings)

        # grid description
        self.name = "CHeB"
        self.longname = "Core Helium Burning"
        self.description = "Testing"

        return

    def check_table_specific(self, table_df):
        """
        Function to do specific check of the table
        """

        return table_df

    def create_interpolation_table_hook(self, df):

        df_he_flash, df_no_he_flash = self.split_tracks_post_he_ignition(df=df)

        df_remapped = self.remap_data_to_orthogonal_grid(
            df=df,
            varX=self.scalar_input_columns[1],
            varY=self.scalar_input_columns[2],
            multiprocesses=True,
        )

        df_checked = self.check_table(table_df=df_remapped)

        # add table to MINT tables dict
        self.MINT_tables = {
            self.format_interpolation_table_output_filename(): df_checked
        }

        return


if __name__ == "__main__":

    # Get the command line argument containing the JSON string
    json_str = sys.argv[1]

    # Load the JSON string into a dictionary
    my_dict = json.loads(json_str)

    grid_builder = CoreHeliumBurningTableBuilder(settings=my_dict)

    grid_builder.build_interpolation_grid()
