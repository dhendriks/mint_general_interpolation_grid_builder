#!/usr/bin/python3

""" functions used in process_detailed_mesa_models"""

import numpy as np
from scipy.integrate import cumulative_trapezoid

from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.convection_and_tides import (
    # calc_Menv_simp,
    calc_tidal_ceoffs,
    compute_moment_of_inertia_factor,
    compute_moment_of_inertia_factor_fallback,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.cheby import (
    calc_all_interpolate_cheby,
    cheby,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.tides_routines import (
    core_env,
    core_env_overshooting,
    # find_convenv_interface,
)


def get_cheby_quantities(output_dict, p, rel_mass, cheby_coords):

    # add all chebyshev quantities in log form
    cheby_input_dic = {
        "CHEBYSHEV_MASS": np.log10(p.mass),
        "CHEBYSHEV_TEMPERATURE": p.logT,
        "CHEBYSHEV_DENSITY": p.logRho,
        "CHEBYSHEV_TOTAL_PRESSURE": p.logP,
        "CHEBYSHEV_GAS_PRESSURE": np.log10(p.pgas),
        "CHEBYSHEV_RADIUS": np.log10(p.radius),
        "CHEBYSHEV_GAMMA1": np.log10(p.gamma1),
        "CHEBYSHEV_PRESSURE_SCALE_HEIGHT": np.log10(p.pressure_scale_height),
        "CHEBYSHEV_DIFFUSION_COEFFICIENT": p.log_D_mix,
        "CHEBYSHEV_HYDROGEN_MASS_FRACTION": np.log10(p.h1),
        "CHEBYSHEV_HELIUM_MASS_FRACTION": np.log10(p.he4),
    }

    cheby_output_dic = calc_all_interpolate_cheby(
        input_dic=cheby_input_dic, rel_mass=rel_mass, cheby_coords=cheby_coords
    )

    # add array quanitties to dict
    output_dict = {
        **output_dict,
        **cheby_output_dic,
    }

    return output_dict


def get_convective_regions_and_tides(
    output_dict, p, m, target_index, all_results_list, scalar_output_columns
):

    mass_conv_core = (
        m.hist.mass_conv_core[target_index] / m.hist.star_mass[target_index]
    )
    Mc = (
        m.hist.hydrogen_exhausted_core_mass[target_index]
        / m.hist.star_mass[target_index]
    )
    R = m.hist.radius[target_index]

    # if largest conv zone is in core
    if m.hist.conv_mx1_top[target_index] < Mc:

        # set conv core quantities
        radius_conv_core = m.hist.conv_mx1_top_r[target_index] / R
        mass_ov_core = m.hist.mx1_top[target_index]
        radius_ov_core = m.hist.mx1_top_r[target_index] / R

        # set conv envelope quantities
        if m.hist.conv_mx2_bot[target_index] > Mc:
            mass_conv_env = (
                m.hist.conv_mx2_top[target_index] - m.hist.conv_mx2_bot[target_index]
            )
            mass_conv_env_top = m.hist.conv_mx2_top[target_index]
            radius_conv_env = (
                m.hist.conv_mx2_top_r[target_index]
                - m.hist.conv_mx2_bot_r[target_index]
            ) / R
            radius_conv_env_top = m.hist.conv_mx2_top_r[target_index] / R

        else:
            mass_conv_env = 0
            mass_conv_env_top = 1.0
            radius_conv_env = 0
            radius_conv_env_top = 1.0

    # if largest conv region in envelope
    else:

        # set conv env quantities
        mass_conv_env = (
            m.hist.conv_mx1_top[target_index] - m.hist.conv_mx1_bot[target_index]
        )
        mass_conv_env_top = m.hist.conv_mx1_top[target_index]
        radius_conv_env = (
            m.hist.conv_mx1_top_r[target_index] - m.hist.conv_mx1_bot_r[target_index]
        ) / R
        radius_conv_env_top = m.hist.conv_mx1_top_r[target_index] / R

        # set conv core quantities
        if m.hist.conv_mx2_bot[target_index] < Mc:
            radius_conv_core = m.hist.conv_mx2_top_r[target_index] / R
            mass_ov_core = m.hist.mx2_top[target_index]
            radius_ov_core = m.hist.mx2_top_r[target_index] / R
        else:
            if m.hist.mix_type_2[target_index] == 2:  # test if core is convective
                radius_conv_core = m.hist.mix_relr_top_1[target_index]
            else:
                radius_conv_core = 0
            if (
                m.hist.mix_type_2[target_index] == 2
            ):  # test if next-to-core layer is overshooting
                mass_ov_core = m.hist.mix_qtop_2[target_index]
                radius_ov_core = m.hist.mix_relr_top_2[target_index]
            else:
                mass_ov_core = mass_conv_core
                radius_ov_core = radius_conv_core

    output_dict["CONVECTIVE_CORE_MASS_FRACTION"] = mass_conv_core
    output_dict["CONVECTIVE_CORE_RADIUS_FRACTION"] = radius_conv_core
    output_dict["CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION"] = mass_ov_core
    output_dict["CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION"] = radius_ov_core
    output_dict["CONVECTIVE_ENVELOPE_MASS_FRACTION"] = mass_conv_env
    output_dict["CONVECTIVE_ENVELOPE_RADIUS_FRACTION"] = radius_conv_env
    output_dict["CONVECTIVE_ENVELOPE_MASS_TOP_FRACTION"] = mass_conv_env_top
    output_dict["CONVECTIVE_ENVELOPE_RADIUS_TOP_FRACTION"] = radius_conv_env_top

    if "K2" in scalar_output_columns:
        output_dict["K2"] = m.hist.apsidal_constant_k2[target_index]

    if ("TIDAL_E2" or "TIDAL_E_FOR_LAMBDA") in scalar_output_columns:

        if all_results_list:
            previous_etwo = all_results_list[-1]["TIDAL_E2"]
        else:
            previous_etwo = 0

        (etwo, e_for_lambda) = calc_tidal_ceoffs(
            p=p, Mcore=mass_conv_core, previous_etwo=previous_etwo
        )

        output_dict["TIDAL_E2"] = etwo  # E2
        output_dict["TIDAL_E_FOR_LAMBDA"] = e_for_lambda  # E

    if "MOMENT_OF_INERTIA_FACTOR" in scalar_output_columns:

        try:
            # compute the moment of inertia factor with Claret's formula
            moment_of_inertia_factor = compute_moment_of_inertia_factor(p)
        except:
            # compute the moment of inertia factor with a simpler sum
            moment_of_inertia_factor = compute_moment_of_inertia_factor_fallback(
                p=p, start_zone=0
            )

        output_dict["MOMENT_OF_INERTIA_FACTOR"] = moment_of_inertia_factor

    if "HELIUM_CORE_MOMENT_OF_INERTIA_FACTOR" in scalar_output_columns:

        try:
            core_boundary = m.hist.he_core_k[target_index]
        except:
            core_boundary = np.where(p.mass < p.he_core_mass)[0][0]
        core_moment_of_inertia_factor = compute_moment_of_inertia_factor_fallback(
            p=p, start_zone=core_boundary
        )

        output_dict[
            "HELIUM_CORE_MOMENT_OF_INERTIA_FACTOR"
        ] = core_moment_of_inertia_factor

    if "CARBON_CORE_MOMENT_OF_INERTIA_FACTOR" in scalar_output_columns:

        try:
            core_boundary = m.hist.co_core_k[target_index]
        except:
            core_boundary = np.where(p.mass < p.co_core_mass)[0][0]
        core_moment_of_inertia_factor = compute_moment_of_inertia_factor_fallback(
            p=p, start_zone=core_boundary
        )

        output_dict[
            "CARBON_CORE_MOMENT_OF_INERTIA_FACTOR"
        ] = core_moment_of_inertia_factor

    return output_dict
