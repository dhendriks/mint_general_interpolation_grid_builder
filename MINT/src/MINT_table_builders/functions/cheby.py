###################################################################################
# chebyshev functions

import numpy as np
from scipy import interpolate
import math


def cheby(M, location):
    """
    This routine generates a Chebyshev-style distribution of points from 0 to 1

    Input:  M   number of points for the mass-coordinate array
                M-2 points follow the Chebyshev distribution
                first and last are core and surface
            location    place where the points are more-densely packed
                        location can be
                        "surface" "surf" or "s",
                        "core" or "c",
                        "both" or "b"
    Output: array of size M containing mass coordinates
    """
    # TODO allow the place where the distribution is denser to be at any radius

    cheb = np.zeros(M)  # initialize

    if location == "core" or location == "c":
        M -= 1
        if (M % 2) == 0:  # if M is even
            N = 2 * M
        else:
            N = 2 * M + 1
        for k in range(1, M):
            cheb[k] = math.cos(math.pi * (2.0 * k - 1.0) / (2.0 * N))
        cheb[0] = 1.0

    if location == "surface" or location == "s" or location == "surf":
        M -= 1
        if (M % 2) == 0:  # if M is even
            N = 2 * M
        else:
            N = 2 * M + 1
        for k in range(1, M):
            cheb[M - k] = 1 - math.cos(math.pi * (2.0 * k - 1.0) / (2.0 * N))
        cheb[-1] = 0.0
        cheb[0] = 1.0

    if location == "both" or location == "b":
        N = M - 1
        for k in range(1, M):
            cheb[M - k] = (1 - math.cos(math.pi * (2.0 * k - 1.0) / (2.0 * N))) / 2
        cheb[-1] = 0.0
        cheb[0] = 1.0

    return cheb


# # NOT IN USE ANYMORE
# def find_cheby_zones(mass, he_core_mass, cheby_coords):
#     """finds zone numbers corresponding to cheby coords where 0 = center, 1 = he core, 2 = surface
#     INPUTS: mass = array of mass coordinates for each zone, must be in order from high (surface) to low (core)
#             he_core_mass = mass location of he core
#             cheby_coords = array of fractional coordinates at which quanitites are sampled ordered from low (core) to high (surface)
#     OUPUT: cheby_zones = array of zone numbers at which quantities are sampled ordered from high (near core) to low (near surface)"""
#     # convert mass coordinates to fractional coordinates where 1 = he core and 2 = stellar surface
#     core_zone_num = np.argmin(abs(mass - he_core_mass))
#     rel_mass = np.append(
#         1 + (mass[:core_zone_num] - he_core_mass) / (mass[0] - he_core_mass),
#         mass[core_zone_num:] / he_core_mass,
#     )
#     # find zone numbers that match cheby coords
#     cheby_zones = []
#     for x in cheby_coords:
#         cheby_zones += [np.abs(rel_mass - x).argmin()]
#     return cheby_zones


def calc_all_interpolate_cheby(input_dic, rel_mass, cheby_coords):
    ouput_dic = {}
    for quantity_name in input_dic:
        cheby_log_quantity = interpolate_cheby(
            rel_mass=rel_mass,
            log_quantity=input_dic[quantity_name],
            cheby_coords=cheby_coords,
        )
        ouput_dic[quantity_name] = 10 ** (cheby_log_quantity)
    return ouput_dic


def interpolate_cheby(rel_mass, log_quantity, cheby_coords):
    """
    Calculates various quantities interpolated on a chebyshev grid
    INPUT: rel_mass = relative mass coordinate of zones in increasing order e.g. for MS, rel_mass = m(r)/Mstar
           log_quantity = log values of desired quantity at each zone e.g. log_T
    OUPUT: interpolated values of log_quanitity at points of chebyshev grid
    """
    # set up interpolation grid
    lgq = np.log10(rel_mass)

    f = interpolate.interp1d(
        lgq,
        np.flip(log_quantity),
        kind="linear",
        # fill_value="extrapolate"
    )

    # remove 0 if in cheby coords
    if 0 in cheby_coords:
        # remove central point and just use value of log quanitity for center zone point
        cheby_coords = np.delete(cheby_coords, np.where(cheby_coords == 0))
        cheby_log_quantity = np.array([log_quantity[-1]])
    else:
        cheby_coords = cheby_coords
        cheby_log_quantity = np.array([])

    cheby_log_quantity = np.append(cheby_log_quantity, f(np.log10(cheby_coords)))

    if np.isnan(cheby_log_quantity).any():
        print(rel_mass)
        print(cheby_log_quantity)

    return cheby_log_quantity


def cheby_points(a, b, N=50):
    """calculates cheby coords for interval between a and b with N points"""

    cheb_array = np.polynomial.chebyshev.chebpts1(N - 2)
    cheb_array = np.append([a, b], (cheb_array + 1) * (b - a) / 2 + a)
    cheb_array = np.sort(cheb_array)

    # k = np.arange(1, N + 1, 1)
    # xk = 0.5 * (a + b) + 0.5 * (b - a) * np.cos((2 * k - 1) * np.pi / (2 * N))
    # return np.flip(xk)

    return cheb_array
