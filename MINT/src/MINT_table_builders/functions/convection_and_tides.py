#!/usr/bin/python3
"""tidal calculations and convective regions"""

import math

from scipy import interpolate
import numpy as np

from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.tides_routines import (
    E_calc,
    E2_calc,
    core_env,
    core_env_overshooting,
    find_convenv_interface,
)


def calc_tidal_ceoffs(p, Mcore, previous_etwo):
    # tidal coefficients from Gio
    if float(Mcore > 1e-3):
        # nonzero E2
        try:
            etwo = E2_calc(p)
        except Exception as e:
            # print(e)
            # print(f'\nE2 calc failed: Mi = {p.star_mass}, h = {p.center_h1}, using previous table value\n')
            etwo = previous_etwo
        if math.isnan(etwo):
            # The standard routine for E2 might yield a NaN because of some spikes in
            # the gradients output by MESA
            # If this happens use the fallback calculation for E2
            # (relies on the profile immediately before)
            etwo = previous_etwo
    else:
        # print(f'Mcore = {Mcore} setting E2 = 0')
        etwo = 0
    e_for_lambda = float(E_calc(p))

    # print(etwo)

    return etwo, e_for_lambda


def compute_moment_of_inertia_factor(p):
    """
    This routine computes the moment of inertia factor, sometimes defined as k2 or beta^2
    but DIFFERENT from "apsidal constant k2" in MESA
    This calculation relies on Claret's formula

    Input:  p       MESA profile opened with mesa_reader
    Output: moif    moment of inertia factor        (float)
    """

    # TODO put Rsun Msun in a dedicated general model, or extract MESA values
    Rsun = 6.957e10
    Msun = 1.9885e33
    mantissa = np.zeros(p.num_zones)
    rho = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        rho[i] = math.pow(10, p.logRho[i]) * math.pow(Rsun, 3) / Msun
        mantissa[i] = 8.0 * np.pi / 3.0 * rho[i] * math.pow(p.radius[i], 4)
    r = p.radius

    mantissa_f = interp1d(r, mantissa, kind="cubic")
    I = quad(fff, p.radius[-1], p.radius[0], args=(mantissa_f))[0]
    moif = I / (p.mass[0] * math.pow(p.radius[0], 2))
    # print('MOIF = I/(MR^2) = {}'.format(moif))
    return moif


def compute_moment_of_inertia_factor_fallback(p, start_zone):
    """
    Fallback calculation of the moment of inertia factor, sometimes defined as k2 or beta
    This calculation relies on Rob's simplified formula + no python complex operations

    Input:  p       MESA profile opened with mesa_reader
    Output: moif    moment of inertia factor        (float)
    """
    I = 0

    for i in range(start_zone, p.num_zones - 1):
        I += 2.0 / 3.0 * p.radius[i] * p.radius[i] * (p.mass[i] - p.mass[i + 1])
    moif = I / (p.mass[0] * math.pow(p.radius[0], 2))
    return moif


# def calc_Menv_simp(p, Renv_simp):
#     if Renv_simp == 0:
#         Menv_simp = 1
#     elif Renv_simp == 1:
#         Menv_simp = 0
#     else:
#         Menv_simp = 1.0 - interpolate.interp1d(
#             p.radius / p.radius[0], p.mass / p.mass[0]
#         )(Renv_simp)
#     return Menv_simp


# def find_conv_core_regions(p):
#     """finds the largest and second largest convective regions in the core and returns the mass and radius coordinates of their boundaries"""
#     conv_regions = np.where((p.mlt_mixing_type == 1) & (p.mass < p.he_core_mass))[0]
#     di = np.diff(conv_regions)
#     pos = np.where(di > 2)[0] + 1
#     regions = np.split(conv_regions, pos)
#     mass_locs = np.zeros(4)
#     radius_locs = np.zeros(4)
#     if len(conv_regions > 0):
#         # add main convective region to array
#         mass_locs[0:2] = [0, p.mass[regions[-1][0]]]
#         radius_locs[0:2] = [0, p.radius[regions[-1][0]]]
#         if len(regions) == 1:
#             mass_locs[2:4] = [mass_locs[1], mass_locs[1]]
#             radius_locs[2:4] = [radius_locs[1], radius_locs[1]]
#         else:
#             # loop through other convective regions to find the next largest
#             for i in regions[:-1]:
#                 if p.mass[i[0]] - p.mass[i[-1]] > mass_locs[3] - mass_locs[2]:
#                     mass_locs[2:4] = [p.mass[i[-1]], p.mass[i[0]]]
#                     radius_locs[2:4] = [p.radius[i[-1]], p.radius[i[0]]]
#     return mass_locs, radius_locs


# def find_conv_envelope(p):
#     # finds largest convective envelope region
#     conv_regions = np.where((p.mlt_mixing_type == 1) & (p.mass > p.he_core_mass))[0]
#     di = np.diff(conv_regions)
#     pos = np.where(di > 2)[0] + 1
#     regions = np.split(conv_regions, pos)
#     mass_contained = np.zeros(len(regions))
#     if len(conv_regions) > 0:
#         for i, r in enumerate(regions):
#             mass_contained[i] = p.mass[r[0]] - p.mass[r[-1]]
#         largest_region = regions[np.argmax(mass_contained)]
#         return (
#             p.mass[largest_region[-1]],
#             p.mass[largest_region[0]],
#             p.radius[largest_region[-1]],
#             p.radius[largest_region[0]],
#         )
#     else:
#         return 0, 0, 0, 0
