"""
This routine contains everything necessary
to compute E2 and lambda, and related timescales
"""
#########################
# IMPORTS AND CONSTANTS #
#########################

import math
from scipy.integrate import odeint
from scipy.interpolate import interp1d
import scipy.integrate as integrate
from scipy.integrate import solve_ivp
from scipy.special import exp10
import numpy as np

import warnings

warnings.filterwarnings("ignore", category=RuntimeWarning)

M_sun = 1.989100000000000004902937633489e33
R_sun = 6.956600000000000000000000000000e10
L_sun = 3.83e33
gamma2 = 0.273842802214636749091880574269
lhs_max = 0.1608143712577743784075630870383797813272448099162409747318701358
G = 6.674080000000000428212382002843e-08

alpha_mlt = 2.0
alphaprime = 0.762 * alpha_mlt
x_photosphere = 0.98


#######################
# NECESSARY FUNCTIONS #
#######################
def cell2face(val, dm, dm_is_m=False, m_center=0):
    """
    Some quantities in MESA are defined at center of the cell, some at outer boundary of cell.
    This routine computes model quantities at the outer meshpoint rather than the center of the cell
    Necessary for some derivatives - fix provided by Walter van Rossem (MESA ML 18/10/2019)

    Input:	val		MESA variable to shift from cell to face 	(array of floats)
                dm		MESA dm (array of mass contained in each cell)
                                or MESA mass (included mass within star at cell)(array of flots)
                dm_is_m		False if dm is actually the MESA dm array
                                True if dm is actually the MESA mass array	(Boolean)
                m_center	Value of mass at centre of star	(usually 0)	(float)
    Output:	face		val array, shifted from center of cell to
                                cell boundary (half-a-cell shift)		(array of floats)
    """
    if dm_is_m:
        dm = np.diff(dm, append=m_center)
    face = np.zeros_like(val)
    face[0] = val[0]
    face[1:] = (val[:-1] * dm[1:] + val[1:] * dm[:-1]) / (dm[1:] + dm[:-1])
    return face


# derivative(y,x) is the function to be called
def derivative(y, x):
    """
    Derivative function, pretty self-explanatory
    """
    return np.gradient(y, x, edge_order=2)


#######################
def bisection(f, arg, a, b, N):
    """Approximate solution of f(x)=0 on interval [a,b] by bisection method.

    Parameters
    ----------
    f : function
        The function for which we are trying to approximate a solution f(x)=0.
    a,b : numbers
        The interval in which to search for a solution. The function returns
        None if f(a)*f(b) >= 0 since a solution is not guaranteed.
    N : (positive) integer
        The number of iterations to implement.

    Returns
    -------
    x_N : number
        The midpoint of the Nth interval computed by the bisection method. The
        initial interval [a_0,b_0] is given by [a,b]. If f(m_n) == 0 for some
        midpoint m_n = (a_n + b_n)/2, then the function returns this solution.
        If all signs of values f(a_n), f(b_n) and f(m_n) are the same at any
        iteration, the bisection method fails and return None.
    """

    # print("Inside bisec we have {} and {}".format(f(a, arg), f(b, arg)))
    # print("for {} {}".format(a, b))
    if f(a, arg) * f(b, arg) >= 0:
        print("Bisection method fails.")
        return None
    a_n = a
    b_n = b
    for _ in range(1, N + 1):
        m_n = (a_n + b_n) / 2
        f_m_n = f(m_n, arg)
        if f(a_n, arg) * f_m_n < 0:
            a_n = a_n
            b_n = m_n
        elif f(b_n, arg) * f_m_n < 0:
            a_n = m_n
            b_n = b_n
        elif f_m_n == 0:
            print("Found exact solution.")
            return m_n
        else:
            print("Bisection method fails.")
            return None  # tentative quickfix
    return (a_n + b_n) / 2


################################
# CONVECTIVE ENVELOPE ROUTINES #
################################
def produce_E_array(p):
    """
    This routine computes the structure quantity E defined in Zahn 1989 (eq. 6)
    Even if E makes sense only at the core/envelope interface we compute it
    throughout the star. The array is then interpolated at the core boundary coordinate.

    Input  : p		a MESA profile read with mesa_reader
    Output : E		E values throughout the star pre-interpolation	(array of floats)
    """

    # Retrieve density and pressure values, shift from center to surface of meshpoint
    rho = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        rho[i] = math.pow(10, cell2face(p.logRho, p.mass, dm_is_m=True, m_center=0)[i])
    pressure = cell2face(10 ** (p.logP), p.mass, dm_is_m=True)

    # compute rhoprime = Mstar/4*pi*Rstar
    M_star = p.star_mass * M_sun
    R_star = p.radius[0] * R_sun
    rhoprime = M_star / (4 * np.pi * math.pow(R_star, 3))

    # Compute h = (Hp * Rstar/ rconv**2  * mconv/Mstar )
    r = p.radius * R_sun
    m = p.mass * M_sun
    hp = -1.0 * pressure * np.gradient(p.radius, pressure) * R_sun
    h = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        h[i] = hp[i] * R_star / math.pow(r[i], 2) * m[i] / M_star

    # Compute E = rhoconv/rhoprime * h**(-3/2)
    E = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        if h[i] > 0:
            E[i] = rho[i] / rhoprime * math.pow(h[i], -1.5)
        else:
            # print("in E definition h < 0 for i = {}".format(i))
            E[i] = 0

    return E


##################################################################################################
def find_convenv_interface(p):
    """
    This function finds the core envelope interface precisely, by searching the roots of gradr-gradL
    It simplifies many small changes in stability inside larger zones, to avoid including surface
    ionization zones (intermediate-mass stars) and semiconvective bubbles (massive stars)

    Input:	p	a MESA profile read with mesa_reader
    Output: interf	the location of the core/envelope interface in units of relative radius x	(float)
    """

    # First we roughly assess where the core boundary is
    x = p.radius / p.radius[0]

    # Generate ledoux stability criterion
    stability = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        if (p.gradr[i] - p.gradL[i]) > 0:
            stability[i] = 0.0  # convection
        else:
            stability[i] = 1.0  # radiative

    # Count number of stability layers
    # Increment by one every time the stability changes, and save the coordinates
    count = 0
    layer_top = []  # layer_top contains the top coord of each layer
    layer_base = []  # layer_base contains the bottom coord of each layer
    layer_top = np.append(layer_base, 0)
    for i in range(p.num_zones - 1):
        if stability[i] != stability[i + 1]:
            # print("change in stability")
            count = count + 1
            layer_base = np.append(layer_base, i)
            layer_top = np.append(layer_top, i)
    layer_base = np.append(layer_base, -1)
    # print("number of layers = ", count + 1)

    # Check the amount of relative mass included in each layer
    # remove them if the relative included mass is below a given threshold
    if p.star_mass < 100:  # set threshold
        thresh = 0.01
    else:
        thresh = 0.01

    # for j in range(count + 1):
    #     print(
    #         "layer {} goes from m={} to {}, is it radiative? {}".format(
    #             j,
    #             p.mass[int(layer_base[j])],
    #             p.mass[int(layer_top[j])],
    #             stability[int(layer_base[j])]
    #         )
    #     )
    #     print(
    #         stability[int(layer_base[j])],
    #         (p.mass[int(layer_top[j])] - p.mass[int(layer_base[j])]) / p.star_mass,
    #     )

    # if there is a thin enough surface radiative layer,
    # change stability to convective (ignore radiation)
    if (
        stability[int(layer_base[0])] == 1
        and (p.mass[int(layer_top[0])] - p.mass[int(layer_base[0])])
        < thresh * p.star_mass
    ):
        # print("there is a thin radiative surface we can pop {}".format(
        #         p.mass[int(layer_base[0])] ) )
        for i in range(int(layer_top[0]), int(layer_base[0] + 1)):
            stability[i] = 0.0

    # if there are thin enough convective layers,
    # change stability to radiative (ignore convection)
    # this criterion is meant for low-Z high-mass stars, it might be too loose
    for j in range(1, count + 1):
        if (
            stability[int(layer_base[j])] == 0
            and (p.mass[int(layer_top[j])] - p.mass[int(layer_base[j])])
            < thresh * p.star_mass
        ):
            # print("layer {} could be popped, for mesh points {} to {}".format(
            #         j, layer_base[j], layer_top[j] ) )
            # then pop this one
            for i in range(int(layer_top[j]), int(layer_base[j] + 1)):
                stability[i] = 1.0

    # remove the top layers to ignore tiny surface convective or radiative layers
    # this criterion is meant for intermediate-mass star ionization zones, it might be too loose
    # THE THRESHOLD BELOW IS FOR RGB STARS
    if p.star_mass > 20:  # set threshold
        thresh = 0.02
    else:
        thresh = 0.003
    #    # THE THRESHOLD BELOW IS FOR MS STARS
    #    if p.star_mass > 100:  # set threshold
    #        thresh = 0.005
    #    else:
    #        thresh = 0.003
    # TODO: automate the choice of threshold by passing evolutionary stage?
    #       maybe change strategy for surface layers?

    for i in range(p.num_zones - 1):  # spot "base of surface zone"
        if (
            (p.mass[i] - (1.0 - thresh) * p.star_mass)
            * (p.mass[i + 1] - (1.0 - thresh) * p.star_mass)
        ) <= 0:
            isurf = i
            # print("Surface layers at depth dm={} reach mesh point {}".format(thresh, i))
            break
    for i in range(isurf):
        # set stability in the surface zone to what it is at its base
        stability[i] = stability[isurf]

    # At this point the simplified criterion is ready: sample it
    iconv = -1
    if stability[0] == 0:
        for i in range(p.num_zones - 1):
            if stability[i] == 0 and stability[i + 1] == 1:
                iconv = i
                break

    if iconv == -1:
        #      print('no convective zone it seems')
        interf = 1.0
    else:
        #      print('convective zone in between r={} and r={}'.format(x[iconv], x[iconv+1]))
        # Interpolate the difference between the gradients: careful with cubic interpolation !
        gradr_minus_gradL = interp1d(x, p.gradr - p.gradL, kind="linear")

        # Uses a bisection: this avoids the spurious roots introduced by the cubic interpolation
        #      print('we call bisec with {} and {}'.format(
        # ggg(x[iconv],gradr_minus_gradL), ggg(x[iconv+1],gradr_minus_gradL))
        # )
        interf = bisection(ggg, gradr_minus_gradL, x[iconv + 1], x[iconv], 40)
    #      print("interf to find xb =", interf)
    # print('comparison post-interf:', iconv, x[iconv], interf)
    # if star is fully convective
    if max(stability) == 0:
        # print("star is fully convective")
        interf = 0.0

    return interf


################################
def xa_function(x, rhs):
    """
    Function necessary to compute xa, the depth at which the viscosity changes defintiion
    Definition in Zahn 1989 eqs (14a, 14b, 9)

    Input:	x	reduced radius from MESA profile	(array of floats)
                rhs	right-hand-side term in eq 14 of Zahn89	(float)
    """
    return math.pow(x, 7.0 / 6.0) * math.pow((1 - x), 1.5) - rhs


################################
def compute_xa(Ecoeff, ratio):
    """
    This routine computes the value of xa (see Zahn 1989 eq. 14b)
    xa is the depth at which the convective turnover time is no longer
    sufficiently long wrt the convective turnover time

    Input:  Ecoeff : the quantity E evaluated at core boundary	(float)
            ratio : Pi/(2tf) the ratio of the tidal period to twice the friction time	(float)
    Output: xa the root of Zahn 1989 eq. 14b	(float)
    """

    # Ecoeff = interp1d(p.radius/p.radius[0],E)(xb)
    # tfriction = p.star_mass*M_sun
    #             * math.pow(p.photosphere_r*R_sun,2)
    #             / (p.photosphere_L*L_sun)
    rhs = (
        math.pow(2.5, 1.5)
        * math.pow(alphaprime, -2.0 / 3.0)
        * math.pow(Ecoeff, -1.0 / 3.0)
        * ratio
    )  # Pilm/(2*tfriction)

    if rhs > lhs_max:
        xa = None
    else:
        xa = bisection(xa_function, rhs, 7.0 / 16.0, 1, 40)

    # quick check
    if xa is None and rhs <= lhs_max:
        print("ERROR: no value for xa when expected")
    print("for ratio=", ratio, "we find rhs=", rhs, " xa=", xa)

    return xa


################################
def compute_lambda_bracket(xa, xb):
    """
    This function computes the integrals in the definition of lambda
    (see Zahn 1989 eq. 14a,b)
    It can be either one integral (if xa undefined) or two integrals

    Input : xb  the core boundary relative radius	(float)
            xa  (maybe undefined) location below which viscosity changes definition	(None or float)
    Output: bracket term made of 1 or 2 integrals 	(float)
    """
    # if xa is not defined
    if xa is None:
        integral_xb = integrate.quad(
            lambda x: math.pow(x, 22.0 / 3.0) * math.pow(1.0 - x, 2.0), xb, 1
        )
        return integral_xb[0]

    # if xa is defined
    integral_xa = integrate.quad(
        lambda x: math.pow(x, 22.0 / 3.0) * math.pow(1 - x, 2.0), xa, 1
    )
    integral_xb = integrate.quad(
        lambda x: math.pow(x, 37.0 / 6.0) * math.pow(1 - x, 1.0 / 2.0), xb, xa
    )
    return (
        integral_xa[0]
        + math.pow(xa, 7.0 / 6.0) * math.pow(1 - xa, 1.5) * integral_xb[0]
    )


################################
def compute_lambda_lm(p, ratio):
    """
    This function computes lambda_lm from Zahn 1989 (eq. 14a)
    Assembles the contents of various variables above

    Input: 	p	 MESA profile read with mesa_reader
                ratio	 Pi/(2tf) the ratio of the tidal period to twice the friction time   (float)
    Output:	lambdalm tidal coefficient from Zahn 1989	(float)
    """
    # check that star is not fully convective
    fully_conv = True
    for i in range(p.num_zones - 1, 0, -1):
        # print(i, p.radius[i] / p.radius[0], p.ledoux_stable[i], fully_conv)
        if p.radius[i] / p.radius[0] > 0.98:
            break
        if p.ledoux_stable[i] == 1:
            fully_conv = False
            break
    # print("final", fully_conv)

    # compute E
    # this part could be simplified I reckon
    if not fully_conv:
        E = produce_E_array(p)
        xb = find_convenv_interface(p)

        Ecoeff = interp1d(p.radius / p.radius[0], E)(xb)
        xa = compute_xa(p, Ecoeff, ratio)

        # compute lambda coefficient
        lambdalm = (
            0.8725
            * math.pow(alphaprime, 4.0 / 3.0)
            * math.pow(Ecoeff, 2.0 / 3.0)
            * compute_lambda_bracket(xa, xb)
        )
        # print("for xb =", xb, " and xa=", xa, "we find lambdalm=", lambdalm)

    else:
        xb = 0  # no radiative core
        E = produce_E_array(p)
        # E is an average value on the inner 1% in x -> E value is not super smooth from MESA runs

        x = np.linspace(p.radius[-1] / p.radius[0], 0.01, 100)
        Ecoeff = 0
        for i in range(100):
            Ecoeff += interp1d(p.radius / p.radius[0], E)(x[i]) * 0.01
        # print("E in the calculation (Zahn predicts 45.48 for this) = {}".format(Ecoeff))

        xa = compute_xa(p, Ecoeff, ratio)
        # print("xa=", xa)

        lambdalm = (
            0.8725
            * math.pow(alphaprime, 4.0 / 3.0)
            * math.pow(Ecoeff, 2.0 / 3.0)
            * compute_lambda_bracket(xa, xb)
        )

    return lambdalm


###############################
# RADIATIVE ENVELOPE ROUTINES #
###############################
def dUyy_dx(U, x, Ayy, Byy):
    """
    Definition of function for odeint to solve eq. (B.17)
    Here U is a vector such that y=U[0] and z=U[1].

    Input:	U	vector for odeint	(list of 2 floats)
                x	variable for the function	(float)
                Ayy	obtained from interp in routine result	(function)
                Byy	obtained from interp in routine result	(function)
    Output:	input for odeint	(float)
    """
    # This function should return [y', z']
    #    print('tentative of interp at x=', x)
    if x > 1.0:
        x = 1.0  # this is a crappy fix I came up with on 11.03.21 GMM
    return [U[1], -Ayy(x) / x * U[1] - Byy(x) / math.pow(x, 2.0) * U[0]]


##################################################################################################
def dU_dx(x, U, A, w):
    """
    Definition of function for odeint to solve eq. (B.13)
    Here U is a vector such that y=U[0] and z=U[1].

    This function should return [y', z']

    U = [ X, dX/dx ]
    we return
    dU/dx = [ dX/dx , d^2X / dx^2 ]

    where d^2X/dx^2 = 6X/x^2 - A dX/dx = 6/x^2 U[0] - A dX/dx
    from Siess+2013 eq.(B13)

    Input:	x	variable for the function	(float)
                U	vector described above		(list of 2 floats)
                A	structure quantity from result	(function)
                w	weights? not sure this is needed
    Output	dU/dx as defined above			(list of 2 floats)
    """
    X = U[0]
    dXdx = U[1]
    return [dXdx, -A(x) * dXdx + 6.0 * X / (x * x)]


############################################################
def Xresid(x, X, dXdx, d2Xdx2, A):
    """
    Function expression of Siess+2013 eq. (B.13) for solver
    This routine returns the residuals for the X differential equation

    Input:	x 	point at which function is evaluated 	(float)
                X, dXdx, d2Xdx2		terms of the equation at x	(floats)
                A	structure quantity (function)
    Output:	residual for the X differential equation
    """
    return x * x * d2Xdx2 + A(x) * x * x * dXdx - 6.0 * X


############################################################
def Xseries(x, A, test=True, nmax=100):
    """
    Compute series solution to Siess+2013 eq. (B.13)
    This routine computes the series solution to the X equation

    Input:	x	point at which function is evaluated    (float)
                A	structure quantity 			(function)
                test	if True, test convergence of the series (Boolean)
                nmax	max number of terms in X series		(integer)
    Output:	n	number of iterations for convergence	(integer)
                Xsum	X(x) result of differential eq at x	(float)
                Xsum1	dX/dx(x) first derivative of X at x	(float)
                Xsum2	dX^2/dx^2(x) second derivativ of X at x	(float)
    """
    r = 1.0 / x
    r1 = r
    r2 = r
    converged = False

    Xsum = 0.0  # X
    Xsum1 = 0.0  # dX/dx
    Xsum2 = 0.0  # d^2X / dx^2
    n = 0

    while converged == False:
        dx = r * x
        dx1 = r1 * x
        dx2 = r2 * x

        Xsum += dx
        Xsum1 += dx
        Xsum2 += dx

        n += 1
        n3 = n + 3
        r = dx * A(x) * n3 / (6.0 - n3 * (n + 4.0))

        if n > 10 and abs(r) < 1e-12:
            converged = True
        else:
            n2 = n + 2
            r1 = r * n3 / n2
            r2 = r * n2 * n3 / ((n + 1) * (n + 2))

        if test:
            assert abs(r) < 1e30, "overflow in X series"
            assert n < nmax, "X series ran away with large n2"

    Xsum *= x * x * x  # X
    Xsum1 *= 3.0 * x * x  # dX/dx
    Xsum2 *= 6.0 * x  # d^2X/dx^2

    return (n, [Xsum, Xsum1, Xsum2])


##################################################################################################
def resultX(xs, A, Ap):
    """
    Calculation of X from eq.(B.13) with coefficients a given by eq. (B.16)

    Input: xs locations at which we want to find X 	(array)
           A  is drho/dx from interpolations		(function)
           Ap is d^2rho / dx^2 at these locations	(function)
    Output: X and dX/dt		solution of the solve_ivp run (2-float array)
    """

    # first shell
    x = xs[0]
    (n, X0) = Xseries(x, A)

    # show the results at the first point
    resid = Xresid(x, X0[0], X0[1], X0[2], A)
    #    print('X at first shell converged after',n,
    #          ' terms: X=',X0[0],'dX/dx=',X0[1],'d^2X/dx^2=',X0[2])
    #    print('X residual at first shell',resid)

    # initial state
    U0 = [X0[0], X0[1]]

    # call solve_ivp to find X(x) through the star
    solution = solve_ivp(
        fun=dU_dx,
        t_span=(xs[0], xs[-1]),
        y0=U0,  # initial conditions
        method="RK45",  # solver of choice
        t_eval=xs,  # solution points
        rtol=1e-3,  # relative tolerance [1e-3]
        atol=1e-6,  # absolute tolerance [1e-6]
        # dense_output = True,   # turn off dense output
        args=(A, 0),  # args for dU_dx
    )

    # solution.y[0] is X
    # solution.y[1] is dX/dx
    # return both X and dX/dx
    return solution


# TODO complete comments for functions below this line
##################################################################################################
def resultY(xs, Ayy, Byy):
    """
    Calculation of Y from Siess+2013 eq. (B.17)

    Input: xs 	x values on which to compute Y 	(array of floats)
           Ayy 	structure quantity interpolated	(function)
           Byy  structure quantity interpolated (function)
    Output: Y 	result of Y differential equation at points xs	(array of floats)
    """
    disc = math.sqrt((Ayy(xs[0]) - 1.0) ** 2.0 - 4.0 * Byy(xs[0]))
    D1 = (1.0 - Ayy(xs[0]) + disc) / 2.0
    D2 = (1.0 - Ayy(xs[0]) - disc) / 2.0
    #    print ("D1,D2", D1, D2)
    Y0 = math.pow(xs[0], D1)
    Yp0 = math.pow(D1 * xs[0], D1 - 1)

    # use odeint to solve the ode
    Uyy0 = [Y0, Yp0]
    #    print('debug2', xs)
    Uyys = odeint(dUyy_dx, Uyy0, xs, args=(Ayy, Byy))
    Y = Uyys[:, 0]
    return Y


################################
def ggg(x, gradr_minus_grada):
    """
    This function is a workaround for the root call in find_interface_precise
    It needs to be called as an outside function
    Input : x   	is the point where to evaluate function		(float)
            gradr_minus_grada 	the interpolated gradient difference	(function)
    Output: function gradr_minus_grada evaluated at x			(float)
    """
    return gradr_minus_grada(x)


################################
def find_convcore_interface(p):
    """
    This function finds the interface more precisely wrt find_interface
    to do so I interpolate the gradr-grada function and look for the roots

    Input:	p	a MESA profile read with mesa_reader
    Output: interf	the location of the core/envelope interface in units of relative radius x	(float)
    """

    iconv = -1
    # first we roughly assess where the core boundary is
    x = p.radius / p.radius[0]
    for i in range(p.num_zones - 1):
        if (p.gradr[i] - p.gradL[i]) < 0 and (p.gradr[i + 1] - p.gradL[i + 1]) > 0:
            iconv = i
    if iconv == -1:
        # print('no convective core')
        interf = 1
    else:
        # print('convective zone in between r={} and r={}'.format(x[iconv], x[iconv+1]))
        gradr_minus_gradL = interp1d(x, p.gradr - p.gradL, kind="cubic")

        # Uses a bisection
        interf = bisection(ggg, gradr_minus_gradL, x[iconv], x[iconv + 1], 40)
    #  print("interf to find xb =", interf)
    return interf


##################################################################################################
def compute_E2_bracket(p):
    """
    This routine computes [R/gs d/dx (-gB/x**2)f ]**(-1/3) from eq (B.1)
    This bit aims at computing a proper derivative, there are many things to do and check

    Input:	p	a MESA profile read with mesa_reader
    Output: bracket term from E2 equation descrived above	(float)
    """

    xp = find_convcore_interface(p)
    # N2_from_nabla3 =
    #    - G*M_sun/R_sun/R_sun*p.mass/p.radius/p.radius *
    # (-p.dlnRho_dlnT_const_Pgas)/p.pressure_scale_height/R_sun * (p.gradT-p.gradL)
    N2_from_nabla3 = p.brunt_N2
    # - G*M_sun/R_sun/R_sun*p.mass/p.radius/p.radius *
    # (-p.dlnRho_dlnT_const_Pgas)/p.pressure_scale_height/R_sun * (p.gradT-p.gradL)

    # compute bracket
    gs = G * p.mass[0] * M_sun / (p.radius[0] * p.radius[0] * R_sun * R_sun)  # gs

    d_over_dx_term = interp1d(
        p.radius / p.radius[0],
        derivative(N2_from_nabla3 / p.radius / p.radius, p.radius),
    )
    #    print('gs', gs)
    #    print('N2_from_nabla3', N2_from_nabla3)
    #    print('d_over_dx_term(xp)', d_over_dx_term(xp))
    return pow(d_over_dx_term(xp) * p.radius[0] * R_sun / gs, -1.0 / 3.0)


##################################################################################################
def E2(p):
    """
    This routine computes E2 for a given profile

    Input:	p	a MESA profile read with mesa_reader
    Output:     E2 	coefficient for dynamical tides	(float)
    """
    # First we compute the constant terms in E2 -> eq. (B.1) with n=2
    Rstar = p.radius[0] * R_sun
    Mstar = p.mass[0] * M_sun

    # find the convection zone interface shell
    # interface_shell = find_interface(p)
    xp = find_convcore_interface(p)

    # logrho_f is an array of log10(density) at each shell face
    logrho_f = cell2face(p.logRho, p.mass, dm_is_m=True, m_center=0)
    logP_f = cell2face(p.logP, p.mass, dm_is_m=True, m_center=0)

    # density at the convection zone interface
    # rhof_bef = exp10(logrho_f[interface_shell])
    # changed to use the more accurate inference of density
    #    print ('Rstar, Mstar, xp', Rstar, Mstar, xp)
    rhof = exp10(interp1d(p.radius / p.radius[0], logrho_f)(xp))
    #    print ('Rstar, Mstar, rhof', Rstar, Mstar, rhof)

    ############################################################
    # solve for H2

    # array of relative radii
    x = p.radius / p.radius[0]

    # xs are the locations where we solve for X,Y hence H2
    xs = np.linspace(x[p.num_zones - 1], x_photosphere, 2000)

    # d log10 rho / d x
    dlogrho_dx = derivative(logrho_f, x)

    for i in range(len(x) - 1):
        if i > 0:
            dx = x[i] - x[i - 1]
            # print("shell=",i,"x=",x[i],"dr=",p.radius[i]-p.radius[i-1],
            #       'dx=',dx,'dlogrho/dx=',dlogrho_dx[i])
    #            if abs(dlogrho_dx[i])>1e3 :
    #                print("Large derivative error")
    #                print("i-1 =",i-1,' x=',x[i-1],
    #                      'rho',p.logRho[i-1],'dlogrho/dx=',dlogrho_dx[i-1])
    #                print("i   =",i,' x=',x[i],'dx=',x[i]-x[i-1],
    #                      'rho',p.logRho[i],'dlogrho/dx=',dlogrho_dx[i])
    #                print("i+1 =",i+1,' x=',x[i+1],'dx=',x[i+1]-x[i],
    #                      'rho',p.logRho[i+1],'dlogrho/dx=',dlogrho_dx[i+1])
    # exit()

    # d^2 log10 rho / d x^2
    d2logrho_dx2 = derivative(dlogrho_dx, x)

    # dlnrho/dx
    dlnrho_dx = math.log(10) * dlogrho_dx

    # d^2 lnrho / dx^2
    d2lnrho_dx2 = math.log(10) * d2logrho_dx2

    # A = -d ln(rho) / d x = - ln(10) d log10(rho) / dx
    A = interp1d(x, -dlnrho_dx, kind="cubic")

    # Ap = - d^2 ln(rho) / d x^2 = dA / dx
    dAdx = interp1d(x, d2lnrho_dx2, kind="cubic")

    # solve for X and dX/dx
    solution = resultX(xs, A, dAdx)
    X = solution.y[0]
    dXdx = solution.y[1]

    A_array = []
    dAdx_array = []

    for _x in xs:
        A_array.append(A(_x))
        dAdx_array.append(dAdx(_x))

    # solve for Y
    # compute rhobar
    rho_over_rhobar = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        rho_over_rhobar[i] = (
            exp10(p.logRho[i])
            / (p.mass[i] * M_sun)
            * (4.0 / 3.0 * np.pi * math.pow(p.radius[i] * R_sun, 3.0))
        )

    #    print('x range for interpolation at Ayy', x)
    Ayy = interp1d(x, 6.0 * (rho_over_rhobar - 1.0), kind="cubic")
    Byy = interp1d(x, 6.0 * (1.0 - 2.0 * rho_over_rhobar), kind="cubic")

    #    print('xs fed into resultY functions',xs)
    Y = resultY(xs, Ayy, Byy)  # solve for Y

    # compute the integral
    W = X * (
        np.gradient(np.gradient(Y, xs, edge_order=2), xs, edge_order=2)
        - 6.0 * Y / (xs * xs)
    )

    # find the interface relative radius
    #    print ("the interface is at ", xp)

    integral = 0.0
    for i in range(len(xs) - 1):
        # if (xs[i]<xmax):
        if xs[i] < xp:
            integral += (xs[i + 1] - xs[i]) * (W[i + 1] + W[i]) / 2.0
        else:
            break
    #        print ("when integrating: i, sx_i, int, Xi, Y1", i, xs[i], integral, X[i],Y[-1])
    H2 = integral / X[i - 1] / Y[-1]  # we divide by X at interface and Y at core

    # exit()

    # putting everything together
    result = (
        gamma2
        * rhof
        * math.pow(Rstar, 3)
        / Mstar
        * compute_E2_bracket(p)
        * math.pow(H2, 2)
    )

    return result


##################################################################################################
def E2_siess(p, zams_age, tams_age):
    """
    E2 fits as described in Siess+2013 section B.4
    At the moment this only works with Z  = 0.02,
    otherwise you have to replace the array a with an interpolated table

    Ages at the ZAMS and TAMS are provided to compare with Siess's fit

    Input:	p	  MESA profile from mesa_reader
                zams_age  MESA age at the ZAMS		(float)
                tams_age  MESA age at the TAMS		(float)
    Output:	E2 estimated the Siess way	(float)
    """
    # TODO make this work for other metallicities
    # TODO use Siess values as a check

    logM = math.log10(p.star_mass)
    logZ = math.log10(0.02)

    # Coefficients only valid for Z=0.02
    # starts at 0 -> index shifted by 1 wrt the paper
    a = [
        2.5054,
        -8.8783,
        -2.2369,
        -2.5918,
        19.6305,
        57.4532,
        -302.7735,
        6.5366,
        576.7844,
        -58.2294,
    ]

    # Main sequence lifetimes from Siess+2013 eq.(B.24)
    # t_MS1 = (
    #    4881 + 31787 * math.pow(p.star_mass, 1.3) + 7522 * math.pow(p.star_mass, 2.5)
    # )
    # t_MS2 = 3467 * math.pow(p.star_mass, 1.2) + 1125 * math.pow(p.star_mass, 2.5)
    # t_MS3 = 1 - 0.0281 * logZ - 0.0139 * math.pow(logZ, 2) - 0.00179 * math.pow(logZ, 3)
    #
    # math.pow(10,t_MS1/t_MS2*t_MS3)   #using Siess's fit

    t = min((p.star_age - zams_age) / tams_age, 1)
    ttt = math.pow(t, 2) / (1 + t)

    # Compute E2 from eq. (B.23)
    log_E2 = a[0] * logM + a[1] + a[2] * logM * ttt + a[3] * ttt
    log_E2 += (a[4] * logM + a[5]) / (
        a[6] * logM + a[7] + a[8] * logM * ttt + a[9] * ttt
    )

    return math.pow(10, log_E2)


##################################################################################################
def E2_zahn(p):
    """
    We compute E2 from Zahn's scaling in eq. (51)
    the stellar mass is in solar units already

    Input:	p	  MESA profile from mesa_reader
    utput:	E2 estimated the Zahn way	(float)
    """
    return 1.592e-9 * math.pow(p.star_mass, 2.84)


##################################################################################################


def find_profile(path, number):
    """
    Routine to find the number of the last profile saved for a given evolution.mod file

    Input:	path	subfolder containing MESA run	(string)
                number  model number of the profile file to be found (integer)
    Output:	filename of profile matching the proper model number (string)
    """
    # having a different profile name can be an issue here -> implement a check GMM
    # what if the profile file cannot be found? what if it doesn't exist? GMM
    f = open("{}/profiles.index".format(path))
    nl = int(f.readline().split()[0])
    for i in range(nl):
        tmp = (
            f.readline().split()
        )  # 0 is profile num, 1 is useless, 2 is profile file number
        #    print tmp[0]
        if int(tmp[0]) == number:
            return "{}/profile{}.data".format(path, tmp[2])


################################################################################
def core_env(p):
    """
    These routine provides the convective envelope/core masses and radii
    It relies on a rederivation of the core interface

    Input:  p   MESA profile opened with mesa_reader
    Output: Mcore   mass included in core           (float)
        Menv    mass included in envelope       (float)
        Rcore   radius at the surface of the core   (float)
        Renv    radius at the base of the envelope  (float)

    Check documentation for more details on how these quantities are defined!
    """

    # quick and dirty fix
    # find where the radius is 98% of the photosphere one
    for j in range(p.radius.__len__()):
        # decreasing radius
        if (
            p.radius[j] > 0.98 * p.photosphere_r
            and p.radius[j + 1] < 0.98 * p.photosphere_r
        ):
            jx = j + 1  # jx is where R=0.98R

    sch_simple = np.zeros(p.radius.__len__())
    for j in range(p.radius.__len__()):
        if j < jx:
            sch_simple[j] = p.ledoux_stable[jx]
        else:
            sch_simple[j] = p.ledoux_stable[j]
    #   print( (p.initial_mass-p.mass[jx])/p.initial_mass )

    if sch_simple[0] == 0:
        #       print("we have a convective env")
        for i in range(sch_simple.__len__() - 1):
            if sch_simple[i] == 0 and sch_simple[i + 1] == 1:
                Renv = p.radius[i] / p.radius[0]
                Menv = 1.0 - p.mass[i] / p.mass[0]
                break
    else:
        Renv = 1.0
        Menv = 0.0

    if sch_simple[-1] == 0:
        #       print("we have a convective core")
        for i in range(sch_simple.__len__() - 1):
            if sch_simple[-i - 1] == 0 and sch_simple[-i - 2] == 1:
                Rcore = p.radius[-i - 1] / p.radius[0]
                Mcore = p.mass[-i - 1] / p.mass[0]
                break
    else:
        Rcore = 0.0
        Mcore = 0.0

    if np.amax(sch_simple) == 0:
        #       print("the star is actually fully convective")
        Mcore = 1  # p.star_mass
        Menv = 1  # p.star_mass
        Rcore = 1  # p.radius[0]
        Renv = 0.0

    # TODO include a check wrt the core size given by MESA

    return Mcore, Menv, Rcore, Renv


################################################################################
def core_env_overshooting(h):
    """
    This routine provides the core mass and radius with overshooting
    It relies on the mixing types provided by the MESA code, and
    NOT on a rederivation of core properties as is done in core_env

    Input:  h   MESA fistory file opened with mesa_reader
    Output: Mov mass included in core INCLUDING overshooting    (float)
        Rov radius at surface of core+overshooting layer    (float)
    """

    # retrieve the overshooting mass from MESA mix_type and mix_qtop
    if h.mix_type_1[-1] == 1:  # test if core is convective
        if h.mix_type_2[-1] == 3:  # test if next-to-core layer is overshooting
            Mov = h.mix_qtop_2[-1]
        else:
            Mov = h.mix_qtop_1[-1]
    else:
        Mov = 0

    # retrieve the overshooting radius from MESA mix_relr_type and mix_relr_top
    if h.mix_relr_type_1[-1] == 1:  # test if core is convective
        if h.mix_relr_type_2[-1] == 3:  # test if next-to-core layer is overshooting
            Rov = h.mix_relr_top_2[-1]
        else:
            Rov = h.mix_relr_top_1[-1]
    else:
        Rov = 0

    return Mov, Rov


################################################################################
def E2_calc(p):
    """
    This routine is a very simple wrapper for the tides routines, and computes E2
    E2 is the coefficient needed to compute the circularisation and
    synchronisation timescales in Zahn's theory of dynamical tides

    Input:  p       MESA profile opened with mesa_reader
    Output: E2  coefficient for the dynamical tides   (float)
    """
    # from tides_routines import E2, find_convcore_interface

    xcore = find_convcore_interface(p)
    x = E2(p)
    return x


################################################################################
def E2_calc_fallback(profile_for_E2):
    """
    This routine is a fallback case when the profile originally used for E2 yields a NAN.
    Such errors can happen when the profile has a grid point with a faulty
    negative (grad-gradL) point inside the core.

    The fix is to take the previous model
    Input:  p       MESA profile ORIGINALLY USED for E2
    Output: E2  coefficient for the dynamical tides     (float)
    """
    # from tides_routines import E2, find_convcore_interface

    print("E2 is NAN, we revert to fallback option : use previous model")
    #    print("ERROR ERROR ETWO IS NAN, {}".format(profile_for_E2.replace(fold,'').replace('/profile','').replace('.data','')))
    # try doing it with the previous model see if it NaNs again
    #    print('RETRY ETWO')

    # Change the profile that is actually used to the immediately previous one
    tmp = (
        int(
            profile_for_E2.replace(fold, "")
            .replace("/profile", "")
            .replace(".data", "")
        )
        - 1
    )  # new profile number
    tmpstr = "{}/profile{}.data".format(fold, tmp)
    pbis = mr.MesaData(tmpstr)

    # Recompute E2 from the other profile
    xcore = find_convcore_interface(pbis)
    x = E2(pbis)
    #    print('ETWO', tmpstr, E2_calc(pbis))
    return x


################################################################################
def E_calc(p):
    """
    This routine is a wrapper for the tides routines, and computes E at envelope base
    E is the coefficient needed to compute the circularisation and
    synchronisation timescales in Zahn's theory of equilibrium tides

    Input:  p   MESA profile opened with mesa_reader
    Output: Ecoeff  coefficient for the equilibrium tides   (float)
    """
    # from tides_routines import produce_E_array, find_convcore_interface

    #  # Test if star is fully convective -> this block is coded properly but seems useless here
    #  fully_conv = True
    #  for i in range(p.num_zones-1,0,-1):
    #      print(i, p.radius[i]/p.radius[0], p.ledoux_stable[i], fully_conv)
    #      if p.radius[i]/p.radius[0] >0.98:
    #          break
    #      if p.ledoux_stable[i] == 1:
    #          fully_conv = False
    #          break

    E = produce_E_array(p)
    xb = find_convenv_interface(p)
    if xb < 0.01:
        Ecoeff = interp1d(p.radius / p.radius[0], E)(0.01)
    else:
        Ecoeff = interp1d(p.radius / p.radius[0], E)(xb)

    if xb > 0.997:
        Ecoeff = 0

    return Ecoeff


################################################################################


def compute_moment_of_inertia_factor(p):
    """
    This routine computes the moment of inertia factor, sometimes defined as k2 or beta^2
    but DIFFERENT from "apsidal constant k2" in MESA
    This calculation relies on Claret's formula

    Input:  p       MESA profile opened with mesa_reader
    Output: moif    moment of inertia factor        (float)
    """

    # TODO put Rsun Msun in a dedicated general model, or extract MESA values
    Rsun = 6.957e10
    Msun = 1.9885e33
    mantissa = np.zeros(p.num_zones)
    rho = np.zeros(p.num_zones)
    for i in range(p.num_zones):
        rho[i] = math.pow(10, p.logRho[i]) * math.pow(Rsun, 3) / Msun
        mantissa[i] = 8.0 * np.pi / 3.0 * rho[i] * math.pow(p.radius[i], 4)
    r = p.radius

    mantissa_f = interp1d(r, mantissa, kind="cubic")
    I = quad(fff, p.radius[-1], p.radius[0], args=(mantissa_f))[0]
    moif = I / (p.mass[0] * math.pow(p.radius[0], 2))
    # print('MOIF = I/(MR^2) = {}'.format(moif))
    return moif


def compute_moment_of_inertia_factor_fallback(p):
    """
    Fallback calculation of the moment of inertia factor, sometimes defined as k2 or beta
    This calculation relies on Rob's simplified formula + no python complex operations

    Input:  p       MESA profile opened with mesa_reader
    Output: moif    moment of inertia factor        (float)
    """
    I = 0

    for i in range(p.num_zones - 1):
        I += 2.0 / 3.0 * p.radius[i] * p.radius[i] * (p.mass[i] - p.mass[i + 1])
    moif = I / (p.mass[0] * math.pow(p.radius[0], 2))
    return moif
