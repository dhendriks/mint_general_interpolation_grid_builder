#!/usr/bin/python3

"""derivative and integration calculations"""

import os

import numpy as np
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from PyPDF2 import PdfMerger

from mint_general_interpolation_grid_builder.functions.table_functions.mint_interpolation_table_reader import (
    mint_interpolation_table_reader,
)
from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)


def smooth_quantity(y, num_points=5):
    """Mean smoothing of quantity with phase in at edges and including num_points each side"""
    if len(y) < 2 * num_points + 1:
        num_points = len(y) // 2 - 1
        # print('reducing size of smoothing region')
    y_smoothed = []
    for i, yi in enumerate(y):
        if i < num_points:
            num_points_smooth = i
        elif i > len(y) - 1 - num_points:
            num_points_smooth = len(y) - 1 - i
        else:
            num_points_smooth = num_points
        # print(num_points_smooth)
        y_smoothed += [np.mean(y[i - num_points_smooth : i + 1 + num_points_smooth])]

    return y_smoothed


def fit_rejuvenation_with_signoid(m, h, mesa_time, target_h):
    # fit data where rejuvenation occurs
    new_times = mesa_time
    if any(np.diff(m.hist.center_h1) > 0):
        rejuv_region = m.hist.center_h1[np.where(np.diff(m.hist.center_h1) > 0)]
        rejuv_region = [
            k for k in rejuv_region if (k > target_h[-4] and k < target_h[3])
        ]
        if len(rejuv_region) > 1:
            min_h_index = np.where(h < np.min(rejuv_region))[0][0]
            max_h_index = np.where(h > np.max(rejuv_region))[0][-1]
            index_list = [
                max_h_index - 3,
                max_h_index - 2,
                max_h_index - 1,
                max_h_index,
                min_h_index,
                min_h_index + 1,
                min_h_index + 2,
                min_h_index + 3,
            ]

            t_section = mesa_time[index_list] / mesa_time[-1]
            x_section = h[index_list]

            p0 = [
                np.min(t_section) - np.max(t_section),
                np.median(x_section),
                1,
                max(t_section),
            ]
            popt, pcov = curve_fit(sigmoid, x_section, t_section, p0, method="dogbox")

            fitted_time = (
                sigmoid(
                    h[max_h_index : min_h_index + 1], popt[0], popt[1], popt[2], popt[3]
                )
                * mesa_time[-1]
            )
            new_times = np.sort(
                np.append(
                    mesa_time[:max_h_index],
                    np.append(fitted_time, mesa_time[min_h_index + 1 :]),
                )
            )

    return new_times


def sigmoid(x, L, x0, k, b):
    # for use in fit_rejuvenation_with_signoid
    y = L / (1 + np.exp(-k * (x - x0))) + b
    return y


def derivative_4_adapted(x, t, x_targets, target_indexes, decreasing=True):
    """adpated from Gio's derivative_4 routine to work with one history file
    INPUT: x, t, x_targets
    OUPUT: dx/dt, d2x/dt2
    """
    # fig,ax=plt.subplots()

    dxdt = []
    d2xdt2 = []

    # repeat first and last values so routine compatible for end points
    x_targets_wrapped = [x[0]] + list(x_targets) + [x[-1]]

    for i, target in enumerate(x_targets):
        # select section of data around x_target
        if decreasing == True:
            uplim = (target + x_targets_wrapped[i]) / 2
            lowlim = (target + x_targets_wrapped[i + 2]) / 2
            boundary_index = np.where(x < lowlim)[0][0]
        else:
            lowlim = (target + x_targets_wrapped[i]) / 2
            uplim = (target + x_targets_wrapped[i + 2]) / 2
            boundary_index = np.where(x > uplim)[0][0]

        index_section = np.where((x > lowlim) & (x < uplim))[0]
        index_section = [j for j in index_section if j < boundary_index]
        new_index_section = index_section

        # end = np.where(np.diff(index_section)>1)[0]
        # if len(end) > 0 :
        #     print(end[0])
        #     index_section = index_section[:end[0]]

        # new_index_section = []
        # for j,ind in enumerate(index_section):
        #     if x[index_section[j]] < x[index_section[j-1]]:
        #         new_index_section+=[ind]

        t_section = t[new_index_section]
        x_section = x[new_index_section]
        # print(t_section, x_section)

        # ax.plot(x_section,t_section)

        # fit 2nd order polynomial to data
        params, params_covariance = curve_fit(
            test_func, t_section, x_section, p0=[0, 0, 0]
        )

        # calculate derivatives at x_target and add derivatives to list
        dxdt += [params[0] * 2 * t[target_indexes[i]] + params[1]]
        d2xdt2 += [params[0] * 2]

    # plt.show()

    return dxdt, d2xdt2


def test_func(value, a, b, c):
    # used for deriative_4_adapted
    """
    Very simple second-order polynomial function
    Called by some fitting routine below

    Input: a b c        coefficients of polynomial ax^2+bx+c    (floats)
           value        point where the function is computed    (float)
    Output: polynomial ax^2+bx+c at x=value
    """
    #    return a * np.cos(b * x)
    return a * value * value + b * value + c


def quadractic(x, a, b):
    return 0.8 + b * x + a * x ** 2


def quadractic_solver(y, a, b, c=0.8):
    # c = 0.979-y
    return (-b - np.sqrt(b ** 2 - 4 * a * (c - y))) / (2 * a)


def integrate_euler(X, dX_dt, delta_t):
    deriv = interpolate.interp1d(X, dX_dt)
    X_values = [X[0]]

    step = 0
    simulating = True
    while simulating:
        Xi = X_values[-1]
        try:
            X_values += [Xi + deriv(Xi) * delta_t]
        except:
            break

        step += 1

        # if max steps
        if step > 50000:
            simulating = False

    # print(X_values)
    # print(step)

    n = np.arange(0, len(X_values))
    t = [delta_t * i for i in n]
    # t = np.arange(0, delta_t * len(n), delta_t)

    # interpolate to get times at target_he values
    time = interpolate.interp1d(X_values, t, fill_value="extrapolate")

    return time(X)


def integrate_euler_2D(X, dX_dt, delta_t, dZ_dt):

    deriv = interpolate.interp1d(X, dX_dt)
    X_values = [X[0]]

    Z_values = [0]
    Z_deriv = interpolate.interp1d(X, dZ_dt)

    step = 0
    simulating = True
    while simulating:
        Xi = X_values[-1]
        Zi = Z_values[-1]
        try:
            X_values += [Xi + deriv(Xi) * delta_t]
            Z_values += [Zi + Z_deriv(Xi) * delta_t]
        except:
            break

        step += 1

        # if max steps
        if step > 50000:
            simulating = False

    # print(X_values)
    # print(step)

    n = np.arange(0, len(X_values))
    t = [delta_t * i for i in n]
    # t = np.arange(0, delta_t * len(n), delta_t)

    # interpolate to get times at X values
    time = interpolate.interp1d(X_values, t, fill_value="extrapolate")

    # interpolate to get Z at X values
    Z_interp = interpolate.interp1d(X_values, Z_values, fill_value="extrapolate")

    return time(X), Z_interp(X)
