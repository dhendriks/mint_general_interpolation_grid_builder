import os
from datetime import datetime


def get_mass_list(direc_path):

    # Get a list of directories
    directories = os.listdir(direc_path)

    # Initialize an empty list to store directories that can be converted to float
    float_directories = []

    # Iterate through the directories and try to convert each one to float
    for directory in directories:
        try:
            float_directories.append(float(directory))
        except ValueError:
            pass  # Ignore directories that cannot be converted to float

    return float_directories


def get_run_time_mass(M, direc_path):
    slurm_output_filepath = os.path.join(direc_path, f"{M}/slurm.out")
    timestamps = []
    try:
        with open(slurm_output_filepath, "r") as file:
            for line in file:
                if line.startswith("DATE:"):
                    date_str = line.strip().split(": ")[1]
                elif line.startswith("TIME:"):
                    time_str = line.strip().split(": ")[1]
                    timestamp_str = f"{date_str} {time_str}"
                    timestamp = datetime.strptime(timestamp_str, "%Y-%m-%d %H:%M:%S")
                    timestamps.append(timestamp)

        # Print the extracted timestamps
        # print(timestamps)

        # Calculate the difference in time
        time_difference = (timestamps[1] - timestamps[0]).total_seconds() / 3600
        return time_difference
    except:
        return
