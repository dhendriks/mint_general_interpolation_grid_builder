import numpy as np
import matplotlib.pyplot as plt

from mint_general_interpolation_grid_builder.interpolation_table_builders.functions.tabulation_functions import (
    integrate_euler,
    fit_rejuvenation_with_signoid,
)


def rejuvenation_solutions(self, m, target_locs, mesa_time, h, t0, target_h, save_dir):
    #################################################################
    # comparison of methods to reduce error when rejuvenation occurs
    extra_h = np.array([0.225, 0.215, 0.205, 0.195, 0.185])
    # extra_h = np.append(extra_h,10**np.array([-2.25,-2.75,-3.25,-3.75,-4.25,-4.75,-5.25,-5.75]))
    high_res_targets = target_locs
    for n in extra_h:
        high_res_targets = np.append(
            high_res_targets, np.where(m.hist.center_h1 < n)[0][0]
        )
    high_res_targets = np.sort(high_res_targets)
    high_res_h = m.hist.center_h1[high_res_targets]
    high_res_times = m.hist.star_age[high_res_targets] - t0

    fig, ax = plt.subplots(3, 1, sharex=True, figsize=(8, 16))

    delta_t = mesa_time[-1] * 0.0005

    ax[0].plot(m.hist.center_h1, m.hist.star_age - t0, label="full MESA data")
    ax[1].plot(m.hist.center_h1, np.gradient(m.hist.center_h1, m.hist.star_age - t0))
    ax[2].plot(h, mesa_time)

    # normal resolution
    ax[0].plot(h, mesa_time, marker="s", markersize=4, label="normal res")
    ax[1].plot(h, np.gradient(h, mesa_time), marker="s")
    int_time = integrate_euler(h, np.gradient(h, mesa_time), delta_t)
    error = abs((int_time[-1] - mesa_time[-1]) / mesa_time[-1] * 100)
    ax[2].plot(h, int_time, marker="s", label=str(round(error, 2)))

    # increase resolution in X
    ax[0].plot(
        high_res_h, high_res_times, marker="o", markersize=4, label="increased res"
    )
    ax[1].plot(high_res_h, np.gradient(high_res_h, high_res_times), marker="o")
    int_time = integrate_euler(
        high_res_h, np.gradient(high_res_h, high_res_times), delta_t
    )
    error = abs((int_time[-1] - mesa_time[-1]) / mesa_time[-1] * 100)
    ax[2].plot(high_res_h, int_time, marker="o", label=str(round(error, 2)))

    # fit using a signoid
    new_times = fit_rejuvenation_with_signoid(
        m=m, h=h, mesa_time=mesa_time, target_h=target_h
    )
    ax[0].plot(h, new_times, marker="+", label="fit age")
    ax[1].plot(h, np.gradient(h, new_times), marker="+")
    int_time = integrate_euler(h, np.gradient(h, new_times), delta_t)
    error = abs((int_time[-1] - mesa_time[-1]) / mesa_time[-1] * 100)
    ax[2].plot(h, int_time, marker="+", label=str(round(error, 2)))

    # signoid with high resolution
    new_times_high_res = fit_rejuvenation_with_signoid(
        m=m, h=high_res_h, mesa_time=high_res_times, target_h=target_h
    )
    ax[0].plot(
        high_res_h, new_times_high_res, marker="x", label="fit age + increased res"
    )
    ax[1].plot(high_res_h, np.gradient(high_res_h, new_times_high_res), marker="x")
    int_time = integrate_euler(
        high_res_h, np.gradient(high_res_h, new_times_high_res), delta_t
    )
    error = abs((int_time[-1] - mesa_time[-1]) / mesa_time[-1] * 100)
    ax[2].plot(high_res_h, int_time, marker="x", label=str(round(error, 2)))

    ax[2].set_xlabel("Xc")
    ax[0].set_ylabel("Time (years)")
    ax[1].set_ylabel("dXc/dt $(yrs^{-1]})$")
    ax[2].set_ylabel("Time (years)")

    ax[0].set_xlim(0.1, 0.3)
    ax[0].set_ylim(2e9, 5e9)
    ax[1].set_ylim(-3e-10, 0)
    ax[2].set_ylim(2e9, 5e9)
    ax[0].legend()
    ax[2].legend()
    # plt.show()
    plt.savefig(save_dir + "plots/1.2M_rejuvenation_solutions.pdf")

    return
