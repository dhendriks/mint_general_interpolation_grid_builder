#!/usr/bin/python3

from pathlib import Path
import os
import sys

import multiprocessing
import setproctitle
import pickle
import numpy as np

import pandas as pd
from PyPDF2 import PdfMerger
import mesaPlot as mp
from scipy.integrate import cumulative_trapezoid

from mint_general_interpolation_grid_builder.core.InterpolationTableBuilder import (
    InterpolationTableBuilder,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_utility import MINTUtility


from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.quality_check import (
    QualityChecking,
)

from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.table_writing import (
    TableWriting,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.MESA_models_extraction import (
    MESAModelExtraction,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.interpolation_coordinate_remapping import (
    InterpolationCoordinateRemapping,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.MINT_testing import (
    MINTTester,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.binary_plots import (
    BinaryPlots,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.documentation import (
    InterpolationTableDocumentation,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder_extensions.phase_transitions import (
    PhaseTransitions,
)
from mint_general_interpolation_grid_builder.MINT.config.header_description_dict import (
    header_description_dict_scalars,
    header_description_dict_vectors,
)

from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.extracting_mesa_quantities import (
    get_cheby_quantities,
    get_convective_regions_and_tides,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.derivatives import (
    smooth_quantity,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.functions.MESA_run_time import (
    get_mass_list,
    get_run_time_mass,
)
from mint_general_interpolation_grid_builder.functions.plotting_routines.general_plot_routines.general_plot_routine_ms import (
    general_plot_routine as general_plot_routine_ms,
)
from mint_general_interpolation_grid_builder.functions_natalie.custom_mpl_settings import (
    load_mpl_rc,
)


class MINTTableBuilder(
    InterpolationTableBuilder,
    MINTUtility,
    QualityChecking,
    TableWriting,
    InterpolationTableDocumentation,
    MESAModelExtraction,
    BinaryPlots,
    InterpolationCoordinateRemapping,
    MINTTester,
    PhaseTransitions,
):
    def __init__(self, settings):

        # Main init
        self.settings = settings
        self.job_list = []
        self.job_results = []

        # Init mixin classes
        InterpolationTableBuilder.__init__(self, settings)
        MINTUtility.__init__(self, settings)
        QualityChecking.__init__(self)
        TableWriting.__init__(self)
        InterpolationTableDocumentation.__init__(self)
        MESAModelExtraction.__init__(self)
        BinaryPlots.__init__(self)
        InterpolationCoordinateRemapping.__init__(self)
        MINTTester.__init__(self)
        PhaseTransitions.__init__(self)

        # load matplotlib settings
        load_mpl_rc()

        # dictionaries storing all possible table quantities, split into scalars and vectors (must start with CHEBYSHEV)

        # header scalars
        self.p_dict_scalars = header_description_dict_scalars

        # header vectors
        self.p_dict_vectors = header_description_dict_vectors

        return

    def pre_readout_hook(self):

        # set up temp directory to store data
        self.temp_directory = os.path.join(self.settings["grid_directory"], "temp")
        Path(self.temp_directory).mkdir(parents=True, exist_ok=True)

        # set up plots directory
        self.plots_directory = os.path.join(self.settings["grid_directory"], "plots")
        Path(self.plots_directory).mkdir(parents=True, exist_ok=True)

        #########################################
        # analytics
        self.save_run_time_df()

        return

    def retrieve_detailed_mesa_models(self):
        """
        Provides job list from available masses

        """

        ##############################################################
        # read masses from available directories and create job list

        masses = self.get_masses_list_from_available_directories()
        print("\nTotal Masses Found:")
        print(masses)

        # masses = [0.08,0.1,0.2,1.0]
        # masses = [1.03]

        if self.evol_phase == "MS":
            masses_dict = {Mt: [Mt] for Mt in masses}
        else:
            masses_dict = self.get_binary_masses_dict_from_available_directories(masses)
            print("\nMasses dict for binary grid:")
            print(masses_dict)

        self.get_job_list(masses_dict=masses_dict)

        return

    def pre_multiprocessing_hook(self):
        """Quality checks on full MESA history files before building the table"""

        ########################################################################
        # print model info

        self.print_models_in_grid_info()

        if self.evol_phase == "MS":
            self.fill_table_edges_with_dummy_data()
        else:
            self.binary_history_file_plots()

        return

    def process_detailed_mesa_models(self, job_dict, result_queue):
        """
        This deals with individual jobs (i.e. tracks, stars etc)

        job_dict contains total mass and the corresponding initial_masses

        """

        # global variables
        Mt = job_dict["total_mass"]
        tmpfile_name_list = []
        self.round_precision = 5

        # loop over initial massses
        for Mi in job_dict["initial_masses"].keys():

            all_results_list = []

            # skip if bad mass
            if not job_dict["initial_masses"][Mi]["use_model"]:
                continue

            m, direc = self.load_history_file(Mt=Mt, Mi=Mi)

            ######################################
            # handle time and derivatives

            # only use unique targets locs for calculating gradients
            target_locs = np.unique(job_dict["initial_masses"][Mi]["target_locs"])

            # convert mesa ages to time since first time proxy target
            t0 = m.hist.star_age[target_locs[0]]
            mesa_time = m.hist.star_age[target_locs] - t0

            # smooth times to better resolve age
            # apply smoothing 3 times to reduce sharp edges
            new_times = smooth_quantity(smooth_quantity(smooth_quantity(mesa_time)))
            if np.any(np.diff(new_times) <= 0):
                raise ValueError("Time decreased: ", Mt, Mi)

            # calculate time proxy derivatives
            time_proxy_first_deriv = np.gradient(
                m.hist.interp_variable_hit[target_locs], new_times
            )
            time_proxy_second_deriv = np.gradient(time_proxy_first_deriv, new_times)
            if (self.scalar_input_columns[1] == "CENTRAL_DEGENERACY") and (
                time_proxy_first_deriv <= 0
            ).any():
                raise ValueError("Negative time proxy derivative:", Mt, Mi)

            # smooth core mass points
            if "CARBON_CORE_MASS_FRACTION" in self.scalar_input_columns:
                core_mass = m.hist.co_core_mass
            else:
                core_mass = m.hist.he_core_mass
            Mc_smoothed = smooth_quantity((core_mass / m.hist.star_mass)[target_locs])

            # calculate core mass derivatives
            first_deriv_Mc = np.gradient(Mc_smoothed, new_times)
            second_deriv_Mc = np.gradient(first_deriv_Mc, new_times)

            #######################################
            # cycle through interp variable targets

            target_locs = job_dict["initial_masses"][Mi]["target_locs"]
            self.target_locs = target_locs
            for i, target_index in enumerate(target_locs):

                # handle dummy data on MS
                if target_index == target_locs[i - 1]:
                    output_dict = all_results_list[-1].copy()
                    output_dict["WARNING_FLAG"] = 1

                    if self.evol_phase == "MS":
                        output_dict["CENTRAL_HYDROGEN"] = self.time_proxy_save_targets[
                            i
                        ]

                    all_results_list.append(output_dict)
                    continue

                # Construct output dictionary
                output_dict = {}

                ############################################
                # load profile
                p = mp.MESA()
                try:
                    p.loadProfile(
                        f=direc, num=m.hist.model_number[target_index], silent=True
                    )
                except:
                    print("Can not load profile file")
                    print(direc, m.hist.model_number[target_index])
                    sys.exit()
                p = p.prof

                ############################################
                # main quantities

                if "AGE" in self.scalar_output_columns:
                    output_dict["AGE"] = new_times[
                        i
                    ]  # model age since first interp variable target
                if "AGE_FRACTION" in self.scalar_output_columns:
                    output_dict["AGE_FRACTION"] = (
                        new_times[i] / new_times[-1]
                    )  # model age since first interp variable target as a fraction of total age
                if "RADIUS" in self.scalar_output_columns:
                    output_dict["RADIUS"] = p.photosphere_r  # photosphere radius (Rsun)
                if "LUMINOSITY" in self.scalar_output_columns:
                    output_dict[
                        "LUMINOSITY"
                    ] = p.photosphere_L  # photosphere luminosity (Lsun)
                if "LUMINOSITY_DIV_EDDINGTON_LUMINOSITY" in self.scalar_output_columns:
                    output_dict["LUMINOSITY_DIV_EDDINGTON_LUMINOSITY"] = (
                        10 ** m.hist.log_L_div_Ledd[target_index]
                    )
                if "NEUTRINO_LUMINOSITY" in self.scalar_output_columns:
                    output_dict["NEUTRINO_LUMINOSITY"] = (
                        10 ** m.hist.log_Lneu[target_index]
                    )
                if "HYDROGEN_LUMINOSITY" in self.scalar_output_columns:
                    output_dict["HYDROGEN_LUMINOSITY"] = (
                        10 ** m.hist.log_LH[target_index]
                    )
                if "HELIUM_LUMINOSITY" in self.scalar_output_columns:
                    output_dict["HELIUM_LUMINOSITY"] = (
                        10 ** m.hist.log_LHe[target_index]
                    )
                if "CENTRAL_DEGENERACY" in self.scalar_output_columns:
                    output_dict["CENTRAL_DEGENERACY"] = m.hist.center_degeneracy[
                        target_index
                    ]

                ############################################
                # core quantities

                if (
                    "HYDROGEN_EXHUASTED_CORE_MASS_FRACTION"
                    in self.scalar_output_columns
                ):
                    output_dict["HYDROGEN_EXHUASTED_CORE_MASS_FRACTION"] = (
                        m.hist.hydrogen_exhausted_core_mass / m.hist.star_mass
                    )[target_index]
                if "HELIUM_CORE_MASS_FRACTION" in self.scalar_output_columns:
                    output_dict["HELIUM_CORE_MASS_FRACTION"] = (
                        p.he_core_mass / p.star_mass
                    )
                if "HELIUM_CORE_RADIUS_FRACTION" in self.scalar_output_columns:
                    output_dict["HELIUM_CORE_RADIUS_FRACTION"] = (
                        m.hist.he_core_radius[target_index]
                        / 10 ** m.hist.log_R[target_index]
                    )
                if "CARBON_CORE_MASS_FRACTION" in self.scalar_output_columns:
                    output_dict["CARBON_CORE_MASS_FRACTION"] = (
                        m.hist.co_core_mass[target_index]
                        / m.hist.star_mass[target_index]
                    )
                if "CARBON_CORE_RADIUS_FRACTION" in self.scalar_output_columns:
                    output_dict["CARBON_CORE_RADIUS_FRACTION"] = (
                        m.hist.co_core_radius[target_index]
                        / 10 ** m.hist.log_R[target_index]
                    )

                if "MEAN_MOLECULAR_WEIGHT_CORE" in self.scalar_output_columns:
                    output_dict["MEAN_MOLECULAR_WEIGHT_CORE"] = m.hist.center_mu[
                        target_index
                    ]
                if "MEAN_MOLECULAR_WEIGHT_AVERAGE" in self.scalar_output_columns:
                    output_dict["MEAN_MOLECULAR_WEIGHT_AVERAGE"] = cumulative_trapezoid(
                        p.mu, p.mass, initial=0
                    )[-1] / (p.mass[-1] - p.mass[0])

                ############################################
                # evol phase transitions

                if "HELIUM_IGNITED_FLAG" in self.scalar_output_columns:
                    if (m.hist.ignited_helium[-1] == 1) and (
                        target_index == self.target_locs[-1]
                    ):
                        he_ignited = 1
                    else:
                        he_ignited = 0
                    output_dict["HELIUM_IGNITED_FLAG"] = he_ignited
                if "HELIUM_IGNITION_PARAMETER" in self.scalar_output_columns:
                    if m.hist.center_he4[-1] < m.hist.center_he4.max() - 0.001:
                        max_LHe = np.max(m.hist.log_LHe[self.target_locs])
                    else:
                        max_LHe = np.max(m.hist.log_LHe[self.target_locs]) + 1
                    output_dict["HELIUM_IGNITION_PARAMETER"] = (
                        m.hist.log_LHe[target_index] - max_LHe
                    )
                if "NEON_IGNITED_FLAG" in self.scalar_output_columns:
                    if (m.hist.ignited_neon[-1] == 1) and (
                        target_index == self.target_locs[-1]
                    ):
                        neon_ignited = 1
                    else:
                        neon_ignited = 0
                    output_dict["NEON_IGNITED_FLAG"] = neon_ignited
                if "ONSET_THERMAL_PULSES_FLAG" in self.scalar_output_columns:
                    if (m.hist.TP_flag[-1] == 1) and (
                        target_index == self.target_locs[-1]
                    ):
                        TP_flag = 1
                    else:
                        TP_flag = 0
                    output_dict["ONSET_THERMAL_PULSES_FLAG"] = TP_flag

                ############################################
                # central abundances

                element_dict = {
                    "HYDROGEN": "h1",
                    "HELIUM": "he4",
                    "CARBON": "c12",
                    "OXYGEN": "o16",
                    "NITROGEN": "n14",
                    "NEON": "ne20",
                }
                for element, isotope in element_dict.items():
                    quantity_name = f"CENTRAL_{element}"
                    if quantity_name in self.scalar_output_columns:
                        output_dict[quantity_name] = m.hist.data[f"center_{isotope}"][
                            target_index
                        ]

                ############################################
                # timescales

                timescale_dict = {
                    "KELVIN_HELMHOLTZ": "kh_timescale",
                    "DYNAMICAL": "dynamic_timescale",
                    "NUCLEAR": "nuc_timescale",
                }
                for timescale, mesa_name in timescale_dict.items():
                    quantity_name = f"TIMESCALE_{timescale}"
                    if quantity_name in self.scalar_output_columns:
                        output_dict[quantity_name] = m.hist.data[mesa_name][
                            target_index
                        ]

                ############################################
                # convection and tides

                output_dict = get_convective_regions_and_tides(
                    output_dict=output_dict,
                    p=p,
                    m=m,
                    target_index=target_index,
                    all_results_list=all_results_list,
                    scalar_output_columns=self.scalar_output_columns,
                )

                ######################################################################
                # vector quantities
                rel_mass = self.calc_rel_mass(p)

                output_dict = get_cheby_quantities(
                    output_dict=output_dict,
                    p=p,
                    rel_mass=rel_mass,
                    cheby_coords=self.cheby_coords,
                )

                ######################################################################
                # scalar input quanities

                output_dict["MASS"] = Mt

                output_dict[self.scalar_input_columns[1]] = m.hist.interp_variable_hit[
                    target_index
                ]
                output_dict[
                    f"FIRST_DERIVATIVE_{self.scalar_input_columns[1]}"
                ] = time_proxy_first_deriv[i]
                output_dict[
                    f"SECOND_DERIVATIVE_{self.scalar_input_columns[1]}"
                ] = time_proxy_second_deriv[i]

                if "HELIUM_CORE_MASS_FRACTION" in self.scalar_input_columns:
                    output_dict["HELIUM_CORE_MASS_FRACTION"] = Mc_smoothed[i]
                    output_dict[
                        f"FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION"
                    ] = first_deriv_Mc[i]
                    output_dict[
                        f"SECOND_DERIVATIVE_HELIUM_CORE_MASS_FRACTION"
                    ] = second_deriv_Mc[i]
                if "CARBON_CORE_MASS_FRACTION" in self.scalar_input_columns:
                    output_dict["CARBON_CORE_MASS_FRACTION"] = Mc_smoothed[i]
                    output_dict[
                        f"FIRST_DERIVATIVE_CARBON_CORE_MASS_FRACTION"
                    ] = first_deriv_Mc[i]
                    output_dict[
                        f"SECOND_DERIVATIVE_CARBON_CORE_MASS_FRACTION"
                    ] = second_deriv_Mc[i]

                ##############################################
                # warning flag
                output_dict["WARNING_FLAG"] = 0

                #############################################
                # initial mass
                output_dict["INITIAL_MASS"] = Mi

                ########################################################################
                # Add to all results
                output_dict = {
                    key: self.format_table_value(value)
                    for key, value in output_dict.items()
                }
                all_results_list.append(output_dict)

            #########################################
            # save track to temp file

            tmpfile_name = os.path.join(self.temp_directory, str(Mt) + "_" + str(Mi))

            tmpfile_name_list.append(tmpfile_name)

            with open(tmpfile_name, "wb") as f:
                pickle.dump({"results": all_results_list}, f)

        ###########################################
        # return results of multiprocessing

        process = multiprocessing.current_process()
        # print("Process {} finished loop".format(process))

        # pass back the job dict with a result stored in it
        job_dict["result"] = tmpfile_name_list

        result_queue.put(job_dict)
        process = multiprocessing.current_process()
        print("Process {} added result to queue".format(process))

        return

    def pre_gridbuilding_hook(self):
        """
        Dummy function to do some pre-gridbuilding checks (i.e. see the results of the multiprocessing have been structured correctly)
        """

        #######################################
        # make plots

        # check stellar age resolved
        if self.settings["plot_all_models"]:
            self.integrate_to_check_stellar_age_resolved()

        return

    def create_interpolation_table(self):
        """
        Override of the placeholder create_interpolation_table

        Deals with combining all the results of the individual jobs and building up the interpolation table.

        Depending on how the results are stored, we can now build the interpolation table

        """

        # order job_results in total mass
        self.job_results = sorted(self.job_results, key=lambda x: x["total_mass"])

        ################################################################
        # create full dataframe

        df_combined = self.combine_temp_datafiles_into_dataframe()

        df_combined = self.handle_columns_interpolation_table(df=df_combined)

        # write csv file of tracks
        tracks_filename = "tracks_Z%7.2e_%s.csv" % (
            self.settings["metallicity"],
            self.evol_phase,
        )
        tracks_filepath = os.path.join(self.settings["grid_directory"], tracks_filename)
        print("Saving tracks to csv file: ", tracks_filepath)
        df_combined.to_csv(tracks_filepath)

        ###########################################################
        # evol phase specific operations

        self.create_interpolation_table_hook(df=df_combined)

        #####################################################
        # write tables to files

        print("Writing MINT tables")
        for filename, df_table in self.MINT_tables.items():

            # check table
            df_table = self.check_table(table_df=df_table)

            table_file_path = os.path.join(self.settings["grid_directory"], filename,)
            self.write_dataframe_to_table_file(
                file_path=table_file_path, df=df_table,
            )
            self.compress_table_file(file_path=table_file_path)

        return

    def create_interpolation_table_hook(self, df):
        """default version of hook"""

        df_checked = self.check_table(table_df=df)
        self.MINT_tables = {
            self.format_interpolation_table_output_filename(): df_checked
        }

        return

    def prepare_table_for_mint(self, df, evol_phase, multiprocesses=True):

        if evol_phase == "GB":
            df = self.extrapolate_GB_tracks(df)

        df_remapped = self.remap_data_to_orthogonal_grid(
            df=df,
            varX=self.settings[f"{evol_phase}_scalar_input_columns"][1],
            varY=self.settings[f"{evol_phase}_scalar_input_columns"][2],
            multiprocesses=multiprocesses,
        )

        if evol_phase == "GB":
            df_remapped = self.add_helium_ignition_boundary(df=df_remapped)

        return df_remapped

    def post_gridbuilding_hook(self):
        """
        Output info on succesful and unsuccesful models and make plots

        """
        #######################################
        # make plots

        if self.evol_phase == "MS":
            general_plot_routine_ms(
                dataset_filename=os.path.join(
                    self.settings["grid_directory"],
                    self.format_interpolation_table_output_filename(),
                ),
                output_pdfs_dir=os.path.join(self.plots_directory, "table_quantities"),
                combined_pdf_filename=os.path.join(
                    self.plots_directory, "table_quantities_all.pdf"
                ),
            )

        #######################################
        # testing

        self.test_MINT_CME()

        return

    @staticmethod
    def calc_rel_mass(p):
        # default version of relative mass for when a he core exists
        # overidden in MS_sublcass for when there is no he core
        # force central point to have rel_mass = 0 and surface point to have rel_mass = 2

        Mc = p.he_core_mass
        mass = p.mass[1:-1]
        core_zones = np.flip(mass[np.where(mass < Mc)])
        env_zones = np.flip(mass[np.where(mass >= Mc)])

        rel_mass = np.sort(np.append([0, 2], np.append(core_zones, env_zones)))

        if np.isnan(rel_mass).any():
            print(rel_mass)
            print(Mc, p.star_mass)
            raise ValueError

        if np.isinf(rel_mass).any():
            raise ValueError("infinite value of relative mass: ", Mc, p.star_mass)

        return rel_mass

    @staticmethod
    def split_tracks_post_he_ignition(df):
        """
        Split tracks into two tables depending on wether they went through the helium-flash
        """

        # make sure each row has unique index
        df.reset_index(inplace=True, drop=True)

        df_he_flash = pd.DataFrame(columns=df.columns)
        df_no_he_flash = pd.DataFrame(columns=df.columns)

        unique_masses = df["MASS"].unique()

        for M in unique_masses:
            df_mass = df[df["MASS"] == M]
            initial_masses = df_mass["INITIAL_MASS"].unique()
            transition_initial_mass = df_mass.loc[
                (df_mass["HELIUM_CORE_MASS_FRACTION"] * df_mass["MASS"]).idxmin()
            ]["INITIAL_MASS"]
            if len(initial_masses[initial_masses <= transition_initial_mass]) > 1:
                df_he_flash = pd.concat(
                    (
                        df_he_flash,
                        df_mass[df_mass["INITIAL_MASS"] <= transition_initial_mass],
                    )
                )
            if len(initial_masses[initial_masses >= transition_initial_mass]) > 1:
                df_no_he_flash = pd.concat(
                    (
                        df_no_he_flash,
                        df_mass[df_mass["INITIAL_MASS"] >= transition_initial_mass],
                    )
                )

        # change interpolation coordinate for he-flash table to be absolute core mass
        # df_he_flash['HELIUM_CORE_MASS_FRACTION'] = df_he_flash["HELIUM_CORE_MASS_FRACTION"]*df_he_flash['MASS']

        return df_he_flash, df_no_he_flash

    @staticmethod
    def combine_plots_into_multiplot_pdf(list_of_values, direc):
        ###################################################
        # combine plots into one pdf

        # Set up the pdf stuff
        merger = PdfMerger()
        pdf_page_number = 0

        for label in list_of_values:
            output_name = os.path.join(direc, f"{label}.pdf")
            if os.path.exists(output_name):
                merger, pdf_page_number = add_pdf_and_bookmark(
                    merger=merger,
                    pdf_filename=output_name,
                    page_number=pdf_page_number,
                    bookmark_text=f"{label}",
                )

        # wrap up the pdf
        combined_pdf_filename = direc + "_all.pdf"
        merger.write(combined_pdf_filename)
        merger.close()

        return

    def save_run_time_df(self):
        df_run_time = pd.DataFrame(
            columns=self.evol_phase_list[
                : self.evol_phase_list.index(self.evol_phase) + 1
            ]
        )

        for evol_phase in df_run_time.columns:
            direc_path = self.settings["grid_directory"].replace(
                self.evol_phase, evol_phase
            )
            mass_list = get_mass_list(direc_path=direc_path)
            for M in mass_list:
                df_run_time.loc[M, evol_phase] = get_run_time_mass(
                    M=M, direc_path=direc_path
                )
            df_run_time.sort_index(inplace=True)

        df_run_time.to_csv(
            os.path.join(self.settings["grid_directory"], "run_time.csv")
        )

        return df_run_time


if __name__ == "__main__":
    InterpolationTableBuilder_instance = InterpolationTableBuilder(settings={})
