"""
Main location to define the sequence of available MINT classes

TODO: import the actual config and just load all the objects with a default config
"""

from mint_general_interpolation_grid_builder.MINT.config.mint_settings import (
    mint_defaults,
)
from mint_general_interpolation_grid_builder.MINT.config.table_columns import (
    table_columns,
)

from mint_general_interpolation_grid_builder.core.example_table_builder import (
    ExampleTableBuilder,
)

##################
# Actual subclasses:

# MS:
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MS_table_builder import (
    MainSequenceTableBuilder,
)

# HG/GB
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.GB_table_builder import (
    RedGiantBranchTableBuilder,
)

# CHeB
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.CHeB_table_builder import (
    CoreHeliumBurningTableBuilder,
)

# EAGB:
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.EAGB_table_builder import (
    EarlyAsymptoticGiantBranchTableBuilder,
)

# # TPAGB:
# from mint_general_interpolation_grid_builder.interpolation_table_builders.TPAGB_table_builder import (
#     TPAGBTableBuilder,
# )

# HeMS:
# HeHG:
# HeGB:
# HeWD:
# COWD:
# ONeWD:
# NS:

#
config = {**mint_defaults, **table_columns}

#
MINT_interpolation_table_builder_instances = []

##############
# Configure which subclasses are used

# # ExampleTableBuilder
# ExampleTableBuilder_instance = ExampleTableBuilder(settings=config)
# MINT_interpolation_table_builder_instances.append(ExampleTableBuilder_instance)

# MS:
MainSequenceTableBuilder_instance = MainSequenceTableBuilder(settings=config)
MINT_interpolation_table_builder_instances.append(MainSequenceTableBuilder_instance)

# HG:

# GB:
RedGiantBranchTableBuilder_instance = RedGiantBranchTableBuilder(settings=config)
MINT_interpolation_table_builder_instances.append(RedGiantBranchTableBuilder_instance)

# CHeB
CoreHeliumBurningTableBuilder_instance = CoreHeliumBurningTableBuilder(settings=config)
MINT_interpolation_table_builder_instances.append(
    CoreHeliumBurningTableBuilder_instance
)

# EAGB:
EarlyAsymptoticGiantBranchTableBuilder_instance = (
    EarlyAsymptoticGiantBranchTableBuilder(settings=config)
)
MINT_interpolation_table_builder_instances.append(
    EarlyAsymptoticGiantBranchTableBuilder_instance
)

# # TPAGB:
# TPAGBTableBuilder_instance = TPAGBTableBuilder(settings=config)
# MINT_interpolation_table_builder_instances.append(TPAGBTableBuilder_instance)

# HeMS:
# HeHG:
# HeGB:
# HeWD:
# COWD:
# ONeWD:
# NS:
