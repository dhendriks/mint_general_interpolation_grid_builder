#!/usr/bin/python3

import os
import re
import json
import math
import sys

import numpy as np

from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MINT_table_builder import (
    MINTTableBuilder,
)


class RedGiantBranchTableBuilder(MINTTableBuilder):
    def __init__(self, settings):

        self.evol_phase = "GB"

        # Init mixin classes
        MINTTableBuilder.__init__(self, settings=settings)

        # grid description
        self.name = "GB"
        self.longname = "Hertzsprung Gap and Red Giant Branch"
        self.description = "Testing"

        return

    def check_table_specific(self, table_df):
        """
        Function to do specific check of the table
        """

        return table_df

    def create_interpolation_table_hook(self, df):

        df_mint = self.prepare_table_for_mint(df=df, evol_phase=self.evol_phase,)

        # add table to MINT tables dict
        self.MINT_tables = {self.format_interpolation_table_output_filename(): df_mint}

        # split into he-flash and no he-flash tables
        # df['HELIUM_CORE_MASS'] = df['HELIUM_CORE_MASS_FRACTION']*df["MASS"]
        # min_core_mass_he_ignition = df[df['HELIUM_IGNITED_FLAG']==1]['HELIUM_CORE_MASS'].min()
        # min_degen_he_flash = df[(df['HELIUM_IGNITED_FLAG']==1) & (df['HELIUM_CORE_MASS']==min_core_mass_he_ignition)]['CENTRAL_DEGENERACY'].values[0]
        # print('Transition degeneracy for he-flash = ', min_degen_he_flash)
        # he_flash_condition = df.groupby(['MASS', 'INITIAL_MASS'])['CENTRAL_DEGENERACY'].transform('max') > min_degen_he_flash
        # df_he_flash = df[he_flash_condition]

        # tables = {
        #     'he_flash':df_he_flash,
        #     'no_he_flash': df[~he_flash_condition],
        #     }
        # self.MINT_tables = {}

        # for table_name, df_table in tables.items():

        #     # remap
        #     print('Remapping ', table_name)
        #     df_remapped = self.remap_data_to_orthogonal_grid_parallel(df=df_table)

        #     # save csv
        #     print('Saving remapped ', table_name)
        #     filename = "_remapped_Z%7.2e_%s.csv" %(self.settings["metallicity"],self.evol_phase)
        #     df_remapped.to_csv(os.path.join(self.settings["grid_directory"],table_name+filename))

        #     # check table
        #     print('Checking ', table_name)
        #     df_checked = self.check_table(table_df=df_remapped)

        #     # add table to MINT tables dict
        #     self.MINT_tables[
        #         table_name+'_'+self.format_interpolation_table_output_filename()
        #         ] = df_checked

        return


if __name__ == "__main__":

    # Get the command line argument containing the JSON string
    json_str = sys.argv[1]

    # Load the JSON string into a dictionary
    my_dict = json.loads(json_str)

    grid_builder = RedGiantBranchTableBuilder(settings=my_dict)

    grid_builder.build_interpolation_grid()
