"""
File where a list of available MESA grid runners instances is created for use in other functions

TODO: import the actual config and just load all the objects with a default config
"""

##################
# Actual subclasses:

# MS:
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MS_grid_runner import (
    MainSequenceGridRunner,
)

# # TAMS
# from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.TAMS_MASS_CHANGE_grid_runner import (
#     TAMSMassChangeGridRunner,
# )

# HG/GB:
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.GB_grid_runner import (
    RedGiantBranchGridRunner,
)

# CHeB
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.CHeB_grid_runner import (
    CoreHeliumBurningGridRunner,
)

# EAGB:
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.EAGB_grid_runner import (
    EarlyAsymptoticGiantBranchGridRunner,
)

# TPAGB:
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.TPAGB_grid_runner import (
    ThermallyPulsingAsymptoticGiantBranchGridRunner,
)

# HeMS:
# HeHG:
# HeGB:
# HeWD:
# COWD:
# ONeWD:
# NS:


def return_available_MINT_grid_runners(config):

    ##############
    # Configure which subclasses are used
    MINT_MESA_grid_runners_instances = []

    # MS:
    MainSequenceGridRunner_instance = MainSequenceGridRunner(settings=config)
    MINT_MESA_grid_runners_instances.append(MainSequenceGridRunner_instance)

    # HG:
    # GB:
    RedGiantBranchGridRunner_instance = RedGiantBranchGridRunner(settings=config)
    MINT_MESA_grid_runners_instances.append(RedGiantBranchGridRunner_instance)

    # CHeB
    CoreHeliumBurningGridRunner_instance = CoreHeliumBurningGridRunner(settings=config)
    MINT_MESA_grid_runners_instances.append(CoreHeliumBurningGridRunner_instance)

    # EAGB:
    EarlyAsymptoticGiantBranchGridRunner_instance = (
        EarlyAsymptoticGiantBranchGridRunner(settings=config)
    )
    MINT_MESA_grid_runners_instances.append(
        EarlyAsymptoticGiantBranchGridRunner_instance
    )

    # TPAGB:
    ThermallyPulsingAsymptoticGiantBranchGridRunner_instance = (
        ThermallyPulsingAsymptoticGiantBranchGridRunner(settings=config)
    )
    MINT_MESA_grid_runners_instances.append(
        ThermallyPulsingAsymptoticGiantBranchGridRunner_instance
    )

    # HeMS:
    # HeHG:
    # HeGB:
    # HeWD:
    # COWD:
    # ONeWD:
    # NS:

    return MINT_MESA_grid_runners_instances
