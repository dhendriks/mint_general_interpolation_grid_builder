#!/usr/bin/python3


import os
import re
import json
import multiprocessing
import math
import sys

import numpy as np
import pandas as pd
import mesaPlot as mp
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from pathlib import Path


from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner import (
    MINTGridRunner,
)


class MainSequenceGridRunner(MINTGridRunner):
    def __init__(self, settings):

        self.evol_phase = "MS"

        # Init mixin classes
        MINTGridRunner.__init__(self, settings)

        # print("h targets = \n", self.target_h)

        return

    def decide_masses_dict(self):

        # models with masses between 0.08 and 1000 Msun

        masses_dict = {
            self.round_sig_figs(m, 3): self.round_sig_figs(m, 3)
            for m in np.sort(self.settings["initial_masses"])
        }

        print(" Masses to run = \n", masses_dict)

        self.masses_dic = masses_dict

        return

    @staticmethod
    def round_sig_figs(x, sig):
        from math import log10, floor

        # return float('{:g}'.format(float('{:.{p}g}'.format(i, p=n))))
        return round(round(x, sig - int(floor(log10(abs(x)))) - 1), sig)

    def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):
        phase_specific_run_script = f"cp inlist_MS inlist\n./star > {mass}.out"
        self.set_run_script(phase_specific_run_script)

        self.inlists_dict["inlist_MS"]["star_job"][
            "save_model_filename"
        ] = "'{}'".format(os.path.join(self.saved_models_directory, f"{mass}_TAMS.mod"))

        self.inlists_dict["inlist_MS"]["controls"][
            "xa_central_lower_limit(1)"
        ] = "1d-20"

        return


if __name__ == "__main__":

    example_settings = {
        "max_queue_size": 10,
        "num_processes": 10,
        "metallicity": 0.02,
        "grid_directory": "/ms_grid/Z0p02_grid_test/",  # path from home
        "auto_submit": True,
    }

    grid_builder = MainSequenceGridRunner(settings=example_settings)

    grid_builder.write_and_run_MESA_grid()
