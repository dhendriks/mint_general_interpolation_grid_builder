#!/usr/bin/python3


import os
import re
import json
import multiprocessing
import math
import sys

import numpy as np
import pandas as pd
import mesaPlot as mp
import matplotlib.pyplot as plt
from pathlib import Path

from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner import (
    MINTGridRunner,
)

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    invert_dict,
)


class TAMSMassChangeGridRunner(MINTGridRunner):
    def __init__(self, settings):

        self.evol_phase = "MC"

        # Init mixin classes
        MINTGridRunner.__init__(self, settings)

        self.scalar_input_columns = ["MASS", "CENTRAL_DEGENERACY", "CENTRAL_HYDROGEN"]
        self.scalar_output_columns = ["RADIUS", "LUMINOSITY", "AGE"]
        self.vector_output_columns = []

        self.stellar_mass_fractional_difference = 0.1
        self.core_mass_fraction_difference = 0.02

        return

    def decide_masses_dict(self):

        ##########################################################
        # decide initial masses

        masses = self.find_model_masses_from_previous_evol_phase()

        #########################################################
        # set up masses dict

        masses_dict = self.masses_dict_for_binary_grid(masses)

        # masses_dict = {0.18: [0.54], 0.22: [0.54], 1.65: [0.54]}
        # masses_dict = {5.22:[0.541,0.608,0.69]}
        # print(masses_dict)

        self.masses_dic = masses_dict

        return

    def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):

        #################################################
        # write inlists

        phase_specific_run_script = self.linux_to_run_mass_change(
            Mi=mass, final_masses=secondary_masses,
        )
        self.set_run_script(phase_specific_run_script)

        self.inlists_dict["inlist_MC"]["star_job"][
            "save_model_filename"
        ] = "'{}'".format(
            os.path.join(self.saved_models_directory, f"{mass}_TAMS_endMf.mod")
        )
        self.inlists_dict["inlist_MC"]["star_job"][
            "load_model_filename"
        ] = "'{}'".format(
            os.path.join(self.saved_models_directory, f"{mass}_TAMS_{mass}Mf.mod")
        )
        self.inlists_dict["inlist_MC"]["controls"]["log_directory"] = "'{}'".format(
            os.path.join(self.logs_dir, f"LOGS_endMf")
        )
        self.inlists_dict["inlist_MC"]["controls"]["photo_directory"] = "'{}'".format(
            os.path.join(self.logs_dir, f"photos_endMf")
        )

        return

    def find_model_masses_from_previous_evol_phase(self):
        self.prev_evol_phase_saved_models_directory = os.path.join(
            self.settings["metallicity_directory"], "MS", "saved_models"
        )
        print("getting models masses from MS")
        masses = []
        for mod_file in os.listdir(self.prev_evol_phase_saved_models_directory):
            mass = mod_file.replace("_TAMS.mod", "")
            masses += [float(mass)]
        masses = np.sort(masses)

        return masses

    def masses_dict_for_binary_grid(self, masses):
        # function to decide final masses

        print("computing final masses for binary grid")

        # get core masses
        Mc_dict = self.get_initial_mass_core_mass_rel(masses=masses)
        largest_radiative_mass = min(Mc_dict, key=Mc_dict.get)
        SSE_masses = list(Mc_dict.keys())
        Mc_dict = {
            key: value
            for key, value in Mc_dict.items()
            if key >= largest_radiative_mass
        }

        # space final masses
        final_masses = self.space_quantity(
            masses, frac=self.stellar_mass_fractional_difference
        )

        # dictionary for storing initial and final masses
        mass_dict = {}

        # Loop through each final mass
        for Mf in final_masses:

            # Create an empty list to store the initial masses for this final mass
            initial_masses = []

            # Loop through each initial mass
            for Mi in Mc_dict.keys():

                # If the core mass is less than the final mass, append it to the new values list
                if (Mc_dict[Mi] < Mf) and (Mf < 5 * Mi):

                    if len(initial_masses) == 0:
                        initial_masses.append(Mi)

                    # if difference in core mass is greater than allowed spacing
                    elif (
                        abs(Mc_dict[Mi] - Mc_dict[initial_masses[-1]]) / Mf
                        > self.core_mass_fraction_difference
                    ):
                        initial_masses.append(Mi)

                    # add all models in transition region for he-flash
                    elif (
                        Mc_dict[Mi] > 0.08
                        and Mc_dict[Mi] < 0.2
                        and Mc_dict[Mi] / Mf > 0.05
                    ):
                        initial_masses.append(Mi)

                # add helium star i.e. smallest mass with core larger than final mass
                if Mc_dict[Mi] > Mf:
                    initial_masses.append(Mi)
                    break

            # make sure single star evolution model included
            if Mf not in initial_masses:
                if Mf > largest_radiative_mass:
                    initial_masses.append(Mf)
                # else:
                #     initial_masses.append(float(np.min(SSE_masses)))

            # Add the new values list to the new dictionary with the same key
            initial_masses.sort()
            mass_dict[Mf] = initial_masses

        # calculate total number of models
        num_models = sum(len(value) for value in mass_dict.values())
        print(f"Total num models = {num_models}")

        print(mass_dict)

        # invert dictionary
        mass_dict = invert_dict(mass_dict)

        return mass_dict

    def get_initial_mass_core_mass_rel(self, masses):
        """return dictionary of initial masses where the values are the core mass at TAMS"""

        m = mp.MESA()
        p = mp.MESA()

        # cycle through MS models and retrieve core masses
        Mc_dict = {}
        for mass in masses:
            # print(mass)
            direc = os.path.join(
                self.settings["logs_directory"].replace(self.evol_phase, "MS"),
                str(mass),
                "LOGS_MS",
            )

            if mass < float(0.5):
                # print(f"{mass} is enriched in helium, not including in grid")
                continue

            m.loadHistory(direc)
            TAMS_index = np.argmin(abs(m.hist.center_h1 - 1e-4))
            Mc_dict[mass] = round(m.hist.he_core_mass[TAMS_index], 5)

        # print(Mc_dict)

        return Mc_dict

    @staticmethod
    def space_quantity(quantity, frac=0.2):
        """makes sure values of some quanitiy are spaced out by more than some fraction"""

        if len(quantity) == 0:
            return list(quantity)

        spaced_quantity = [quantity[0]]
        for i, m in enumerate(quantity, 1):
            if m > (1 + frac) * spaced_quantity[-1]:
                spaced_quantity.append(m)

        # make sure final value is included
        if quantity[-1] not in spaced_quantity:
            spaced_quantity.append(quantity[-1])

        return spaced_quantity

    @staticmethod
    def space_quantity_delta(quantity, delta=0.02):
        """makes sure values of some quanitiy are spaced out by more than some absolute quantity"""
        # print(quantity)

        if len(quantity) == 0:
            return list(quantity)

        spaced_quantity = [quantity[0]]
        for i, m in enumerate(quantity, 1):
            if m > delta + spaced_quantity[-1]:
                spaced_quantity.append(m)

        # make sure final value is included
        if quantity[-1] not in spaced_quantity:
            spaced_quantity.append(quantity[-1])

        return spaced_quantity

    @staticmethod
    def round_down_mass_sig_figs(mass):
        sigfigs = 2
        if mass >= 1:
            power = int(np.log10(mass)) + 1
        else:
            power = int(np.log10(mass))
        return mass // 10 ** (power - sigfigs) / 10 ** (-power + sigfigs)

    def linux_to_run_mass_change(self, Mi, final_masses):

        lower_masses = [i for i in final_masses if i < Mi][::-1]
        higher_masses = [i for i in final_masses if i > Mi]

        # list of final masses for mass change, stary by decreasing mass from Mi then increase mass from Mi
        Mf_list = lower_masses + higher_masses

        ############################################################
        # mass change runs

        # get ready to run mass change
        script = f"""
cp {self.prev_evol_phase_saved_models_directory}/{Mi}_TAMS.mod {self.saved_models_directory}/{Mi}_TAMS_{Mi}Mf.mod
"""
        # loop through final masse
        for i, Mf in enumerate(Mf_list):
            run_file_path = os.path.join(
                os.path.dirname(os.path.abspath(__file__)), "functions/TAMS_run.py"
            )
            script += f"\npython3 {run_file_path} {Mi} {Mf}\n"

        return script


#     def linux_to_run_mass_change(self,Mi,final_masses):

#         lower_masses = [i for i in final_masses if i < Mi][::-1]
#         higher_masses = [i for i in final_masses if i > Mi]

#         # list of final masses for mass change, stary by decreasing mass from Mi then increase mass from Mi
#         Mf_list = lower_masses + higher_masses
#         # list of masses for starting model for mass change
#         Mf_prev_list = (
#             [Mi]
#             + lower_masses[:-1]
#             + [Mi]
#             + higher_masses[:-1]
#         )

#         TAMS_model_path = os.path.join(os.path.split(self.prev_evol_phase_saved_models_directory)[0],f'{Mi}/TAMS.mod')

#         ############################################################
#         # mass change runs

#         # get ready to run mass change
#         script = f"""
# sed -i 's/do_one inlist_MS/#do_one inlist_MS/g' rn
# sed -i 's/#do_one inlist_change_mass/do_one inlist_change_mass/g' rn
# cp {self.prev_evol_phase_saved_models_directory}/{Mi}_TAMS.mod {self.saved_models_directory}/{Mi}_TAMS_{Mi}Mf.mod
# """
#         # loop through final masse
#         for i, Mf in enumerate(Mf_list):

#             prev_Mf = Mf_prev_list[i]

#             prev_final_mass_path = os.path.join(
#                 self.settings['grid_directory'],
#                 'saved_models',
#                 f'{Mi}_TAMS_{prev_Mf}Mf.mod',
#                 )
#             final_mass_path = os.path.join(
#                 self.settings['grid_directory'],
#                 'saved_models',
#                 f'{Mi}_TAMS_{Mf}Mf.mod',
#                 )

#             script += f"""
# sed -i 's/x_ctrl(2) = final mass/x_ctrl(2) = {Mf}/g' inlist_change_mass
# sed -i 's/endMf/{Mf}Mf/g' inlist_change_mass"""

#             if Mf < Mi:
#                 script += f"""
# FILE={prev_final_mass_path}
# if [ -f "$FILE" ]; then
#     sed -i "s/{Mi}_TAMS_{Mi}Mf.mod/{Mi}_TAMS_{prev_Mf}Mf.mod/g" inlist_change_mass
# fi"""
#             else:
#                 script+=f"""
# sed -i "s/{Mi}_TAMS_{Mi}Mf.mod/{Mi}_TAMS_{prev_Mf}Mf.mod/g" inlist_change_mass"""

#             script+=f"""
# ./rn > {Mf}.out"""

#             # mass accretion
#             if Mf > Mi:
#                 # if fails then try another mass accretion rate
#                 # if still fails then start with more evolved model
#                 script += f"""
# FILE={final_mass_path}
# if [ ! -f "$FILE" ]; then
#     sed -i "s/x_ctrl(1) = 1d-8/x_ctrl(1) = 1d-4/g" inlist_change_mass
#     ./rn >> {Mf}.out
# fi
# if [ ! -f "$FILE" ]; then
#     cp {TAMS_model_path} {self.saved_models_directory}/{Mi}_TAMS_{Mi}Mf.mod
#     sed -i "s/{prev_Mf}/{Mi}/g" inlist_change_mass
#     ./rn >> {Mf}.out
# fi"""
#             # mass loss
#             else:
#                 # if fails try star cut
#                 script += f"""
# FILE={final_mass_path}
# if [ ! -f "$FILE" ]; then
#     sed -i "s/x_ctrl(3) = 1.25/x_ctrl(3) = 100/g" inlist_change_mass
#     ./rn >> {Mf}.out
# fi"""

#             script+=f"""
# sed -i 's/x_ctrl(2) = {Mf}/x_ctrl(2) = final mass/g' inlist_change_mass
# sed -i 's/{Mf}Mf/endMf/g' inlist_change_mass
# sed -i "s/{Mi}_TAMS_{prev_Mf}Mf.mod/{Mi}_TAMS_{Mi}Mf.mod/g" inlist_change_mass
# """

#         return script


if __name__ == "__main__":

    example_settings = {
        **main_config,
        "max_queue_size": 10,
        "num_processes": 10,
        "metallicity": 0.02,
        "grid_directory": "/rgb_grid/Z0p02_grid_binary_test/",
        "copy_models_from_previous_evol_phase": False,
        "binary_grid": False,
        "auto_submit": False,
    }

    grid_builder = TAMSMassChangeGridRunner(settings=example_settings)

    # grid_builder.create_GB_MESA_grid()

    grid_builder.write_and_run_MESA_grid()
