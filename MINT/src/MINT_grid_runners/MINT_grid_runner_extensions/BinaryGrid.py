#!/usr/bin/python3

"""Functions relating to working of binary grid"""

import os


class BinaryGrid:
    def __init__(self):
        pass

    def masses_dict_for_binary_grid(self):

        print(f"getting models masses from {self.previous_evol_phase}")
        masses_dict = {}
        for mod_file in os.listdir(self.prev_evol_phase_saved_models_directory):
            Mi = float(mod_file.split("_")[0])
            Mf = float(mod_file.split("_")[-1].replace("Mf.mod", ""))
            if masses_dict.get(Mf, None) is not None:
                masses_dict[Mf] = masses_dict[Mf] + [Mi]
            else:
                masses_dict[Mf] = [Mi]

        masses_dict_sorted = {
            Mt: sorted(list((masses_dict[Mt])))
            for Mt in sorted(list(masses_dict.keys()))
        }

        return masses_dict_sorted

    def linux_to_run_binary_grid(self, initial_masses):

        script = f"cp inlist_{self.evol_phase} inlist\n"

        for Mi in initial_masses:
            script += """
sed -i 's/Mi_/{Mi}_/g' inlist
sed -i 's/LOGS_{evol_phase}_Mi/LOGS_{evol_phase}_{Mi}/g' inlist
sed -i 's/photos_{evol_phase}_Mi/photos_{evol_phase}_{Mi}/g' inlist
sed -i 's/initial_mass = Mi/initial_mass = {Mi}/g' inlist_common
./star > {Mi}.out
sed -i 's/{Mi}_/Mi_/g' inlist
sed -i 's/LOGS_{evol_phase}_{Mi}/LOGS_{evol_phase}_Mi/g' inlist
sed -i 's/photos_{evol_phase}_{Mi}/photos_{evol_phase}_Mi/g' inlist
sed -i 's/initial_mass = {Mi}/initial_mass = Mi/g' inlist_common
""".format(
                Mi=Mi, evol_phase=self.evol_phase
            )

        return script
