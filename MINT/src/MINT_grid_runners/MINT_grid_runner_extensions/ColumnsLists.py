#!/usr/bin/python3

""" Creation of history and profile columns lists"""

import os

from mint_general_interpolation_grid_builder.MINT.config.header_description_dict import (
    header_description_dict_scalars,
    header_description_dict_vectors,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    set_control_value_in_all_inlists,
)


class ColumnsLists:
    """
    Columns lists class-extension for MESAGridRunner class
    """

    def __init__(self):
        pass

    def set_paths_to_columns_lists(self):

        # set location of relevant evol phase

        self.settings["inlists"][f"inlist_{self.evol_phase}"]["star_job"][
            "history_columns_file"
        ] = "'{}'".format(
            os.path.join(
                self.settings["grid_directory"],
                f"history_columns_{self.evol_phase}.list",
            )
        )
        self.settings["inlists"][f"inlist_{self.evol_phase}"]["star_job"][
            "profile_columns_file"
        ] = "'{}'".format(
            os.path.join(
                self.settings["grid_directory"],
                f"profile_columns_{self.evol_phase}.list",
            )
        )

        return

    def write_hist_prof_columns_lists(self):
        ################################################
        # write history and profile columns lists

        all_columns_list = (
            self.scalar_input_columns
            + self.scalar_output_columns
            + self.vector_output_columns
        )

        necessary_hist_columns = [
            "model_number",
            "log_Teff",
            "log_LH",
            "log_LHe",
            "center h1",
            "center he4",
        ]
        necessary_prof_columns = [
            "superad_reduction_factor",
        ]

        hist_columns = necessary_hist_columns + self.mesa_output_parameter_function(
            interpolation_table_parameter_list=all_columns_list,
            search_str="mesa_history_dependency",
        )
        prof_columns = necessary_prof_columns + self.mesa_output_parameter_function(
            interpolation_table_parameter_list=all_columns_list,
            search_str="mesa_profile_dependency",
        )

        hist_file = os.path.join(
            self.settings["grid_directory"], f"history_columns_{self.evol_phase}.list"
        )
        with open(hist_file, "w") as f:
            f.write("\n".join(hist_columns).strip())
        prof_file = os.path.join(
            self.settings["grid_directory"], f"profile_columns_{self.evol_phase}.list"
        )
        with open(prof_file, "w") as f:
            f.write("\n".join(prof_columns).strip())

        return

    @staticmethod
    def mesa_output_parameter_function(interpolation_table_parameter_list, search_str):
        """
        Function to handle selecting the MESA output parameters based on the interpolation table output values
        """

        global header_description_dict_vectors, header_description_dict_scalar

        # Combine the header scription dicts
        combined_header_description_dict = {
            **header_description_dict_scalars,
            **header_description_dict_vectors,
        }

        #
        mesa_output_parameter_list = []
        for parameter in interpolation_table_parameter_list:
            mesa_output_parameter_list += combined_header_description_dict[
                parameter
            ].get(search_str, [])

        return sorted(list(set(mesa_output_parameter_list)))
