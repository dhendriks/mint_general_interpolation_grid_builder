#!/usr/bin/python3

"""
Slurm class-extension for MINT_subclass class
"""

import json
import os


class Slurm:
    """
    Slurm class-extension for MINT_subclass class
    Allows running of MINT grids on HPC using slurm
    """

    def __init__(self):
        pass

    def write_slurm_tabulation_script(self):
        """slurm script to perform tabulation"""

        dic = {
            **self.settings.copy(),
            "evol_phase": self.evol_phase,
        }
        dic.pop("inlists", None)

        dic["settings_str"] = json.dumps(dic)

        script = """#!/bin/bash

#SBATCH --partition={slurm_partition}
#SBATCH --job-name="tabulate_{evol_phase}"
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={num_processes}
#SBATCH --time=8:00:00
#SBATCH -o {grid_directory}/table_output
#SBATCH -e {grid_directory}/table_error
{extra_sbatch_commands}

cd $SLURM_SUBMIT_DIR

{tabulation_setup_code}

python3 /users/nr00492/mint_general_interpolation_grid_builder/MINT/src/MINT_table_builders/{evol_phase}_table_builder.py '{settings_str}'
""".format(
            **dic
        )

        filename = os.path.join(self.settings["grid_directory"], "slurm_tabulate.sub")

        # write to slurm file
        with open(filename, "w") as f:
            f.write(script)

        return
