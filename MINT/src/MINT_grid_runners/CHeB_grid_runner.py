#!/usr/bin/python3

import os
import re
import json
import multiprocessing
import math
import sys

import numpy as np
import pandas as pd
import mesaPlot as mp
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from pathlib import Path


from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner import (
    MINTGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.config.output import (
    format_save_model_filename,
    format_logs_foldername,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.functions.run_tracks import (
    lines_that_contain,
    run_mass_track,
    get_items_before_and_after,
    check_for_sufficient_resolution,
    run_TAMS_mass_change,
)


class CoreHeliumBurningGridRunner(MINTGridRunner):
    def __init__(self, settings):

        self.evol_phase = "CHeB"

        # Init mixin classes
        MINTGridRunner.__init__(self, settings)

        # print("he targets = ", self.target_he)

        return

    def decide_masses_dict(self):

        masses = self.find_model_masses_from_previous_evol_phase()

        self.masses_dic = {M: M for M in masses}

        return

    def run_mass(self, mass: float) -> None:
        """Runs MINT process for a given mass.

        Uses run time bisection method to fill core mass fraction space.

        Args:
            mass (float): Mass to run.

        """
        # get initial mass- core mass relation
        Mc_dict = self.get_initial_mass_core_mass_rel(mass=mass)
        print("\nMi vs Mc")
        print(Mc_dict)

        # make list of avail models based of chosen criteria
        avail_models = np.array(sorted(Mc_dict.keys()))
        # add a helium star
        print("\nInitial masses available = ", avail_models)

        # start with lowest initial mass, highest initial mass and CME models
        masses_to_run = [avail_models[0], avail_models[-1]]
        if float(mass) in avail_models:
            masses_to_run += [float(mass)]
        # add degenerate transition model
        min_core_mass_model = min(Mc_dict, key=Mc_dict.get)
        if min_core_mass_model != avail_models[0]:
            masses_to_run += [min_core_mass_model]
        masses_to_run = sorted(set(masses_to_run))

        print("\nInitial masses to run: ", masses_to_run)

        complete_models = []
        for Mi in masses_to_run:
            print("\nRunning Mi = ", Mi)

            print("Running CHeB")
            run_mass_track(mass, Mi, "CHeB")
            complete_models += [Mi]
            complete_models.sort()

            if len(complete_models) > 1:
                for M2 in get_items_before_and_after(complete_models, Mi):
                    print("Comparing to ", M2)
                    add_more_res = check_for_sufficient_resolution(
                        M1=Mi,
                        M2=M2,
                        logs_dir=os.path.join(
                            self.settings["logs_directory"],
                            f"{mass}/{format_logs_foldername(self.evol_phase)}",
                        ),
                        evol_phase=self.evol_phase,
                    )
                    if add_more_res:
                        inbetween_mass = avail_models[
                            np.argmin(abs(avail_models - (Mi + M2) / 2))
                        ]
                        if inbetween_mass not in masses_to_run:
                            masses_to_run += [inbetween_mass]

            print("Initial masses run = ", complete_models)
            print("Initial masses to run: ", masses_to_run)

        return

    def get_initial_mass_core_mass_rel(self, mass):
        """return dictionary of initial masses where the values are the core mass at TAMS"""

        # cycle through GB models and retrieve core masses
        m = mp.MESA()
        print("getting models initial masses from GB")
        Mc_dict = {}
        for mod_file in os.listdir(self.prev_evol_phase_saved_models_directory):
            M = mod_file.split('_')[0]
            if (float(M) == mass) and (mod_file.split('_')[1]=='ZACHeB'):
                Mi = float(mod_file.split('_')[-1].replace(".mod",""))
                logs_dir=os.path.join(
                    self.settings["logs_directory"].replace(self.evol_phase, "GB"), "{}/{}".format(mass,format_logs_foldername("GB", Mi))
                )
                m.loadHistory(logs_dir)
                Mc_dict[Mi] = round(m.hist.he_core_mass[-1], 5)

        return Mc_dict

    def find_model_masses_from_previous_evol_phase(self):
        print(
            f"\nGetting models masses from {self.previous_evol_phase}:",
            self.prev_evol_phase_saved_models_directory,
        )

        masses = []
        for mod_file in os.listdir(self.prev_evol_phase_saved_models_directory):
            mass = mod_file.split("_")[0]
            masses += [float(mass)]
        masses = np.sort(masses)

        print(masses)

        return masses

    # def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):

    #     run_file_path = os.path.join(
    #         os.path.dirname(os.path.abspath(__file__)),
    #         "functions/run_chosen_core_masses.py",
    #     )
    #     phase_specific_run_script = f"rm restart_photo\ncp inlist_{self.evol_phase} inlist\npython3 {run_file_path} {mass}"
    #     self.set_run_script(phase_specific_run_script)

    #     self.inlists_dict["inlist_CHeB"]["star_job"][
    #         "save_model_filename"
    #     ] = "'{}'".format(
    #         os.path.join(self.saved_models_directory, f"Mi_TACHeB_{mass}Mf.mod")
    #     )
    #     self.inlists_dict["inlist_CHeB"]["star_job"][
    #         "load_model_filename"
    #     ] = "'{}'".format(
    #         os.path.join(
    #             self.prev_evol_phase_saved_models_directory, f"Mi_ZACHeB_{mass}Mf.mod"
    #         )
    #     )
    #     self.inlists_dict["inlist_CHeB"]["controls"]["log_directory"] = "'{}'".format(
    #         os.path.join(self.logs_dir, f"LOGS_CHeB_Mi")
    #     )
    #     self.inlists_dict["inlist_CHeB"]["controls"]["photo_directory"] = "'{}'".format(
    #         os.path.join(self.logs_dir, f"photos_CHeB_Mi")
    #     )
    #     self.inlists_dict["inlist_common"]["controls"]["initial_mass"] = "Mi"

    #     return


if __name__ == "__main__":

    example_settings = {
        **main_config,
        "max_queue_size": 10,
        "num_processes": 10,
        "metallicity": 0.02,
        "grid_directory": "/rgb_grid/Z0p02_grid_binary_test/",
        "copy_models_from_previous_evol_phase": False,
        "binary_grid": False,
        "auto_submit": False,
    }

    grid_builder = CoreHeliumBurningGridRunner(settings=example_settings)

    # grid_builder.create_CHeB_MESA_grid()

    grid_builder.write_and_run_MESA_grid()
