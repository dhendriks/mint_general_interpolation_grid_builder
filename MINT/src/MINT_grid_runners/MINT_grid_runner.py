#!/usr/bin/python3

from pathlib import Path
import os
import re
import json
import math
import pickle
import multiprocessing
import sys

import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
from pathlib import Path
from typing import Optional, Any


from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.MESA_grid_runner import (
    MESAGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_utility import MINTUtility
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner_extensions.ColumnsLists import (
    ColumnsLists,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner_extensions.BinaryGrid import (
    BinaryGrid,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner_extensions.slurm import (
    Slurm,
)
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    find_and_replace_text,
)
from mint_general_interpolation_grid_builder.MINT.config.output import (
    format_logs_foldername,
    format_photos_foldername,
    format_save_model_filename,
)


class MINTGridRunner(MESAGridRunner, MINTUtility, BinaryGrid, ColumnsLists, Slurm):
    def __init__(self, settings):

        self.settings = settings

        MESAGridRunner.__init__(self, settings=settings)
        MINTUtility.__init__(self, settings=settings)
        ColumnsLists.__init__(self)
        BinaryGrid.__init__(self)
        Slurm.__init__(self)

        self.grid_runner_dict = {
            "MS": "MainSequenceGridRunner",
            "GB": "RedGiantBranchGridRunner",
            "CHeB": "CoreHeliumBurningGridRunner",
            "EAGB": "EarlyAsymptoticGiantBranchGridRunner",
        }

    def pre_MESA_grid_writing_hook_extra(self):

        ################################################################
        # Set up column lists

        self.set_paths_to_columns_lists()

        # write history and profile columns lists to grid directory
        self.write_hist_prof_columns_lists()

        #############################################################
        # write slurm submission file for tabulation
        if self.settings["workload_manager"] == "slurm":
            self.write_slurm_tabulation_script()

        #############################################################
        # if save_photos_at_grid_points = true
        if self.settings["save_photos_at_grid_points"] == True:
            self.settings["inlists"]["inlist_common"]["controls"][
                "x_logical_ctrl(3)"
            ] = ".true. !save photos at grid points"

        #############################################################
        # Extract just the inlists needed
        inlist_list = [
            "inlist_common",
            f"inlist_{self.evol_phase}",
            "inlist_MC",
        ]
        self.settings["inlists"] = {
            inlist_name: self.settings["inlists"][inlist_name]
            for inlist_name in inlist_list
        }

        return

    def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):

        #################################################
        # write inlists

        # mass change inlist
        self.inlists_dict["inlist_MC"]["star_job"][
            "save_model_filename"
        ] = "'{}'".format(
            os.path.join(
                self.saved_models_directory,
                format_save_model_filename("MC", mass, "Mi"),
            )
        )
        self.inlists_dict["inlist_MC"]["star_job"][
            "load_model_filename"
        ] = "'{}'".format(
            os.path.join(
                self.saved_models_directory,
                format_save_model_filename("MC", "Mt", "Mi"),
            )
        )
        self.inlists_dict["inlist_MC"]["controls"]["log_directory"] = "'{}'".format(
            os.path.join(self.logs_dir, f"LOGS_MC")
        )
        self.inlists_dict["inlist_MC"]["controls"]["photo_directory"] = "'{}'".format(
            os.path.join(self.logs_dir, f"photos_MC")
        )

        self.inlists_dict["inlist_MC"]["controls"]["x_ctrl(2)"] = str(mass)

        # general evol phase inlist
        self.inlists_dict[f"inlist_{self.evol_phase}"]["star_job"][
            "save_model_filename"
        ] = "'{}'".format(
            os.path.join(
                self.saved_models_directory,
                format_save_model_filename(self.evol_phase, mass, "Mi"),
            )
        )
        self.inlists_dict[f"inlist_{self.evol_phase}"]["star_job"][
            "load_model_filename"
        ] = "'{}'".format(
            os.path.join(
                self.prev_evol_phase_saved_models_directory,
                format_save_model_filename(self.previous_evol_phase, mass, "Mi"),
            )
        )
        self.inlists_dict[f"inlist_{self.evol_phase}"]["controls"][
            "log_directory"
        ] = "'{}'".format(
            os.path.join(self.logs_dir, format_logs_foldername(self.evol_phase))
        )
        self.inlists_dict[f"inlist_{self.evol_phase}"]["controls"][
            "photo_directory"
        ] = "'{}'".format(
            os.path.join(self.logs_dir, format_photos_foldername(self.evol_phase))
        )
        self.inlists_dict["inlist_common"]["controls"]["initial_mass"] = "Mi"

        # specific evol phase inlists
        if self.evol_phase == "GB":
            # GB models come from mass change process instead
            self.inlists_dict["inlist_GB"]["star_job"][
                "load_model_filename"
            ] = "'{}'".format(
                os.path.join(
                    self.saved_models_directory,
                    format_save_model_filename("MC", mass, "Mi"),
                )
            )

            if mass > 5.0:
                self.inlists_dict["inlist_GB"]["controls"]["max_model_number"] = "5000"

            self.inlists_dict["inlist_GB"]["controls"]["HB_limit"] = "0.9"

        return

    def format_settings_filename(self):
        """
        Function to format the settings filename
        """

        return "MINT_Z%7.2e_%s_settings.json" % (
            float(self.settings["metallicity"]),
            self.evol_phase,
        )

    def edit_run_star_extras(self):
        #################################################
        # set interpolation variables targets in run_star_extras

        # include agb module params
        module_name = "agb"
        module_dirpath = os.path.join(self.mesa_src_dir, f"rse_modules/{module_name}")
        find_and_replace_text(
            file=os.path.join(
                self.settings["grid_directory"], "src/run_star_extras.f90"
            ),
            find_text="implicit none",
            replace_text="implicit none\n\ninclude '{}'".format(
                os.path.join(module_dirpath, f"{module_name}_params.inc")
            ),
        )

        save_targets_text = """
integer :: save_targets_length = {}
real, dimension({}) :: save_targets = {}
        """.format(
            len(self.time_proxy_save_targets),
            len(self.time_proxy_save_targets),
            self.time_proxy_save_targets_list_str,
        )

        find_and_replace_text(
            file=os.path.join(
                self.settings["grid_directory"], "src/run_star_extras.f90"
            ),
            find_text="implicit none",
            replace_text="implicit none\n" + save_targets_text,
        )

        return

    def write_run_script(self, mass):
        """Load python function to control run.

        """
        grid_runner_name = self.grid_runner_dict[self.evol_phase]
        settings_filename = self.format_settings_filename()

        python_run_script = f"""
import json

from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.{self.evol_phase}_grid_runner import {grid_runner_name}

with open('../{settings_filename}') as json_file:
    mint_settings = json.load(json_file)
grid_runner = {grid_runner_name}(settings = mint_settings)
grid_runner.run_mass({mass})
"""
        with open(os.path.join(self.final_directory, "run_MINT.py"), "w") as f:
            f.write(python_run_script)

        this_file = os.path.abspath(__file__)
        check_complete_status_path = os.path.join(
            this_file.split("MINT")[0],
            "core/MESAGridRunner/src/functions/check_complete_status.py",
        )
        incomplete_model_info_path = os.path.join(
            this_file.split("MINT")[0],
            "MINT/src/MINT_grid_runners/functions/incomplete_model_info.py",
        )

        rn_script = f"""

echo "RUNNING" > STATUS.txt
python {check_complete_status_path} {self.settings['logs_directory']}

python3 run_MINT.py

echo "FINISHED" > STATUS.txt
python {check_complete_status_path} {self.settings['logs_directory']}
python {incomplete_model_info_path} > ../../../grid_info"""

        find_and_replace_text(
            file=os.path.join(self.final_directory, "rn"),
            find_text="./star",
            replace_text=rn_script,
        )

        return

    def run_mass(self, mass: float) -> None:
        """Runs MINT process for a given mass.

        Must be implemented for each evolutionary phase.

        Args:
            mass (float): Mass to run.

        """
        raise NotImplementedError


if __name__ == "__main__":
    MINTGridRunner_instance = MINTGridRunner()
