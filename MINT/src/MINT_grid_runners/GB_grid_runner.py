#!/usr/bin/python3


import os
import re
import json
import multiprocessing
import math
import sys

import numpy as np
import pandas as pd
import mesaPlot as mp
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from pathlib import Path

from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner import (
    MINTGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.functions.run_tracks import (
    lines_that_contain,
    run_mass_track,
    get_items_before_and_after,
    check_for_sufficient_resolution,
    run_TAMS_mass_change,
)
from mint_general_interpolation_grid_builder.MINT.config.output import (
    format_save_model_filename,
    format_logs_foldername,
)


class RedGiantBranchGridRunner(MINTGridRunner):
    def __init__(self, settings):

        self.evol_phase = "GB"

        # Init mixin classes
        MINTGridRunner.__init__(self, settings)

        self.stellar_mass_fractional_difference = 0.1
        self.core_mass_fraction_difference = 0.02

        return

    def decide_masses_dict(self):

        masses = self.find_model_masses_from_previous_evol_phase()

        final_masses = self.space_quantity(
            masses, frac=self.stellar_mass_fractional_difference
        )

        self.masses_dic = {M: M for M in final_masses}

        for Mi in masses:
            os.system(
                f"cp {self.prev_evol_phase_saved_models_directory}/{format_save_model_filename('MS', Mi, Mi)} {self.saved_models_directory}/{format_save_model_filename('MC', Mi, Mi)}"
            )

        return

    def run_mass(self, mass: float) -> None:
        """Runs MINT process for a given mass.

        Uses run time bisection method to fill core mass fraction space.

        Args:
            mass (float): Mass to run.

        """
        # get initial mass- core mass relation
        masses = self.find_model_masses_from_previous_evol_phase()
        print(masses)
        Mc_dict = self.get_initial_mass_core_mass_rel(masses=masses)

        # find largest radiative hydrogen burning mass and exclude models below this
        largest_radiative_mass = min(Mc_dict, key=Mc_dict.get)
        Mc_dict = {
            key: value
            for key, value in Mc_dict.items()
            if key >= largest_radiative_mass
        }
        print("\nMi vs Mc")
        print(Mc_dict)

        # make list of avail models based of chosen criteria
        avail_models = [M for M in Mc_dict.keys() if Mc_dict[M] < mass and mass < 5 * M]
        # add a helium star
        avail_models += [masses[list(masses).index(avail_models[-1]) + 1]]
        print("\nInitial masses available = ", avail_models)

        # start with lowest initial mass, highest initial mass and CME models
        masses_to_run = [avail_models[0], avail_models[-1]]
        if float(mass) in avail_models:
            masses_to_run += [float(mass)]
        masses_to_run = sorted(set(masses_to_run))

        print("\nInitial masses to run: ", masses_to_run)

        saved_models_directory = os.path.join(
            self.settings["grid_directory"], "saved_models"
        )

        complete_models = []
        for Mi in masses_to_run:
            print("\nRunning Mi = ", Mi)

            if Mi != mass:
                print("Running mass change")
                run_TAMS_mass_change(saved_models_directory, Mi, mass)

                # if mass change fails then try closest Mi instead
                if not os.path.exists(
                    os.path.join(
                        saved_models_directory,
                        format_save_model_filename("MC", mass, Mi),
                    )
                ):
                    closest_model = min(
                        [model for model in avail_models if model not in masses_to_run],
                        key=lambda x: abs(x - Mi),
                    )
                    print(f"{Mi} mass change failed, will try {closest_model} instead")
                    masses_to_run += [closest_model]
                    continue

            print("Running GB")
            run_mass_track(mass, Mi, "GB")
            complete_models += [Mi]
            complete_models.sort()

            if len(complete_models) > 1:
                for M2 in get_items_before_and_after(complete_models, Mi):
                    print("Comparing to ", M2)
                    add_more_res = check_for_sufficient_resolution(
                        M1=Mi,
                        M2=M2,
                        logs_dir=os.path.join(
                            self.settings["logs_directory"],
                            f"{mass}/{format_logs_foldername(self.evol_phase)}",
                        ),
                        evol_phase=self.evol_phase,
                    )
                    if add_more_res:
                        inbetween_mass = avail_models[
                            np.argmin(abs(avail_models - (Mi + M2) / 2))
                        ]
                        if inbetween_mass not in masses_to_run:
                            masses_to_run += [inbetween_mass]

            print("Initial masses run = ", complete_models)
            print("Initial masses to run: ", masses_to_run)

        return

    def find_model_masses_from_previous_evol_phase(self):
        self.prev_evol_phase_saved_models_directory = os.path.join(
            self.settings["metallicity_directory"],
            self.previous_evol_phase,
            "saved_models",
        )
        print(
            f"\nGetting models masses from {self.previous_evol_phase}:",
            self.prev_evol_phase_saved_models_directory,
        )

        masses = []
        for mod_file in os.listdir(self.prev_evol_phase_saved_models_directory):
            mass = mod_file.split('_')[0]
            masses += [float(mass)]
        masses = np.sort(masses)

        return masses

    def get_initial_mass_core_mass_rel(self, masses):
        """return dictionary of initial masses where the values are the core mass at TAMS"""

        m = mp.MESA()
        p = mp.MESA()

        # cycle through MS models and retrieve core masses
        Mc_dict = {}
        for mass in masses:
            # print(mass)
            direc = os.path.join(
                self.settings["logs_directory"].replace(self.evol_phase, "MS"),
                str(mass),
                "LOGS_MS",
            )

            if mass < float(0.5):
                # print(f"{mass} is enriched in helium, not including in grid")
                continue

            m.loadHistory(direc)
            TAMS_index = np.argmin(abs(m.hist.center_h1 - 1e-4))
            Mc_dict[mass] = round(m.hist.he_core_mass[TAMS_index], 5)

        # print(Mc_dict)

        return Mc_dict

    @staticmethod
    def space_quantity(quantity, frac=0.2):
        """makes sure values of some quanitiy are spaced out by more than some fraction"""

        if len(quantity) == 0:
            return list(quantity)

        spaced_quantity = [quantity[0]]
        for i, m in enumerate(quantity, 1):
            if m > (1 + frac) * spaced_quantity[-1]:
                spaced_quantity.append(m)

        # make sure final value is included
        if quantity[-1] not in spaced_quantity:
            spaced_quantity.append(quantity[-1])

        return spaced_quantity

    @staticmethod
    def space_quantity_delta(quantity, delta=0.02):
        """makes sure values of some quanitiy are spaced out by more than some absolute quantity"""
        # print(quantity)

        if len(quantity) == 0:
            return list(quantity)

        spaced_quantity = [quantity[0]]
        for i, m in enumerate(quantity, 1):
            if m > delta + spaced_quantity[-1]:
                spaced_quantity.append(m)

        # make sure final value is included
        if quantity[-1] not in spaced_quantity:
            spaced_quantity.append(quantity[-1])

        return spaced_quantity

    @staticmethod
    def round_down_mass_sig_figs(mass):
        sigfigs = 2
        if mass >= 1:
            power = int(np.log10(mass)) + 1
        else:
            power = int(np.log10(mass))
        return mass // 10 ** (power - sigfigs) / 10 ** (-power + sigfigs)


if __name__ == "__main__":

    example_settings = {
        **main_config,
        "max_queue_size": 10,
        "num_processes": 10,
        "metallicity": 0.02,
        "grid_directory": "/rgb_grid/Z0p02_grid_binary_test/",
        "copy_models_from_previous_evol_phase": False,
        "binary_grid": False,
        "auto_submit": False,
    }

    grid_builder = RedGiantBranchGridRunner(settings=example_settings)

    # grid_builder.create_GB_MESA_grid()

    grid_builder.write_and_run_MESA_grid()
