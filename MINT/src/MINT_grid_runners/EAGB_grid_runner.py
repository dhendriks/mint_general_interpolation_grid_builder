#!/usr/bin/python3

import os
import re
import json
import multiprocessing
import math
import sys

import numpy as np
import pandas as pd
import mesaPlot as mp
from scipy.optimize import curve_fit
from scipy import interpolate
import matplotlib.pyplot as plt
from pathlib import Path

from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MINT_grid_runner import (
    MINTGridRunner,
)


class EarlyAsymptoticGiantBranchGridRunner(MINTGridRunner):
    def __init__(self, settings):

        self.evol_phase = "EAGB"

        # Init mixin classes
        MINTGridRunner.__init__(self, settings)

        return

    def decide_masses_dict(self):

        ##########################################################
        # decide masses_dict

        masses_dict = self.masses_dict_for_binary_grid()
        # remove masses with only one initial mass
        masses_dict = {Mt: Mis for Mt, Mis in masses_dict.items() if len(Mis) > 1}

        print(masses_dict)

        self.masses_dic = masses_dict

        return

    def write_run_directory_extras_hook(self, mass, secondary_masses, settings_dic):

        #################################################
        # write inlists

        run_file_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "functions/run_chosen_core_masses.py",
        )
        phase_specific_run_script = f"rm restart_photo\ncp inlist_{self.evol_phase} inlist\npython3 {run_file_path} {mass}"
        self.set_run_script(phase_specific_run_script)

        self.inlists_dict["inlist_EAGB"]["star_job"][
            "save_model_filename"
        ] = "'{}'".format(
            os.path.join(self.saved_models_directory, f"Mi_TPAGB_{mass}Mf.mod")
        )
        self.inlists_dict["inlist_EAGB"]["star_job"][
            "load_model_filename"
        ] = "'{}'".format(
            os.path.join(
                self.prev_evol_phase_saved_models_directory, f"Mi_TACHeB_{mass}Mf.mod"
            )
        )
        self.inlists_dict["inlist_EAGB"]["controls"]["log_directory"] = "'{}'".format(
            os.path.join(self.logs_dir, f"LOGS_EAGB_Mi")
        )
        self.inlists_dict["inlist_EAGB"]["controls"]["photo_directory"] = "'{}'".format(
            os.path.join(self.logs_dir, f"photos_EAGB_Mi")
        )
        self.inlists_dict["inlist_common"]["controls"]["initial_mass"] = "Mi"
        self.inlists_dict["inlist_common"]["controls"]["eta_center_limit"] = 150

        # change he core mass definition
        self.inlists_dict["inlist_common"]["controls"][
            "he_core_boundary_h1_fraction"
        ] = "1d-4"

        # relax varcontrol target
        self.inlists_dict["inlist_common"]["controls"]["varcontrol_target"] = "1d-3"

        # # relax dX/X timestep limit
        # self.inlists_dict['inlist_EAGB']['controls']['dX_div_X_limit(1)'] = '1d99 ! h1'

        return


if __name__ == "__main__":

    example_settings = {
        **main_config,
        "max_queue_size": 10,
        "num_processes": 10,
        "metallicity": 0.02,
        "grid_directory": "/rgb_grid/Z0p02_grid_binary_test/",
        "copy_models_from_previous_evol_phase": False,
        "binary_grid": False,
        "auto_submit": False,
    }

    grid_builder = RedGiantBranchGridRunner(settings=example_settings)

    # grid_builder.create_EAGB_MESA_grid()

    grid_builder.write_and_run_MESA_grid()
