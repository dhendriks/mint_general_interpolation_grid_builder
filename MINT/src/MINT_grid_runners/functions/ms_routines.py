"""
AUTHOR: Giovanni Mirouh
MESA grid-building routine for Eureka
This script generates inlists and scripts, so far includes
only the normal evolution with appropriate stops.
Relaxation will come with a future update
GMM          Jan 2021
"""

### Imports
import math
import numpy as np

#############################################################
def chemical_composition(dictionary):
    """
    This routine provides the composition of the ZAMS for a given value of Z
    For consistency Z is provided inside the routine.

    Input: 	dictionary	parameters for the run		(dictionary)
    Output:     dictionary      parameters for the run + X Y Z appended         (dictionary)
    """
    # TODO check that zams_y and zams_x are not defined at beginning
    # TODO if they are, check they sum to 1 and allow that instead of computing
    # this last bit would allow the "easy" calculation of the HeMS

    # read Z
    zams_z = dictionary["zams_z"]

    # define Y
    # Galactic Chemical Evolution in the form of dY/dZ    #Y = 0.28 + dY/dZ*(Z-0.02)
    # values for derivative in http://www.pas.rochester.edu/~emamajek/memo_dydz.html
    dY_dZ = 2.0
    zams_y = 0.28 + dY_dZ * (zams_z - 0.02)

    # define X as what is left
    zams_x = 1.0 - zams_y - zams_z

    # include checks? X+Y+Z = 1 and X,Y,Z>0 ? GMM

    dictionary["zams_x"] = zams_x
    dictionary["zams_y"] = zams_y
    print(
        "Building a grid with (X_0, Y_0, Z_0)=({zams_x}, {zams_y}, {zams_z})".format(
            **dictionary
        )
    )
    return dictionary  # zams_x, zams_y, zams_z


#############################################################
def mass_range_to_compute():
    """
    This routine defines the mass values at which we compute evolutionary
     tracks.  The sampling can be modified at will as the MINT interpolation process
     does not have prerequisites on the sampled masses.

     Input: 	none
     Output:	mass_range	list of masses to compute in grid	(list of floats)
    """

    # The list of masses samples the range 0.32 to 100 solar
    # masses logarithmically taking 100 steps
    # (that is, logM goes from -0.5 to 2 with steps of 0.025),
    # rounded to the second decimal digit.
    mass_range = np.linspace(-0.5, 2.0, 101)
    for i in range(mass_range.__len__()):
        mass_range[i] = round(math.pow(10, mass_range[i]), 2)
        if mass_range[i] == 1.19:
            mass_range[i] = 1.2
        if mass_range[i] == 1.26:
            mass_range[i] = 1.27

    # One can append masses below and above the .32-100Msun range.
    # At the moment those extra masses are hardcoded

    # THE EXTRA MASSES ADDED BELOW ARE TO BE TUNED BY HAND
    # DEPENDING ON WHAT MESA ACTUALLY CONVERGED IN COMPUTING
    # TODO: automate timestep change to get convergence
    # TODO: automate detection if a track is incomplete

    # If low-mass end of MS is requested
    # Following the logarithmic rule for low masses would lead to
    # repeated/very close masses when rounding
    #    mass_range = np.concatenate((np.array(
    #            ([0.1, 0.11, 0.13, 0.14, 0.16, 0.18, 0.2, 0.22, 0.25, 0.28])),
    #            np.array((mass_range)),) )
    # If high-mass MS is requested
    #    mass_range = np.concatenate((np.array((mass_range)),
    #           np.array(([125.89, 158.49, 199.53, 251.19, 316.23]))) )
    mass_range = np.concatenate(
        (np.array((mass_range)), np.array(([125.89, 158.49, 199.53, 251.19])))
    )

    # If very high-mass MS is requested
    # High masses have convergence issues so we use a larger step in logM.
    #    mass_range = np.concatenate( (np.array((mass_range)),
    #            np.array(([398.11, 501.19, 630.96, 794.33, 1000]))))

    #   BYPASS STUFF TO GET SOME SPECIFIC MASSES - TESTING PURPOSES
    #    mass_range=[0.6,1.0,1.58,2.11,2.51,3.16,5.01,10.,21.13, 50.12, 100.0, 316.23]
    return mass_range


#############################################################
def central_hydrogen_range_to_compute(zams_x):
    """
    This routine defines the core hydrogen values to be computed
    These values are the same for all masses to obtain a regularly-spaced
    grid on the main sequence.
    These depend on Z and X at ZAMS, and will define the grid points

    Input: 	zams_x		ZAMS hydrogen mass fraction 	(float)
    Output:	xtarget_range	list of central hydrogens to compute in grid
                                in increasing order		(list of floats)
    """

    # The first core H mass fraction to stop at is that of PMS-epsilon
    # here, epsilon = 0.0015 following advice from Aaron Dotter on MESA ML
    first_x = zams_x - 0.0015

    #  # TAMS (low-X) deceleration
    #  xtarget_range = np.linspace(-6, -2, 9)
    #  for i in range(xtarget_range.__len__()):
    #    xtarget_range[i]   = math.pow(10, xtarget_range[i])

    # This is kind of a stupid fix -> should impose a format here
    xtarget_range = [
        1e-12,
        3.16227766017e-12,
        1e-11,
        3.16227766017e-11,
        1e-10,
        3.16227766017e-10,
        1e-09,
        3.16227766017e-09,
        1e-08,
        3.16227766017e-08,
        1e-07,
        3.16227766017e-07,
        1e-06,
        3.16227766017e-06,
        1e-05,
        3.16227766017e-05,
        0.0001,
        0.000316227766017,
        0.001,
        0.00316227766017,
        0.01,
    ]

    x = 0.01
    while x < first_x:
        # ZAMS (high-X) slow acceleration
        if x < first_x - 0.03:
            x += 0.01
            xtarget_range = np.append(xtarget_range, round(min(x, first_x), 5))
        else:
            x += 0.005
            xtarget_range = np.append(xtarget_range, round(min(x, first_x), 5))
    # xtarget_range = xtarget_range[::-1]

    # added by Natalie
    # extra_h = np.array([0.225,0.215,0.205,0.195,0.185])
    extra_h = 10 ** np.linspace(-2.25, -11.75, 20)
    xtarget_range = np.sort(np.append(xtarget_range, extra_h))

    return xtarget_range


################################################################################
# HAS BEEN RELOCATED
def cheby(M, location):
    """
    This routine generates a Chebyshev-style distribution of points from 0 to 1

    Input:  M   number of points for the mass-coordinate array
                M-2 points follow the Chebyshev distribution
                first and last are core and surface
            location    place where the points are more-densely packed
                        location can be
                        "surface" "surf" or "s",
                        "core" or "c",
                        "both" or "b"
    Output: array of size M containing mass coordinates
    """
    # TODO allow the place where the distribution is denser to be at any radius

    cheb = np.zeros(M)  # initialize

    if location == "core" or location == "c":
        M -= 1
        if (M % 2) == 0:  # if M is even
            N = 2 * M
        else:
            N = 2 * M + 1
        for k in range(1, M):
            cheb[k] = math.cos(math.pi * (2.0 * k - 1.0) / (2.0 * N))
        cheb[0] = 1.0

    if location == "surface" or location == "s" or location == "surf":
        M -= 1
        if (M % 2) == 0:  # if M is even
            N = 2 * M
        else:
            N = 2 * M + 1
        for k in range(1, M):
            cheb[M - k] = 1 - math.cos(math.pi * (2.0 * k - 1.0) / (2.0 * N))
        cheb[-1] = 0.0
        cheb[0] = 1.0

    if location == "both" or location == "b":
        N = M - 1
        for k in range(1, M):
            cheb[M - k] = (1 - math.cos(math.pi * (2.0 * k - 1.0) / (2.0 * N))) / 2
        cheb[-1] = 0.0
        cheb[0] = 1.0

    return cheb


#############################################################
def generate_inlist_ms(dictionary):
    """
    This routine redirects the print statement to the proper MESA version
    Input/output is passed straight to/from the correct inlist-generating routine

    Input:  dictionary          parameters for this run (incl. version number)  (dictionary)
    Output: inlist              contents of the inlist file for this run        (string)
            OR ValueError       if the version number does not match available routines
    """
    # TODO add a test in main to check we're using a proper MESA version
    mesa_version = dictionary["mesa_version"]
    if int(mesa_version) == 12115:
        return generate_inlist_ms_12115(dictionary)
    elif int(mesa_version) == 12778:
        return generate_inlist_ms_12778(dictionary)
    elif int(mesa_version) == 15140:
        return generate_inlist_ms_15140(dictionary)
    else:
        print("You are trying to invoke an unavailable version of MESA")
        raise ValueError


#############################################################
def generate_inlist_ms_12115(dictionary):
    """
    This routine generates an inlist based on
    all parameters in the input dictionary

    Input:  dictionary          parameters for this run (incl. version number)  (dictionary)
    Output: inlist              contents of the inlist file for this run        (string)

    This is for version 12115
    """

    # For a range of masses, we turn convective premixing on
    # This is useful for the masses where burning changes from pp to CNO
    if (dictionary["mass"] < 1.06) or (dictionary["mass"] > 1.4):
        # enable premixing
        dictionary[
            "premix_bit"
        ] = """
        !~~~~~~~~~~ convective premixing
        do_conv_premix = .true.
        conv_premix_avoid_increase = .true.
        conv_premix_time_factor = 0
        conv_premix_fix_pgas = .true.
        conv_premix_dump_snapshots = .false."""
    else:
        dictionary[
            "premix_bit"
        ] = """
        !~~~~~~~~~~ convective premixing
        do_conv_premix = .false."""

    # Modulate timestep in years so end of MS is zoomed in
    # this is needed to have enough profiles and compute derivatives
    if dictionary["xtarget"] < 0.01:
        dictionary["starting_timestep"] = 1.0
    else:
        dictionary["starting_timestep"] = 100

    inlist = """!! Automatically generated inlist for grid   !! DO NOT EDIT

    &star_job
        read_extra_star_job_inlist1 = .true.
        extra_star_job_inlist1_name = '{directory_ms}/inlists/inlist_common_ms_12115'
        history_columns_file = '{directory_ms}/inlists/hist.list'
        profile_columns_file = '{directory_ms}/inlists/prof.list'

        create_pre_main_sequence_model = {pms}
        load_saved_model = {load_model}
        saved_model_name = '{model_start}'
        save_model_when_terminate = .true.
        save_model_filename = '{directory_ms}/results/{log_dir}/evolution.mod'
        show_log_description_at_start = .false.
	years_for_initial_dt = {starting_timestep}

        eosDT_cache_dir =      '{directory_ms}/caches/M{mass}/eosDT_cache'
        eosPT_cache_dir =      '{directory_ms}/caches/M{mass}/eosPT_cache'
        eosDE_cache_dir =      '{directory_ms}/caches/M{mass}/eosDE_cache'
        ionization_cache_dir = '{directory_ms}/caches/M{mass}/ionization_cache'
        kap_cache_dir =        '{directory_ms}/caches/M{mass}/kap_cache'
        rates_cache_dir =      '{directory_ms}/caches/M{mass}/rates_cache'
    / ! end of star_job namelist

    &controls
        read_extra_controls_inlist1 = .true.
        extra_controls_inlist1_name = '{directory_ms}/inlists/inlist_common_ms_12115'
        initial_z = {zams_z}
        Zbase = {zams_z}
        initial_mass = {mass}

{premix_bit}

!!~~~~~~~~~~~~~~ output directory ~~~~~~~~~~~~~~~~~
        log_directory = '{directory_ms}/results/{log_dir}'
        extra_terminal_output_file = '{directory_ms}/results/{log_dir}/term.out'

!!~~~~~~~~~~~~~~ stop condition ~~~~~~~~~~~~~~~~~
        xa_central_lower_limit_species(1) = 'h1'
        xa_central_lower_limit(1) = {xtarget}
        when_to_stop_rtol = {rtol}
        when_to_stop_atol = 0d-8
        delta_XH_cntr_limit = {delta}
!       alpha_semiconvection = 1d-1

    / ! end of controls namelist

    &pgstar
    / ! end of pgstar namelist""".format(
        **dictionary
    )
    return inlist


#############################################################
def generate_inlist_ms_12778(dictionary):
    """
    This routine generates an inlist based on
    all parameters in the input dictionary

    Input:  dictionary          parameters for this run (incl. version number)  (dictionary)
    Output: inlist              contents of the inlist file for this run        (string)

    This is for version 12778
    wrt version 12115, overshooting parameters have a different syntax
    """

    # For a range of masses, we turn convective premixing on
    # This is useful for the masses where burning changes from pp to CNO
    if (dictionary["mass"] < 1.06) or (dictionary["mass"] > 1.4):
        # enable premixing
        dictionary[
            "premix_bit"
        ] = """
        !~~~~~~~~~~ convective premixing
        do_conv_premix = .true.
        conv_premix_avoid_increase = .true.
        conv_premix_time_factor = 0
        conv_premix_fix_pgas = .true.
        conv_premix_dump_snapshots = .false."""
    else:
        dictionary[
            "premix_bit"
        ] = """
        !~~~~~~~~~~ convective premixing
        do_conv_premix = .false."""

    # Modulate timestep in years so end of MS is zoomed in
    # this is needed to have enough profiles and compute derivatives
    if dictionary["xtarget"] < 0.01:
        dictionary["starting_timestep"] = 1.0
    else:
        dictionary["starting_timestep"] = 100

    inlist = """!! Automatically generated inlist for grid   !! DO NOT EDIT

    &star_job
        read_extra_star_job_inlist1 = .true.
        extra_star_job_inlist1_name = '{directory_ms}/inlists/inlist_common_ms_12778'
        history_columns_file = '{directory_ms}/inlists/hist.list'
        profile_columns_file = '{directory_ms}/inlists/prof.list'

        create_pre_main_sequence_model = {pms}
        load_saved_model = {load_model}
        saved_model_name = '{model_start}'
        save_model_when_terminate = .true.
        save_model_filename = '{directory_ms}/results/{log_dir}/evolution.mod'
        show_log_description_at_start = .false.
	years_for_initial_dt = {starting_timestep}

        eosDT_cache_dir =      '{directory_ms}/caches/M{mass}/eosDT_cache'
        eosPT_cache_dir =      '{directory_ms}/caches/M{mass}/eosPT_cache'
        eosDE_cache_dir =      '{directory_ms}/caches/M{mass}/eosDE_cache'
        ionization_cache_dir = '{directory_ms}/caches/M{mass}/ionization_cache'
        kap_cache_dir =        '{directory_ms}/caches/M{mass}/kap_cache'
        rates_cache_dir =      '{directory_ms}/caches/M{mass}/rates_cache'
    / ! end of star_job namelist

    &controls
        read_extra_controls_inlist1 = .true.
        extra_controls_inlist1_name = '{directory_ms}/inlists/inlist_common_ms_12778'
        initial_z = {zams_z}
        Zbase = {zams_z}
        initial_mass = {mass}

{premix_bit}

!!~~~~~~~~~~~~~~ output directory ~~~~~~~~~~~~~~~~~
        log_directory = '{directory_ms}/results/{log_dir}'
        extra_terminal_output_file = '{directory_ms}/results/{log_dir}/term.out'

!!~~~~~~~~~~~~~~ stop condition ~~~~~~~~~~~~~~~~~
        xa_central_lower_limit_species(1) = 'h1'
        xa_central_lower_limit(1) = {xtarget}
        when_to_stop_rtol = {rtol}
        when_to_stop_atol = 0d-8
        delta_XH_cntr_limit = {delta}
!       alpha_semiconvection = 1d-1

    / ! end of controls namelist

    &pgstar
    / ! end of pgstar namelist""".format(
        **dictionary
    )
    return inlist


#############################################################
def generate_inlist_ms_15140(dictionary):
    """
    This routine generates an inlist based on
    all parameters in the input dictionary

    Input:  dictionary          parameters for this run (incl. version number)  (dictionary)
    Output: inlist              contents of the inlist file for this run        (string)

    This is for version 15140
    version 15140 uses the overshooting parameter syntax from versions >=12778
    wrt to v12778, there are two more sections in the inlist, &kap and &eos, and fewer controls
    """

    # For a range of masses, we turn convective premixing on
    # This is useful for the masses where burning changes from pp to CNO
    if (dictionary["mass"] < 1.06) or (dictionary["mass"] > 1.4):
        # enable premixing
        dictionary[
            "premix_bit"
        ] = """
        !~~~~~~~~~~ convective premixing
        do_conv_premix = .true.
        conv_premix_avoid_increase = .true.
        conv_premix_time_factor = 0
        conv_premix_fix_pgas = .true.
        conv_premix_dump_snapshots = .false."""
    else:
        dictionary[
            "premix_bit"
        ] = """
        !~~~~~~~~~~ convective premixing
        do_conv_premix = .false."""

    # Modulate timestep in years so end of MS is zoomed in
    # this is needed to have enough profiles and compute derivatives
    if dictionary["xtarget"] < 0.01:
        dictionary["starting_timestep"] = 1.0
    else:
        dictionary["starting_timestep"] = 100

    inlist = """!! Automatically generated inlist for grid   !! DO NOT EDIT

    &star_job
        read_extra_star_job_inlist1 = .true.
        extra_star_job_inlist1_name = '{directory_ms}/inlists/inlist_common_ms_15140'
        history_columns_file = '{directory_ms}/inlists/hist.list'
        profile_columns_file = '{directory_ms}/inlists/prof.list'

        create_pre_main_sequence_model = {pms}
        load_saved_model = {load_model}
        saved_model_name = '{model_start}'
        save_model_when_terminate = .true.
        save_model_filename = '{directory_ms}/results/{log_dir}/evolution.mod'
        show_log_description_at_start = .false.
	years_for_initial_dt = {starting_timestep}

        eosDT_cache_dir =      '{directory_ms}/caches/M{mass}/eosDT_cache'
        eosPT_cache_dir =      '{directory_ms}/caches/M{mass}/eosPT_cache'
        eosDE_cache_dir =      '{directory_ms}/caches/M{mass}/eosDE_cache'
        ionization_cache_dir = '{directory_ms}/caches/M{mass}/ionization_cache'
        kap_cache_dir =        '{directory_ms}/caches/M{mass}/kap_cache'
        rates_cache_dir =      '{directory_ms}/caches/M{mass}/rates_cache'
    / ! end of star_job namelist

    &eos
    /

    &kap
        Zbase = {zams_z}
    /

    &controls
        read_extra_controls_inlist1 = .true.
        extra_controls_inlist1_name = '{directory_ms}/inlists/inlist_common_ms_15140'
        initial_z = {zams_z}
        initial_mass = {mass}

{premix_bit}

!!~~~~~~~~~~~~~~ output directory ~~~~~~~~~~~~~~~~~
        log_directory = '{directory_ms}/results/{log_dir}'
        extra_terminal_output_file = '{directory_ms}/results/{log_dir}/term.out'

!!~~~~~~~~~~~~~~ stop condition ~~~~~~~~~~~~~~~~~
        xa_central_lower_limit_species(1) = 'h1'
        xa_central_lower_limit(1) = {xtarget}
        when_to_stop_rtol = {rtol}
        when_to_stop_atol = 0d-8
        delta_XH_cntr_limit = {delta}
!       alpha_semiconvection = 1d-1

    / ! end of controls namelist

    &pgstar
    / ! end of pgstar namelist""".format(
        **dictionary
    )
    return inlist


###############################################################################################
def generate_script_ms(dictionary, coreh_range):
    """
    This routine generates the script that has to be submitted on Eureka
    to compute the main-sequence grid

    Input: dictionary		parameters for the runs		(dictionary)
           coreh_range 		core hydrogen values to sample	(list of floats)
    """
    # TODO replace the submitting of several sbatches with an array job
    ###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The first part of the script is a general header
    script = """#!/bin/bash

#SBATCH --partition=shared
#SBATCH --job-name="{zams_z}_{mass}"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --mem=20G
#SBATCH --time=12:00:00
#SBATCH -o slurm.%j.out
#SBATCH -e slurm.%j.err

export OMP_NUM_THREADS=16

cd $SLURM_SUBMIT_DIR

source /users/m17487/.mesarc-r{mesa_version}
#/usr/bin/touch star""".format(
        **dictionary
    )

    # The routine then prints the commands for each coreh step, in decreasing order
    for coreh in coreh_range:
        dictionary["xtarget"] = coreh
        dictionary["log_dir"] = "M{mass}_Xc{xtarget}_ev".format(**dictionary)
        # What comes next is different for each Xtarget
        script += """\n\n#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export MESA_OUTPUT={directory_ms}/results/{log_dir}
export MESA_INLIST={directory_ms}/inlists/{log_dir}.in

rm  $MESA_OUTPUT/*
mkdir -p $MESA_OUTPUT

cp $MESA_INLIST $MESA_OUTPUT/inlist
./star """.format(
            **dictionary
        )

    script += """\n\n#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mv slurm.$SLURM_JOB_ID.out $MESA_OUTPUT
mv slurm.$SLURM_JOB_ID.err $MESA_OUTPUT """.format(
        **dictionary
    )

    return script
