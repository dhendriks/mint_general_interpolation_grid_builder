#!/usr/bin/python3

# runs the chosen evolutionary phase for a given total mass
# spaces out tracks to fill the parameter space

import os
import sys
import re

import mesaPlot as mp
import numpy as np

from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.functions.run_tracks import (
    lines_that_contain,
    run_mass_track,
    get_items_before_and_after,
    check_for_sufficient_resolution,
)


#########################################################################
# get initial and final masses from command line arguements
final_mass = float(sys.argv[1])

global evol_phase
evol_phase = os.getcwd().split("/")[-2]
evol_phase_list = ["MS", "MC", "GB", "CHeB", "EAGB", "TPAGB"]
previous_evol_phase = evol_phase_list[evol_phase_list.index(evol_phase) - 1]

########################################################################
# get list of available starting models
# start by running largest core mass model, SSE model and smallest core mass model

saved_models_dir = os.path.split(os.getcwd())[0].replace(
    evol_phase, os.path.join(previous_evol_phase, "saved_models")
)
# print(saved_models_dir)


for line in lines_that_contain("log_directory", "inlist"):
    logs_dir = line.split("'")[-2]

# get list of available models
avail_models = np.sort(
    [
        float(mod.split("_")[0])
        for mod in os.listdir(saved_models_dir)
        if float(mod.split("_")[-1][:-6]) == final_mass
    ]
)

# start with lowest initial mass, highest initial mass and CME models
masses_to_run = [avail_models[0], avail_models[-1]]
if float(final_mass) in avail_models:
    masses_to_run += [float(final_mass)]
masses_to_run = sorted(set(masses_to_run))


# if after helium ignition, also run lowest core mass model
if evol_phase_list.index(evol_phase) >= evol_phase_list.index("CHeB"):
    # get initial core masses
    m = mp.MESA()
    initial_core_masses = {}
    for Mi in avail_models:
        previous_logs_dir = logs_dir.replace("Mi", str(Mi)).replace(
            evol_phase, previous_evol_phase
        )
        m.loadHistory(previous_logs_dir)
        initial_core_masses[Mi] = m.hist.he_core_mass[-1]
    print(initial_core_masses)

    min_core_mass_model = min(initial_core_masses, key=initial_core_masses.get)
    if min_core_mass_model not in masses_to_run:
        masses_to_run += [min_core_mass_model]

    # # don't use lowest initial mass models if very similar to SSE model (i.e. both strong He-flash)
    # if float(final_mass) in avail_models:
    #     min_initial_mass = min(avail_models)
    #     if (
    #         abs(
    #             initial_core_masses[min_initial_mass]
    #             - initial_core_masses[float(final_mass)]
    #         )
    #         < 0.01
    #     ):
    #         avail_models = np.delete(
    #             avail_models, np.argwhere(avail_models == min_initial_mass)
    #         )

    # print("Initial masses available: ", avail_models)

print("Initial masses to run: ", masses_to_run)

########################################################################
# Loop through initial masses

complete_models = []
for Mi in masses_to_run:
    print("Running Mi = ", Mi)
    run_mass_track(Mi)
    complete_models += [Mi]
    complete_models.sort()

    if len(complete_models) > 1:
        for M2 in get_items_before_and_after(complete_models, Mi):
            print("Comparing to ", M2)
            add_more_res = check_for_sufficient_resolution(
                M1=Mi, M2=M2, logs_dir=logs_dir
            )
            if add_more_res:
                inbetween_mass = avail_models[
                    np.argmin(abs(avail_models - (Mi + M2) / 2))
                ]
                if inbetween_mass not in masses_to_run:
                    masses_to_run += [inbetween_mass]

    print("Initial masses run = ", complete_models)
    print("Initial masses to run: ", masses_to_run)
