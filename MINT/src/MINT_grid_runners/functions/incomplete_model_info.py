#!/usr/bin/python3

import os
from pathlib import Path
import sys

############################################################
# hacky script to scan through a grid of MESA runs for MINT
# and report success / failures in a compact way
############################################################

# work in directory provided as cmd-line arg, or current directory
if len(sys.argv) > 1:
    grid_dir = sys.argv[1]
else:
    grid_dir = os.getcwd()

# list of subfolder full paths
def subfolders_paths(dir):
    return [f.path for f in os.scandir(dir) if f.is_dir()]


# list of subfolders (not full paths, just names)
def subfolders(dir):
    return [f.name for f in os.scandir(dir) if f.is_dir()]


# check if string x is a float
def isfloat(x):
    try:
        float(x)
        return True
    except:
        return False


# convert string x to an int or float if possible,
# otherwise return the string
def tonum(x):
    try:
        y = int(x)
    except:
        try:
            y = float(x)
        except:
            y = x
    return y


# metallicity sorter
def sortZ(x):
    return x.lstrip("Z")


# given a line of data and header names as a list
# covert the data to a dict with keys from the header
# and return this dict.
def linedict(header, line):
    x = {}
    data = line.strip().split()
    for pair in zip(header, data):
        x[pair[0]] = tonum(pair[1])
    return x


# check status of model sequence located in dir
def check_status(phase, dir, lendirstrings):

    # where is the history file?
    historyfile = os.path.join(dir, "LOGS_" + phase, "history.data")

    if os.path.exists(historyfile):
        f = open(historyfile, "r")
        # skip some lines (being explicit in case you need them)
        f.readline()  # numbers
        f.readline()  # version number etc.
        f.readline()  # blank
        f.readline()  # blank
        f.readline()  # numbers

        # next is the header
        header = f.readline().strip().split()

        # loop over lines in the history file
        data = None
        for line in f:
            data = linedict(header, line)
            # process line-by-line here if you want to
            # ...

        if data:
            # check final line of data
            if phase == "MS":
                # main sequence stars
                if data["center_h1"] > 1e-6:
                    # failed to reach target hydrogen abundance
                    print(
                        f"{dir:{lendirstrings}s} Final Xc = {data['center_h1']:12g} : failed to reach Xc=1e-6 after {data['model_number']:6} models, L/Ledd = {10.0**data['log_L_div_Ledd']:12g}, degen = {data['center_degeneracy']:12g}"
                    )

    else:
        print(f"{dir:{lendirstrings}s} history file missing")


# make list of metallicity folders
Zfolders = subfolders(grid_dir)
Zfolders.sort(key=sortZ)

# find available phases of evolution
phases = {}
for Zfolder in Zfolders:
    Zpath = os.path.join(grid_dir, Zfolder)
    phasefolders = list(s for s in subfolders(Zpath) if s[0] != "_")
    for phase in phasefolders:
        phases[phase] = phase
phases = list(phases.keys())

phases = ["MS"]

print(f"Found evolution phases {phases}")

# get list of model sequence dirs and
# length of the longest dir string
lendirstrings = 0
model_sequence_dirs = []
for Zfolder in Zfolders:
    for phase in phases:
        d = os.path.join(Zfolder, phase)
        if os.path.exists(d):
            masses = list(s for s in subfolders(d) if isfloat(s))
            if masses:
                masses.sort(key=float)
                for M in masses:
                    model_sequence_dir = os.path.join(d, M)
                    model_sequence_dirs.append(model_sequence_dir)
                    lendirstrings = max(lendirstrings, len(model_sequence_dir))

# loop to check status of each evolutionary run
for model_sequence_dir in model_sequence_dirs:
    try:
        text = Path(os.path.join(model_sequence_dir, "STATUS.txt")).read_text()
    except:
        continue
    if text == "FINISHED\n":
        # model sequence has finished, with
        # or without error
        check_status(phase, model_sequence_dir, lendirstrings)
    else:
        # status isn't finished
        print(f"{model_sequence_dir:{lendirstrings}s} not finished")
