#!/usr/bin/python3

# runs the TPAGB to fill the parameter space

import os
import sys
import re

import mesaPlot as mp
import numpy as np

from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    find_and_replace_text,
)


def lines_that_contain(string, fp):
    return [line for line in fp if string in line]


def run_cme_model(M):

    find_and_replace_text(
        file="inlist_common",
        find_text="x_logical_ctrl\(3\) = .false.",
        replace_text="x_logical_ctrl(3) = .true.",
    )
    find_and_replace_text(
        file="inlist_common", find_text="Mi", replace_text=f"{M}",
    )
    find_and_replace_text(
        file="inlist", find_text="Mi", replace_text=f"{M}",
    )

    # os.system(f"./star >> {M}.out")

    return


def run_mass_loss_model(M, NTP):

    find_and_replace_text(
        file="inlist", find_text=f"LOGS_NTP", replace_text=f"LOGS_TP{NTP}",
    )
    find_and_replace_text(
        file="inlist", find_text=f"photos_NTP", replace_text=f"photos_TP{NTP}",
    )

    os.system(f"./re t00{NTP} >> TP{NTP}.out")

    find_and_replace_text(
        file="inlist", find_text=f"LOGS_TP{NTP}", replace_text=f"LOGS_NTP",
    )
    find_and_replace_text(
        file="inlist", find_text=f"photos_TP{NTP}", replace_text=f"photos_NTP",
    )
    return


M = float(sys.argv[1])
run_cme_model(M=M)

with open("inlist", "r") as fp:
    for line in lines_that_contain("log_directory", fp):
        logs_dir = line.split("'")[-2]

m = mp.MESA()
m.loadHistory(logs_dir)
time = m.hist.star_age[-1]
Menv = m.hist.star_mass[0] - m.hist.he_core_mass[0]
wind_mass_loss_rate = Menv / time
print("Mass loss rate = ", wind_mass_loss_rate)

find_and_replace_text(
    file="inlist_common",
    find_text="x_logical_ctrl\(3\) = .true.",
    replace_text="x_logical_ctrl(3) = .false.",
)
find_and_replace_text(
    file="inlist", find_text=f"LOGS_TPAGB_{M}", replace_text=f"LOGS_NTP",
)
find_and_replace_text(
    file="inlist", find_text=f"photos_TPAGB_{M}", replace_text=f"photos_NTP",
)
find_and_replace_text(
    file="inlist",
    find_text="mass_change = 0d0",
    replace_text=f"mass_change = -{wind_mass_loss_rate}",
)
find_and_replace_text(
    file="inlist",
    find_text="x_integer_ctrl/(2/) = 10",
    replace_text="x_integer_ctrl(2) = 20",
)

for NTP in [1, 2, 4, 8]:
    print(NTP)
    run_mass_loss_model(M=M, NTP=NTP)
