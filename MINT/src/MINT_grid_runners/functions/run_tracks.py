import re
import os

import mesaPlot as mp
import numpy as np

from mint_general_interpolation_grid_builder.MINT.config.output import (
    format_logs_foldername,
    format_photos_foldername,
    format_save_model_filename,
)
from mint_general_interpolation_grid_builder.MINT.config.core_mass_fraction_res import (
    core_mass_fraction_res_dict,
)


def find_and_replace_text(file: str, find_text: str, replace_text: str) -> None:
    """Replaces text in a file.

    Args:
        file (str): file location
        find_text (str): text to replace
        replace_text (str): text to replace with
    """
    with open(file, "r+") as f:
        text = f.read()
        text = re.sub(find_text, replace_text, text)
        f.seek(0)
        f.write(text)
        f.truncate()

    return


def lines_that_contain(string: str, filename: str) -> list:
    """Searches file for entire lines containing a string.

    Args:
        string (str): String to search for
        filename (str): Filename to search

    Returns:
        _type_: List of all lines that contain string
    """
    with open(filename, "r") as fp:
        return [line for line in fp if string in line]


def run_mass_track(mass: float, Mi: float, evol_phase: str) -> None:
    """Run mass track.

    Args:
        Mi (float): Initial mass
        evol_phase (str): Evolutionary phase label
    """

    if evol_phase == "GB":
        prev_evol_phase = "MC"
    elif evol_phase == "CHeB":
        prev_evol_phase = "GB"
    elif evol_phase == "EAGB":
        prev_evol_phase == "CHeB"

    find_and_replace_text(
        file=f"inlist_{evol_phase}",
        find_text=lines_that_contain("load_model_filename", f"inlist_{evol_phase}")[
            0
        ].split("/")[-1],
        replace_text=format_save_model_filename(prev_evol_phase, mass, Mi) + "'\n",
    )
    find_and_replace_text(
        file=f"inlist_{evol_phase}",
        find_text=lines_that_contain("save_model_filename", f"inlist_{evol_phase}")[
            0
        ].split("/")[-1],
        replace_text=format_save_model_filename(evol_phase, mass, Mi) + "'\n",
    )
    find_and_replace_text(
        file=f"inlist_{evol_phase}",
        find_text=lines_that_contain("log_directory", f"inlist_{evol_phase}")[0].split(
            "/"
        )[-1],
        replace_text=format_logs_foldername(evol_phase, Mi) + "'\n",
    )
    find_and_replace_text(
        file=f"inlist_{evol_phase}",
        find_text=lines_that_contain("photo_directory", f"inlist_{evol_phase}")[
            0
        ].split("/")[-1],
        replace_text=format_photos_foldername(evol_phase, Mi) + "'\n",
    )
    find_and_replace_text(
        file="inlist_common",
        find_text="initial_mass = Mi",
        replace_text=f"initial_mass = {Mi}",
    )

    os.system(f"./star inlist_{evol_phase} >> {Mi}.out")

    return


def check_for_sufficient_resolution(
    M1: float, M2: float, logs_dir: str, evol_phase: str
) -> bool:
    """Checks resolution in core mass fraction between adjacent tracks.

    Args:
        M1 (float): mass of first track
        M2 (float): mass of second track
        logs_dir (str): location of logs directory
        evol_phase (str): evol phase

    Returns:
        bool: If there is sufficient resolution
    """

    allowed_Mc_diff_fraction = core_mass_fraction_res_dict[evol_phase]

    m1 = mp.MESA()
    m1.loadHistory(logs_dir.replace("Mi", str(M1)))
    m2 = mp.MESA()
    m2.loadHistory(logs_dir.replace("Mi", str(M2)))

    common_save_targets = np.intersect1d(
        m1.hist.data["interp_variable_hit"], m2.hist.data["interp_variable_hit"]
    )
    common_save_targets = np.delete(
        common_save_targets, np.where(common_save_targets == 0)
    )
    # print(common_save_targets)

    if len(common_save_targets) <= 2:
        add_more_res = True
    else:
        count = 0
        add_more_res = False
        for target in common_save_targets[:]:
            Mc_diff = (
                m1.hist.data["he_core_mass"][
                    np.where(m1.hist.data["interp_variable_hit"] == target)[0]
                ]
                - m2.hist.data["he_core_mass"][
                    np.where(m2.hist.data["interp_variable_hit"] == target)[0]
                ]
            )
            # print(target, Mc_diff/m1.hist.data["star_mass"][0])
            if abs(Mc_diff) > allowed_Mc_diff_fraction * m1.hist.data["star_mass"][0]:
                count += 1

        if count > 2:
            add_more_res = True

    return add_more_res


def update_models_run_status_file(text):
    with open("models_run.txt", "a") as myfile:
        myfile.write(text)


def get_items_before_and_after(lst: list, item: float) -> list:
    """Find list items before and after specificied item.

    Args:
        lst (list): list
        item (float): known item

    Returns:
        list: items before and after known item
    """
    index = lst.index(item)
    before = lst[index - 1] if index > 0 else None
    after = lst[index + 1] if index < len(lst) - 1 else None
    return [M for M in [before, after] if M is not None]


def run_TAMS_mass_change(
    saved_models_dir: str, initial_mass: float, final_mass: float
) -> None:
    """Runs the TAMS mass change process for a specified initial and final mass

    Args:
        saved_models_dir (str): location of models
        initial_mass (float): initial mass to use
        final_mass (float): final mass to produce
    """

    avail_models = np.sort(
        [
            float(mod.split("_")[-1][:-6])
            for mod in os.listdir(saved_models_dir)
            if mod.split("_")[0] == str(initial_mass)
        ]
    )
    print("Available models:: ", avail_models)

    # attempted_models = np.sort(
    #     [
    #         float(file[:-4])
    #         for file in os.listdir(os.getcwd())
    #         if (file[-3:] == "out" and file[:5] != "slurm")
    #     ]
    # )
    # print('Attempted models: ', attempted_models)  # read inlist

    # read inlist
    with open("inlist_MC", "r") as f:
        inlist = f.read()

    try:
        # loosing mass
        if float(initial_mass) > float(final_mass):
            print("Choosing lowest mass model")
            prev_mass = avail_models[avail_models > float(final_mass)][0]
            # # if using wind choose lowest mass available
            # if re.search("x_ctrl\(3\) = 0.0", inlist):
            #     prev_mass = avail_models[avail_models > float(final_mass)][0]
            # # if using star cut then start from inital mass
            # else:
            #     prev_mass = initial_mass
        # accreting mass
        else:
            # prev_mass = max(
            #     attempted_models[np.where(attempted_models < float(final_mass))][-1],
            #     float(initial_mass),
            # )
            prev_mass = max(
                avail_models[np.where(avail_models < float(final_mass))][-1],
                float(initial_mass),
            )
    except:
        prev_mass = initial_mass

    print(f"Starting from {prev_mass}")

    ######################################################################
    # prepare inlist

    find_and_replace_text(
        file="inlist_MC",
        find_text=lines_that_contain("load_model_filename", "inlist_MC")[0].split("/")[
            -1
        ],
        replace_text=format_save_model_filename("MC", prev_mass, initial_mass) + "'\n",
    )
    find_and_replace_text(
        file="inlist_MC",
        find_text=lines_that_contain("save_model_filename", "inlist_MC")[0].split("/")[
            -1
        ],
        replace_text=format_save_model_filename("MC", final_mass, initial_mass) + "'\n",
    )
    find_and_replace_text(
        file="inlist_common",
        find_text=lines_that_contain("log_directory", "inlist_MC")[0].split("LOGS")[-1],
        replace_text=format_logs_foldername("MC", initial_mass) + "'\n",
    )
    find_and_replace_text(
        file="inlist_common",
        find_text=lines_that_contain("photo_directory", "inlist_MC")[0].split("photos")[
            -1
        ],
        replace_text=format_photos_foldername("MC", initial_mass) + "'\n",
    )
    find_and_replace_text(
        file="inlist_common",
        find_text="initial_mass = Mi",
        replace_text=f"initial_mass = {initial_mass}",
    )

    # run
    # print(f"Running {final_mass}")
    os.system(f"./star inlist_MC > {initial_mass}.out")

    # If final model doesn't exist, try again
    if not os.path.exists(
        os.path.join(
            saved_models_dir, format_save_model_filename("MC", final_mass, initial_mass)
        )
    ):

        ######################################################################
        # 2. if accreting mass and slow mass accretion rate fails try faster rate
        if float(initial_mass) < float(final_mass):

            # check if mass accretion rate used was slow rate
            if re.search("x_ctrl\(1\) = 1d-8", inlist):
                print("Trying faster mass accretion rate")

                # change to higher mass accretion rate
                find_and_replace_text(
                    file="inlist_MC",
                    find_text="x_ctrl\(1\) = 1d-8",
                    replace_text="x_ctrl(1) = 1d-4",
                )

                # run
                os.system(f"./star inlist_MC >> {initial_mass}.out")

        #######################################################################
        # 3. if stripping mass and wind doesn't work try mass cut
        elif float(initial_mass) > float(final_mass):
            print("Trying star cut")

            # # check if star cut limit set low
            # if re.search("x_ctrl\(3\) = 1", inlist):

            # change to force star cut
            find_and_replace_text(
                file="inlist_MC",
                find_text="x_ctrl\(3\) = 1",
                replace_text="x_ctrl(3) = 100",
            )
            # change starting mass to initial mass model
            find_and_replace_text(
                file="inlist_MC",
                find_text=lines_that_contain("load_model_filename", "inlist_MC")[
                    0
                ].split("/")[-1],
                replace_text=format_save_model_filename(
                    "MC", initial_mass, initial_mass
                )
                + "'\n",
            )
            # run
            os.system(f"./star inlist_MC >> {final_mass}.out")

            find_and_replace_text(
                file="inlist_MC",
                find_text="x_ctrl\(3\) = 100",
                replace_text="x_ctrl(3) = 1",
            )
