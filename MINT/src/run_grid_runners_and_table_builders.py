#!/usr/bin/python3
"""
Control function that handles the stellar phases

TODO: make some abstractions in this function. Lots of repeated steps
"""

import os

from mint_general_interpolation_grid_builder.MINT.config.table_columns import (
    table_columns,
)

from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MS_grid_runner import (
    MainSequenceGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.MS_table_builder import (
    MainSequenceTableBuilder,
)

# from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.MC_grid_runner import (
#     TAMSMassChangeGridRunner,
# )
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.GB_grid_runner import (
    RedGiantBranchGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.GB_table_builder import (
    RedGiantBranchTableBuilder,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.CHeB_grid_runner import (
    CoreHeliumBurningGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.CHeB_table_builder import (
    CoreHeliumBurningTableBuilder,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.EAGB_grid_runner import (
    EarlyAsymptoticGiantBranchGridRunner,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.EAGB_table_builder import (
    EarlyAsymptoticGiantBranchTableBuilder,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.TPAGB_grid_runner import (
    ThermallyPulsingAsymptoticGiantBranchGridRunner,
)


def run_grid_general(
    grid_config,
    stellar_phases_list,
    run_mesa=False,
    build_interpolation_tables=False,
    pre_grid_hook=None,
    post_grid_hook=None,
):
    """
    General control function
    """

    #############################################
    # Run pre-grid hook
    if pre_grid_hook is not None:
        pre_grid_hook(config=grid_config)

    #############################################
    # Process stellar phases
    for stellar_phase_dict in stellar_phases_list:
        #########
        # Copy dict
        stellar_phase_config = {
            **grid_config,
        }

        #########
        #
        stellar_phase_config["grid_directory"] = os.path.join(
            stellar_phase_config["metallicity_directory"], stellar_phase_dict["name"]
        )

        #########
        # run pre-stellar-phase function
        if stellar_phase_dict.get("pre_stellar_phase_function", None) is not None:
            stellar_phase_config = stellar_phase_dict["pre_stellar_phase_function"](
                stellar_phase_config
            )

        #########
        #
        if run_mesa and stellar_phase_dict.get("run_mesa", True):

            #########
            # run pre-mesa function
            if stellar_phase_dict.get("pre_mesa_function", None) is not None:
                stellar_phase_config = stellar_phase_dict["pre_mesa_function"](
                    stellar_phase_config
                )

            # Set up object
            MESA_grid_builder = stellar_phase_dict["MESAGridRunner"](
                settings=stellar_phase_config
            )

            # Run mesa
            MESA_grid_builder.write_and_run_MESA_grid()

            #########
            # run post-mesa function
            if stellar_phase_dict.get("post_mesa_function", None) is not None:
                stellar_phase_config = stellar_phase_dict["post_mesa_function"](
                    stellar_phase_config
                )

        #########
        # Build interpolation table
        if build_interpolation_tables and stellar_phase_dict.get(
            "build_interpolation_tables", True
        ):

            #########
            # run pre-mesa function
            if (
                stellar_phase_dict.get("pre_interpolation_table_function", None)
                is not None
            ):
                stellar_phase_config = stellar_phase_dict[
                    "pre_interpolation_table_function"
                ](stellar_phase_config)

            #########
            #
            if stellar_phase_config["workload_manager"] == "slurm":
                slurm_path = os.path.join(
                    stellar_phase_config["grid_directory"], "slurm_tabulate.sub"
                )
                os.system("sbatch {}".format(slurm_path))
            else:
                # Set up object
                interpolation_grid_builder = stellar_phase_dict[
                    "interpolation_grid_builder"
                ](settings=stellar_phase_config)

                # Build interpolation table
                interpolation_grid_builder.build_interpolation_grid()

            #########
            # run post-mesa function
            if (
                stellar_phase_dict.get("post_interpolation_table_function", None)
                is not None
            ):
                stellar_phase_config = stellar_phase_dict[
                    "post_interpolation_table_function"
                ](stellar_phase_config)

        #########
        # run post-stellar-phase function
        if stellar_phase_dict.get("post_stellar_phase_function", None) is not None:
            stellar_phase_config = stellar_phase_dict["post_stellar_phase_function"](
                stellar_phase_config
            )

    #############################################
    # Run pre-grid hook
    if post_grid_hook is not None:
        post_grid_hook(config=grid_config)


def run_grid_new(
    grid_config,
    stellar_type_handle_dict=None,
    run_mesa=False,
    build_interpolation_tables=False,
    pre_grid_hook=None,
    post_grid_hook=None,
):
    """
    Suggested new design for the function

    TODO: we can put postprocessing runners in here
    """

    # Extra config
    grid_config["metallicity_directory"] = os.path.join(
        grid_config["grids_root_directory"], "Z" + str(grid_config["metallicity"])
    )

    # add table columns to config
    grid_config = {**grid_config, **table_columns}

    if stellar_type_handle_dict is None:
        stellar_type_handle_dict = {}

    #########
    # setup of stellar phase dict
    stellar_phases_list = []

    # # Example
    # if stellar_type_handle_dict.get("EXAMPLE", False):
    #     stellar_phases_list.append(
    #         {
    #             'name': 'MS'
    #             'pre_stellar_phase_function': example,
    #             'run_mesa': True,
    #             'pre_mesa_function': example,
    #             'MESAGridRunner': ExampleGridRunner
    #             'post_mesa_function': example,
    #             'build_interpolation_tables': True,
    #             'pre_interpolation_table_function': example,
    #             'interpolation_grid_builder': ExampleTableBuilder
    #             'post_interpolation_table_function': example,
    #             'post_stellar_phase_function': example,
    #         }
    #     )

    #########
    # MS phase config
    if stellar_type_handle_dict.get("MS", False):
        stellar_phases_list.append(
            {
                "name": "MS",
                "MESAGridRunner": MainSequenceGridRunner,
                "PostprocessingMESAGridRunners": [AccretionMESATableBuilder],
                "interpolation_grid_builder": MainSequenceTableBuilder,
            }
        )

    #########
    # TAMS phase config
    if stellar_type_handle_dict.get("TAMS_MASS_CHANGE", False):
        stellar_phases_list.append(
            {
                "name": "TAMS_MASS_CHANGE",
                "MESAGridRunner": TAMSMassChangeGridRunner,
                "build_interpolation_tables": False,
            }
        )

    #########
    # GB phase config
    if stellar_type_handle_dict.get("GB", False):
        stellar_phases_list.append(
            {
                "name": "GB",
                "MESAGridRunner": RedGiantBranchGridRunner,
                "interpolation_grid_builder": RedGiantBranchTableBuilder,
            }
        )

    #########
    # CHeB phase config
    if stellar_type_handle_dict.get("CHeB", False):
        stellar_phases_list.append(
            {
                "name": "CHeB",
                "MESAGridRunner": CoreHeliumBurningGridRunner,
                "interpolation_grid_builder": CoreHeliumBurningTableBuilder,
            }
        )

    #########
    # EAGB phase config
    if stellar_type_handle_dict.get("EAGB", False):
        stellar_phases_list.append(
            {
                "name": "EAGB",
                "MESAGridRunner": EarlyAsymptoticGiantBranchGridRunner,
                "interpolation_grid_builder": EarlyAsymptoticGiantBranchTableBuilder,
            }
        )

    #########
    # TPAGB phase config
    if stellar_type_handle_dict.get("TPAGB", False):
        stellar_phases_list.append(
            {
                "name": "TPAGB",
                "MESAGridRunner": ThermallyPulsingAsymptoticGiantBranchGridRunner,
                "interpolation_grid_builder": ThermallyPulsingAsymptoticGiantBranchTableBuilder,
            }
        )

    # #########
    # # COWD phase config
    # if stellar_type_handle_dict.get("CO_WD", False):
    #     stellar_phases_list.append(
    #         {
    #             'name': 'CO_WD'
    #             'MESAGridRunner': CarbonOxygenWhiteDwarfMESAGridRunner
    #             'interpolation_grid_builder': CarbonOxygenWhiteDwarfTableBuilder
    #         }
    #     )

    #########
    # Run the grid
    run_grid_general(
        grid_config=grid_config,
        stellar_phases_list=stellar_phases_list,
        run_mesa=run_mesa,
        build_interpolation_tables=build_interpolation_tables,
        pre_grid_hook=pre_grid_hook,
        post_grid_hook=post_grid_hook,
    )


def run_grid(
    grid_config,
    stellar_type_handle_dict=None,
    run_mesa=False,
    build_interpolation_tables=False,
    pre_grid_hook=None,
    post_grid_hook=None,
):
    """
    Control function that handles the stellar phases.

    This function orchestrates the execution of various phases in stellar evolution simulations
    based on the provided `grid_config` and optional parameters.

    Parameters:
    - grid_config (dict): A dictionary containing configuration parameters for the stellar grid.
    - stellar_type_handle_dict (dict, optional): A dictionary mapping stellar type handles to boolean
      values indicating whether to include the corresponding phase. Defaults to None.
    - run_mesa (bool, optional): A boolean flag indicating whether to run MESA simulations for each phase.
      Defaults to False.
    - build_interpolation_tables (bool, optional): A boolean flag indicating whether to build interpolation
      tables for each phase. Defaults to False.
    - pre_grid_hook (function, optional): A function to be executed before starting the grid calculations.
      It should accept a single argument `config` which is the `grid_config` dictionary. Defaults to None.
    - post_grid_hook (function, optional): A function to be executed after completing the grid calculations.
      It should accept a single argument `config` which is the `grid_config` dictionary. Defaults to None.

    Returns:
    - None

    Notes:
    - Each phase introduces some configuration before execution, which may overwrite values in `grid_config`.
    - User-defined pre and post grid hooks can be provided to execute custom functions before and after the calculations.
      These functions must be structured as `def pre_grid_hook(config)`.

    Example usage:
    ```python
    grid_config = {...}  # Define grid configuration
    run_grid(grid_config, stellar_type_handle_dict={"MS": True, "GB": True}, run_mesa=True)
    ```
    """

    grid_config["metallicity_directory"] = os.path.join(
        grid_config["grids_root_directory"], "Z" + str(grid_config["metallicity"])
    )

    # add table columns to config
    grid_config = {**grid_config, **table_columns}

    if stellar_type_handle_dict is None:
        stellar_type_handle_dict = {}

    #############################################
    # Run pre-grid hook
    if pre_grid_hook is not None:
        pre_grid_hook(config=grid_config)

    #############################################
    # MS:
    if stellar_type_handle_dict.get("MS", False):
        ms_config = {
            **grid_config,
        }

        print("Main Sequence")

        ms_config["grid_directory"] = os.path.join(
            grid_config["metallicity_directory"], "MS"
        )

        # Handle MESA
        if run_mesa:

            ms_config["MESA_run_max_hours"]

            # Set up object
            MS_MESA_grid_builder = MainSequenceGridRunner(settings=ms_config)

            # Run mesa
            MS_MESA_grid_builder.write_and_run_MESA_grid()

        # Build interpolation table
        if build_interpolation_tables:

            if ms_config["workload_manager"] == "slurm":
                slurm_path = os.path.join(
                    ms_config["grid_directory"], "slurm_tabulate.sub"
                )
                os.system("sbatch {}".format(slurm_path))
            else:
                # Set up object
                MS_interpolation_grid_builder = MainSequenceTableBuilder(
                    settings=ms_config
                )

                # Build interpolation table
                MS_interpolation_grid_builder.build_interpolation_grid()

    ###############################################
    # TAMS Mass Change
    # if stellar_type_handle_dict.get("MC", False):
    #     tams_config = {
    #         **grid_config,
    #     }

    #     print("TAMS Mass Change")

    #     tams_config["grid_directory"] = os.path.join(
    #         grid_config["metallicity_directory"], "MC"
    #     )

    #     # Handle MESA
    #     if run_mesa:

    #         print("Running MESA")

    #         # Set up object
    #         TAMS_MESA_grid_builder = TAMSMassChangeGridRunner(settings=tams_config)

    #         # Run mesa
    #         TAMS_MESA_grid_builder.write_and_run_MESA_grid()

    #############################################
    # GB:
    if stellar_type_handle_dict.get("GB", False):
        rgb_config = {
            **grid_config,
        }

        rgb_config["grid_directory"] = os.path.join(
            grid_config["metallicity_directory"], "GB"
        )

        # Handle MESA
        if run_mesa:
            # Set up object
            GB_MESA_grid_builder = RedGiantBranchGridRunner(settings=rgb_config)

            # Run mesa
            GB_MESA_grid_builder.write_and_run_MESA_grid()

        # Build interpolation tablen
        if build_interpolation_tables:

            if rgb_config["workload_manager"] == "slurm":
                slurm_path = os.path.join(
                    rgb_config["grid_directory"], "slurm_tabulate.sub"
                )
                os.system("sbatch {}".format(slurm_path))
            else:
                # Set up object
                GB_interpolation_grid_builder = RedGiantBranchTableBuilder(
                    settings=rgb_config
                )
                # Build interpolation table
                GB_interpolation_grid_builder.build_interpolation_grid()

    #############################################
    # CHeB:
    if stellar_type_handle_dict.get("CHeB", False):
        cheb_config = {
            **grid_config,
        }

        cheb_config["grid_directory"] = os.path.join(
            grid_config["metallicity_directory"], "CHeB"
        )

        # Handle MESA
        if run_mesa:
            # Set up object
            CHeB_MESA_grid_builder = CoreHeliumBurningGridRunner(settings=cheb_config)

            # Run mesa
            CHeB_MESA_grid_builder.write_and_run_MESA_grid()

        # Build interpolation tablen
        if build_interpolation_tables:

            if rgb_config["workload_manager"] == "slurm":
                slurm_path = os.path.join(
                    rgb_config["grid_directory"], "slurm_tabulate.sub"
                )
                os.system("sbatch {}".format(slurm_path))
            else:
                # Set up object
                CHeB_interpolation_grid_builder = CoreHeliumBurningTableBuilder(
                    settings=cheb_config
                )
                # Build interpolation table
                CHeB_interpolation_grid_builder.build_interpolation_grid()

    #############################################
    # EAGB:
    if stellar_type_handle_dict.get("EAGB", False):
        eagb_config = {
            **grid_config,
        }

        eagb_config["grid_directory"] = os.path.join(
            grid_config["metallicity_directory"], "EAGB"
        )

        # Handle MESA
        if run_mesa:
            # Set up object
            EAGB_MESA_grid_builder = EarlyAsymptoticGiantBranchGridRunner(
                settings=eagb_config
            )

            # Run mesa
            EAGB_MESA_grid_builder.write_and_run_MESA_grid()

        # Build interpolation tablen
        if build_interpolation_tables:
            # Set up object
            EAGB_interpolation_grid_builder = EarlyAsymptoticGiantBranchTableBuilder(
                settings=eagb_config
            )
            # Build interpolation table
            EAGB_interpolation_grid_builder.build_interpolation_grid()

    #############################################
    # TPAGB:
    if stellar_type_handle_dict.get("TPAGB", False):
        tpagb_config = {
            **grid_config,
        }

        tpagb_config["grid_directory"] = os.path.join(
            grid_config["metallicity_directory"], "TPAGB"
        )

        # Handle MESA
        if run_mesa:
            # Set up object
            TPAGB_MESA_grid_builder = ThermallyPulsingAsymptoticGiantBranchGridRunner(
                settings=tpagb_config
            )

            # Run mesa
            TPAGB_MESA_grid_builder.write_and_run_MESA_grid()

        # Build interpolation tablen
        if build_interpolation_tables:
            # Set up object
            TPAGB_interpolation_grid_builder = ThermallyPulsingAsymptoticGiantBranchTableBuilder(
                settings=tpagb_config
            )
            # Build interpolation table
            TPAGB_interpolation_grid_builder.build_interpolation_grid()

    #############################################
    # CO WD:
    # if stellar_type_handle_dict.get("CO_WD", False):
    #     co_wd_config = {
    #         **grid_config,
    #     }

    #     co_wd_config["grid_directory"] = os.path.join(
    #         grid_config["metallicity_directory"], "CO_WD"
    #     )

    #     # Handle MESA
    #     if run_mesa:
    #         # Set up object
    #         CO_WD_MESA_grid_builder = CarbonOxygenWhiteDwarfMESAGridRunner(settings=co_wd_config)

    #         # Run mesa
    #         CO_WD_MESA_grid_builder.write_and_run_MESA_grid()

    #############################################
    # Run pre-grid hook
    if post_grid_hook is not None:
        post_grid_hook(config=grid_config)
