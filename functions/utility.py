def format_interpolation_table_output_filename(stellar_phase, metallicity):
    """
    Function to format the output file
    """

    return "MINT_{}_{}.dat".format(stellar_phase, metallicity)


def format_settings_filename(stellar_phase, metallicity):
    """
    Function to format the settings filename
    """

    return "MINT_{}_{}_settings.json".format(stellar_phase, metallicity)


def format_mesa_output_filename():
    """
    Function to format
    """
