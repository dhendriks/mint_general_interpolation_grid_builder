CHeB MINT_GRID
==============

We can configure binary_c to output information about certain events. See Notebook ....

In the following subsections we describe the content of each of these event output logs.




Input scalars CHeB table
------------------------
Below we list the input scalar parameters for the CHeB MINT grid.

.. list-table:: Input scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - MASS
     - Total stellar mass. Unit: [$\mathrm{M_{\odot}}$].
   * - CENTRAL_HELIUM
     - Central helium mass fraction XHe.
   * - HELIUM_CORE_MASS
     - Fractional He core mass.

Output scalars CHeB table
-------------------------
Below we list the output scalar parameters for the CHeB MINT grid.

.. list-table:: Output scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - AGE
     - Model age. Unit: [$\mathrm{yr}$].
   * - CARBON_CORE_MASS
     - Co core mass (Msun).
   * - CONVECTIVE_CORE_MASS_1
     - Relative mass coord of outer boundary of main convective region in core (Mcore,conv,1/Mstar).
   * - CONVECTIVE_CORE_MASS_2
     - Relative mass coord of inner boundary of secondary convective region in core.
   * - CONVECTIVE_CORE_MASS_3
     - Relative mass coord of outer boundary of secondary convective region in core.
   * - CONVECTIVE_CORE_MASS_OVERSHOOT
     - Relative mass cord of main conevctive region in core including overshooting & semiconvection mixing.
   * - CONVECTIVE_CORE_RADIUS_1
     - Relative radius coord of outer boundary of main convective region in core (Rcore,conv,1/Rstar).
   * - CONVECTIVE_CORE_RADIUS_2
     - Relative radius coord of inner boundary of secondary convective region in core.
   * - CONVECTIVE_CORE_RADIUS_3
     - Relative radius coord of outer boundary of secondary convective region in core.
   * - CONVECTIVE_CORE_RADIUS_OVERSHOOT
     - Relative radius cord of main conevctive region in core including overshooting & semiconvection mixing.
   * - CONVECTIVE_ENVELOPE_MASS_BOT
     - Relative mass coord of bottom of convective envelope  (Menv,bot/Mstar).
   * - CONVECTIVE_ENVELOPE_MASS_TOP
     - Relative mass coord of top of convective envelope  (Menv,top/Mstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_BOT
     - Relative radius coord of bottom of convective envelope (Renv,bot/Rstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_TOP
     - Relative radius coord of top of convective envelope (Renv,top/Rstar).
   * - FIRST_DERIVATIVE_CENTRAL_CARBON
     - DXCcore/dt. Unit: [$\mathrm{yr^{-1}}$].
   * - FIRST_DERIVATIVE_CENTRAL_HELIUM
     - DYcore/dt. Unit: [$\mathrm{yr^{-1}}$].
   * - FIRST_DERIVATIVE_CENTRAL_OXYGEN
     - DXOcore/dt. Unit: [$\mathrm{yr^{-1}}$].
   * - FIRST_DERIVATIVE_HELIUM_CORE_MASS
     - DMc/dt (Msun/yr). Unit: [$\mathrm{M_{\odot}\,yr^{-1}}$].
   * - HELIUM_CORE_RADIUS
     - Relative core radius (Rcore/Rstar).
   * - K2
     - Apsidal constant NOT THE moment of inertia factor BEWARE.
   * - LUMINOSITY
     - Photosphere luminosity. Unit: [$\mathrm{L_{\odot}}$].
   * - MEAN_MOLECULAR_WEIGHT_AVERAGE
     - Mean molecular weight average through star.
   * - MEAN_MOLECULAR_WEIGHT_CORE
     - Mean molecular weight at central mesh point.
   * - MOMENT_OF_INERTIA_FACTOR
     - Beta^2 from Claret AA 541, A113 (2012)= I/(MR^2).
   * - RADIUS
     - Photospheric radius. Unit: [$\mathrm{R_{\odot}}$].
   * - SECOND_DERIVATIVE_CENTRAL_CARBON
     - D2XCcore/dt2. Unit: [$\mathrm{yr^{-2}}$].
   * - SECOND_DERIVATIVE_CENTRAL_HELIUM
     - D2Ycore/dt2. Unit: [$\mathrm{yr^{-2}}$].
   * - SECOND_DERIVATIVE_CENTRAL_OXYGEN
     - D2XOcore/dt2. Unit: [$\mathrm{yr^{-2}}$].
   * - TIDAL_E2
     - Tidal E2 from Zahn.
   * - TIDAL_E_FOR_LAMBDA
     - Tidal E for Zahns lambda.
   * - TIMESCALE_DYNAMICAL
     - Dynamical timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_KELVIN_HELMHOLTZ
     - Kelvin-Helmholtz timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_NUCLEAR
     - Nuclear timescale. Unit: [$\mathrm{yr}$].

Output vectors CHeB table
-------------------------
Below we list the output vector parameters for the CHeB MINT grid. These structural quantities are determined on a chebychev mass grid.

.. list-table:: Output vectors
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - CHEBYSHEV_DENSITY
     - Density on Chebyshev grid. Unit: [$\mathrm{g\,cm^{-3}}$].
   * - CHEBYSHEV_GAMMA1
     - Adiabatic Gamma1 on Chebyshev grid.
   * - CHEBYSHEV_GAS_PRESSURE
     - Gas pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_PRESSURE_SCALE_HEIGHT
     - Pressure scale height on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_RADIUS
     - Radius on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_TEMPERATURE
     - Temperature on Chebyshev grid. Unit: [$\mathrm{K}$].
   * - CHEBYSHEV_TOTAL_PRESSURE
     - Total pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
