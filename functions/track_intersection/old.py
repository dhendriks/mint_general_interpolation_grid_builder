def load_mesa_history_data(filename):
    """
    Function to load history data
    """

    # load history file
    m = mp.MESA()
    m.loadHistory(filename_in=filename)
    m.hist.he_core_mass_fraction = m.hist.he_core_mass / m.hist.star_mass

    return m.hist


def load_history_data(files_list):
    """ """

    loaded_dicts = []

    for file_dict in files_list:
        hist = load_mesa_history_data(filename=file_dict["filename"])

        file_dict["hist"] = hist

        loaded_dicts.append(file_dict)

    return loaded_dicts


def check_for_intersection(all_MESA_track_data, config):
    """
    Function to check for intersecting tracks
    """

    #
    interpolation_parameters = config["interpolation_parameters"]

    # loop over results of query
    # TODO: create method to query the data if there are enough interpolation variables

    # TODO: Loop over the queried data and check intersection

    if len(interpolation_parameters) == 2:
        # TODO: compare the first 2 for intersection
        pass
    if len(interpolation_parameters) > 2:
        # TODO: find unique values for the first parameter
        # TODO: query for each unique value of first parameter
        # TODO: set up intersection checking with the rest of the parameters
        # TODO: generate permutations that do not contain reversed duplicates (i.e. (0,1) and (1,0) are the same)
        # TODO:

        #
        for i, j in combinations_with_replacement(range(len(all_MESA_track_data)), 2):
            if i == j:
                continue
                check_for_self_intersection(all_MESA_track_data[i], config)

        # check for other intersection
        check_for_other_intersection(
            all_MESA_track_data[i],
            all_MESA_track_data[j],
            trajectory_parameters=interpolation_parameters[1:],
            config=config,
        )


# def track_intersection_main(mesa_result_directory, config):
#     """
#     Main function to find intersecting tracks in the MESA model results

#     This function will get a list of diciotniaries with at least filenames to history files

#     NOTE: OLD. See plot_functions
#     """

#     # load all the MESA results
#     all_MESA_track_data = readout_all_MESA_tracks(
#         mesa_result_directory=mesa_result_directory, config=config
#     )

#     # find self-intersecting tracks
#     check_for_self_intersection(all_MESA_track_data=all_MESA_track_data, config=config)

#     # find intersecting tracks
#     check_for_intersection(all_MESA_track_data=all_MESA_track_data, config=config)

#     # TODO: optional: generate new interpolation variables to have non-intersection


def readout_single_MESA_track(mesa_track_info, config):
    """
    Function to readout the data of a single MESA track
    """

    # TODO: extract MESA track info
    MESA_track_data = {}

    return MESA_track_data


def readout_all_MESA_tracks(mesa_result_directory, config):
    """
    Function to readout and return all the MESA tracks
    """

    all_MESA_track_data = {}
    for mesa_track_info in mesa_result_directory:
        MESA_track_data = readout_single_MESA_track(
            mesa_track_info=mesa_track_info, config=config
        )

    return all_MESA_track_data


def check_for_self_intersection(all_MESA_track_data, config):
    """
    Function to check for self-intersecting tracks
    """

    # TODO: check self-intersection
    for MESA_track_data in all_MESA_track_data:
        pass
