"""
Functions regarding the MESA track intersection

Each model is represented as a track

Each of these tracks are inspected for self-intersection and for intersection with another.

TODO: currently, the sweep intersector code returns the endpoints of the segments of the tracks that are intersecting. that might be a bit confusing.
"""

from itertools import combinations_with_replacement

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

from mint_general_interpolation_grid_builder.functions.track_intersection.intersection_algorithms.sweep_intersector.SweepIntersectorLib.SweepIntersector import (
    SweepIntersector,
)

from itertools import combinations_with_replacement


def plot_tracks(
    extracted_track_data,
    unique_self_intersection_locations,
    unique_non_self_intersecting_locations,
    plot_settings,
):
    """
    Function to plot tracks
    """

    ####
    #
    fig = plt.figure(figsize=(20, 20))

    #
    gs = fig.add_gridspec(nrows=1, ncols=1)

    # create axes
    ax = fig.add_subplot(gs[:, :])

    ###########
    # Plot the tracks
    for track_data in extracted_track_data:
        ax.plot(track_data["x"], track_data["y"])

    ###########
    # Plot the self-intersection locations
    for self_intersection_locations in unique_self_intersection_locations:
        ax.plot(self_intersection_locations[0], self_intersection_locations[1], "bo")

    ###########
    # Plot the other-intersection locations
    for non_self_intersection_locations in unique_non_self_intersecting_locations:
        ax.plot(
            non_self_intersection_locations[0],
            non_self_intersection_locations[1],
            "ro",
        )

    ##########
    # Make up
    ax.legend()

    #
    ax.set_xlabel(x_param)
    ax.set_ylabel(y_param)

    # Handle saving
    show_and_save_plot(fig, plot_settings)


def sweepintersector(start_x_list, start_y_list, end_x_list, end_y_list):
    """
    Call to sweep intersector
    """

    segList = []

    # Add start and end points to list
    for (start_x, start_y, end_x, end_y) in zip(
        start_x_list, start_y_list, end_x_list, end_y_list
    ):
        segList.append(((start_x, start_y), (end_x, end_y)))

    # Set up the SweepIntersector
    isector = SweepIntersector()

    # Find all the intersections
    isectDic = isector.findIntersections(segList)

    # TODO: instead of storing the endpoints, we should actually calculate the intersection locations.
    # Extract the intersections
    num_intersections = len(isectDic.items())
    intersection_locations = []
    for seg, isects in isectDic.items():
        intersection_locations += isects

    # Check the unique intersection location
    unique_intersection_locations = []
    for intersection_location in intersection_locations:
        if not intersection_location in unique_intersection_locations:
            unique_intersection_locations.append(intersection_location)

    num_unique_intersections = len(unique_intersection_locations)

    return num_unique_intersections, unique_intersection_locations


def check_intersection_with_sweepintersector(track_list):
    """
    Function to track intersection of a list of tracks
    """

    start_x_list = []
    start_y_list = []

    end_x_list = []
    end_y_list = []

    # get start and endpoints
    for track in track_list:
        start_x_list += list(track["x"][:-1])
        start_y_list += list(track["y"][:-1])

        end_x_list += list(track["x"][1:])
        end_y_list += list(track["y"][1:])

    # call to sweep intersector
    num_unique_intersections, unique_intersection_locations = sweepintersector(
        start_x_list=start_x_list,
        start_y_list=start_y_list,
        end_x_list=end_x_list,
        end_y_list=end_y_list,
    )

    return num_unique_intersections, unique_intersection_locations


def check_track_intersection(tracks, scalar_input_columns, config):
    """
    Function that actually handles the track intersection

    Will check for self intersection, intersection with others

    Currently returns the locations of self-intersection and other-intersection
    """

    # Extract the dataframes
    track_dataframes = [track["df"] for track in tracks]

    # Select the correct parameters to perform the track intersection on
    x_param = scalar_input_columns[-2]
    y_param = scalar_input_columns[-1]

    # Loop over the tracks and extract the relevant colummn data
    extracted_track_data = []
    for track in tracks:
        # TODO: implement checks and filters

        #
        x_vals = tracks["df"][x_param].to_numpy()
        y_vals = tracks["df"][y_param].to_numpy()

        # add track data to track_list
        extracted_track_data.append({"x": x_vals, "y": y_vals})

    ################
    # check for self intersection
    unique_self_intersection_locations = []
    for track_data in extracted_track_data:
        # Get intersection data
        (
            num_self_intersections,
            self_intersection_locations,
        ) = check_intersection_with_sweepintersector(track_list=[track_data])

        # Create unique list
        for intersection_location in self_intersection_locations:
            if not intersection_location in unique_self_intersection_locations:
                unique_self_intersection_locations.append(intersection_location)

    ################
    # Check for intersection
    unique_intersection_locations = []

    # Loop over combinations (removing reversed duplicates and self)
    for i, j in combinations_with_replacement(range(len(extracted_track_data)), 2):
        # Skip first
        if i == j:
            continue

        # get intersection data
        (
            num_intersections,
            intersection_locations,
        ) = check_intersection_with_sweepintersector(
            track_list=[extracted_track_data[i], extracted_track_data[j]]
        )

        # Create unique list
        for intersection_location in intersection_locations:
            if not intersection_location in unique_intersection_locations:
                unique_intersection_locations.append(intersection_location)

    # Get intersections that are not self-intersecting
    unique_non_self_intersecting_locations = list(
        set(unique_intersection_locations) - set(unique_self_intersection_locations)
    )

    # Handle plotting
    if config["handle_track_intersection_plotting"]:
        plot_tracks(
            extracted_track_data=extracted_track_data,
            unique_self_intersection_locations=unique_self_intersection_locations,
            unique_non_self_intersecting_locations=unique_non_self_intersecting_locations,
            plot_settings=plot_settings,
        )

    #
    return unique_self_intersection_locations, unique_non_self_intersecting_locations


def track_intersection_main(tracks, scalar_input_columns, config):
    """
    Main track intersection function that handles some of the logic
    """

    #####
    # Select the correct situation
    if scalar_input_columns == 2:
        # This probably means that we are in the MS stellar type
        #   Here we only have to pass the tracks to the intersection checker

        # Check for intersections
        check_track_intersection(
            tracks=tracks, scalar_input_columns=scalar_input_columns, config=config
        )

    elif scalar_input_columns == 3:
        # With 3 input columns we need to query and loop over the unique masses

        #####
        # Add mass to the tracks to filter more easily.
        unique_masses = []
        for track in tracks:
            track["mass"] = track["df"].iloc[0]["MASS"]

            if not track["mass"] in unique_masses:
                unique_masses.append(track["mass"])

        #####
        # Loop over unique masses.
        for unique_mass in unique_masses:
            current_mass_tracks = []

            # Get tracks that have the correct current mass
            for track in tracks:
                if track["mass"] == unique_mass:
                    current_mass_tracks.append(track)

            # Pass these to the track intersection code
            check_track_intersection(
                tracks=current_mass_tracks,
                scalar_input_columns=scalar_input_columns,
                config=config,
            )

    else:
        raise ValueError(
            "Number is scalar input columns ({}) not supported by the track intersection algorithm".format(
                scalar_input_columns
            )
        )
