import mesaPlot as mp

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors

#
from david_phd_functions.plotting.utils import show_and_save_plot
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc

load_mpl_rc()
from mint_general_interpolation_grid_builder.functions.track_intersection.intersection_algorithms.sweep_intersector.SweepIntersectorLib.SweepIntersector import (
    SweepIntersector,
)

from itertools import combinations_with_replacement


if __name__ == "__main__":
    files_list = [
        {
            "filename": "intersection_testdata/history_files_natalie/history_1.data",
            "name": "1",
        },
        {
            "filename": "intersection_testdata/history_files_natalie/history_2.data",
            "name": "2",
        },
        {
            "filename": "intersection_testdata/history_files_natalie/history_3.data",
            "name": "3",
        },
    ]

    # # Plot tracks
    plot_tracks(
        files_list=files_list,
        plot_settings={"show_plot": True},
        handle_intersecting_data=True,
    )

    # # Check for intersection
    # check_intersection_main(files_list=files_list)

    # #
    # for el in files_list:
    #     # print(el['filename'])
    #     hist = load_mesa_history_data(filename=el["filename"])
    #     # print(hist)
    #     # print(dir(hist))
