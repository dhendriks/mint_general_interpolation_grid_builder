import copy

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

################################################################################
def fff(x, function):
    """
    This function is a workaround for the root call in find_interface_precise
    It needs to be called as an outside function
    Input : x   is the point where to evaluate function
            gradr_minus_grada array  is the interpolated gradient difference
    """
    return function(x)


################################################################################

filename = "grid_output_rg.dat"

header_lines = []
data_lines = []

finished_headers = 0

# read in data
with open(filename, "r") as f:
    for line in f:
        if not finished_headers:
            header_lines.append(line.strip())
        else:
            data_lines.append(line)

        if line.startswith("# END HEADER"):
            finished_headers = 1

# Find indices of column descriptors:
start_index_column_descriptors = header_lines.index("# START MINT COLUMN DESCRIPTORS")
end_index_column_descriptors = header_lines.index("# END MINT COLUMN DESCRIPTORS")

# Get the column name information
column_names = copy.copy(
    header_lines[start_index_column_descriptors + 2 : end_index_column_descriptors - 1]
)

# put non-chebyshev column names and indices in a dict
column_names_dict = {}
column_names_list = []
max_value_non_chebychev = 0
for column_name in column_names:
    if not column_name.startswith("# CHEBYSHEV"):
        split = column_name[2:].split()
        name = split[0]
        value = int(split[1])
        column_names_dict[name] = value
        column_names_list.append(name)

        if max_value_non_chebychev < value:
            max_value_non_chebychev = value
            value = max_value_non_chebychev

data_non_chebychev = []
data_chebychev = []

# Properly read out the data now:
for data_line in data_lines:
    split = data_line.split()
    data_non_chebychev.append([float(el) for el in split[:max_value_non_chebychev]])
    data_chebychev.append(split[max_value_non_chebychev:])


# Load the dataframe
df = pd.DataFrame(data_non_chebychev, columns=column_names_list)

# Put data back together:
combined_data_lines = []

non_chebychev_array = df.to_numpy()

for (non_chebychev_line, chebychev_line) in zip(non_chebychev_array, data_chebychev):
    formatted_non_cheb_line = " ".join(
        ["{:+.15E}".format(el) for el in non_chebychev_line]
    )
    formatted_cheb_line = " ".join(chebychev_line)

    combined_line = formatted_non_cheb_line + " " + formatted_cheb_line
    combined_data_lines.append(combined_line)

# Open and write
with open("output.dat", "w") as f:
    for line in header_lines:
        f.write(line.strip() + "\n")

    for line in combined_data_lines:
        f.write(line.strip() + "\n")

# for (original_line, new_line) in zip(data_lines, combined_data_lines):
#    if not (original_line.strip()==new_line.strip()):
#        print(original_line, new_line)
