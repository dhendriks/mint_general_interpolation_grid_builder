"""
Main script
"""

#
from mint_general_interpolation_grid_builder.functions.plotting_routines.general_plot_routines.general_plot_routine_ms import (
    general_plot_routine as general_plot_routine_ms,
)

# from functions.general_plot_routines.general_plot_routine_HG import general_plot_routine as general_plot_routine_HG
# from functions.general_plot_routines.general_plot_routine_RG import general_plot_routine as general_plot_routine_RG
# from functions.general_plot_routines.general_plot_routine_CHeB import general_plot_routine as general_plot_routine_CHeB

#
example_grid_MS = "example_data_natalie/MS/grid_Z2.00e-02.dat"
example_grid_CHeB = "example_data_natalie/MS/grid_Z2.00e-02.dat"
# example_grid_HG = '/home/gio/Work/test_hgrg_end/grid_output_hg_allmasses2.dat'
# example_grid_RG = '/home/gio/Work/test_hgrg_end/grid_output_rg_test_whole.dat'
# example_grid_CHeB = 'example_data/example_CHeB_grid/grid_Z0p02_degen_v4.dat'
# example_grid_CHeB = 'example_data/example_CHeB_grid/grid_Z0p02_non_degen_v4.dat'

#
general_plot_routine_ms(
    example_grid_MS, "output/MS_pdfs", "output/MS_combined_output.pdf"
)
# general_plot_routine_HG(example_grid_HG, 'output/HG_pdfs', 'output/HG_combined_output.pdf')
# general_plot_routine_RG(example_grid_RG, 'output/RG_pdfs', 'output/RG_combined_output.pdf')
# general_plot_routine_CHeB(example_grid_CHeB, 'output/CHeB_pdfs', 'output/CHeB_combined_output.pdf')
