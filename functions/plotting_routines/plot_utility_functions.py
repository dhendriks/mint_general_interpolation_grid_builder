"""
utility functions for handling pdf writing
"""

import os
import copy
import time

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

from PyPDF2 import PdfMerger, PdfReader


def align_axes(fig, axes_list, which_axis="x"):
    """
    Function to align the x or y axis of a list of axes
    """

    if which_axis == "x":
        getter = "get_xlim"
        setter = "set_xlim"
    elif which_axis == "y":
        getter = "get_ylim"
        setter = "set_ylim"
    else:
        raise ValueError("not implemented yet")

    min_val = 1e9
    max_val = 1e-9

    # Find the min and max
    for axis in axes_list:
        lims = axis.__getattribute__(getter)()

        min_val = np.min([min_val, lims[0]])
        max_val = np.max([max_val, lims[1]])

    # Set the min and max
    for axis in axes_list:
        axis.__getattribute__(setter)([min_val, max_val])


def get_length_pdf(filename):
    """
    Function to get the pagecount of the pdf
    """

    if os.path.isfile(filename):
        with open(filename, "rb") as pdf_file:
            pdf_reader = PdfReader(pdf_file)
            length_pdf = len(pdf_reader.pages)
    else:
        length_pdf = 0
    return length_pdf


def check_and_update_parent_list(parent_list, shift, current_level, bookmark):
    """
    Function to check the length of the parent list and update accordingly
    """

    # Make current bookmark available as possible parent
    if len(parent_list) + shift < current_level:
        parent_list.append(copy.deepcopy(bookmark))

        if len(parent_list) + shift < current_level:
            raise ValueError(
                "issue with parent list length: current_level: {} len(parent_list): {}".format(
                    current_level, len(parent_list)
                )
            )

    # If we jump back to a higher level, we need to remove the previous parent elements
    elif len(parent_list) + shift > current_level:
        parent_list = parent_list[:current_level]
        parent_list.append(copy.deepcopy(bookmark))

    return parent_list


def get_bookmarks(filepath: str):
    # WARNING! One page can have multiple bookmarks!
    bookmarks = []
    with fitz.open(filepath) as doc:
        # toc = doc.getToC()  # [[lvl, title, page, …], …]
        toc = doc.get_toc()
        for level, title, page in toc:
            bookmarks.append({"title": title, "level": level, "page": page})
    return bookmarks


def add_pdf_and_bookmark(
    merger, pdf_filename, page_number, bookmark_text=None, add_source_bookmarks=False
):
    """
    Function to wrap bookmark stuff
    """

    # Add into merger
    merger.append(pdf_filename)

    # Add bookmark
    if bookmark_text and isinstance(bookmark_text, str):
        parent = merger.add_outline_item(title=bookmark_text, pagenum=page_number)

        # Construct list of parent bookmarks
        parent_list = [parent]

        if add_source_bookmarks:
            #  get the bookmarks that the source has:
            source_bookmarks = get_bookmarks(pdf_filename)

            if source_bookmarks:

                # Go over the bookmarks
                for bookmark_dict in source_bookmarks:
                    current_level = bookmark_dict["level"]

                    # In the case that we are adding source bookmarks, we need
                    bookmark = merger.addBookmark(
                        bookmark_dict["title"],
                        page_number + bookmark_dict["page"] - 1,
                        parent=parent_list[current_level - 1],
                    )  # add child bookmark

                    parent_list = check_and_update_parent_list(
                        parent_list, -1, current_level, bookmark
                    )

    else:
        # Construct list of parent bookmarks
        parent_list = []

        if add_source_bookmarks:
            #  get the bookmarks that the source has:
            source_bookmarks = get_bookmarks(pdf_filename)

            if source_bookmarks:
                # Go over the bookmarks
                for bookmark_dict in source_bookmarks:
                    current_level = bookmark_dict["level"]

                    # In the case that we are adding source bookmarks, we need
                    if current_level == 1:
                        bookmark = merger.addBookmark(
                            bookmark_dict["title"],
                            page_number + bookmark_dict["page"] - 1,
                        )  # add child bookmark
                    else:
                        bookmark = merger.addBookmark(
                            bookmark_dict["title"],
                            page_number + bookmark_dict["page"] - 1,
                            parent=parent_list[current_level - 1],
                        )  # add child bookmark

                    parent_list = check_and_update_parent_list(
                        parent_list, 0, current_level, bookmark
                    )

    # Inspect the pagelength of the pdf
    length_pdf = get_length_pdf(pdf_filename)
    page_number += length_pdf

    return merger, page_number


def add_plot_info(fig, plot_settings):
    """
    Function to add the info at the top of the plot
    """

    ###############
    # Standard plotting adjust
    fig.subplots_adjust(
        top=plot_settings.get("top", 0.85), hspace=plot_settings.get("hspace", 1),
    )

    shift = plot_settings.get("shift", -0.05)
    fontsize = plot_settings.get("fontsize", 14)

    # Add info to the plot
    plt.figtext(
        0.5,
        0.925 - shift,
        "Simulation name: {}".format(plot_settings.get("simulation_name", "")),
        fontsize=fontsize,
        ha="center",
    )
    plt.figtext(
        0.5,
        0.9 - shift,
        "Generated on: {}".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())),
        fontsize=fontsize,
        ha="center",
    )
    plt.figtext(
        0.5,
        0.875 - shift,
        "Runname: {}".format(plot_settings.get("runname", "")),
        fontsize=fontsize,
        ha="center",
    )

    return fig


def show_and_save_plot(plot_settings):
    """
    Wrapper to handle the saving and the showing of the plots
    """

    if plot_settings.get("output_name", None):
        if os.path.dirname(plot_settings["output_name"]):
            os.makedirs(os.path.dirname(plot_settings["output_name"]), exist_ok=True)
        plt.savefig(plot_settings["output_name"])
    if plot_settings.get("show_plot", False):
        plt.show(block=plot_settings.get("block", True))
    else:
        plt.close()


def load_mpl_rc():
    # https://matplotlib.org/users/customizing.html
    mpl.rc(
        "axes",
        labelweight="normal",
        linewidth=2,
        labelsize=30,
        grid=True,
        titlesize=40,
        facecolor="white",
    )

    mpl.rc("savefig", dpi=100)

    mpl.rc("lines", linewidth=4, color="g", markeredgewidth=2)

    mpl.rc(
        "ytick",
        **{
            "labelsize": 30,
            "color": "k",
            "left": True,
            "right": True,
            "major.size": 12,
            "major.width": 2,
            "minor.size": 6,
            "minor.width": 2,
            "major.pad": 12,
            "minor.visible": True,
            "direction": "inout",
            "left": True,
            "right": True,
        }
    )

    mpl.rc(
        "xtick",
        **{
            "labelsize": 30,
            "top": True,
            "bottom": True,
            "major.size": 12,
            "major.width": 2,
            "minor.size": 6,
            "minor.width": 2,
            "major.pad": 12,
            "minor.visible": True,
            "direction": "inout",
        }
    )

    mpl.rc("legend", frameon=False, fontsize=30, title_fontsize=30)

    mpl.rc("contour", negative_linestyle="solid")

    mpl.rc(
        "figure",
        figsize=[16, 16],
        titlesize=30,
        dpi=100,
        facecolor="white",
        edgecolor="white",
        frameon=True,
        max_open_warning=10,
        # autolayout=True
    )

    mpl.rc(
        "legend",
        fontsize=20,
        handlelength=2,
        loc="best",
        fancybox=False,
        numpoints=2,
        framealpha=None,
        scatterpoints=3,
        edgecolor="inherit",
    )

    mpl.rc("savefig", dpi="figure", facecolor="white", edgecolor="white")

    mpl.rc("grid", color="b0b0b0", alpha=0.5)

    mpl.rc("image", cmap="viridis")

    mpl.rc("font", weight="bold", serif="Palatino", size=20)

    mpl.rc("errorbar", capsize=2)

    mpl.rc("mathtext", default="sf")


#
LINESTYLE_TUPLE = [
    ("solid", "solid"),  # Same as (0, ()) or '-'
    ("dotted", "dotted"),  # Same as (0, (1, 1)) or '.'
    ("dashed", "dashed"),  # Same as '--'
    ("dashdot", "dashdot"),  # Same as '-.'
    # ('loosely dotted',        (0, (1, 10))),
    # ('dotted',                (0, (1, 1))),
    ("densely dotted", (0, (1, 1))),
    ("loosely dashed", (0, (5, 10))),
    # ('dashed',                (0, (5, 5))),
    ("densely dashed", (0, (5, 1))),
    ("loosely dashdotted", (0, (3, 10, 1, 10))),
    # ('dashdotted',            (0, (3, 5, 1, 5))),
    ("densely dashdotted", (0, (3, 1, 1, 1))),
    ("dashdotdotted", (0, (3, 5, 1, 5, 1, 5))),
    ("loosely dashdotdotted", (0, (3, 10, 1, 10, 1, 10))),
    ("densely dashdotdotted", (0, (3, 1, 1, 1, 1, 1))),
    ("solid", "solid"),  # Same as (0, ()) or '-'
    ("dotted", "dotted"),  # Same as (0, (1, 1)) or '.'
    ("dashed", "dashed"),  # Same as '--'
    ("dashdot", "dashdot"),  # Same as '-.'
]
linestyles = [el[1] for el in LINESTYLE_TUPLE]
