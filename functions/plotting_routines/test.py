"""
Testing some stuff for the plotting
"""

import numpy as np

from mint_general_interpolation_grid_builder.functions.mint_interpolation_table_reader import (
    mint_interpolation_table_reader,
)
from mint_general_interpolation_grid_builder.functions.plotting_routines.general_plot_routines.general_plot_routine_CHeB import (
    general_plot_routine as general_plot_routine_CHeB,
)


example_grid_CHeB = "example_data_natalie/CHeB/grid_Z2.00e-02_CHeB.dat"
dataset_filename = example_grid_CHeB


# #######################
# # Get the dataframes and some info about the columns
# (df, non_chebychev_column_names_dict,) = mint_interpolation_table_reader(
#     dataset_filename, remove_space=False, return_single_dataframe=True
# )

#
general_plot_routine_CHeB(
    example_grid_CHeB,
    "output/CHeB_pdfs",
    "output/CHeB_combined_output.pdf",
    show_plots=True,
)

quit()

# Determine the columns we need to plot
all_column_names = [col for col in df.columns if not col.startswith("CHEBY")]
xy_column_names = ["MASS", "CENTRAL_HYDROGEN"]
plot_column_names = sorted(list(set(all_column_names) - set(xy_column_names)))

# Make sure the dataframe is filtered and selected
df = df[all_column_names]
df = df.sort_values(by=xy_column_names)


x_arrays = df[xy_column_names[0]].to_numpy()
y_arrays = df[xy_column_names[1]].to_numpy()
z_arrays = df["AGE"].to_numpy()

centers = np.unique(sorted(x_arrays))
# print(np.unique(sorted(x_arrays)))

import matplotlib.pyplot as plt

fig, ax = plt.subplots()

##################
# Plot the included metallicities
ax.vlines(
    centers,
    color="orange",
    lw=1,
    linestyle="--",
    alpha=1,
    ymax=0.1,
    ymin=0,
    transform=ax.get_xaxis_transform(),
)


lim = ax.get_xlim()
ax.set_xticks(list(ax.get_xticks()))
ax.set_xlim(lim)


plt.xscale("log")
plt.show()
