# Plotting routines for MS interpolation table
# NOT CURRENTLY IN USE

import re

import matplotlib.pyplot as plt
import mesaPlot as mp
import numpy as np

from mint_general_interpolation_grid_builder.functions_natalie.custom_mpl_settings import (
    load_mpl_rc,
)
from mint_general_interpolation_grid_builder.functions.table_functions.mint_interpolation_table_reader import (
    mint_interpolation_table_reader,
)


def plot_L_div_Ledd(masses_dict, logs_dir, Z, save_dir):

    print("Plotting L/Ledd")

    load_mpl_rc()
    fig, ax = plt.subplots()
    m = mp.MESA()

    for Mt in masses_dict:
        Mt_text = re.sub("\.", "p", str(Mt)) + "M"

        direc = logs_dir + Mt_text + "/LOGS_MS/"

        # load history file
        try:
            m.loadHistory(f=direc)
        except:
            print("M = " + str(Mt) + " does not exist")

        ax.plot(m.hist.star_mass, m.hist.log_L_div_Ledd)

        print(Mt)

    ax.set(xlabel="$M_*$", ylabel="$\log(L/L_{\mathrm{edd}})$")
    ax.legend(title="$Z=$" + str(Z))
    plt.savefig(save_dir + "L_div_Ledd.pdf", dpi=200)

    return


def plot_he_core_mass_TAMS(table_dir, save_dir):
    # plot of he core mass at TAMS vs mass

    # read interpolation table into dataframe
    df, columns = mint_interpolation_table_reader(
        input_file=table_dir + ".dat", remove_space=False, return_single_dataframe=False
    )

    # list of masses
    y = np.sort(np.fromiter(df.keys(), dtype=float))

    he_core_mass = []
    for mass in y:
        df_mass_sec = df[str(mass)]
        he_core_mass += [df_mass_sec["HELIUM_CORE_MASS"].iloc[0]]
    fig, ax = plt.subplots()
    ax.plot(y, he_core_mass)
    ax.set_xlabel("Mass (Msun)")
    ax.set_ylabel("TAMS core mass (Msun)")
    plt.savefig(save_dir + "plots/he_core_mass_TAMS.pdf", dpi=200)

    return


def zams_HR(self, masses):
    # NEEDS FIXING
    m = mp.MESA()
    zams_HR = []
    for Mi in np.unique(masses):
        mass_text = re.sub("\.", "p", str(Mi)) + "M"
        metallicity_text = "Z" + re.sub("\.", "p", str(self.settings["metallicity"]))
        f = (
            self.settings["logs_directory"]
            + mass_text
            + "_"
            + metallicity_text
            + "/LOGS_to_CHeB"
        )
        # print(f)
        try:
            m.loadHistory(f)
        except:
            print("cant load data")
            continue
        i = np.where(m.hist.center_h1 < 0.699)[0][0]
        zams_HR += [[m.hist.log_Teff[i], m.hist.log_L[i]]]
    zams_HR = np.array(zams_HR)
    return zams_HR
