"""
General plot routine for the MS grid
"""

import os
import traceback
from PyPDF2 import PdfMerger

from mint_general_interpolation_grid_builder.functions.plotting_routines.functions import (
    plot_data_2d,
    create_plotting_data,
)
from mint_general_interpolation_grid_builder.functions.table_functions.mint_interpolation_table_reader import (
    mint_interpolation_table_reader,
)


from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)

from mint_general_interpolation_grid_builder.MINT.config.header_description_dict import (
    header_description_dict_scalars,
    header_description_dict_vectors,
)


def get_traceback(e):
    lines = traceback.format_exception(type(e), e, e.__traceback__)
    return "".join(lines)


def general_plot_routine(
    dataset_filename, output_pdfs_dir, combined_pdf_filename, show_plots=False
):
    """
    Function to plot all the columns (non-structural) of a MINT dataset.

    Input:
        dataset_filename: path to the MINT dataset
    """

    # Make output dir if necessary:
    os.makedirs(output_pdfs_dir, exist_ok=True)

    # # Get the dataframes and some info about the columns
    # (
    #     dataframe_dict,
    #     non_chebychev_column_names_dict,
    # ) = mint_interpolation_table_reader(dataset_filename, remove_space=False)

    #######################
    # Get the dataframes and some info about the columns
    (df, non_chebychev_column_names_dict,) = mint_interpolation_table_reader(
        dataset_filename, remove_space=False, return_single_dataframe=True
    )

    # Determine the columns we need to plot
    all_column_names = [col for col in df.columns if not col.startswith("CHEBY")]
    xy_column_names = ["MASS", "CENTRAL_HYDROGEN"]
    plot_column_names = sorted(list(set(all_column_names) - set(xy_column_names)))

    # Make sure the dataframe is filtered and selected
    df = df[all_column_names]
    df = df.sort_values(by=xy_column_names)

    # print(df)
    # quit()

    ##################
    # Set up the pdf stuff
    merger = PdfMerger()
    pdf_page_number = 0

    # Go over the columns
    for column_name in plot_column_names:
        print("Plotting {}".format(column_name))
        output_name = os.path.join(output_pdfs_dir, "{}.pdf".format(column_name))

        # Select data
        x_arrays = df[xy_column_names[0]].to_numpy()
        y_arrays = df[xy_column_names[1]].to_numpy()
        z_arrays = df[column_name].to_numpy()

        # Run the plot-2d but catch exception
        try:
            plot_data_2d(
                x_arrays,
                y_arrays,
                z_arrays,
                #
                x_parameter=xy_column_names[0],
                y_parameter=xy_column_names[1],
                z_parameter=column_name,
                parameter_dict=header_description_dict_scalars,
                # contourlevels,
                # linestyles_contourlevels,
                plot_settings={
                    "runname": column_name,
                    "show_plot": show_plots,
                    "output_name": output_name,
                },
            )
            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                output_name,
                pdf_page_number,
                bookmark_text="{}".format(column_name),
            )
        except Exception as e:
            print(
                "Plotting column {} failed with the following exception information:".format(
                    column_name
                )
            )
            print(get_traceback(e))

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\t wrote combined pdf to {}".format(combined_pdf_filename))
