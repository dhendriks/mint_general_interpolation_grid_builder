"""
General plot routine for the MS grid
"""

import os
import pandas as pd

# from PyPDF2 import PdfFileMerger

# from functions.functions import (
#     readout_datafile_and_return_dataframes,
#     plot_data_2d,
#     create_plotting_data,
#     plot_with_unstructured_data,
# )
from PyPDF2 import PdfMerger


from mint_general_interpolation_grid_builder.functions.plotting_routines.functions import (
    plot_data_2d,
    create_plotting_data,
    plot_data_scatter,
)
from mint_general_interpolation_grid_builder.functions.grid_building_class.header_description_dict import (
    header_description_dict_scalars,
    header_description_dict_vectors,
)


from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_pdf_and_bookmark,
)
from mint_general_interpolation_grid_builder.functions.mint_interpolation_table_reader import (
    mint_interpolation_table_reader,
)


def general_plot_routine(
    dataset_filename, output_pdfs_dir, combined_pdf_filename, show_plots=False
):
    """
    Function to plot all the columns (non-structural) of a MINT dataset.

    Input:
        dataset_filename: path to the MINT dataset
    """

    # Make output dir if necessary:
    os.makedirs(output_pdfs_dir, exist_ok=True)

    #######################
    # Get the dataframes and some info about the columns
    (df, non_chebychev_column_names_dict,) = mint_interpolation_table_reader(
        dataset_filename, remove_space=False, return_single_dataframe=True
    )

    #########################
    # Set up the pdf stuff
    merger = PdfMerger()
    pdf_page_number = 0

    #########################
    # Determine the columns we need to plot
    all_column_names = [col for col in df.columns if not col.startswith("CHEBY")]
    xy_column_names = ["CENTRAL_HELIUM", "HELIUM_CORE_MASS"]
    # xy_column_names = ["CENTRAL_HELIUM", "HELIUM_CORE_RADIUS"]
    slice_column = "MASS"
    plot_column_names = sorted(
        list(set(all_column_names) - set(xy_column_names) - set([slice_column]))
    )

    # Slice the values
    for slice_val in sorted(df[slice_column].unique()):
        query = "{} == {}".format(slice_column, slice_val)

        if slice_val < 5:
            continue
        print("{}={}".format(slice_column, slice_val))
        #
        slice_df = df.query(query)
        # loop over results
        for column_name in plot_column_names:
            print("Plotting {}".format(column_name))
            slice_output_pdfs_dif = os.path.join(
                output_pdfs_dir, "{}={}".format(slice_column, slice_val)
            )
            output_name = os.path.join(
                slice_output_pdfs_dif, "{}.pdf".format(column_name)
            )

            # Select data
            x_arrays = slice_df[xy_column_names[0]].to_numpy()
            y_arrays = slice_df[xy_column_names[1]].to_numpy()
            z_arrays = slice_df[column_name].to_numpy()

            # # Run the plot-2d but catch exception
            # plot_data_2d(
            #     x_arrays,
            #     y_arrays,
            #     z_arrays,
            #     #
            #     x_parameter=xy_column_names[0],
            #     y_parameter=xy_column_names[1],
            #     z_parameter=column_name,
            #     parameter_dict=header_description_dict_scalars,
            #     # contourlevels,
            #     # linestyles_contourlevels,
            #     plot_settings={
            #         "runname": column_name,
            #         "show_plot": show_plots,
            #         "output_name": output_name,
            #     },
            # )

            #
            plot_data_scatter(
                x_arrays,
                y_arrays,
                z_arrays,
                #
                x_parameter=xy_column_names[0],
                y_parameter=xy_column_names[1],
                z_parameter=column_name,
                parameter_dict=header_description_dict_scalars,
                # contourlevels,
                # linestyles_contourlevels,
                plot_settings={
                    "runname": column_name,
                    "show_plot": show_plots,
                    "output_name": output_name,
                    # "custom_ylim": [-0.1, 1.1],
                },
            )

            merger, pdf_page_number = add_pdf_and_bookmark(
                merger,
                output_name,
                pdf_page_number,
                bookmark_text="{}={}: {}".format(slice_column, slice_val, column_name),
            )

    # wrap up the pdf
    merger.write(combined_pdf_filename)
    merger.close()
    print("\tFinished plotting HG results for {}".format(dataset_filename))
    print("\t wrote combined pdf to {}".format(os.path.abspath(combined_pdf_filename)))
