"""
Functions to read out the MINT data sets
"""

import copy
import pandas as pd
import numpy as np
import astropy.units as u

import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib as mpl

from scipy import interpolate

import warnings

from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    add_plot_info,
    show_and_save_plot,
    load_mpl_rc,
    linestyles,
)

load_mpl_rc()
warnings.simplefilter("ignore", UserWarning)


dimensionless_unit = u.m / u.m


#####
# Some useful numpy functions
def create_centers_from_bins(bins):
    """
    Function to create centers from bin edges
    """

    return (bins[1:] + bins[:-1]) / 2


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges


def create_bin_dict(array):
    """
    Function to create a dictionary with bin-info given an array of regularly spaced data
    """

    # sort and make unique
    unique_array = np.unique(array)
    sorted_array = np.sort(unique_array)

    # create bincenters
    # bin_centers = create_centers_from_bins(bins=sorted_array)
    bin_edges = create_bins_from_centers(centers=sorted_array)

    return {"centers": sorted_array, "edges": bin_edges}


def create_plotting_data(dataframe_dict, x_column, y_column, z_column):
    """
    Function to create the data (x, y, z arrays) of the columns we want to plot. Will raise error when trying to plot something that is not there
    """

    #
    x_arrays = []
    y_arrays = []
    z_arrays = []

    #
    for dataframe_key in dataframe_dict:
        x_arrays.append(dataframe_dict[dataframe_key][x_column].to_numpy())
        y_arrays.append(dataframe_dict[dataframe_key][y_column].to_numpy())
        z_arrays.append(dataframe_dict[dataframe_key][z_column].to_numpy())

    #
    x_arrays = np.array(x_arrays)
    y_arrays = np.array(y_arrays)
    z_arrays = np.array(z_arrays)

    return x_arrays, y_arrays, z_arrays


def add_unit(parameter_description_dict):
    """
    Function to construct the unit string from a description dict
    """

    # Add unit (in latex)
    unit_string = ""
    if "unit" in parameter_description_dict:
        try:
            if parameter_description_dict["unit"] != dimensionless_unit:
                unit_string = " [{}]".format(
                    parameter_description_dict["unit"].to_string("latex_inline")
                )
        except:
            if parameter_description_dict["unit"].unit != dimensionless_unit:
                unit_string = " [{}]".format(
                    parameter_description_dict["unit"].unit.to_string("latex_inline")
                )

    return unit_string


def filter_arrays(x_arrays, y_arrays, z_arrays):
    """
    Function to filter the arrays
    """

    #
    filtered_x_arrays = x_arrays
    filtered_y_arrays = y_arrays
    filtered_z_arrays = z_arrays

    # remove zero values
    filtered_x_arrays = filtered_x_arrays[filtered_z_arrays != 0]
    filtered_y_arrays = filtered_y_arrays[filtered_z_arrays != 0]
    filtered_z_arrays = filtered_z_arrays[filtered_z_arrays != 0]

    # remove dummy values
    filtered_x_arrays = filtered_x_arrays[filtered_z_arrays > -1e90]
    filtered_y_arrays = filtered_y_arrays[filtered_z_arrays > -1e90]
    filtered_z_arrays = filtered_z_arrays[filtered_z_arrays > -1e90]

    return filtered_x_arrays, filtered_y_arrays, filtered_z_arrays


def make_label(parameter, parameter_dict):
    """
    Function to make a label
    """

    label = parameter.replace("_", " ") + add_unit(
        parameter_description_dict=parameter_dict[parameter]
    )

    return label


def is_empty(arr):
    """
    Function to determine whether an array is filled with zeros
    """

    return np.sum(np.abs(arr)) == 0


def plot_data_2d(
    x_arrays,
    y_arrays,
    z_arrays,
    x_parameter,
    y_parameter,
    z_parameter,
    parameter_dict,
    contourlevels=None,
    linestyles_contourlevels=None,
    add_contourlevels=False,
    add_value_ticklines=True,
    plot_settings=None,
):
    """
    Function to handle the plotting of the 2-d data, including adding a colorbar and contourlines
    """

    if plot_settings is None:
        plot_settings = {}

    # extract the scales
    x_scale = parameter_dict[x_parameter].get("plot_scale", "log")
    y_scale = parameter_dict[y_parameter].get("plot_scale", "log")
    z_scale = parameter_dict[z_parameter].get("plot_scale", "linear")

    # Construct the labels
    x_label = make_label(x_parameter, parameter_dict)
    y_label = make_label(y_parameter, parameter_dict)
    z_label = make_label(z_parameter, parameter_dict)

    ######
    # Filter the arrays
    # x_arrays, y_arrays, z_arrays = filter_arrays(x_arrays, y_arrays, z_arrays)

    ######
    # Create bin structures
    x_bin_dict = create_bin_dict(x_arrays)
    y_bin_dict = create_bin_dict(y_arrays)

    X, Y = np.meshgrid(x_bin_dict["centers"], y_bin_dict["centers"])

    ######
    # construct the histogram
    z_hist = np.histogram2d(
        x_arrays,
        y_arrays,
        bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
        weights=z_arrays,
    )

    #####
    # Set up the figure
    fig = plt.figure(figsize=(20, 20))

    ###########
    # It could be that the filtered z_array is entirely empty. In that case, dont do anything anymore and just save the figure and leave
    if not is_empty(z_hist[0]):

        # Store copy of values
        z_array_color_vals = copy.deepcopy(z_hist[0])

        # change dummy val to 0
        z_array_color_vals[z_array_color_vals <= -1e90] = 0

        ###########
        # The z scale: needs to be decided here so we can use it in the plot
        if z_scale == "linear":
            min_plot_value = z_array_color_vals.min()
            max_plot_value = z_array_color_vals.max()

            norm = colors.Normalize(vmin=min_plot_value, vmax=max_plot_value)
        elif z_scale == "log":
            min_plot_value = z_array_color_vals[z_array_color_vals != 0].min()
            max_plot_value = z_array_color_vals[z_array_color_vals != 0].max()

            norm = colors.LogNorm(vmin=min_plot_value, vmax=max_plot_value)
        elif z_scale == "symlog":
            smallest_scale = np.abs(z_array_color_vals)[
                np.abs(z_array_color_vals) > 0
            ].min()

            min_plot_value = z_array_color_vals[z_array_color_vals != 0].min()
            max_plot_value = z_array_color_vals[z_array_color_vals != 0].max()

            #
            norm = colors.SymLogNorm(
                linthresh=smallest_scale,
                linscale=2,
                vmin=min_plot_value,
                vmax=max_plot_value,
                base=10,
            )
        else:
            msg = "zscale option {} not available. Abort".format(z_scale)
            print(msg)
            raise ValueError(msg)

        #
        gs = fig.add_gridspec(nrows=1, ncols=11)

        ax = fig.add_subplot(gs[:, :9])

        ax_cb = fig.add_subplot(gs[:, 10])

        # #
        # _ = ax.pcolormesh(
        #     X,
        #     Y,
        #     z_hist[0].T,
        #     norm=norm,
        #     shading="auto",
        #     antialiased=True,
        #     rasterized=True,
        # )

        ax.hist2d(
            x_arrays,
            y_arrays,
            bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
            weights=z_arrays,
            antialiased=True,
            rasterized=True,
        )

        # make colorbar
        cbar = mpl.colorbar.ColorbarBase(ax_cb, cmap=mpl.cm.viridis, norm=norm,)

        ##################
        # Set contourlevels and lines on the colorbar
        if add_contourlevels:
            if (
                (not contourlevels is None)
                and (not linestyles_contourlevels is None)
                and (len(contourlevels) <= len(linestyles_contourlevels))
            ):
                CS_mt_region_1 = ax.contour(
                    x_arrays,
                    y_arrays,
                    z_arrays,
                    levels=contourlevels,
                    colors="k",
                    linestyles=linestyles_contourlevels,
                )

                # Set lines on the colorbar
                for contour_i, _ in enumerate(contourlevels):
                    cbar.ax.plot(
                        [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
                        [contourlevels[contour_i]] * 2,
                        "black",
                        linestyle=linestyles_contourlevels[contour_i],
                    )

        ##################
        # plot extra ticklines for the x-values
        if add_value_ticklines:
            value_ticks_color = "red"
            value_ticks_lw = 2
            value_ticks_ls = "solid"
            value_ticks_alpha = 1

            ax.vlines(
                x_bin_dict["centers"],
                color=value_ticks_color,
                lw=value_ticks_lw,
                linestyle=value_ticks_ls,
                alpha=value_ticks_alpha,
                ymax=0.01,
                ymin=0,
                transform=ax.get_xaxis_transform(),
            )

            # plot extra ticklines for the y-values
            ax.hlines(
                y_bin_dict["centers"],
                color=value_ticks_color,
                lw=value_ticks_lw,
                linestyle=value_ticks_ls,
                alpha=value_ticks_alpha,
                xmax=0.01,
                xmin=0,
                transform=ax.get_yaxis_transform(),
            )

        ####
        # Make up
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        cbar.ax.set_ylabel(z_label)

        #
        ax.set_title(z_label)

        #
        ax.set_ylim([y_arrays.min(), y_arrays.max()])
        ax.set_xlim([x_arrays.min(), x_arrays.max()])

        #
        ax.set_xscale(x_scale)
        ax.set_yscale(y_scale)

        #
        fig.tight_layout()
    else:
        print("Found empty dataset")

    # #
    # if plot_settings.get("add_plot_info", True):
    #     # Call plot info (time generated etc)
    #     fig = add_plot_info(fig, plot_settings)

    #######################
    # Save and finish
    show_and_save_plot(plot_settings)


def plot_data_scatter(
    x_arrays,
    y_arrays,
    z_arrays,
    x_parameter,
    y_parameter,
    z_parameter,
    parameter_dict,
    contourlevels=None,
    linestyles_contourlevels=None,
    add_contourlevels=False,
    add_value_ticklines=True,
    plot_settings=None,
    return_fig=False,
):
    """
    Function to handle the plotting of the 2-d data, including adding a colorbar and contourlines
    """

    if plot_settings is None:
        plot_settings = {}

    # extract the scales
    x_scale = parameter_dict[x_parameter].get("plot_scale", "log")
    y_scale = parameter_dict[y_parameter].get("plot_scale", "log")
    z_scale = parameter_dict[z_parameter].get("plot_scale", "linear")

    # Construct the labels
    x_label = make_label(x_parameter, parameter_dict)
    y_label = make_label(y_parameter, parameter_dict)
    z_label = make_label(z_parameter, parameter_dict)

    #####
    # Set up the figure
    # fig = plt.figure(figsize=(20, 20))

    # #
    # gs = fig.add_gridspec(nrows=1, ncols=11)

    # ax = fig.add_subplot(gs[:, :9])

    # ax_cb = fig.add_subplot(gs[:, 10])

    fig, ax = plt.subplots()

    sc = ax.scatter(x_arrays, y_arrays, c=z_arrays, s=100)

    plt.colorbar(sc)

    # print(x_arrays)
    # print(y_arrays)
    # print(z_arrays)
    # quit()

    # ###########
    # # It could be that the filtered z_array is entirely empty. In that case, dont do anything anymore and just save the figure and leave
    # if not is_empty(z_hist[0]):

    #     # Store copy of values
    #     z_array_color_vals = copy.deepcopy(z_hist[0])

    #     # change dummy val to 0
    #     z_array_color_vals[z_array_color_vals <= -1e90] = 0

    #     ###########
    #     # The z scale: needs to be decided here so we can use it in the plot
    #     if z_scale == "linear":
    #         min_plot_value = z_array_color_vals.min()
    #         max_plot_value = z_array_color_vals.max()

    #         norm = colors.Normalize(vmin=min_plot_value, vmax=max_plot_value)
    #     elif z_scale == "log":
    #         min_plot_value = z_array_color_vals[z_array_color_vals != 0].min()
    #         max_plot_value = z_array_color_vals[z_array_color_vals != 0].max()

    #         norm = colors.LogNorm(vmin=min_plot_value, vmax=max_plot_value)
    #     elif z_scale == "symlog":
    #         smallest_scale = np.abs(z_array_color_vals)[
    #             np.abs(z_array_color_vals) > 0
    #         ].min()

    #         min_plot_value = z_array_color_vals[z_array_color_vals != 0].min()
    #         max_plot_value = z_array_color_vals[z_array_color_vals != 0].max()

    #         #
    #         norm = colors.SymLogNorm(
    #             linthresh=smallest_scale,
    #             linscale=2,
    #             vmin=min_plot_value,
    #             vmax=max_plot_value,
    #             base=10,
    #         )
    #     else:
    #         msg = "zscale option {} not available. Abort".format(z_scale)
    #         print(msg)
    #         raise ValueError(msg)

    #     #
    #     gs = fig.add_gridspec(nrows=1, ncols=11)

    #     ax = fig.add_subplot(gs[:, :9])

    #     ax_cb = fig.add_subplot(gs[:, 10])

    #     # #
    #     # _ = ax.pcolormesh(
    #     #     X,
    #     #     Y,
    #     #     z_hist[0].T,
    #     #     norm=norm,
    #     #     shading="auto",
    #     #     antialiased=True,
    #     #     rasterized=True,
    #     # )

    #     ax.hist2d(
    #         x_arrays,
    #         y_arrays,
    #         bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
    #         weights=z_arrays,
    #         antialiased=True,
    #         rasterized=True,
    #     )

    # # make colorbar
    # cbar = mpl.colorbar.ColorbarBase(
    #     ax_cb,
    #     cmap=mpl.cm.viridis,
    #     norm=colors.Normalize(clip=False),
    # )

    #     ##################
    #     # Set contourlevels and lines on the colorbar
    #     if add_contourlevels:
    #         if (
    #             (not contourlevels is None)
    #             and (not linestyles_contourlevels is None)
    #             and (len(contourlevels) <= len(linestyles_contourlevels))
    #         ):
    #             CS_mt_region_1 = ax.contour(
    #                 x_arrays,
    #                 y_arrays,
    #                 z_arrays,
    #                 levels=contourlevels,
    #                 colors="k",
    #                 linestyles=linestyles_contourlevels,
    #             )

    #             # Set lines on the colorbar
    #             for contour_i, _ in enumerate(contourlevels):
    #                 cbar.ax.plot(
    #                     [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
    #                     [contourlevels[contour_i]] * 2,
    #                     "black",
    #                     linestyle=linestyles_contourlevels[contour_i],
    #                 )

    #     ##################
    #     # plot extra ticklines for the x-values
    #     if add_value_ticklines:
    #         value_ticks_color = "red"
    #         value_ticks_lw = 2
    #         value_ticks_ls = "solid"
    #         value_ticks_alpha = 1

    #         ax.vlines(
    #             x_bin_dict["centers"],
    #             color=value_ticks_color,
    #             lw=value_ticks_lw,
    #             linestyle=value_ticks_ls,
    #             alpha=value_ticks_alpha,
    #             ymax=0.01,
    #             ymin=0,
    #             transform=ax.get_xaxis_transform(),
    #         )

    #         # plot extra ticklines for the y-values
    #         ax.hlines(
    #             y_bin_dict["centers"],
    #             color=value_ticks_color,
    #             lw=value_ticks_lw,
    #             linestyle=value_ticks_ls,
    #             alpha=value_ticks_alpha,
    #             xmax=0.01,
    #             xmin=0,
    #             transform=ax.get_yaxis_transform(),
    #         )

    ####
    # Make up
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    # cbar.ax.set_ylabel(z_label)

    #
    ax.set_title(z_label)

    #
    ax.set_ylim([y_arrays.min(), y_arrays.max()])
    ax.set_xlim([x_arrays.min(), x_arrays.max()])

    if "custom_ylim" in plot_settings:
        ax.set_ylim(plot_settings["custom_ylim"])

    #
    ax.set_xscale(x_scale)
    ax.set_yscale(y_scale)

    # set legend
    ax.legend(title=plot_settings["legend_title"], loc="best")

    #
    fig.tight_layout()
    # else:
    #     print("Found empty dataset")

    # #
    # if plot_settings.get("add_plot_info", True):
    #     # Call plot info (time generated etc)
    #     fig = add_plot_info(fig, plot_settings)

    #######################
    # Save and finish
    if return_fig:
        return fig, {"ax": ax, "ax_cb": ax_cb}

    show_and_save_plot(plot_settings)


def get_xvalues_and_global_limits(dataframe_dict, y_param, y_scale, query=None):
    """
    Function to get the global min and max value
    """

    #
    global_min = 1e90
    global_max = -1e90

    # Find x-valus and global_min and global_max
    x_values = []
    for df_key, df in dataframe_dict.items():
        x_values.append(float(df_key))

        # Perform global query;
        if not query is None:
            df = df.query(query)

        if not df.empty:
            yparams = df[y_param]
            if y_scale == "log":
                yparams = df.query("{}>0".format(y_param))[y_param]

            # find global min and max:
            global_min = np.min([global_min, yparams.min()])
            global_max = np.max([global_max, yparams.max()])

    if global_min == np.nan or global_max == np.nan:
        raise ValueError(
            "The global min or max is nan: global_min: {} global_max: {}",
            global_min,
            global_max,
        )

    return x_values, global_min, global_max


def create_x_y_grids(dataframe_dict, y_param, y_resolution, y_scale, query=None):
    """
    Function to create the x and y grids
    """

    #
    x_values, global_min, global_max = get_xvalues_and_global_limits(
        dataframe_dict, y_param, y_scale, query=query
    )

    # Create x and y arrays
    y_grid = np.linspace(global_min, global_max, y_resolution)
    if y_scale == "log":
        y_grid = 10 ** np.linspace(
            np.log10(global_min), np.log10(global_max), y_resolution
        )
    x_grid = np.array(x_values)

    return x_grid, y_grid


def fill_z_grid_by_slices(y_grid, z_grid, dataframe_dict, y_param, z_param, query=None):
    """
    Function to fill the z-grid by setting up interpolation and filling the part of the z-grid that is spanned by the local min and max with interpolated data
    """

    #
    for df_i, (df_key, df) in enumerate(dataframe_dict.items()):
        # Perform global query
        if not query is None:
            df = df.query(query)
        # Find current min and max
        current_min, current_max = df[y_param].min(), df[y_param].max()

        if not df.empty:
            # Set up the interpolator
            interp = interpolate.interp1d(df[y_param], df[z_param])

            # Get all the y_grid values that are within the current value range
            indices_current_grid = np.where(
                np.logical_and(y_grid >= current_min, y_grid <= current_max)
            )
            current_grid = y_grid[indices_current_grid]
            current_values = interp(current_grid)

            #
            z_grid[df_i][indices_current_grid] = current_values

    #
    return z_grid


def plot_with_unstructured_data(
    dataframe_dict,
    x_param,
    y_param,
    z_param,
    x_scale,
    y_scale,
    z_scale,
    y_resolution=1000,
    contourlevel_dict={},
    label_dict={},
    plot_settings=None,
    query=None,
):
    """
    Main function to plot the unstructured data in a grid
    """

    #
    if plot_settings is None:
        plot_settings = {}

    # Get x_grid and y_grid
    x_grid, y_grid = create_x_y_grids(
        dataframe_dict, y_param, y_resolution, y_scale, query=query
    )

    # Set up z-grid with nans
    z_grid = np.zeros((x_grid.shape[0], y_grid.shape[0]))
    z_grid[:, :] = np.nan

    # Fill the z-grid, in the correct slices, with the interpolated data
    z_grid = fill_z_grid_by_slices(
        y_grid, z_grid, dataframe_dict, y_param, z_param, query=query
    )

    # plot the data
    plot_data_2d(
        x_grid,
        y_grid,
        z_grid.T,
        x_scale=x_scale,
        y_scale=y_scale,
        z_scale=z_scale,
        x_label=label_dict[x_param],
        y_label=label_dict[y_param],
        z_label=label_dict[z_param],
        contourlevels=contourlevel_dict.get(z_param, []),
        linestyles_contourlevels=linestyles,
        plot_settings=plot_settings,
    )
