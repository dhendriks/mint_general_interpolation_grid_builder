"""
Utility functions for the track comparison code
"""

import numpy as np
from scipy import interpolate

import matplotlib
import matplotlib.pyplot as plt

from matplotlib import colors

from mint_general_interpolation_grid_builder.functions.plotting_routines.plot_utility_functions import (
    show_and_save_plot,
    load_mpl_rc,
    align_axes,
)

load_mpl_rc()


# Difference metrics
def calculate_diff(mesa_value_array, mint_value_array):
    """
    Function to calculate the difference between MINT track and MESA track: mint-mesa

    both arrays are expected to have the same length
    """

    diff = mint_value_array - mesa_value_array

    return diff


def calculate_frac_diff(mesa_value_array, mint_value_array):
    """
    Function to calculate the fractional difference between MINT track and MESA track: (mint-mesa)/mesa

    both arrays are expected to have the same length
    """

    frac_diff = (mint_value_array - mesa_value_array) / mesa_value_array

    return frac_diff


def calculate_abs_diff(mesa_value_array, mint_value_array):
    """
    Function to calculate the absolute difference between MINT track and MESA track: |mint-mesa|

    both arrays are expected to have the same length
    """

    abs_diff = np.abs(mint_value_array - mesa_value_array)

    return abs_diff


def calculate_abs_frac_diff(mesa_value_array, mint_value_array):
    """
    Function to calculate the absolute fractional difference between MINT track and MESA track: |(mint-mesa)/mesa|

    both arrays are expected to have the same length
    """

    abs_frac_diff = np.abs((mint_value_array - mesa_value_array) / mesa_value_array)

    return abs_frac_diff


def align_on_parameter(
    MESA_df,
    MINT_df,
    time_proxy_parameter,
    align_parameter,
    initial_age_difference_threshold=1e90,
    test_age_difference=False,
):
    """
    Function to align the MINT table ages to be in line with the MINT ages at the first central degeneracy value of MINT
    """

    # TODO: we maybe need to warn something about this. If the age difference is too large

    #
    first_align_parameter_value_MINT = MINT_df[align_parameter].iloc[0]

    align_parameter_values_MESA = MESA_df[align_parameter]
    time_values_MESA = MESA_df[time_proxy_parameter]

    # Set up interpolator
    interpolator_MESA = interpolate.interp1d(
        align_parameter_values_MESA, time_values_MESA, bounds_error=True
    )

    # Find age of MESA track at first degeneracy of MINT track
    MESA_age_at_align_parameter_value_MINT = interpolator_MESA(
        [first_align_parameter_value_MINT]
    )

    # TODO: fix this. its not checking what I want.
    if test_age_difference:
        # check whether the age difference upon alignment is too large
        if MESA_age_at_align_parameter_value_MINT > initial_age_difference_threshold:
            raise ValueError(
                "The MESA age ({}) that aligns with the first align-parameter ({}) value ({}) exceeds the threshold of {}".format(
                    MESA_age_at_align_parameter_value_MINT,
                    align_parameter,
                    first_align_parameter_value_MINT,
                    initial_age_difference_threshold,
                )
            )

    # shift values of MINT table with that age
    MINT_df.loc[:, time_proxy_parameter] = (
        MINT_df[time_proxy_parameter] + MESA_age_at_align_parameter_value_MINT
    )

    return MESA_df, MINT_df


def load_dataframes(MINT_df, MESA_df, testdata_dir, track):
    """
    Function to load dataframes
    """

    # either use provided dataframes or read them from a file
    if (MINT_df is None) and (MESA_df is None):
        if (testdata_dir is None) or (track is None):
            raise ValueError(
                "Cant compare tracks if neither dataframes are provided in and no track files are provided"
            )

        ############
        # Read out data and make dataframes
        MESA_datafile = os.path.join(testdata_dir, "{}_MESA.csv".format(track))
        MINT_datafile = os.path.join(testdata_dir, "{}_MINT.csv".format(track))

        #
        MESA_df = pd.read_csv(MESA_datafile, header=0, sep=",", index_col=0)
        MINT_df = pd.read_csv(MINT_datafile, header=0, sep=",", index_col=0)

    ###############
    # Safe-guard check if either of the dataframes is empty
    if MESA_df.empty:
        raise ValueError("Error: MESA DF is empty!")

    if MINT_df.empty:
        raise ValueError("Error: MINT DF is empty!")

    return MESA_df, MINT_df


def plot_comparison_function(
    MESA_df,
    MINT_df,
    comparison_dict,
    time_proxy_parameter,
    parameter,
    track,
    difference_function_dict,
    threshold_value,
    transform_function,
    plot_settings,
):
    """
    Routine to plot the comparison between the
    """

    #
    original_data_alpha = 0.5
    original_data_linestyle = "--"
    original_data_linewidth = 2

    data_linestyle = "solid"
    data_linewidth = 3
    data_alpha = 1

    ###################
    # Set up figure
    fig = plt.figure(figsize=(20, 20))
    plt.subplots_adjust(wspace=0, hspace=0)

    #
    gs = fig.add_gridspec(nrows=4, ncols=1)

    # create axes
    ax = fig.add_subplot(gs[:3, :])
    ax_diff = fig.add_subplot(gs[3, :])

    axes_list = [ax, ax_diff]

    ###################
    # plot the tracks for visual inspection

    # Plot original data
    ax.plot(
        MESA_df[time_proxy_parameter],
        MESA_df[parameter],
        linestyle=original_data_linestyle,
        lw=original_data_linewidth,
        alpha=original_data_alpha,
        c="blue",
        label="original MESA",
    )
    ax.plot(
        MINT_df[time_proxy_parameter],
        MINT_df[parameter],
        linestyle=original_data_linestyle,
        alpha=original_data_alpha,
        lw=original_data_linewidth,
        c="red",
        label="original MINT",
    )

    # Plot compared data
    ax.plot(
        comparison_dict["time_values"],
        comparison_dict["parameter_values_MESA"],
        linestyle=data_linestyle,
        lw=data_linewidth,
        alpha=data_alpha,
        c="blue",
        label="clipped MESA",
    )
    ax.plot(
        comparison_dict["time_values"],
        comparison_dict["parameter_values_MINT"],
        linestyle=data_linestyle,
        lw=data_linewidth,
        alpha=data_alpha,
        c="red",
        label="clipped interpolated MINT",
    )

    # plot the absolute difference between the two
    ax_diff.plot(comparison_dict["time_values"], comparison_dict["difference"])
    ax_diff_ylims = ax_diff.get_ylim()

    # draw threshold value
    ax_diff.axhline(threshold_value, c="r", linestyle="--", alpha=0.5)
    ax_diff.set_ylim(ax_diff_ylims)

    ##################
    # Make-up
    ax.set_xticklabels([])

    ylabel = "{}".format(difference_function_dict["fullname"]) + "\n({})".format(
        difference_function_dict["label"]
    )

    if transform_function is not None:
        ylabel += " Used {}".format(transform_function.__name__)

    ax_diff.set_ylabel(ylabel)

    fig.align_ylabels(axes_list)

    #
    align_axes(fig=fig, axes_list=axes_list, which_axis="x")

    ax.set_title(track)
    ax.set_ylabel(parameter)
    ax_diff.set_xlabel(time_proxy_parameter)
    ax.legend(loc="best")

    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(plot_settings)


def check_difference(
    comparison_dict,
    parameter,
    time_proxy_parameter,
    difference_function_dict,
    threshold_value,
):
    """
    Function to check whether the difference exceeds the threshold, print info, and write to report file
    """

    ##########
    # find largest error
    largest_error = np.max(comparison_dict["difference"])

    #
    index_largest_error = np.argmax(comparison_dict["difference"])
    time_proxy_largest_error = comparison_dict["time_values"][index_largest_error]

    print("largest_error", largest_error)
    print("threshold_value", threshold_value)

    #
    passed = largest_error < threshold_value

    ###########
    # Report on error
    print("For parameter {}".format(parameter))
    print(
        "Largest {} error compared to original MESA track: {} (at {} {})".format(
            difference_function_dict["fullname"],
            largest_error,
            time_proxy_parameter,
            time_proxy_largest_error,
        )
    )
    if not passed:
        print("error {} exceeds threshold {}".format(largest_error, threshold_value))

    #######
    # Add some info about the largest error and whether its passed
    comparison_dict["passed"] = passed
    comparison_dict["largest_difference"] = np.max(comparison_dict["difference"])
    comparison_dict["time_proxy_largest_difference"] = time_proxy_largest_error

    return comparison_dict


def compare_track_values(
    MESA_df,
    MINT_df,
    parameter,
    time_proxy_parameter,
    #
    difference_function_dict,
    threshold_value,
    transform_function,
    verbose=False,
):
    """
    Function to compare values of the two dataframes
    """

    #########
    # Extract data of MESA
    original_time_values_MESA = MESA_df[time_proxy_parameter].to_numpy()
    original_parameter_values_MESA = MESA_df[parameter].to_numpy()

    # Extract data of MINT
    original_time_values_MINT = MINT_df[time_proxy_parameter].to_numpy()
    original_parameter_values_MINT = MINT_df[parameter].to_numpy()

    #########
    # Set up interpolator
    interpolator_MINT = interpolate.interp1d(
        original_time_values_MINT,
        original_parameter_values_MINT,
        bounds_error=True,  # NOTE: This already handles the fact that
    )

    ########
    # Clip values
    # TODO: maybe we should move this before doing the interpolation?
    time_values_MESA, parameter_values_MESA = clip_values(
        time_values=original_time_values_MESA,
        parameter_values=original_parameter_values_MESA,
        time_values_other=original_time_values_MINT,
    )

    #########
    # clip MESA output

    #########
    # Interpolate to same ages as MESA
    parameter_values_MINT = interpolator_MINT(time_values_MESA)

    #########
    # transform values
    tmp_parameter_values_MESA = parameter_values_MESA
    tmp_parameter_values_MINT = parameter_values_MINT
    if transform_function is not None:
        tmp_parameter_values_MESA = transform_function(parameter_values_MESA)
        tmp_parameter_values_MINT = transform_function(parameter_values_MINT)

    #########
    # calculate difference
    calculate_difference_function = difference_function_dict["function"]
    difference = calculate_difference_function(
        mesa_value_array=tmp_parameter_values_MESA,
        mint_value_array=tmp_parameter_values_MINT,
    )

    if verbose:
        print(
            "Comparing MESA and MINT track using parameter {} and time_proxy_parameter {}".format(
                parameter, time_proxy_parameter
            )
        )
        if transform_function is not None:
            print(
                "and used transformation function {}".format(
                    transform_function.__name__
                )
            )

        print("time_values_MESA: {}".format(time_values_MESA))
        print("parameter_values_MESA: {}".format(parameter_values_MESA))

        print("time_values_MINT: {}", time_values_MINT)
        print("original_parameter_values_MINT: {}", original_parameter_values_MINT)

    #############
    #
    comparison_dict = {
        #
        "original_time_values_MESA": original_time_values_MESA,
        "original_time_values_MINT": original_time_values_MINT,
        "original_parameter_values_MESA": original_parameter_values_MESA,
        "original_parameter_values_MINT": original_parameter_values_MINT,
        #
        "time_values": time_values_MESA,
        "parameter_values_MESA": parameter_values_MESA,
        "parameter_values_MINT": parameter_values_MINT,
        #
        "difference": difference,
        "threshold_value": threshold_value,
        "difference_function_name": difference_function_dict["name"],
    }

    #########
    # Check difference
    comparison_dict = check_difference(
        comparison_dict=comparison_dict,
        parameter=parameter,
        time_proxy_parameter=time_proxy_parameter,
        difference_function_dict=difference_function_dict,
        threshold_value=threshold_value,
    )

    #
    return comparison_dict


def clip_values(time_values, parameter_values, time_values_other):
    """
    Function to clip the values of a track
    """

    min_time_other = np.min(time_values_other)
    max_time_other = np.max(time_values_other)

    indices = np.where(
        (time_values >= min_time_other) & (time_values <= max_time_other)
    )

    clipped_time_values = time_values[indices]
    clipped_parameter_values = parameter_values[indices]

    return clipped_time_values, clipped_parameter_values


def filter_parameters_to_check(
    header_description_dict_scalars, stellar_phase, MESA_df, MINT_df, verbose
):
    """
    Function to filter out the parameters we need to check for this stellar phase
    """

    #
    all_parameters_to_check = []
    present_parameters_to_check = []

    parameters_skipped_in_general = []
    parameters_skipped_in_current_stellar_phase = []

    parameters_not_present_in_either_df = []
    parameters_not_present_in_MESA_df = []
    parameters_not_present_in_MINT_df = []

    #######
    # Loop over all parameters
    for parameter in header_description_dict_scalars.keys():
        # skip if this parameter should not be tested in general
        if header_description_dict_scalars[parameter].get("exclude_from_test", False):
            parameters_skipped_in_general.append(parameter)
            continue

        # skip if this parameter should not be tested in this particular stellar phase
        exclude_from_test_phase_specific = header_description_dict_scalars[
            parameter
        ].get("exclude_from_test_phase_specific", [])
        if stellar_phase in exclude_from_test_phase_specific:
            parameters_skipped_in_current_stellar_phase.append(parameter)
            continue

        #
        all_parameters_to_check.append(parameter)

    #######
    # Check if parameters are present in either dataframe
    MESA_columns = list(MESA_df.columns)
    MINT_columns = list(MINT_df.columns)

    #
    for parameter in all_parameters_to_check:
        # Check
        if (parameter not in MESA_columns) and (parameter not in MINT_columns):
            parameters_not_present_in_either_df.append(parameter)
            continue
        if parameter not in MESA_columns:
            parameters_not_present_in_MESA_df.append(parameter)
            continue
        if parameter not in MINT_columns:
            parameters_not_present_in_MINT_df.append(parameter)
            continue

        #
        present_parameters_to_check.append(parameter)

    if verbose:
        print("parameters_skipped_in_general: {}".format(parameters_skipped_in_general))
        print(
            "parameters_skipped_in_current_stellar_phase: {}".format(
                parameters_skipped_in_current_stellar_phase
            )
        )
        print(
            "parameters_not_present_in_either_df: {}".format(
                parameters_not_present_in_either_df
            )
        )
        print(
            "parameters_not_present_in_MESA_df: {}".format(
                parameters_not_present_in_MESA_df
            )
        )
        print(
            "parameters_not_present_in_MINT_df: {}".format(
                parameters_not_present_in_MINT_df
            )
        )
        print("present_parameters_to_check: {}".format(present_parameters_to_check))

    return present_parameters_to_check
