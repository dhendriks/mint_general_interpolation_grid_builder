"""
Main script to test the track recovery.

Steps:
- using the MINT interpolation table, generate a synthetic MINT-track, which represents the evolution of the star
- extract the MESA track
- use the AGEs of the MESA track and calculate the MINT-track values by interpolation of the MINT-track to those exact points
- compare at the same AGE values the values of parameters like Luminosity, Temperature, etc.

TODO: consider chebychev comparison
"""

import os

import numpy as np
import pandas as pd

from mint_general_interpolation_grid_builder.functions.testing.track_comparison_utils import (
    #
    align_on_parameter,
    load_dataframes,
    plot_comparison_function,
    compare_track_values,
    filter_parameters_to_check,
)


#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def compare_track(
    time_proxy_parameter,
    align_tracks,
    align_parameter,
    plot_comparison,
    plots_output_dir,
    stellar_phase,
    header_description_dict_scalars,
    track_comparison_difference_functions_dict,
    align_age_on_time_proxy=False,
    track=None,
    autofilter_columns=False,
    testdata_dir=None,
    MESA_df=None,
    MINT_df=None,
    max_end_age_value_difference_threshold=1e9,
    test_age_difference_at_end=True,
    verbose=0,
):
    """
    Main function to compare MESA vs directly-interpolated MINT track
    """

    ################
    # Load data
    MESA_df, MINT_df = load_dataframes(
        MESA_df=MESA_df, MINT_df=MINT_df, testdata_dir=testdata_dir, track=track,
    )

    ################
    # Update the AGE column for MINT
    # MESA_df.loc[:, "AGE_FIXED"] = MESA_df["AGE"]
    MINT_df.loc[:, "AGE"] = MINT_df["INT_TIME"]

    ################
    # Filter parameters to check
    parameters_to_check = filter_parameters_to_check(
        header_description_dict_scalars=header_description_dict_scalars,
        stellar_phase=stellar_phase,
        MESA_df=MESA_df,
        MINT_df=MINT_df,
        verbose=verbose,
    )

    ################
    # whether to align age on the time proxy
    if align_age_on_time_proxy:
        MESA_df, MINT_df = align_on_parameter(
            MESA_df,
            MINT_df,
            time_proxy_parameter=time_proxy_parameter,
            align_parameter="AGE",
        )

    # Align the dataframes on a parameter
    if align_tracks:
        MESA_df, MINT_df = align_on_parameter(
            MESA_df,
            MINT_df,
            time_proxy_parameter=time_proxy_parameter,
            align_parameter=align_parameter,
        )

    ###############
    # Loop over parameters, compare the tracks and store the comparison
    comparison_dicts = {}

    for parameter in parameters_to_check:

        #########
        # get comparison function dict
        difference_function_name = header_description_dict_scalars[parameter][
            "diff_function"
        ]
        difference_function_dict = track_comparison_difference_functions_dict[
            difference_function_name
        ]

        #########
        # get threshold value
        threshold_value = header_description_dict_scalars[parameter][
            "threshold_default"
        ]
        if "threshold_dict" in header_description_dict_scalars[parameter].keys():
            if (
                parameter
                in header_description_dict_scalars[parameter]["threshold_dict"]
            ):
                threshold_value = header_description_dict_scalars[parameter][
                    "threshold_dict"
                ][parameter]

        ########
        # get value transform function
        transform_function = header_description_dict_scalars[parameter].get(
            "transform_function", None
        )

        #########
        # compare track values
        comparison_dict = compare_track_values(
            MESA_df=MESA_df,
            MINT_df=MINT_df,
            parameter=parameter,
            time_proxy_parameter=time_proxy_parameter,
            #
            difference_function_dict=difference_function_dict,
            threshold_value=threshold_value,
            transform_function=transform_function,
            verbose=verbose,
        )

        #
        comparison_dicts[parameter] = comparison_dict

        ##########
        # Plot comparison
        if plot_comparison:
            plot_comparison_function(
                MESA_df=MESA_df,
                MINT_df=MINT_df,
                comparison_dict=comparison_dict,
                time_proxy_parameter=time_proxy_parameter,
                parameter=parameter,
                track=track,
                difference_function_dict=difference_function_dict,
                threshold_value=threshold_value,
                transform_function=transform_function,
                plot_settings={
                    "output_name": os.path.join(
                        plots_output_dir, "{}.pdf".format(parameter)
                    )
                },
            )

    ###############
    # Return the comparison dictionaries
    return comparison_dicts


if __name__ == "__main__":
    from mint_general_interpolation_grid_builder.functions.testing.track_comparison_config import (
        track_comparison_difference_functions_dict,
    )

    from MINT.config.header_description_dict import header_description_dict_scalars

    ###########################
    #
    testdata_root_dir = os.path.join(this_file_dir, "example_testdata", "new_tracks")

    track = "GB_1.33"
    stellar_phase = "GB"

    plot_output_dir = os.path.join(this_file_dir, "plots", "new_tracks", track)

    ############
    # Read out data and make dataframes
    MINT_datafile = os.path.join(testdata_root_dir, "{}_MINT.csv".format(track))
    MESA_datafile = os.path.join(testdata_root_dir, "{}_MESA.csv".format(track))

    #
    MINT_df = pd.read_csv(MINT_datafile, header=0, sep=",", index_col=0)
    MESA_df = pd.read_csv(MESA_datafile, header=0, sep=",", index_col=0)

    # time_proxy_parameter = track_comparison_config[stellar_type]["time_proxy_parameter"]
    # parameter_comparison_dict = track_comparison_config[stellar_type][
    #     "parameter_comparison_dict"
    # ]

    time_proxy_parameter = "CENTRAL_DEGENERACY"

    comparison_dicts = compare_track(
        time_proxy_parameter=time_proxy_parameter,
        align_parameter="",
        align_tracks=False,
        stellar_phase=stellar_phase,
        MINT_df=MINT_df,
        MESA_df=MESA_df,
        plot_comparison=True,
        autofilter_columns=False,
        plots_output_dir=plot_output_dir,
        header_description_dict_scalars=header_description_dict_scalars,
        track_comparison_difference_functions_dict=track_comparison_difference_functions_dict,
    )

    print(comparison_dicts[list(comparison_dicts.keys())[0]].keys())

    ###########
    # tracks = ["GB_1.33"]

    # # choose which parameter
    # time_proxy_parameter = "AGE_FIXED"
    # ALIGN_PARAMETER = "CENTRAL_DEGENERACY"
    # INITIAL_AGE_DIFFERENCE_THRESHOLD = 1e9

    # tracks = ["GB_0.63", "GB_1.33"]
    # # track_type = "old_tracks"

    # # tracks = ["GB_1.03", "GB_1.33"]
    # tracks = ["GB_1.33"]
    # track_type = "new_tracks"

    # ###############
    # # Loop over tracks
    # for track in tracks:
    #     stellar_type = "".join(track.split("_")[:-1])

    #     testdata_dir = os.path.join(testdata_root_dir, track_type)

    #     compare_track(
    #         time_proxy_parameter="CENTRAL_DEGENERACY",
    #         align_parameter=ALIGN_PARAMETER,
    #         align_tracks=False,
    #         stellar_type=stellar_type,
    #         plot_comparison=True,
    #         plots_output_dir=os.path.join(this_file_dir, "plots", track_type, track),
    #         track=track,
    #         testdata_dir=testdata_dir,
    #         test_age_difference_at_end=False,
    #     )
