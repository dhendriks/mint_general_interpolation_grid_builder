"""
File containing the general track comparison configuration used in the routines
that compares the original MESA track to the interpolated track directly
interpolated on the table (i.e. not evolved through binary_c)

- MS
- GB:
- CHeB:
- EAGB
- TPAGB
"""

from mint_general_interpolation_grid_builder.functions.testing.track_comparison_utils import (
    #
    calculate_diff,
    calculate_frac_diff,
    calculate_abs_diff,
    calculate_abs_frac_diff,
)

###########
#
track_comparison_difference_functions_dict = {
    "diff": {
        "name": "diff",
        "fullname": "Difference",
        "function": calculate_diff,
        "label": r"f$_{\rm{MINT}}$-f$_{\rm{MESA}}$",
    },
    "frac_diff": {
        "name": "frac diff",
        "fullname": "Fractional difference",
        "function": calculate_frac_diff,
        "label": r"(f$_{\rm{MINT}}$-f$_{\rm{MESA}}$)/f$_{\rm{MESA}}$",
    },
    "abs_diff": {
        "name": "abs diff",
        "fullname": "Absolute difference",
        "function": calculate_abs_diff,
        "label": r"|f$_{\rm{MINT}}$-f$_{\rm{MESA}}$|",
    },
    "abs_frac_diff": {
        "name": "abs_frac_diff",
        "fullname": "Absolute fractional difference",
        "function": calculate_abs_frac_diff,
        "label": r"|(f$_{\rm{MINT}}$-f$_{\rm{MESA}}$)/f$_{\rm{MESA}}$|",
    },
}


# "EXAMPLE_scalar_input_columns": ["MASS", "CENTRAL_HYDROGEN"],
# "MS_scalar_input_columns": ["MASS", "CENTRAL_HYDROGEN"],
# "GB_scalar_input_columns": [
#     "MASS",
#     "CENTRAL_DEGENERACY",
#     "HELIUM_CORE_MASS_FRACTION",
# ],
# "CHeB_scalar_input_columns": [
#     "MASS",
#     "CENTRAL_HELIUM",
#     "HELIUM_CORE_MASS_FRACTION",
# ],
# "EAGB_scalar_input_columns": [
#     "MASS",
#     "CENTRAL_DEGENERACY",
#     "CARBON_CORE_MASS_FRACTION",
# ],
# "TPAGB_scalar_input_columns": [
#     "MASS",
#     "INTERSHELL_MASS_FRACTION",
#     "HELIUM_CORE_MASS_FRACTION",
# ],
# "MC_scalar_input_columns": [
#     "MASS",
#     "CENTRAL_DEGENERACY",
#     "HELIUM_CORE_MASS_FRACTION",
# ],


# ##########
# #
# #
# # TODO:
# track_comparison_config = {
#     "MS": {
#         "time_proxy_parameter": "CENTRAL_HYDROGEN_FRACTION",
#         "parameter_comparison_dict": {},
#     },
#     "GB": {
#         "time_proxy_parameter": "CENTRAL_DEGENERACY",
#         "parameter_comparison_dict": {
#             # Mass parameters
#             "INITIAL_MASS": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "MASS": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # Age parameters
#             "AGE_FIXED": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # Luminosity parameters
#             "LUMINOSITY": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "LOG_LUMINOSITY": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # Temperature parameters
#             "EFFECTIVE_TEMPERATURE": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "LOG_EFFECTIVE_TEMPERATURE": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # Degeneracy parameters
#             "CENTRAL_DEGENERACY": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "FIRST_DERIVATIVE_CENTRAL_DEGENERACY": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "LOG_FIRST_DERIVATIVE_CENTRAL_DEGENERACY": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # Helium core mass fraction derivative parameters
#             "FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "LOG_FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # Helium luminosity fraction parameters
#             "LOG_HELIUM_LUMINOSITY_FRACTION": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             # TODO: Sort below
#             "HELIUM_CORE_MASS_FRACTION": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "HELIUM_IGNITION_PARAMETER": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "FIRST_DERIVATIVE_CENTRAL_HELIUM": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#             "FIRST_DERIVATIVE_CARBON_CORE_MASS_FRACTION": {
#                 "difference_function": "abs_diff",
#                 "threshold": 1,
#             },
#         },
#     },
# }
