"""
Functionality to build rst pages per stellar type
"""

import os

from mint_general_interpolation_grid_builder.MINT.src.MINT_table_builders.available_MINT_table_builders import (
    MINT_interpolation_table_builder_instances,
)


def build_MINT_interpolation_table_builder_description_pages(output_dir):
    """
    Dummy function for the description pages

    MINT grid (sub)classes should contain the following properties/methods:
    - property: name
    - property: longname
    - property: description
    - method: generate_parameter_description_table

    With these properties we can automatically build online docs
    """

    # Create directory
    os.makedirs(output_dir, exist_ok=True)

    ##########
    # Loop over the MINT_subclasses
    for MINT_subclass_instance in MINT_interpolation_table_builder_instances:

        # Get the parameter description tables
        parameter_description_tables = (
            MINT_subclass_instance.generate_parameter_description_table()
        )

        MINT_subclass_description_rst_name = "{}.rst".format(
            MINT_subclass_instance.name
        )

        with open(
            os.path.join(output_dir, MINT_subclass_description_rst_name), "w"
        ) as f:
            f.write(parameter_description_tables)


if __name__ == "__main__":

    build_MINT_interpolation_table_builder_description_pages("results/")
