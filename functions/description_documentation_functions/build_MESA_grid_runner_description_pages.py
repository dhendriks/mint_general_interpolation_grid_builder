"""
Main function to handle writing the description pages of the MESA grid runners
"""


import os
import shutil
from mint_general_interpolation_grid_builder.core.MESAGridRunner.src.functions.inlist_writing import (
    write_inlists_from_inlists_dictionary,
)
from mint_general_interpolation_grid_builder.MINT.src.MINT_grid_runners.available_MINT_grid_runners import (
    return_available_MINT_grid_runners,
)


def add_inlist_info_to_doc_page(
    config, MINT_MESA_grid_runners_instance, rst_str, cur_dir=None
):
    """
    Function to add the inlist info the the doc page.

    creates the inlist info files, moves them to a correct location, adds links to them in the rst file
    """

    ######
    # Get only the relevant inlists
    local_inlist_names = [
        "inlist_common",
        "inlist_{}".format(MINT_MESA_grid_runners_instance.evol_phase),
    ]
    local_inlists = {}

    for inlist_name in MINT_MESA_grid_runners_instance.settings["inlists"].keys():
        if inlist_name in local_inlist_names:
            local_inlists[inlist_name] = MINT_MESA_grid_runners_instance.settings[
                "inlists"
            ][inlist_name]

    ######
    # write inlists
    inlist_dir = os.path.join(
        config["grid_directory"], MINT_MESA_grid_runners_instance.evol_phase
    )
    os.makedirs(inlist_dir, exist_ok=True)

    #
    write_inlists_from_inlists_dictionary(
        direc_path=inlist_dir,
        inlist_dict=local_inlists,
    )

    ######
    # Move inlist files to a location that sphinx expects them to be
    if cur_dir is not None:
        os.makedirs(
            os.path.join(
                cur_dir,
                config["grid_directory"][1:],
                MINT_MESA_grid_runners_instance.evol_phase,
            ),
            exist_ok=True,
        )

        for inlist in local_inlists.keys():
            filename = os.path.join(inlist_dir, inlist)
            shutil.copyfile(
                filename,
                os.path.join(
                    cur_dir,
                    config["grid_directory"][1:],
                    MINT_MESA_grid_runners_instance.evol_phase,
                    inlist,
                ),
            )

    #############
    # Construct inlist section header
    section_title = "default inlists for phase"
    rst_str += section_title + "\n" + len(section_title) * "-"
    rst_str += "\n" * 5

    # Loop over inlists
    for inlist in local_inlists.keys():
        filename = os.path.join(inlist_dir, inlist)

        # TODO: add inlist name header

        # Add reference to filename to rst file
        rst_str += """
.. literalinclude:: {}
    :language: fortran
""".format(
            filename
        )
        rst_str += "\n"

    return rst_str


def build_MINT_MESA_grid_runner_description_pages(output_dir, config, cur_dir=None):
    """
    Dummy function for the description pages

    MINT grid (sub)classes should contain the following properties/methods:
    - property: name
    - property: longname
    - property: description
    - method: generate_parameter_description_table

    With these properties we can automatically build online docs
    """

    # Create directory
    os.makedirs(output_dir, exist_ok=True)

    #
    MINT_MESA_grid_runners_instances = return_available_MINT_grid_runners(config=config)

    ##########
    # Loop over the MINT_subclasses
    for MINT_MESA_grid_runners_instance in MINT_MESA_grid_runners_instances:

        #########
        # Construct heading
        rst_str = ""
        title = "{} MESA grid runner".format(MINT_MESA_grid_runners_instance.evol_phase)
        rst_str += title + "\n" + len(title) * "="
        rst_str += "\n" * 5

        #########
        # Add inlist section
        rst_str = add_inlist_info_to_doc_page(
            config=config,
            MINT_MESA_grid_runners_instance=MINT_MESA_grid_runners_instance,
            rst_str=rst_str,
            cur_dir=cur_dir,
        )

        ################
        # Handle writing

        # construct filename
        MESA_subclass_description_rst_name = "{}.rst".format(
            MINT_MESA_grid_runners_instance.evol_phase
        )

        # write
        with open(
            os.path.join(output_dir, MESA_subclass_description_rst_name), "w"
        ) as f:
            f.write(rst_str)


if __name__ == "__main__":

    from mint_general_interpolation_grid_builder.MINT.config.mint_settings import (
        mint_defaults,
    )
    from mint_general_interpolation_grid_builder.MINT.config.table_columns import (
        table_columns,
    )
    from mint_general_interpolation_grid_builder.MINT.config.mint_inlists import inlists

    #
    config = {**mint_defaults, **table_columns}
    config["inlists"] = inlists
    config["grid_directory"] = "/tmp"

    #
    build_MINT_MESA_grid_runner_description_pages("results/", config=config)
