"""
Extra functions to build the description pages
"""

import astropy.units as u

dimensionless_unit = u.m / u.m


def build_description_table(MINT_table_name, parameter_list, description_dict):
    """
    Function to create a table containing the description of the parameters in the event log
    """

    #
    indent = "   "

    # Get parameter list and parse descriptions
    parameter_list_with_descriptions = [
        [parameter, parse_description(description_dict=description_dict[parameter]),]
        for parameter in parameter_list
    ]

    # Construct parameter list
    rst_event_table = """
.. list-table:: {}
{}:widths: 25, 75
{}:header-rows: 1

""".format(
        MINT_table_name, indent, indent
    )

    #
    rst_event_table += indent + "* - Parameter\n"
    rst_event_table += indent + "  - Description\n"

    for parameter_el in parameter_list_with_descriptions:
        rst_event_table += indent + "* - {}\n".format(parameter_el[0])
        rst_event_table += indent + "  - {}\n".format(parameter_el[1])

    return rst_event_table


def parse_description(description_dict):
    """
    Function to parse the description for a given parameter
    """

    description_string = description_dict["desc"]

    # Capitalise first letter
    description_string = description_string[0].capitalize() + description_string[1:]

    # Add period
    if description_string[-1] != ".":
        description_string = description_string + "."

    # Add unit (in latex)
    if "unit" in description_dict:

        if description_dict["unit"] != dimensionless_unit:

            if "unit" not in dir(description_dict["unit"]):
                description_string = description_string + " Unit: [{}].".format(
                    description_dict["unit"].to_string("latex_inline")
                )
            else:

                description_string = description_string + " Unit: [{}].".format(
                    description_dict["unit"].unit.to_string("latex_inline")
                )

    return description_string
