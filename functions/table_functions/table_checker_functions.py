"""
Functions to check the table after building.

TODO: implement functionality of test_table.pl (see robs script)
"""

import numpy as np


def _check_bounded(array, lower=-1e-10, upper=1):
    """
    Function to check if any element in the array is properly bounded between 0 and 1
    """

    if np.any(array < lower) or np.any(array > upper):
        return False
    return True


def check_bounded(df, required_columns):
    """
    Function to check if the provided columns are properly bounded between 0 and 1.
    """

    for col in df.columns:
        if col in required_columns:

            bounded = _check_bounded(df[col].to_numpy())

            #
            if not bounded:
                raise ValueError("Column {} is not bounded".format(col))


def check_bad_vals(df, required_columns):
    """
    Function to check if the values in the provided columns do not contain bad values
    """

    # Loop over bad elements and check if any of them have
    bad_value_found = False
    for col in df.columns:
        if col in required_columns:
            arr = df[col].to_numpy()
            arr = np.stack(arr)

            # perform checks
            bad_value_found = np.isnan(arr).any()
            bad_value_found = np.isinf(arr).any()

            #
            if bad_value_found:
                raise ValueError("Bad values found in column {}".format(col))


def _check_positive(arr, allow_zero=False):
    """
    Function to check of the content of an array is always positive
    """

    if allow_zero:
        return (arr >= 0).all()
    return (arr > 0).all()


def check_positive(df, required_columns, allow_zero=False):
    """
    Function to check the content of the columns is always positive.

    Optionally we allow zero.
    """

    for col in df.columns:
        if col in required_columns:
            arr = df[col].to_numpy()

            if not _check_positive(arr):
                raise ValueError(
                    "Non-positive (allow_zero: {}) values found in column {}".format(
                        allow_zero, col
                    )
                )


def _check_negative(arr, allow_zero=False):
    """
    Function to check of the content of an array is always negative.
    """

    if allow_zero:
        return (arr <= 0).all()
    return (arr < 0).all()


def check_negative(df, required_columns, allow_zero=False):
    """
    Function to check the content of the columns is always negative.

    Optionally we allow zero.
    """

    for col in df.columns:
        if col in required_columns:
            arr = df[col].to_numpy()

            if not _check_negative(arr):
                raise ValueError(
                    "Non-negative (allow_zero: {}) values found in column {}".format(
                        allow_zero, col
                    )
                )


def check_for_duplicate_interpolation_variables(
    df, input_columns, allow_duplicates=True
):
    """
    Function to check for duplicate values of the interpolation variables
    """

    duplicates = df.duplicated(subset=input_columns)

    if duplicates.any():
        print("Duplicate values found in the following rows:")
        print(df[duplicates][input_columns])
        if allow_duplicates:
            df.drop_duplicates(subset=input_columns, keep="first", inplace=True)
            print("Removing duplicates")
        else:
            raise ValueError("Duplicate interpolation variables")

    return df


def check_for_non_single_last_interpolation_variable(df, input_columns):

    ################################################
    # check that every mass and center degeneracy point has at least two core mass values
    # if there is only one then delete that row

    group_columns = input_columns[:-1]

    # Count rows with same values of MASS and CENTRAL_DEGENERACY
    count_df = df.groupby(group_columns).size().reset_index(name="count")
    # Remove rows with count = 1
    remove_df = count_df[count_df["count"] == 1]
    df = df[
        ~df.set_index(group_columns).index.isin(
            remove_df.set_index(group_columns).index
        )
    ]

    return df


# def handle_bad_vals(self, df, remove_row_if_bad_vals=True):
#     """
#     Function to check if the values in the provided columns are okay.
#     """

#     # define list of not okay
#     bad_list = [np.nan, np.inf, -np.inf]

#     # check for bad vals
#     if df.isin(bad_list).values.any() == True:
#         print("Nans and infs in table, removing those rows")

#     # printing info on bad vals locs
#     for column in df:
#         if df[column].isin(bad_list).values.any() == True:
#             print(column)
#             nans = df[df[column].isin(bad_list)]
#             for i, row in nans.iterrows():
#                 print(row[self.scalar_input_columns])

#     # remove rows with bad vals
#     if remove_row_if_bad_vals == True:
#         df[~df.isin([np.nan, np.inf, -np.inf]).any(1)]

#     return df


def check_orthogonality(df, input_columns, remove_non_orthogonal_values=True):

    # col = self.scalar_input_columns[0]
    # next_col = self.scalar_input_columns[1]
    # next_col_values = df[next_col].unique()

    # for val in df[col].unique():
    #     df_sec = df.loc[df[col] == val]
    #     if not np.array_equal(df_sec[next_col].unique(), next_col_values):
    #         print("not orthogonal")
    #         if remove_non_orthogonal_values:
    #             print("removing ", col, " = ", val)
    #             df = df.drop(df_sec.index)

    # Check dimensions of table are correct
    if len(df) != df[input_columns].apply(lambda col: len(col.unique())).prod():
        raise ValueError("Table not orthogonal")

    return df
