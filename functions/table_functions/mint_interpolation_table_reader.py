#!/usr/bin/python3

"""
Functions for a general mint interpolation table reader. The input of this is an interpolation table that is formatted for use in binary_c
"""

import copy
import pandas as pd
import numpy as np


def mint_interpolation_table_reader(
    input_file, remove_space=False, return_single_dataframe=False
):
    """
    Function that handles the whole readout process and returns dataframes

    return_single_dataframe;
        - combines the dataframes into 1 single big one
    """

    # Read out the file and save the header and data lines
    header_lines, data_lines = read_header_and_data(input_file)

    # Get the indices for the index/header columns and the column names
    (
        start_index_column_descriptors,
        end_index_column_descriptors,
        column_names,
    ) = get_indices_and_column_names(header_lines)

    # Get index dict and name list for the non-chebychev columns
    (
        non_chebychev_column_names_dict,
        non_chebychev_column_names_list,
        max_value_non_chebychev,
    ) = get_non_chebychev_header_info(column_names, remove_space=remove_space)

    # Get chebyshev column info
    (
        chebychev_column_names_dict,
        chebychev_column_names_list,
    ) = get_chebychev_header_info(column_names, remove_space=remove_space)

    # Get the data of the non-chebychev and the chebychev columns
    data_non_chebychev, data_chebychev = get_data_columns(
        data_lines, max_value_non_chebychev
    )

    # Read out the data and store it per mass
    non_chebychev_data_per_mass_dict = select_data_by_mass(
        data_non_chebychev, non_chebychev_column_names_dict
    )

    # new readout
    all_data_per_mass_dict = select_all_data_by_mass(
        data_lines, non_chebychev_column_names_dict, chebychev_column_names_dict
    )

    # Turn the data into dataframes
    dataframe_dict = turn_data_into_dataframes(
        non_chebychev_data_per_mass_dict, non_chebychev_column_names_list
    )

    # Turn all the data info dataframes
    dataframe_dict = turn_all_data_into_dataframes(all_data_per_mass_dict)

    if return_single_dataframe:
        # set up dataframe
        combined_df = pd.DataFrame()

        # loop over keys in dict
        for key in sorted(list(dataframe_dict.keys())):
            cur_df = dataframe_dict[key]

            # Merge into largere df
            combined_df = pd.concat([combined_df, cur_df], ignore_index=True)
        return combined_df, non_chebychev_column_names_dict

    else:
        return dataframe_dict, non_chebychev_column_names_dict


def read_header_and_data(filename):
    """
    Function to read out the datafile, and find header lines and datalines
    """

    #
    header_lines = []
    data_lines = []

    finished_headers = 0

    # read in data
    with open(filename, "r") as f:
        for line in f:
            if not finished_headers:
                header_lines.append(line.strip())
            else:
                data_lines.append(line)

            if line.startswith("# END HEADER"):
                finished_headers = 1

    #
    return header_lines, data_lines


def get_indices_column_descriptors(header_lines):
    """
    Function to get the first and last index of the column descriptors
    """

    # Find indices of column descriptors:
    start_index_column_descriptors = header_lines.index(
        "# START MINT COLUMN DESCRIPTORS"
    )
    end_index_column_descriptors = header_lines.index("# END MINT COLUMN DESCRIPTORS")

    return start_index_column_descriptors, end_index_column_descriptors


def get_indices_and_column_names(header_lines):
    """
    Function to get the indices and column names for the header
    """

    #
    (
        start_index_column_descriptors,
        end_index_column_descriptors,
    ) = get_indices_column_descriptors(header_lines)

    # Get the column name information
    column_names = copy.copy(
        header_lines[
            start_index_column_descriptors + 2 : end_index_column_descriptors - 1
        ]
    )

    #
    return start_index_column_descriptors, end_index_column_descriptors, column_names


def get_data_columns(data_lines, max_value_non_chebychev):
    """
    Function to read out the data
    """

    data_non_chebychev = []
    data_chebychev = []

    # Properly read out the data now:
    for data_line in data_lines:
        split = data_line.split()

        data_non_chebychev.append([float(el) for el in split[:max_value_non_chebychev]])
        data_chebychev.append(split[max_value_non_chebychev:])

    return data_non_chebychev, data_chebychev


def get_non_chebychev_header_info(column_names, remove_space=False):
    """
    Function to read out the non-chebychev (i.e. non-structural) information
    """

    #
    column_names_dict = {}
    column_names_list = []

    header_match = "# CHEBYSHEV" if not remove_space else "#CHEBYSHEV"
    comment_nums = 2 if not remove_space else 1

    #
    max_value_non_chebychev = 0  # counter for the maximum row
    for column_name in column_names:
        if not column_name.startswith(header_match):
            split = column_name[comment_nums:].split()
            name = split[0]
            value = int(split[1])

            column_names_dict[name] = value
            column_names_list.append(name)

            if max_value_non_chebychev < value:
                max_value_non_chebychev = value
                value = max_value_non_chebychev

    return column_names_dict, column_names_list, max_value_non_chebychev


def get_chebychev_header_info(column_names, remove_space=False):
    """
    Function to read out the chebychev column information
    """

    #
    column_names_dict = {}
    column_names_list = []

    header_match = "# CHEBYSHEV" if not remove_space else "#CHEBYSHEV"
    comment_nums = 2 if not remove_space else 1

    # loop over column names and select the chebyshev start and end index
    for column_name in column_names:
        if column_name.startswith(header_match):
            split = column_name[comment_nums:].split()
            name = split[0]

            # get indices
            indices = split[1].split("-")
            start_index = int(indices[0])
            end_index = int(indices[1])

            # store info
            column_names_dict[name] = {
                "start_index": start_index,
                "end_index": end_index,
            }
            column_names_list.append(name)

    return column_names_dict, column_names_list


def select_data_by_mass(data_non_chebychev, non_chebychev_column_names_dict):
    """
    Function to select the data by mass
    """

    # Set up a dictionary to hold the info for each mass
    non_chebychev_data_per_mass_dict = {}

    for line in data_non_chebychev:
        current_mass = line[non_chebychev_column_names_dict["MASS"] - 1]

        if not current_mass in non_chebychev_data_per_mass_dict:
            non_chebychev_data_per_mass_dict[current_mass] = []
        non_chebychev_data_per_mass_dict[current_mass].append(line)

    return non_chebychev_data_per_mass_dict


def select_all_data_by_mass(
    data_lines, non_chebychev_column_names_dict, chebychev_column_names_dict
):
    """
    Function to select the data per mass

    TODO: somehow i'm missing the last value
    """

    all_data_dict = {}

    # Loop over data line
    for data_line in data_lines:
        split = data_line.split()

        # Get current mass
        current_mass = split[non_chebychev_column_names_dict["MASS"] - 1]
        if not current_mass in all_data_dict:
            all_data_dict[current_mass] = []

        # store data in dict
        current_line_dict = {}

        # Loop over non-chebyshev column name dict
        for (
            non_chebychev_param,
            non_chebychev_index,
        ) in non_chebychev_column_names_dict.items():
            current_line_dict[non_chebychev_param] = float(
                split[non_chebychev_index - 1]
            )

        # Loop over chebyshev column name dict
        for (
            chebychev_param,
            chebychev_index_dict,
        ) in chebychev_column_names_dict.items():
            values = np.array(
                split[
                    chebychev_index_dict["start_index"]
                    - 1 : chebychev_index_dict["end_index"]
                ],
                dtype=float,
            )
            current_line_dict[chebychev_param] = values

        # Append to current mass data list
        all_data_dict[current_mass].append(current_line_dict)

    return all_data_dict


def turn_data_into_dataframes(
    non_chebychev_data_per_mass_dict, non_chebychev_column_names_list
):
    """
    Function to turn the data per mass dict into a dict containing dataframes
    """

    dataframe_dict = {}

    for mass in sorted(non_chebychev_data_per_mass_dict.keys()):
        dataframe_dict[mass] = pd.DataFrame(
            non_chebychev_data_per_mass_dict[mass],
            columns=non_chebychev_column_names_list,
        )

    return dataframe_dict


def turn_all_data_into_dataframes(all_data_per_mass_dict):
    """
    Function to turn all the data per mass dict into a dict containing dataframes
    """

    dataframe_dict = {}

    for mass in sorted(all_data_per_mass_dict.keys()):
        dataframe_dict[mass] = pd.DataFrame.from_dict(all_data_per_mass_dict[mass])

    return dataframe_dict


if __name__ == "__main__":
    #
    dataset_filename = "plotting_routines/example_data_natalie/MS/grid_Z2.00e-02.dat"

    # Get the dataframes and some info about the columns
    (df, non_chebychev_column_names_dict,) = mint_interpolation_table_reader(
        dataset_filename, remove_space=True, return_single_dataframe=True
    )

    print(df)
    print(df.keys())
    print(df["TIDAL_E2"].to_numpy())
    print(df.iloc[0].to_list())

    cheby_keys = [key for key in df.keys() if key.startswith("CHEBY")]
    for cheby_key in cheby_keys:
        print(cheby_key, len(df.iloc[0][cheby_key]))
