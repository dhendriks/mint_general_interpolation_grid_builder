#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  3 16:41:40 2022

@author: natalierees
"""
import matplotlib.pyplot as plt
import numpy as np
import os
import mesaPlot as mp
from pathlib import Path
import re

# Import kippenhahn plotter mkipp
os.chdir("/home/natalierees/mkipp-master")
import mkipp

# os.chdir("/home/natalierees/agb_grid")
# metallicity=0.014
# mass=6.0
# home=str(Path.home())
# grid_directory = '/agb_grid/Z0p014_grid_v1/'
# mass_text = re.sub('\.','p',str(mass))+'M'
# metallicity_text = 'Z'+re.sub('\.','p',str(metallicity))
# label = mass_text+'_'+metallicity_text

# logs = home+grid_directory+label+'/LOGS'

m = mp.MESA()

fig = plt.figure(figsize=(20, 10))
axis = plt.gca()
# axis.plot(models,mass_values,'r-',label = 'min beta location')
kipp_plot = mkipp.kipp_plot(
    mkipp.Kipp_Args(
        # logs_dirs = ['/home/natalierees/cheb_grid/Z0p02_grid_v9/2p05M_Z0p02/LOGS_CHeB_5p12Mf'],
        logs_dirs=["/home/natalierees/MINT_grids/Z0.02/GB/29.39/LOGS_GB_29.39"],
        # clean_data = True,
        # extra_history_cols = [],
        identifier="logT",
        contour_colormap=plt.get_cmap("YlGnBu"),
        levels=np.arange(0.0, 1.001, 0.01),
        # log_levels = True,
        # num_levels = 20,
        # xaxis = "model_number",
        xaxis="star_age",
        # xaxis_divide = 1.0,
        log10_on_data=False,
        # time_units = "yr",
        # xlims = (12000,13500),
        yaxis="mass",
        yaxis_normalize=False,
        show_conv=True,
        show_therm=False,
        show_semi=True,
        show_over=True,
        show_rot=False,
        core_masses=["He"],
        yresolution=100000,
        mass_tolerance=0.000001,
        radius_tolerance=0.000001,
        decorate_plot=True,
        show_plot=True,
        save_file=False,
    ),
    axis=axis,
)
# bar = plt.colorbar(kipp_plot.contour_plot,pad=0.05)
# bar.set_label('log10 T')
# axis.set_xlabel('Model number')
# axis.legend()
axis.set_ylabel("Mass (solar masses)")
# axis.set_ylim(1.0235,1.0265)
# plt.savefig('logTKipp.pdf',dpi=200)
plt.show()
