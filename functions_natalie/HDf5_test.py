#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 15:26:11 2023

@author: natalierees
"""

from nugridpy import nugridse as se

see_fr = se.se("/home/natalierees/agb_grid/Z0p014_grid_v18/3p0M_Z0p014/HDF5")

# example plot
see_fr.plot("model_number", "logL")

# plot of profile quantity
see_fr.plot("mass", "temperature", fname=100)
