"""
Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
    https://www.sphinx-doc.org/en/master/usage/configuration.html
    https://brendanhasz.github.io/2019/01/05/sphinx.html
    https://www.sphinx-doc.org/en/1.5/ext/example_google.html


https://stackoverflow.com/questions/22256995/restructuredtext-in-sphinx-and-docstrings-single-vs-double-back-quotes-or-back

TODO: https://stackoverflow.com/questions/64443832/sorting-table-with-rst-and-python-sphinx-in-html-output implement datatables
"""

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys

import m2r2
from git import Repo


from mint_general_interpolation_grid_builder.functions.description_documentation_functions.build_interpolation_table_builder_description_pages import (
    build_MINT_interpolation_table_builder_description_pages,
)
from mint_general_interpolation_grid_builder.functions.description_documentation_functions.build_MESA_grid_runner_description_pages import (
    build_MINT_MESA_grid_runner_description_pages,
)

from mint_general_interpolation_grid_builder.classes.Config import Config


from mint_general_interpolation_grid_builder.MINT.config.mint_settings import (
    mint_defaults,
)
from mint_general_interpolation_grid_builder.MINT.config.table_columns import (
    table_columns,
)
from mint_general_interpolation_grid_builder.MINT.config.mint_inlists import inlists


this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

#
def patched_m2r2_setup(app):
    """
    Function to handle the markdown parsing better
    """

    try:
        return current_m2r2_setup(app)
    except (AttributeError):
        app.add_source_suffix(".md", "markdown")
        app.add_source_parser(m2r2.M2RParser)
    return dict(
        version=m2r2.__version__,
        parallel_read_safe=True,
        parallel_write_safe=True,
    )


# Include paths for python code
sys.path.insert(0, os.path.abspath("."))

# include paths for c code
cautodoc_root = os.path.abspath("../../")

# -- Project information -----------------------------------------------------
project = "MINT interpolation grids"
copyright = "2024, Natalie Rees, David Hendriks,   Arman Aryaeipour, Giovanni Mirouh, Robert Izzard"
author = (
    "Natalie Rees, David Hendriks,   Arman Aryaeipour, Giovanni Mirouh, Robert Izzard"
)


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "hawkmoth",
    "m2r2",
    "sphinx_rtd_theme",
    "sphinx_autodoc_typehints",  # https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html
    "nbsphinx",
    "sphinx_math_dollar",
    "sphinx.ext.mathjax",
]

mathjax3_config = {
    "tex": {
        "inlineMath": [["\\(", "\\)"]],
        "displayMath": [["\\[", "\\]"]],
    }
}

# Napoleon settings
napoleon_google_docstring = (
    True  # https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
)
napoleon_numpy_docstring = False
napoleon_include_init_with_doc = False
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True

source_suffix = [".rst", ".md"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# html_theme = "alabaster"
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    "css/custom.css",
]

#
html_baseurl = "/docs/"

"""Patching m2r2"""
current_m2r2_setup = m2r2.setup

#
m2r2.setup = patched_m2r2_setup

###########
# Generate the interpolation table description pages
interpolation_table_builder_description_page_dir = os.path.join(
    this_file_dir, "stellar_type_description_pages"
)
build_MINT_interpolation_table_builder_description_pages(
    output_dir=interpolation_table_builder_description_page_dir
)

###########
# Generate configuration description table
config_instance = Config()
config_instance.write_configuration_description_rst(
    os.path.join(this_file_dir, "config_description.rst")
)

#
config = {**mint_defaults, **table_columns}
config["inlists"] = inlists
config["grid_directory"] = "/tmp/MINT_docs"

###########
# Generate the MESA configuration description table
MESA_grid_runner_description_page_dir = os.path.join(
    this_file_dir, "MESA_grid_runner_description_pages"
)
build_MINT_MESA_grid_runner_description_pages(
    output_dir=MESA_grid_runner_description_page_dir,
    config=config,
    cur_dir=this_file_dir,
)
