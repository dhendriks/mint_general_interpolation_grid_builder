MS MINT_GRID
============
Testing main sequence




Input scalars MS table
----------------------
Below we list the input scalar parameters for the MS MINT grid.

.. list-table:: Input scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - MASS
     - Total stellar mass. Unit: [$\mathrm{M_{\odot}}$].
   * - CENTRAL_HYDROGEN
     - Central hydrogen mass fraction Xc.

Output scalars MS table
-----------------------
Below we list the output scalar parameters for the MS MINT grid.

.. list-table:: Output scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - RADIUS
     - Photospheric radius. Unit: [$\mathrm{R_{\odot}}$].
   * - LUMINOSITY
     - Photosphere luminosity. Unit: [$\mathrm{L_{\odot}}$].
   * - LUMINOSITY_DIV_EDDINGTON_LUMINOSITY
     - Luminosity divided by the eddington luminosity.
   * - NEUTRINO_LUMINOSITY
     - Power emitted in neutrinos, nuclear and thermal. Unit: [$\mathrm{L_{\odot}}$].
   * - AGE
     - Model age. Unit: [$\mathrm{yr}$].
   * - HELIUM_CORE_MASS_FRACTION
     - Fractional He core mass (Mc/M), Xc<1d-2.
   * - CENTRAL_DEGENERACY
     - The electron chemical potential at the central mesh point, in units of k*T. Unit: [$\mathrm{J}$].
   * - CONVECTIVE_CORE_MASS_FRACTION
     - Relative convective core mass (Mcore/Mstar).
   * - CONVECTIVE_CORE_RADIUS_FRACTION
     - Relative convective core radius (Rcore/Rstar).
   * - CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION
     - Relative mass cord of main conevctive region in core including overshooting & semiconvection mixing.
   * - CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION
     - Relative radius cord of main conevctive region in core including overshooting & semiconvection mixing.
   * - CONVECTIVE_ENVELOPE_MASS_FRACTION
     - Relative convective envelope mass (Menv/Mstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_FRACTION
     - Relative convective envelope radius (bottom envelope coordinate) (Renv/Rstar).
   * - K2
     - Apsidal constant NOT THE moment of inertia factor BEWARE.
   * - TIDAL_E2
     - Tidal E2 from Zahn.
   * - TIDAL_E_FOR_LAMBDA
     - Tidal E for Zahns lambda.
   * - MOMENT_OF_INERTIA_FACTOR
     - Beta^2 from Claret AA 541, A113 (2012)= I/(MR^2).
   * - TIMESCALE_KELVIN_HELMHOLTZ
     - Kelvin-Helmholtz timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_DYNAMICAL
     - Dynamical timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_NUCLEAR
     - Nuclear timescale. Unit: [$\mathrm{yr}$].
   * - MEAN_MOLECULAR_WEIGHT_CORE
     - Mean molecular weight at central mesh point.
   * - MEAN_MOLECULAR_WEIGHT_AVERAGE
     - Mean molecular weight average through star.
   * - FIRST_DERIVATIVE_CENTRAL_HYDROGEN
     - DXc/dt. Unit: [$\mathrm{yr^{-1}}$].
   * - SECOND_DERIVATIVE_CENTRAL_HYDROGEN
     - D2Xc/dt2. Unit: [$\mathrm{yr^{-2}}$].
   * - WARNING_FLAG
     - Warning flag for binary_c, flag = 1 if data warning, flag = 0 if data reliable.

Output vectors MS table
-----------------------
Below we list the output vector parameters for the MS MINT grid. These structural quantities are determined on a chebychev mass grid.

.. list-table:: Output vectors
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - CHEBYSHEV_MASS
     - Mass on Chebyshev grid. Unit: [$\mathrm{M_{\odot}}$].
   * - CHEBYSHEV_TEMPERATURE
     - Temperature on Chebyshev grid. Unit: [$\mathrm{K}$].
   * - CHEBYSHEV_DENSITY
     - Density on Chebyshev grid. Unit: [$\mathrm{g\,cm^{-3}}$].
   * - CHEBYSHEV_TOTAL_PRESSURE
     - Total pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_GAS_PRESSURE
     - Gas pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_RADIUS
     - Radius on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_GAMMA1
     - Adiabatic Gamma1 on Chebyshev grid.
   * - CHEBYSHEV_PRESSURE_SCALE_HEIGHT
     - Pressure scale height on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_DIFFUSION_COEFFICIENT
     - Eulerian diffusion coefficient on Chebyshev grid. Unit: [$\mathrm{cm^{2}\,s^{-1}}$].
   * - CHEBYSHEV_HELIUM_MASS_FRACTION
     - Helium-4 mass fraction on Chebyshev grid.
   * - CHEBYSHEV_HYDROGEN_MASS_FRACTION
     - Hydrogen-1 mass fraction on Chebyshev grid.
