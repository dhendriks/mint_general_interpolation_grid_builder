TPAGB MINT_GRID
===============
Models evolved from the first thermal pulse to the start of the Post-AGB




Input scalars TPAGB table
-------------------------
Below we list the input scalar parameters for the TPAGB MINT grid.

.. list-table:: Input scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - MASS
     - Total stellar mass. Unit: [$\mathrm{M_{\odot}}$].
   * - CARBON_CORE_MASS
     - Co core mass (Msun).
   * - HELIUM_CORE_MASS
     - Fractional He core mass.

Output scalars TPAGB table
--------------------------
Below we list the output scalar parameters for the TPAGB MINT grid.

.. list-table:: Output scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - RADIUS
     - Photospheric radius. Unit: [$\mathrm{R_{\odot}}$].
   * - LUMINOSITY
     - Photosphere luminosity. Unit: [$\mathrm{L_{\odot}}$].
   * - AGE
     - Model age. Unit: [$\mathrm{yr}$].
   * - HELIUM_CORE_RADIUS
     - Relative core radius (Rcore/Rstar).
   * - FIRST_DERIVATIVE_CARBON_CORE_MASS
     - DMc/dt (Msun/yr). Unit: [$\mathrm{M_{\odot}\,yr^{-1}}$].
   * - FIRST_DERIVATIVE_HELIUM_CORE_MASS
     - DMc/dt (Msun/yr). Unit: [$\mathrm{M_{\odot}\,yr^{-1}}$].
   * - CONVECTIVE_ENVELOPE_MASS_BOT
     - Relative mass coord of bottom of convective envelope  (Menv,bot/Mstar).
   * - CONVECTIVE_ENVELOPE_MASS_TOP
     - Relative mass coord of top of convective envelope  (Menv,top/Mstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_BOT
     - Relative radius coord of bottom of convective envelope (Renv,bot/Rstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_TOP
     - Relative radius coord of top of convective envelope (Renv,top/Rstar).
   * - TIMESCALE_KELVIN_HELMHOLTZ
     - Kelvin-Helmholtz timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_DYNAMICAL
     - Dynamical timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_NUCLEAR
     - Nuclear timescale. Unit: [$\mathrm{yr}$].
   * - MEAN_MOLECULAR_WEIGHT_AVERAGE
     - Mean molecular weight average through star.
   * - MEAN_MOLECULAR_WEIGHT_CORE
     - Mean molecular weight at central mesh point.
   * - K2
     - Apsidal constant NOT THE moment of inertia factor BEWARE.
   * - TIDAL_E2
     - Tidal E2 from Zahn.
   * - TIDAL_E_FOR_LAMBDA
     - Tidal E for Zahns lambda.
   * - MOMENT_OF_INERTIA_FACTOR
     - Beta^2 from Claret AA 541, A113 (2012)= I/(MR^2).
   * - WARNING_FLAG
     - Warning flag for binary_c, flag = 1 if data good, flag = 0 if data unreliable.

Output vectors TPAGB table
--------------------------
Below we list the output vector parameters for the TPAGB MINT grid. These structural quantities are determined on a chebychev mass grid.

.. list-table:: Output vectors
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - CHEBYSHEV_TEMPERATURE
     - Temperature on Chebyshev grid. Unit: [$\mathrm{K}$].
   * - CHEBYSHEV_DENSITY
     - Density on Chebyshev grid. Unit: [$\mathrm{g\,cm^{-3}}$].
   * - CHEBYSHEV_TOTAL_PRESSURE
     - Total pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_GAS_PRESSURE
     - Gas pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_RADIUS
     - Radius on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_GAMMA1
     - Adiabatic Gamma1 on Chebyshev grid.
   * - CHEBYSHEV_PRESSURE_SCALE_HEIGHT
     - Pressure scale height on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
