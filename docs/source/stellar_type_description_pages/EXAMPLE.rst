EXAMPLE MINT_GRID
=================
Description EXAMPLE stellar type




Input scalars EXAMPLE table
---------------------------
Below we list the input scalar parameters for the EXAMPLE MINT grid.

.. list-table:: Input scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - MASS
     - Total stellar mass. Unit: [$\mathrm{M_{\odot}}$].
   * - RADIUS
     - Photospheric radius. Unit: [$\mathrm{R_{\odot}}$].

Output scalars EXAMPLE table
----------------------------
Below we list the output scalar parameters for the EXAMPLE MINT grid.

.. list-table:: Output scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - LUMINOSITY
     - Photosphere luminosity. Unit: [$\mathrm{L_{\odot}}$].
   * - HELIUM_CORE_MASS
     - Fractional He core mass.

Output vectors EXAMPLE table
----------------------------
Below we list the output vector parameters for the EXAMPLE MINT grid. These structural quantities are determined on a chebychev mass grid.

.. list-table:: Output vectors
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - CHEBYSHEV_MASS
     - Mass on Chebyshev grid. Unit: [$\mathrm{M_{\odot}}$].
