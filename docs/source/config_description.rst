Configuration description
=========================


.. list-table::  Config description
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - CHeB_do_predictive_mix
     -
   * - CHeB_grid_directory
     -
   * - CHeB_use_Ledoux_criterion
     -
   * - EAGB_alpha_semiconvection
     -
   * - EAGB_delta_lgL_He_limit
     -
   * - EAGB_grid_directory
     -
   * - EAGB_overshoot_f
     -
   * - EAGB_overshoot_prescription
     -
   * - EAGB_overshoot_scheme
     -
   * - EAGB_use_Ledoux_criterion
     -
   * - EAGB_use_vassilidadis_wood_wind
     -
   * - GB_do_conv_premix
     -
   * - GB_grid_directory
     -
   * - MS_alpha_semiconvection
     -
   * - MS_do_conv_premix
     -
   * - MS_grid_directory
     -
   * - MS_mass_change
     -
   * - MS_overshoot_f
     -
   * - MS_overshoot_f0
     -
   * - MS_overshoot_prescription
     -
   * - MS_overshoot_scheme
     -
   * - MS_use_Ledoux_criterion
     -
   * - MXP_target
     -
   * - TPAGB_delta_lgL_He_limit
     -
   * - TPAGB_overshoot_f0_CE
     -
   * - TPAGB_overshoot_f0_IPCZ
     -
   * - TPAGB_overshoot_f_CE
     -
   * - TPAGB_overshoot_f_IPCZ
     -
   * - TPAGB_overshoot_scheme_CE
     -
   * - TPAGB_overshoot_scheme_IPCZ
     -
   * - TPAGB_use_Ledoux_criterion
     -
   * - TPAGB_use_vassilidadis_wood_wind
     -
   * - VCT_target
     -
   * - aesopus_filename
     -
   * - avoid_HRI
     -
   * - binary_grid
     -
   * - caches_dir
     -
   * - copy_models_from_previous_evol_phase
     -
   * - delta_XH_cntr_limit
     -
   * - delta_XHe_cntr_limit
     -
   * - delta_lg_XHe_cntr_limit
     -
   * - double_exponential_overshoot_D2
     -
   * - double_exponential_overshoot_f2
     -
   * - envelope_mass_limit
     -
   * - eureka_version
     -
   * - grid_directory
     -
   * - kap_lowT_prefix
     -
   * - max_queue_size
     -
   * - mesa_version
     -
   * - metallicity
     -
   * - num_cores_for_MESA
     -
   * - num_processes
     -
   * - save_hdf5_files
     -
   * - table_name
     -
   * - use_alpha_mlt
     -
   * - workload_manager
     -
