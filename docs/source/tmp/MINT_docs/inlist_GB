&star_job

	read_extra_star_job_inlist(1) = .true.
	extra_star_job_inlist_name(1) = 'inlist_common'

	load_saved_model = .true.
	load_model_filename = 'TAMS.mod'
	save_model_when_terminate = .true.
	save_model_filename = 'CHeB.mod'

	required_termination_code_string = 'HB_limit'

	history_columns_file = ''
	profile_columns_file = ''

	set_initial_age = .true.
	initial_age = 0
	set_initial_model_number = .true.
	initial_model_number = 0
	set_initial_cumulative_energy_error = .true.
	new_cumulative_energy_error = 0d0

	set_initial_dt = .true.
	years_for_initial_dt = 1d1

	auto_extend_net = .true.
	h_he_net = 'basic.net'
	co_net = 'co_burn.net'
	adv_net = 'approx21.net'

	change_initial_v_flag = .true.
	change_v_flag = .true.
	new_v_flag = .true.

/ !end of star_job namelist

&eos

	read_extra_eos_inlist(1) = .true.
	extra_eos_inlist_name(1) = 'inlist_common'

/ !end of eos namelist

&kap

	read_extra_kap_inlist(1) = .true.
	extra_kap_inlist_name(1) = 'inlist_common'

/ !end of kap namelist

&controls

	x_integer_ctrl(1) = 2 ! part number

	read_extra_controls_inlist(1) = .true.
	extra_controls_inlist_name(1) = 'inlist_common'

	log_directory = 'LOGS_GB'
	photo_directory = 'photos_GB'

	profile_model = 1 ! save profile for first model

	num_trace_history_values = 6
	trace_history_value_name(1) = 'log_L_div_Ledd'
	trace_history_value_name(2) = 'min_beta'
	trace_history_value_name(3) = 'alpha_mlt_surface'
	trace_history_value_name(4) = 'alpha_mlt_max'
	trace_history_value_name(5) = 'mass_conv_core'
	trace_history_value_name(6) = 'hydrogen_exhausted_core_mass'

	HB_limit = 0.985

	use_other_alpha_mlt = .true.

	min_timestep_limit = 1d0

	max_model_number = 50000

	energy_eqn_option = 'dedt'
	limit_for_rel_error_in_energy_conservation = 1d99
	hard_limit_for_rel_error_in_energy_conservation = 1d99
	max_abs_rel_run_E_err = 0.1d0

	overshoot_scheme(1) = 'exponential'
	overshoot_zone_type(1) = 'nonburn'
	overshoot_zone_loc(1) = 'shell'
	overshoot_bdy_loc(1) = 'any'
	overshoot_f(1) = 0.0174
	overshoot_f0(1) = 0.00174
	overshoot_D0(1) = 0d0
	overshoot_Delta0(1) = 1d0

	do_conv_premix = .false.
	conv_premix_avoid_increase = .true.
	conv_premix_time_factor = 0
	conv_premix_fix_pgas = .true.
	conv_premix_dump_snapshots = .false.

	use_Ledoux_criterion = .true.

	alpha_semiconvection = 0
	semiconvection_option = 'Langer_85 mixing; gradT = gradr'

	num_cells_for_smooth_gradL_composition_term = 20
	threshold_for_smooth_gradL_composition_term = 0.02

	use_superad_reduction = .true.
	superad_reduction_Gamma_limit = 0.5
	superad_reduction_Gamma_limit_scale = 5
	superad_reduction_Gamma_inv_scale = 5
	superad_reduction_diff_grads_limit = 1d-2
	superad_reduction_limit = -1d0

	photo_interval = 1000
	profile_interval = 0
	history_interval = 50
	terminal_interval = 10

	write_header_frequency = 100

	timestep_factor_for_retries = 0.8
	min_timestep_factor = 0.9
	max_timestep_factor = 1.05d0

	retry_hold = 5

	redo_limit = -1

	relax_hard_limits_after_retry = .false.

	scale_max_correction = 0.01d0

	corr_coeff_limit = 1d-1
	ignore_min_corr_coeff_for_scale_max_correction = .true.
	ignore_species_in_max_correction = .true.
	max_resid_jump_limit = 1d99

	restore_mesh_on_retry = .true.
	num_steps_to_hold_mesh_after_retry = 5
	mesh_delta_coeff_for_highT = 1.0d0
	mesh_Pgas_div_P_exponent = 0.5d0

	max_dq = 0.005
	min_dq_for_xa = 1d-5

	max_allowed_nz = 50000
	max_logT_for_k_below_const_q = 100
	max_q_for_k_below_const_q = 0.995
	min_q_for_k_below_const_q = 0.995
	max_logT_for_k_const_mass = 100
	max_q_for_k_const_mass = 0.99
	min_q_for_k_const_mass = 0.99

/ !end of controls namelist
