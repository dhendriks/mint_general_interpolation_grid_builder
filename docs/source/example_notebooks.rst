Example notebooks
=================
We have a set of notebooks that explain and show the usage of the binarycpython features. The notebooks are also stored in the `examples directory in the repository <https://gitlab.com/binary_c/binary_c-python/-/tree/master/examples>`_

The order of the notebooks below is more or less the recommended order to read. The last couple of notebooks are example usecases

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    examples/generating_MINT_data.ipynb
    examples/testing_MINT_tables.ipynb
    examples/using_MINT_data.ipynb
    examples/creating_custom_MESA_grid_runners.ipynb
    examples/postprocessing_MESA_models.ipynb
