.. mint_interpolation_grid_builder documentation master file, created by
   david on 2023-01-31.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MINT interpolation grids documentation!
==================================================
.. mdinclude:: ../../README.md

.. include:: status.rst

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   readme_link
   status
   MINT_table_descriptions
   interpolation_table_builders
   config_description
   MESA_grid_runners
   example_notebooks
   Visit the GitLab repo <https://gitlab.com/binary_c/binary_c-python/>
   Submit an issue <https://gitlab.com/binary_c/binary_c-python/-/issues/new>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
