EAGB MINT_GRID
==============
Testing




Input scalars EAGB table
------------------------
Below we list the input scalar parameters for the EAGB MINT grid.

.. list-table:: Input scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - MASS
     - Total stellar mass. Unit: [$\mathrm{M_{\odot}}$].
   * - CENTRAL_DEGENERACY
     - The electron chemical potential at the central mesh point, in units of k*T. Unit: [$\mathrm{J}$].
   * - CARBON_CORE_MASS_FRACTION
     - Co core mass (Msun).

Output scalars EAGB table
-------------------------
Below we list the output scalar parameters for the EAGB MINT grid.

.. list-table:: Output scalars
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - RADIUS
     - Photospheric radius. Unit: [$\mathrm{R_{\odot}}$].
   * - LUMINOSITY
     - Photosphere luminosity. Unit: [$\mathrm{L_{\odot}}$].
   * - LUMINOSITY_DIV_EDDINGTON_LUMINOSITY
     - Luminosity divided by the eddington luminosity.
   * - NEUTRINO_LUMINOSITY
     - Power emitted in neutrinos, nuclear and thermal. Unit: [$\mathrm{L_{\odot}}$].
   * - HYDROGEN_LUMINOSITY
     - Total thermal power from hydrogen burning. Unit: [$\mathrm{L_{\odot}}$].
   * - HELIUM_LUMINOSITY
     - Total thermal power from triple-alpha, excluding neutrinos. Unit: [$\mathrm{L_{\odot}}$].
   * - AGE
     - Model age. Unit: [$\mathrm{yr}$].
   * - HYDROGEN_EXHUASTED_CORE_MASS_FRACTION
     - Fractional H exhuasted core mass (Mc/M), Xc<1d-6.
   * - HELIUM_CORE_RADIUS_FRACTION
     - Relative core radius (Rcore/Rstar).
   * - HELIUM_CORE_MASS_FRACTION
     - Fractional He core mass (Mc/M), Xc<1d-2.
   * - CARBON_CORE_RADIUS_FRACTION
     - Co core mass (Msun).
   * - CONVECTIVE_CORE_MASS_FRACTION
     - Relative convective core mass (Mcore/Mstar).
   * - CONVECTIVE_CORE_RADIUS_FRACTION
     - Relative convective core radius (Rcore/Rstar).
   * - CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION
     - Relative mass cord of main conevctive region in core including overshooting & semiconvection mixing.
   * - CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION
     - Relative radius cord of main conevctive region in core including overshooting & semiconvection mixing.
   * - CONVECTIVE_ENVELOPE_MASS_FRACTION
     - Relative convective envelope mass (Menv/Mstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_FRACTION
     - Relative convective envelope radius (bottom envelope coordinate) (Renv/Rstar).
   * - CONVECTIVE_ENVELOPE_MASS_TOP_FRACTION
     - Relative mass coord of top of convective envelope  (Menv,top/Mstar).
   * - CONVECTIVE_ENVELOPE_RADIUS_TOP_FRACTION
     - Relative radius coord of top of convective envelope (Renv,top/Rstar).
   * - K2
     - Apsidal constant NOT THE moment of inertia factor BEWARE.
   * - TIDAL_E2
     - Tidal E2 from Zahn.
   * - TIDAL_E_FOR_LAMBDA
     - Tidal E for Zahns lambda.
   * - MOMENT_OF_INERTIA_FACTOR
     - Beta^2 from Claret AA 541, A113 (2012)= I/(MR^2).
   * - HELIUM_CORE_MOMENT_OF_INERTIA_FACTOR
     - Beta^2 from Claret AA 541, A113 (2012)= I/(MR^2) up to the helium core boundary.
   * - CARBON_CORE_MOMENT_OF_INERTIA_FACTOR
     - Beta^2 from Claret AA 541, A113 (2012)= I/(MR^2) up to the carbon core boundary.
   * - TIMESCALE_KELVIN_HELMHOLTZ
     - Kelvin-Helmholtz timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_DYNAMICAL
     - Dynamical timescale. Unit: [$\mathrm{yr}$].
   * - TIMESCALE_NUCLEAR
     - Nuclear timescale. Unit: [$\mathrm{yr}$].
   * - MEAN_MOLECULAR_WEIGHT_CORE
     - Mean molecular weight at central mesh point.
   * - MEAN_MOLECULAR_WEIGHT_AVERAGE
     - Mean molecular weight average through star.
   * - FIRST_DERIVATIVE_CENTRAL_DEGENERACY
     - First derivative of the electron chemical potential at the central mesh point with respect to time. Unit: [$\mathrm{J\,yr^{-1}}$].
   * - SECOND_DERIVATIVE_CENTRAL_DEGENERACY
     - Second derivative of the electron chemical potential at the central mesh point with respect to time. Unit: [$\mathrm{J\,yr^{-2}}$].
   * - FIRST_DERIVATIVE_CARBON_CORE_MASS_FRACTION
     - First derivative of the carbon core mass fraction with respect to time. Unit: [$\mathrm{yr^{-1}}$].
   * - SECOND_DERIVATIVE_CARBON_CORE_MASS_FRACTION
     - Second derivative of the carbon core mass fraction with respect to time. Unit: [$\mathrm{yr^{-2}}$].
   * - CENTRAL_CARBON
     - Central carbon-12 mass fraction XC.
   * - CENTRAL_OXYGEN
     - Central oxygen-16 mass fraction XO.
   * - CENTRAL_NEON
     - Central neon-20 mass fraction XNe.
   * - ONSET_THERMAL_PULSES_FLAG
     - Flag for the onset of thermal pulses, = 1 if pulses started.
   * - WARNING_FLAG
     - Warning flag for binary_c, flag = 1 if data warning, flag = 0 if data reliable.
   * - INITIAL_MASS
     - Initial mass used to form model, allows reconstruction of tracks.

Output vectors EAGB table
-------------------------
Below we list the output vector parameters for the EAGB MINT grid. These structural quantities are determined on a chebychev mass grid.

.. list-table:: Output vectors
   :widths: 25, 75
   :header-rows: 1

   * - Parameter
     - Description
   * - CHEBYSHEV_MASS
     - Mass on Chebyshev grid. Unit: [$\mathrm{M_{\odot}}$].
   * - CHEBYSHEV_TEMPERATURE
     - Temperature on Chebyshev grid. Unit: [$\mathrm{K}$].
   * - CHEBYSHEV_DENSITY
     - Density on Chebyshev grid. Unit: [$\mathrm{g\,cm^{-3}}$].
   * - CHEBYSHEV_TOTAL_PRESSURE
     - Total pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_GAS_PRESSURE
     - Gas pressure on Chebyshev grid. Unit: [$\mathrm{dyn\,cm^{-2}}$].
   * - CHEBYSHEV_RADIUS
     - Radius on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_GAMMA1
     - Adiabatic Gamma1 on Chebyshev grid.
   * - CHEBYSHEV_PRESSURE_SCALE_HEIGHT
     - Pressure scale height on Chebyshev grid. Unit: [$\mathrm{R_{\odot}}$].
   * - CHEBYSHEV_DIFFUSION_COEFFICIENT
     - Eulerian diffusion coefficient on Chebyshev grid. Unit: [$\mathrm{cm^{2}\,s^{-1}}$].
   * - CHEBYSHEV_HELIUM_MASS_FRACTION
     - Helium-4 mass fraction on Chebyshev grid.
   * - CHEBYSHEV_HYDROGEN_MASS_FRACTION
     - Hydrogen-1 mass fraction on Chebyshev grid.
