# General walkthrough
In this document we explain the different main components of the MINT code base, and show how to implement new parts for new stellar phases.

## Main parts of the code
There are three main parts of the code:
- MESA grid runners: classes responsible for generating the MESA models
- Interpolation table builders: classes responsible for turning the MESA models into interpolation tables
- run_grid entries: entries

A fourth part is coming soon:
- MESA grid post-processing runners: classes to post-process MESA models

### MESA grid runners:
The MESA grid runners are classes that handle generating the MESA models. These broadly consist of two main parts:
- [ ] Functionality to prepare the inlists, run_star_extras and other MESA related files that are required for this grid of models
- [ ] Functionality to evolve the MESA grid through some queueing system

## Walkthrough for new implementations
We will now explain how to implement new parts of the code, to cover new stellar phases.

The steps to fully implement the components for a new stellar phase are:
- [ ] Create a new MESA grid runner class
    + [ ] Add relevant configuration to default config file
- [ ] Add an instance of the new MESA grid runner class to XXX
- [ ] (optionally) create a new MESA grid post-processing runner
    + [ ] Add relevant configuration to default config file
- [ ] (optionally) Add an instance of the new MESA grid post-processing runner to XXX
- [ ] Create a new Interpolation table builder class
    + [ ] Add relevant configuration to default config file
- [ ] Add an instance of the new Interpolation table builder class to XXX
- [ ] Add instance creation and entry point function calls to run_grid function and include flags to control

TODO: clean above into better structure
