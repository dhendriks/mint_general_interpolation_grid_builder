#!/usr/bin/python3


"""
Main config for the MINT project
"""

import os
import numpy as np

main_config = {
    #################################
    # Unsorted
    "handle_track_intersection": False,
    "handle_track_intersection_plotting": False,
    #################################
    # email notifications
    "email_notifications_enabled": False,
    "email_notifications_APP_password": os.getenv("MINT_NOTIFICATION_APP_PW"),
    "email_notifications_recipients": [],
    #################################
    # system properties
    "grids_root_directory": "/home/natalierees/MINT_grids/",
    "archive_previous_grid": False,  # moves existing grid with same name and metallicity to archive
    "binary_grid": False,
    ##################################
    # MESA setup
    "module_name": "mint",
    "mesa_version": "15140",
    "num_cores_for_MESA": 12,  # number of processors used by MESA
    "MESA_output_directory": "/users/nr00492/parallel_scratch/MINT_grids",  # path from root
    "caches_dir": "/users/nr00492/parallel_scratch/caches",  # path from home
    "auto_submit": False,
    "save_photos_at_grid_points": False,
    "nice_priority": 0,
    "workload_manager": "slurm",
    "handle_track_intersection": False,
    "restart_unfinished_runs": False,
    #################################
    # table building options
    "max_queue_size": 10,
    "max_result_queue_size": 10,
    "num_processes": 10,
    "plot_all_models": True,
    #################################
    # global grid properties
    "metallicity": 0.02,
    "kap_lowT_prefix": "lowT_fa05_a09p",  # "lowT_fa05_a09p",
    "aesopus_filename": "AESOPUS_AGSS09.h5",  # only used if kap_lowT_prefix = "AESOPUS"
    "mixing_length_alpha": "1.931",  # Cinquegrana & Joyce 2022, solar calibration with AESOPUS opacities
    "initial_y": "-1",  # initial helium mass fraction
    "mesh_delta_coeff": "1.0",
    "prune_bad_cz_min_Hp_height": "0",
    "he_core_boundary_h1_fraction": "0.01",
    "eta_center_limit": 50,
    "eos_extra_controls": "",
    ################################
    # slurm options
    "slurm_partition": "shared",
    "MS_MESA_run_max_hours": 24,
    "MC_MESA_run_max_hours": 24,
    "GB_MESA_run_max_hours": 48,
    "CHeB_MESA_run_max_hours": 48,
    "EAGB_MESA_run_max_hours": 48,
    "TPAGB_MESA_run_max_hours": 168,
    "CO_WD_MESA_run_max_hours": 72,
    "MESA_setup_code": """export MESA_DIR=/home/natalierees/mesa-r15140
export MESASDK_ROOT=/home/natalierees/mesasdk
source $MESASDK_ROOT/bin/mesasdk_init.sh""",
    "tabulation_setup_code": """module load anaconda3/4.3.1
source /users/nr00492/mint/bin/activate""",
    "extra_sbatch_commands": """ """,  # e.g. #SBATCH --exclude=node[32,39], options passed here will override defaults
    ##################################
    # for massive stars that hit Ledd (MESA VI paper)
    "use_superad_reduction": ".true.",
    "superad_reduction_Gamma_limit": "0.5",
    "superad_reduction_Gamma_limit_scale": "5",
    "superad_reduction_Gamma_inv_scale": "5",
    "superad_reduction_diff_grads_limit": "1d-2",
    "superad_reduction_limit": "-1d0",
    ################################
    # MS specific controls
    "initial_masses": list(
        np.append(
            np.linspace(0.08, 2.53, num=50),
            np.logspace(np.log10(2.58), np.log10(1000), num=50),
        )
    ),
    "save_MS_model_at_core_formation": ".false.",
    "MS_terminate_h1_mass_fraction": 0,
    "delta_XH_cntr_limit": "0.0005d0",
    "delta_lg_XH_cntr_limit": "0.01d0",
    "MS_mass_change": "-1d-90",
    "MS_core_overshoot_prescription": "jermyn22",  # set to empty for mass-indepdendent overshooting
    "MS_core_overshoot_scheme": "step",
    "MS_core_overshoot_f": "0.01",  # only used if MS_overshoot_prescription = ""
    "MS_core_overshoot_f0": "0.001",  # only used if MS_overshoot_prescription = ""
    "MS_envelope_overshoot_scheme": "exponential",
    "MS_envelope_overshoot_f": "0.0174",  # choi et al 2016
    "MS_envelope_overshoot_f0": "0.00174",
    "MS_do_conv_premix": ".true.",
    "MS_use_Ledoux_criterion": ".true.",
    "MS_alpha_semiconvection": "0",
    "MS_extra_inlist_controls": "",
    "MS_hist_columns": "",
    "MS_prof_columns": "",
    "MS_save_model_filename": "TAMS.mod",
    ###############################
    # GB specific controls
    "min_initial_mass_for_GB": "0.4",
    "HB_limit": "0",
    "GB_max_model_number": "50000",
    "GB_hydro_on": ".true.",
    "GB_terminate_helium_burning_limit": "1",
    "GB_envelope_overshoot_scheme": "exponential",
    "GB_envelope_overshoot_f": "0.0174",  # choi et al 2016
    "GB_envelope_overshoot_f0": "0.00174",
    "GB_do_conv_premix": ".false.",
    "GB_use_Ledoux_criterion": ".true.",
    "GB_alpha_semiconvection": "0",
    "GB_extra_inlist_controls": "",
    "GB_hist_columns": "",
    "GB_prof_columns": "",
    "GB_saved_model_name": "TAMS.mod",
    "GB_save_model_filename": "CHeB.mod",
    ###############################
    # CHeB specific controls
    "CHeB_terminate_he4_mass_fraction": 0,
    "delta_XHe_cntr_limit": "0.001d0",
    "delta_lg_XHe_cntr_limit": "0.01d0",
    "CHeB_envelope_overshoot_scheme": "exponential",
    "CHeB_envelope_overshoot_f": "0.0174",  # choi et al 2016
    "CHeB_envelope_overshoot_f0": "0.00174",
    "CHeB_do_predictive_mix": ".true.",
    "CHeB_use_Ledoux_criterion": ".false.",
    "CHeB_extra_inlist_controls": "",
    "CHeB_hist_columns": "",
    "CHeB_prof_columns": "",
    "CHeB_saved_model_name": "CHeB.mod",
    "CHeB_save_model_filename": "TACHeB.mod",
    ###############################
    # EAGB specific controls
    "EAGB_delta_lgL_He_limit": "0.01",
    "EAGB_use_Ledoux_criterion": ".false.",
    "EAGB_alpha_semiconvection": "1d-2",
    "EAGB_overshoot_prescription": "",  # set to empty for mass-indepdendent overshooting
    "EAGB_overshoot_scheme": "exponential",
    "EAGB_overshoot_f": "0.005",  # only used if MS_overshoot_prescription = ""
    "EAGB_overshoot_f0": "0.0005",  # only used if MS_overshoot_prescription = ""
    "EAGB_use_vassilidadis_wood_wind": ".true.",
    "EAGB_extra_inlist_controls": "",
    "EAGB_hist_columns": "",
    "EAGB_prof_columns": "",
    "EAGB_saved_model_name": "TACHeB.mod",
    "EAGB_save_model_filename": "1TP.mod",
    ###############################
    # TPAGB specific controls
    "VCT_target": "2d-5",
    "MXP_target": "1.0",
    "avoid_HRI": ".true.",
    "save_hdf5_files": ".false.",
    "envelope_mass_limit": "0.01",
    "use_alpha_mlt": ".true.",
    "TPAGB_use_Ledoux_criterion": ".false.",
    "TPAGB_delta_lgL_He_limit": "0.01",
    "TPAGB_use_vassilidadis_wood_wind": ".true.",
    "TPAGB_overshoot_scheme_IPCZ": "exponential",
    "TPAGB_overshoot_f_IPCZ": "0.008",  # Ritter et al 2018
    "TPAGB_overshoot_f0_IPCZ": "0.0008",
    "TPAGB_overshoot_scheme_CE": "exponential",
    "TPAGB_overshoot_f_CE": "0.016",  # Herwig 2000
    "TPAGB_overshoot_f0_CE": "0.0016",
    # use following for double exponential overshoot of Battino et al 2016
    # "TPAGB_overshoot_scheme_CE":"other",
    # "TPAGB_overshoot_f_CE":"0.014",
    # "TPAGB_overshoot_f0_CE":"0.014",
    "double_exponential_overshoot_f2": "0.25",
    "double_exponential_overshoot_D2": "1d11",
    "TPAGB_extra_inlist_controls": "",
    "TPAGB_hist_columns": "",
    "TPAGB_prof_columns": "",
    "TPAGB_saved_model_name": "1TP.mod",
    "TPAGB_save_model_filename": "postAGB.mod",
    "TPAGB_LOGS_suffix": "",
    "TPAGB_photos_suffix": "",
    ###############################
    # post-AGB specific controls
    "postAGB_extra_inlist_controls": "",
    "postAGB_hist_columns": "",
    "postAGB_prof_columns": "",
    "postAGB_saved_model_name": "postAGB.mod",
    "postAGB_save_model_filename": "WD.mod",
    "postAGB_LOGS_suffix": "",
    "postAGB_photos_suffix": "",
    "postAGB_use_Ledoux_criterion": ".true.",
    "postAGB_use_vassilidadis_wood_wind": ".true.",
}
